image: continuumio/miniconda3

stages:
  - test
  - build
  - deploy

test_coverage:
  stage: test
  needs: [ ]
  tags:
    - ci.inria.fr
    - large
  script:
    - apt-get update
    - apt-get install libgl1 libgl1-mesa-glx libgl1-mesa-dri xvfb -y
    # Install package and dependencies from sources using `pip`
    - python -m pip install -e '.[test]'
    # Run tests with pytest and generate coverage report (config in pyproject.toml)
    - pytest
  coverage: /TOTAL.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/
  timeout: 2h
  rules:
    - when: always

pip_build:
  stage: build
  needs: [ test_coverage ]
  tags:
    - ci.inria.fr
    - medium
  script:
    # https://packaging.python.org/en/latest/guides/distributing-packages-using-setuptools/#wheels
    # Install required tools for building and publishing
    - python -m pip install --upgrade build
    # Build the package (source and wheel distributions)
    - python -m build --wheel
  artifacts:
    paths:
      - dist/
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      when: always

conda_build:
  stage: build
  needs: [ test_coverage ]
  tags:
    - ci.inria.fr
    - medium
  script:
    - conda install conda-build -y
    - mkdir conda-bld
    - conda build -c default -c conda-forge ./conda/ --output-folder conda-bld/
  artifacts:
    paths:
      - conda-bld
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      when: always

pip_upload:
  stage: deploy
  needs: [ pip_build ]
  tags:
    - ci.inria.fr
    - medium
  dependencies:
    - pip_build
  variables:
    GIT_STRATEGY: none # avoid cloning the repository
  script:
    # Install required dependencies:
    # - 'twine' to upload package to PyPI
    # - an up-to-date version of 'packaging' to work with modern `pyproject.toml` (issue with 'license' field)
    - python -m pip install --upgrade twine "packaging>=24.0"
    # Upload the package to PyPI
    - python -m twine upload --username __token__ --password $TWINE_PASSWORD dist/*
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      when: always

conda_upload:
  image: inriamosaic/conda
  stage: deploy
  needs: [ conda_build ]
  tags:
    - ci.inria.fr
    - medium
  dependencies:
    - conda_build
  variables:
    GIT_STRATEGY: none # avoid cloning the repository
  script:
    - ls -al conda-bld/
    - ls -al conda-bld/noarch/
    - ls -al conda-bld/noarch/*.conda
    - anaconda --token $ANACONDA_TOKEN upload conda-bld/noarch/*.conda --user mosaic
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      when: always

pages:
  stage: deploy
  needs: [ test_coverage ]
  tags:
    - ci.inria.fr
    - large
  script:
    - apt-get update
    - apt-get install libgl1-mesa-glx xvfb -y
    - python -m pip install -e '.[doc,nb]'
    - python -m pip install 'pyvista[all]'
    - conda install -c conda-forge libstdcxx-ng  # https://stackoverflow.com/a/71421355
    - python docs/assets/scripts/update_notebooks.py
    - mkdocs build
    - mv ./site public
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
      when: always
    - if: $CI_COMMIT_BRANCH == "develop"
      when: always
