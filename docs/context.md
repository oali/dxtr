# A (very) basic introduction to DEC

In order to grasp the scope of the **Dxtr** library, we need first to introduce its underlying mathematical principles:

- [x] The theory of [*Exterior (Differential) Calculus (EC)*](./context_EC.md).
- [x] its discretized version, [*Discrete Exterior Calculus (DEC)*](./context_DEC.md).

!!! tip "Bibliography for the interested reader"
    This quick introduction is not meant to be exhaustive. It is rather an attempt to summarize the most striking features of *EC* and *DEC* that sparkled our motivation to develop the **Dxtr** library. For the interested reader who want to go deeper, we highly recommand the following lectures:

    - [The founding paper of *DEC* by Mathieu Desbrun and co-authors](https://arxiv.org/pdf/math/0508341)
    - [Anil Hirani's PhD thesis on *DEC*](https://thesis.library.caltech.edu/1885/3/thesis_hirani.pdf)
    - [Melvin Leok's PhD thesis (3rd chapter) on *DEC*](https://thesis.library.caltech.edu/831/7/14_chapter_three_discrete_exterior_calculus_mleok.pdf)
    - [Keenan Crane's introductory course on Discrete Differential Geometry](http://www.cs.cmu.edu/~kmcrane/Projects/DGPDEC/paper.pdf).
    - [A very inspiring paper, also from the *Caltec group*, that ignited our will to build our own *DEC* library](http://multires.caltech.edu/pubs/scomplex.pdf)