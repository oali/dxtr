{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "2015141c",
   "metadata": {},
   "source": [
    "# Laplacian spectral analysis\n",
    "\n",
    "**Abstract:** The Laplacians constitue an ubiquitous familly of differential \n",
    "operators in differential geometry. In *DEC*, a specific one is considered: the \n",
    "Hodge-Laplacian, *a.k.a* the Laplace-De Rham operator. To access its \n",
    "implementation in the **Dxtr** library, we compute its spectrum on spherical \n",
    "simplicial complexes and to compare the results with theoretical expectations.\n",
    "\n",
    "> **Note:** You can [**download this notebook**](./spectral_laplacian.ipynb \"download\") and play with it directly on your local machine."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6a09f867",
   "metadata": {},
   "source": [
    "## Presentation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1836448f",
   "metadata": {},
   "source": [
    "The Hodge-Laplacian operator is defined as:\n",
    "$$\n",
    "\\Delta = d\\delta + \\delta d,\n",
    "\\tag{1}\n",
    "$$\n",
    "where $d$ is the exterior derivative and $\\delta$ the codifferential.\n",
    "\n",
    "On a 2D sphere, the eigenfunctions of the Laplacian operator are called \n",
    "*spherical harmonics*. The corresponding eigenvalues are given by the following \n",
    "formula:\n",
    "$$\n",
    "\\lambda_n = n (n+1),\n",
    "$$\n",
    "with multiplicity: $2n+1$.\n",
    "\n",
    "These eigenvalues –– with their multiplicities –– consitute the spectrum of the \n",
    "**Laplace-Beltrami** operator, that coincide with the Hodge-Laplacian for 0- and  \n",
    "2-forms. \n",
    "\n",
    "Concerning 1-forms, the expected spectrum is slightly modified: It features the \n",
    "same eigenvalues as the Laplace-Beltrami spectrum with a doulbed multiplicity, \n",
    "with the expection of the null eigenvalue that is abscent."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ab53b9e1",
   "metadata": {},
   "source": [
    "## Dependencies"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d9190205",
   "metadata": {},
   "outputs": [],
   "source": [
    "from __future__ import annotations\n",
    "\n",
    "import numpy as np\n",
    "from scipy.sparse import linalg as splng\n",
    "import pandas as pd\n",
    "import plotly.express as px\n",
    "\n",
    "from dxtr import Cochain\n",
    "from dxtr.complexes import sphere\n",
    "from dxtr.operators import laplacian"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11b0a4d3",
   "metadata": {},
   "source": [
    "## Domain definition"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8e989ed9",
   "metadata": {},
   "source": [
    "We are considering 4 spheres of radius 1 with different resolutions.\n",
    "These spheres are instanciated as `SimplicialManifold` for the definition of Laplace-De Rham operator requires the definition of dual cell complexes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2de7fb29",
   "metadata": {},
   "outputs": [],
   "source": [
    "sizes = ['small', 'large', 'massive'] \n",
    "spheres = {size: sphere(size=size, manifold=True) for size in sizes}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5a6bc42b",
   "metadata": {},
   "source": [
    "## Theoretical expectations"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "87e98732",
   "metadata": {},
   "source": [
    "We will consider the **15 first eigenvalues**. Taking into account their multiplicity, \n",
    "this will correspond to 225 eigenvalues in total for 0- & 2-forms and 448 eigenvalues for 1-forms."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "700f6575",
   "metadata": {},
   "outputs": [],
   "source": [
    "N = 15\n",
    "\n",
    "eigvals_continuous = [-n*(n+1) for n in np.arange(N)]\n",
    "multiplicity_continuous = [2*n+1 for n in np.arange(N)] \n",
    "\n",
    "eigenvalues, multiplicity= [], []\n",
    "\n",
    "# Expected eigenvalues and multiplicity for the Laplacian on 0-simplices\n",
    "eigenvalues.append(eigvals_continuous)\n",
    "multiplicity.append(multiplicity_continuous)\n",
    "\n",
    "# Expected eigenvalues and multiplicity for the Laplacian on 1-simplices\n",
    "eigenvalues.append(eigvals_continuous[1:])\n",
    "multiplicity.append([2*m for m in multiplicity_continuous[1:]])\n",
    "\n",
    "# Expected eigenvalues and multiplicity for the Laplacian on 2-simplices\n",
    "eigenvalues.append(eigvals_continuous)\n",
    "multiplicity.append(multiplicity_continuous)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "34af7824",
   "metadata": {},
   "source": [
    "## Resolution"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e114ed5c",
   "metadata": {},
   "source": [
    "We define a function to compute the error between the computed & expected values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "07ad7d09",
   "metadata": {},
   "outputs": [],
   "source": [
    "def error(values:np.ndarray[float], \n",
    "          expectations:np.ndarray[float], average:bool=True) -> float:\n",
    "    \"\"\"Computes the relative RMS error.\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    values\n",
    "        The list of computed values to assess.\n",
    "    expectations\n",
    "        The list of expected values.\n",
    "    average\n",
    "        If True a single, averaged value is returned,\n",
    "        else, an array is returned.\n",
    "    \n",
    "    Returns\n",
    "    -------\n",
    "        The seeked error value.\n",
    "\n",
    "    Notes\n",
    "    -----\n",
    "    * The use of this function to estimate the precision of our framework is\n",
    "      inspired by the Ben-Chen et al (2010) paper.\n",
    "    \"\"\"\n",
    "    \n",
    "    try:\n",
    "        values = np.asarray(values)\n",
    "        expectations = np.asarray(expectations)\n",
    "\n",
    "        assert values.shape == expectations.shape, ('WARNING:'\n",
    "            + f'values shape {values.shape} !='\n",
    "            + f'expectations shape {expectations.shape}')\n",
    "        \n",
    "        res = (values - expectations) ** 2\n",
    "        nrm = (expectations ** 2).sum()\n",
    "        if nrm < 1e-10: nrm = 1\n",
    "        \n",
    "        if average:\n",
    "            return np.sqrt(res.sum() / nrm)\n",
    "        else:\n",
    "            return np.sqrt(res) / np.sqrt(nrm)\n",
    "\n",
    "    except AssertionError as msg:\n",
    "        print(msg)\n",
    "        return None"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd5874ec",
   "metadata": {},
   "source": [
    "We instantiate the Laplacian on all these spheres and compute its eigenvalues using the `scipy.sparse.linalg.eigs` function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "efe97179",
   "metadata": {},
   "outputs": [],
   "source": [
    "results = pd.DataFrame()\n",
    "for size, mfld in spheres.items():\n",
    "    for k in range(3):\n",
    "        \n",
    "        lpc = laplacian(mfld, k).values\n",
    "\n",
    "        j=0\n",
    "        for i, (xpct_ev, multi) in enumerate(zip(eigenvalues[k],\n",
    "                                                multiplicity[k])):\n",
    "            rnk = multi*[i]\n",
    "            eig = multi*[xpct_ev]\n",
    "            mlt = multi*[multi]\n",
    "            odr = np.arange(j, j+multi)\n",
    "\n",
    "            evals, evects = splng.eigs(lpc, k=multi, sigma=xpct_ev, which='LM')\n",
    "            evals = evals.real\n",
    "            evects = evects.real\n",
    "\n",
    "            err = multi*[error(evals[:multi], eig)]\n",
    "            j += multi\n",
    "\n",
    "            # Wrapping results into a DataFrame\n",
    "            res = pd.DataFrame({'Operator': 'Laplacian',\n",
    "                                'Manifold': 'Spheres',\n",
    "                                'Complex size': size,\n",
    "                                'Degree': k,\n",
    "                                'Value': evals,\n",
    "                                'Order':odr,\n",
    "                                'Error': err,\n",
    "                                'Rank': np.array(rnk)+k,\n",
    "                                'Multiplicity': mlt,\n",
    "                                'Expectation': eig})\n",
    "\n",
    "            results = pd.concat((results, res))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "395ec2e0",
   "metadata": {},
   "source": [
    "> **Warning:** The above cell might take a while to compute (FYI: around 10min on a macbook Pro M1)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c1c6f145",
   "metadata": {},
   "source": [
    "## Results"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c7fdc0e6",
   "metadata": {},
   "source": [
    "We first visualize the spectra of the **Hodge-Laplacian** for differential forms of various degrees. To that end, we define a dedicated function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b9df3e52",
   "metadata": {},
   "outputs": [],
   "source": [
    "def eigenvalue_plot(data:pd.DataFrame, show_expectation:bool=True,\n",
    "                    hover_data:bool=True,\n",
    "                    **kwargs) -> plotly.graph_objs._figure.Figure:\n",
    "    \"\"\"Plots the eigenvalues of a differential operator.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    data :\n",
    "        The pandas.DataFrame containing all the relevant information to plot.\n",
    "    show_expectation :\n",
    "        If True  shows the expected values and\n",
    "        a confidence interval around them.\n",
    "    hover_data:\n",
    "        Display some info when hovering points if True.\n",
    "\n",
    "    Other parameters\n",
    "    ----------------\n",
    "    confidence_interval : float\n",
    "        The width of error band to plot around the expected values.\n",
    "        Should be given as `.xx`, e.g. for 5% write `0.05`.\n",
    "    operator_name : str\n",
    "        The name of the operator to display and use for recording.\n",
    "    manifold_name : str\n",
    "        The name of the manifold to display and use for recording.\n",
    "    title : str\n",
    "        The name of the graph.\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    The Figure to display.\n",
    "\n",
    "    Notes\n",
    "    -----\n",
    "    The DataFrame to pass as argument should contain the following columns:\n",
    "    * 'Rank': int, the eigenvalue rank.\n",
    "    * 'Value': float, the computed eigenvalues.\n",
    "    * 'Multiplicity': int, the eigenvalue multiplicity.\n",
    "    * 'Error: float, the relative RMS error between\n",
    "      computed and expected eigenvalues.\n",
    "    * 'Complex size': str, the size/name of the SimplicialManifold\n",
    "      the eigenvalues have been computed on.\n",
    "    \"\"\"\n",
    "    if hover_data:\n",
    "        hd = {'Complex size': False,\n",
    "              'Multiplicity': True,\n",
    "              'Value':':.2f',\n",
    "              'Rank': True,\n",
    "              'Expectation': True,\n",
    "              'Error': ':.2e'}\n",
    "    else:\n",
    "        hd = None\n",
    "    \n",
    "    fig = px.scatter(data, x='Rank', y='Value', \n",
    "                     color='Complex size',\n",
    "                     color_discrete_sequence=px.colors.qualitative.G10,\n",
    "                     labels={'Value': 'Eigenvalues',\n",
    "                             'Complex size': 'Sphere sizes'},\n",
    "                     hover_name='Complex size',\n",
    "                     hover_data=hd)\n",
    "\n",
    "    # Expectation\n",
    "    if show_expectation:\n",
    "        err = kwargs.get('Confidence interval', .01)\n",
    "\n",
    "        rank = data['Rank'].unique()\n",
    "        expectation = data['Expectation'].unique()\n",
    "        theo = px.line(x=rank, y=expectation)\n",
    "        uplim = px.line(x=rank, y=(1+err)*expectation)\n",
    "        lolim = px.line(x=rank, y=(1-err)*expectation)\n",
    "\n",
    "        fig.add_trace(theo.data[0])\n",
    "        fig.add_trace(uplim.data[0])\n",
    "        fig.add_trace(lolim.data[0])\n",
    "\n",
    "    # Cosmetics\n",
    "    fig['data'][-3]['line']['color']='rgba(0,0,0,.25)'\n",
    "    fig['data'][-3]['showlegend']= True\n",
    "    fig['data'][-3]['name'] = 'Theoretical expectation'\n",
    "    fig['data'][-2]['line']['color'] ='rgba(0,0,0,.25)'\n",
    "    fig['data'][-2]['line']['dash'] ='dot'\n",
    "    fig['data'][-1]['line']['color'] ='rgba(0,0,0,.25)'\n",
    "    fig['data'][-1]['line']['dash'] ='dot'\n",
    "    fig['data'][-1]['showlegend'] = True\n",
    "    fig['data'][-1]['name'] = f'{err:.0%} Confidence interval'\n",
    "\n",
    "    if 'Operator' in data.columns:\n",
    "        default_operator_name = data['Operator'].unique()[0]\n",
    "    else:\n",
    "        default_operator_name = 'Unknown operator'\n",
    "    \n",
    "    if 'Manifold' in data.columns:\n",
    "        default_manifold_name = data['Manifold'].unique()[0]\n",
    "    else:\n",
    "        default_manifold_name = 'Unknown operator'\n",
    "\n",
    "    operator = kwargs.get('operator_name', default_operator_name)\n",
    "    manifold = kwargs.get('manifold_name', default_manifold_name)\n",
    "\n",
    "    fig.update_layout(template='plotly_white',\n",
    "                      title=kwargs.get('title', \n",
    "                                       f'<b>{operator}</b> | {manifold}'),\n",
    "                      showlegend=kwargs.get('showlegend', True))\n",
    "\n",
    "\n",
    "    return fig"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "405f89d0",
   "metadata": {},
   "source": [
    "Now, we can plot the sought spectra:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c9ef881b",
   "metadata": {},
   "outputs": [],
   "source": [
    "for k in [0, 1, 2]:\n",
    "    slct = results['Degree'] == k\n",
    "    eigenvalue_plot(results[slct], \n",
    "                    operator_name=f'{k}-Laplacian operator').show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4145d2bf",
   "metadata": {},
   "source": [
    "**Remarks on the graphs above:**\n",
    "* For the *massive* & *large* spheres, the first 200 (on 0-simplices) & 400 (on 1-simplices) eigenvalues are in the 1% band around the expected values.\n",
    "* Accuracy is better on 1-simplices because in a given closed mesh there is always more 1-simplices than 0-simplices."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "36911f2d",
   "metadata": {},
   "source": [
    "**Error:**\n",
    "\n",
    "We quantify the precision of the eigenvalues estimation by computing a relative RMS error. We define a specific function to visualize this error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8c2461f4",
   "metadata": {},
   "outputs": [],
   "source": [
    "def error_plot(data:pd.DataFrame, show_ref:bool=True,\n",
    "               save:bool=False, **kwargs) -> plotly.graph_objs._figure.Figure:\n",
    "    \"\"\"Illustrates the precision of the eigenvalue estimation of a differential operator.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    data :\n",
    "        The pandas.DataFrame containing all the relevant information to plot.\n",
    "    show_ref :\n",
    "        If True marks the precision obtained by Ben-Chen and coworkers in\n",
    "        their 2010 seminal paper.\n",
    "    save :\n",
    "        Save the figure on disk if True.\n",
    "\n",
    "    Other parameters\n",
    "    ----------------\n",
    "    format: str\n",
    "        The type of image to save, default is pdf, can also be png, jpeg, svg.\n",
    "    figure_folder_path: str\n",
    "        The path to the folder wher to store the figures.\n",
    "        Default is `../../data/results/figures/`.\n",
    "    operator_name: str\n",
    "        The name of the operator to display and use for recording.\n",
    "    manifold_name: str\n",
    "        The name of the manifold to display and use for recording.\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "    The Figure to display.\n",
    "\n",
    "    Notes\n",
    "    -----\n",
    "    The DataFrame to pass as argument should contain the following columns:\n",
    "    * 'Rank': int, the eigenvalue rank.\n",
    "    * 'Multiplicity': int, the eigenvalue multiplicity.\n",
    "    * 'Error: float, the relative RMS error to display.\n",
    "    * 'Complex size': str, the size/name of the SimplicialManifold\n",
    "      the eigenvalues have been computed.\n",
    "    \"\"\"\n",
    "    \n",
    "    fig = px.scatter(data, x='Rank', y='Error',\n",
    "                      color='Complex size',\n",
    "                      log_x=True, log_y=True,\n",
    "                      color_discrete_sequence=px.colors.qualitative.G10,\n",
    "                      labels={'Error': 'Relative RMS error',\n",
    "                              'Complex size': 'Sphere sizes'},\n",
    "                      hover_name='Complex size', \n",
    "                      hover_data={'Complex size': False,\n",
    "                                  'Multiplicity':True,\n",
    "                                  'Error': ':.2e'}\n",
    "                    )\n",
    "\n",
    "    if show_ref:\n",
    "        # Reference point from Ben-Chen et al 2010\n",
    "        slct = data['Order'] == 180\n",
    "        rank = list(data.loc[slct, 'Rank'].unique())\n",
    "        \n",
    "        ref = px.scatter(x=rank, y=[.0016],\n",
    "                        symbol_sequence=['square'],\n",
    "                        color_discrete_sequence=['black']).data[0]\n",
    "        \n",
    "        xline = px.line(x=[0]+rank,  y=[.0016, .0016]).data[0]\n",
    "        yline = px.line(x=2*rank,  y=[0, .5]).data[0]\n",
    "\n",
    "        fig.add_trace(ref)\n",
    "        fig.add_trace(xline)\n",
    "        fig.add_trace(yline)\n",
    "\n",
    "        fig['data'][-3]['showlegend'] = True\n",
    "        fig['data'][-3]['name'] = 'Ben-Chen et al (2010)'\n",
    "        fig['data'][-2]['line']['color'] ='rgb(0,0,0)'\n",
    "        fig['data'][-2]['line']['dash'] ='dash'\n",
    "        fig['data'][-1]['line']['color'] ='rgb(0,0,0)'\n",
    "        fig['data'][-1]['line']['dash'] ='dash'\n",
    "\n",
    "    # Cosmetic\n",
    "    if 'Operator' in data.columns:\n",
    "        default_operator_name = data['Operator'].unique()[0]\n",
    "    else:\n",
    "        default_operator_name = 'unknown operator'\n",
    "    \n",
    "    if 'Manifold' in data.columns:\n",
    "        default_manifold_name = data['Manifold'].unique()[0]\n",
    "    else:\n",
    "        default_manifold_name = 'unknown operator'\n",
    "\n",
    "    operator = kwargs.get('operator_name', default_operator_name)\n",
    "    manifold = kwargs.get('manifold_name', default_manifold_name)\n",
    "    \n",
    "    fig.update_layout(template='plotly_white',\n",
    "                      title=f'<b>{operator}</b> | {manifold}')\n",
    "\n",
    "    return fig"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e4275dba",
   "metadata": {},
   "outputs": [],
   "source": [
    "for k in [0, 1, 2]:\n",
    "    slct = results['Degree'] == k\n",
    "    error_plot(results[slct], operator_name=f'{k}-Laplacian operator').show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c742fa56",
   "metadata": {},
   "source": [
    "**Discussion:**\n",
    "* On the graphs above is reported a value (black square) obtained by Ben-Chen and coworkers in their [seminal paper](https://onlinelibrary.wiley.com/doi/10.1111/j.1467-8659.2010.01779.x) of 2010. We use this values as a comparator for they used the same approach as us although on a slightly different differential operator.\n",
    "* For the `large` sphere (with 19.9k vertices, the closest to the 20k vertices structure used by Ben-Chen) we get an accuracy of the same order of magnitude but sligthly bigger."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "56143a3b",
   "metadata": {},
   "source": [
    "**Conclusion:**\n",
    "Eigenvalues of the Hodge-Laplacian operator can be computed with a reasonably good precision for differential forms up to degree 2."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "dxtr",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.18"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": true,
   "autoclose": false,
   "autocomplete": true,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": false,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
