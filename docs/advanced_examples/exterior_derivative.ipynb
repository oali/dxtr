{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exterior derivative operator in `dxtr`\n",
    "\n",
    "**Abstract:** \n",
    "The `exterior_derivative()` operator is the corner stone of any *DEC* implementation. In this notebook we assess the precision of its implementation in the **Dxtr** library.\n",
    "\n",
    "> **Note:** You can [**download this notebook**](./exterior_derivative.ipynb \"download\") and play with it directly on your local machine."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Presentation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Given a k-form ($\\omega\\in\\Lambda^k(\\mathcal{M})$) defined on a n-dimentional manifold ($\\mathcal{M}$), the external derivative is defined as:\n",
    "$$\n",
    "\\omega = \\omega_{i_1\\dots i_k}dx^{i_1}\\wedge\\dots\\wedge dx^{i_k}\n",
    "\\quad \\Rightarrow \\quad\n",
    "d(\\omega) = \\frac{\\partial\\omega_{1\\dots k}}{\\partial x^j}dx^j\\wedge dx^{i_1}\\wedge\\dots\\wedge dx^{i_k},\n",
    "$$\n",
    "where the Einstein summation convention is used.\n",
    "\n",
    "In the specific cases of 0-forms defined on a 2D domain we have:\n",
    "$$\n",
    "\\omega^{(0)} = \\omega(\\xi^0, \\xi^1) \n",
    " \\Rightarrow\n",
    "d(\\omega^{(0)}) = \\frac{\\partial \\omega}{\\partial\\xi^0}d\\xi^0 + \\frac{\\partial \\omega}{\\partial\\xi^1}d\\xi^1\n",
    "$$\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To assess the precision of our implementation of the exterior derivative, we will consider simple differential forms defined on the 2D sphere of unit radius."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dependencies"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from __future__ import annotations\n",
    "\n",
    "from typing import Optional\n",
    "\n",
    "import copy as cp\n",
    "import numpy as np\n",
    "import numpy.linalg as lng\n",
    "import pandas as pd\n",
    "import pyvista as pv\n",
    "import plotly.express as px\n",
    "\n",
    "from dxtr import Cochain, SimplicialManifold\n",
    "from dxtr.operators import d\n",
    "from dxtr.complexes import sphere, disk\n",
    "from dxtr.visu import visualize\n",
    "\n",
    "from dxtr.complexes.simplicialmanifold import edge_vectors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Domains definition"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will consider two domains of interest:\n",
    "* A **disk** of unit radius.\n",
    "* A **sphere** of unit radius.\n",
    "\n",
    "While the disk is flat and feature boundaries, the sphere is curved and closed. \n",
    "Investigating the exterior derivative on both complexes will help us understand the influence of both boundaries and curvature on its precision."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Sphere"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sph = sphere(manifold=True)\n",
    "sph.name = 'Sphere'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will need to measure the position of simplices with respect to the *north* \n",
    "*pole* of this sphere. To that end, we define a function to compute this \n",
    "*curvilinear abscissae*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def curvilinear_abscissae(manifold:SimplicialManifold, \n",
    "                          k:int, dual:bool=False) -> np.ndarray[float]:\n",
    "   \"\"\"Computes the arclength between the k-simplex circumcenters and the north\n",
    "      pole of a given n-simplicial complex.\n",
    "\n",
    "      Notes\n",
    "      -----\n",
    "        * We add a special case for the dual edges because the circumcenters \n",
    "          of the primal edges do not necessarily corresponds to the ones of the \n",
    "          dual edges. And this could impact the precision estimation.\n",
    "   \"\"\"\n",
    "   \n",
    "   if dual & (k==1):\n",
    "      cfids = manifold.cofaces(1)\n",
    "      x = manifold[k+1].circumcenters[cfids].mean(axis=-2)\n",
    "   else:\n",
    "      x = cp.deepcopy(manifold[k].circumcenters)\n",
    "\n",
    "   z = x[:,-1]\n",
    "   r_xy = lng.norm(x[:,:2], axis=-1)\n",
    "   theta = np.arctan2(r_xy, z)\n",
    "   r = lng.norm(x, axis=-1)\n",
    "   print(f'Checking radius precision:{r.mean():.3e}+-{r.std():.2e}')\n",
    "   return r * theta"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now compute the curvilinear positions for the 0-simplices and the 1-simplices of the primal complex as well as for the 0-cells and the 1-cells of the dual one:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "complex_names = ['primal', 'dual']\n",
    "\n",
    "curv_abs = {name: curvilinear_abscissae(sph, 2*k)\n",
    "            for k, name in enumerate(complex_names)}\n",
    "\n",
    "# A variable that we will use a lot: \n",
    "# the curvilinear abscissae of the 1-simplices circumcenters\n",
    "\n",
    "curv_abs_edges = {name: curvilinear_abscissae(sph, 1, name=='dual')\n",
    "                  for name in complex_names}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remark:**  Note that the precision of the circumcenters of the simplices of degrees >0 is significantly worst that the precision on the vertices. This might have an impact of the precision of our implementation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def colatitude_vector(manifold:SimplicialManifold, k:int, \n",
    "                      dual:bool=False) -> np.ndarray[float]:\n",
    "   \"\"\"Computes the unit vector field tangent to k simplices along the\n",
    "      curvilinear abscissae.\n",
    "   \"\"\"\n",
    "\n",
    "   Nk = manifold[k].size\n",
    "\n",
    "   # if k==1:\n",
    "   #    cfids = manifold.cofaces(k)\n",
    "   #    pos = manifold[k+1].circumcenters[cfids].mean(axis=-2)\n",
    "   # else:\n",
    "   pos = cp.deepcopy(manifold[k].circumcenters)\n",
    "\n",
    "   ez =np.repeat(np.array([[0,0,1]]), Nk, axis=0)\n",
    "   \n",
    "   ephi = np.cross(ez, pos, axis=-1)\n",
    "   \n",
    "   etheta = np.cross(ephi, pos, axis=-1)\n",
    "   etheta /= lng.norm(etheta, axis=-1).reshape((Nk,1))\n",
    "   \n",
    "   return etheta"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Disk"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dsk = disk(manifold=True)\n",
    "dsk.name = 'Disk'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's get the radial positions of 0-simplices and 0-cells:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "radius = {name: lng.norm(dsk[2*k].circumcenters, axis=-1) \n",
    "          for k, name in enumerate(['primal', 'dual'])}\n",
    "\n",
    "radius_edges = {name: lng.norm(dsk[1].circumcenters, axis=-1)\n",
    "                  for name in complex_names}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** 1-simplices and 1-cells share the same circumcenters on the disk. That is why we do not distinguish 'primal' and 'dual' cases in the computation above."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also define a function to compute the radial direction on every simplex circumcenter:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def radial_vector(manifold:SimplicialManifold, k:int, \n",
    "                      dual:bool=False) -> np.ndarray[float]:\n",
    "   \"\"\"Computes the unit vector field tangent to k simplices along the\n",
    "      radial direction on the disk.\n",
    "   \"\"\"\n",
    "   \n",
    "   Nk = manifold.shape[k]\n",
    "   pos = cp.deepcopy(manifold[k].circumcenters)\n",
    "   \n",
    "   return pos / lng.norm(pos, axis=-1).reshape(Nk, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we define a boolean flag to mark the border of the disk on the 1-simplices."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "is_border = np.isin(np.arange(dsk[1].size), dsk.border()[1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Defining the test function and the expected results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To test our implementation of the exterior derivative, we propose to compute the \n",
    "exterior derivative of a simple 0-form:\n",
    "$$f(s) = \\cos(\\omega s), \\tag{1}$$ \n",
    "where $s$ depicts the **curvilinear abscissae** with respect to a reference position ($\\boldsymbol{x}_0$) defined on the considered domain, $\\Omega$.\n",
    "\n",
    "* **In the case of the sphere,** we choose as a reference position the *\"north pole\"* ($\\boldsymbol{x}_0 = [0, 0, 1]$) and consequently, the curvilinear abscissae has the usual expression: $s = R\\theta$, where $R=1$ in our case and $\\theta\\in [0, \\pi[$ stands for the **colatitude angle**.\n",
    "* **In the case of the disk,** the reference position is its center ($\\boldsymbol{x}_0 = [0, 0, 0]$) and the curvilinear abscissae simply corresponds to the radial distance $s = r\\in [0,1]$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We define the test function to use, given by $\\text{eq.}(1)$:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def f(s:np.ndarray[float], w:float=1) -> np.ndarray[float]:\n",
    "    \"\"\"The function to derive.\"\"\"\n",
    "    return np.cos(w*s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As the considered 0-form $f$ only depends on the curvilinear abscissae $s$, its\n",
    "exterior derivative will verify:\n",
    "$$\n",
    "df = \\frac{\\partial f}{\\partial s} \\boldsymbol{ds},\n",
    "$$\n",
    "with $\\boldsymbol{ds}= \\boldsymbol{e}_s^\\flat$ the 1-form dual of the vector field $\\boldsymbol{e}_s$, tangent to the considered manifold, along the isolines $s=cste$.\n",
    "\n",
    "Deriving $\\text{eq.}(1)$ yields:\n",
    "$$\n",
    "\\frac{\\partial f}{\\partial s} = -\\omega\\sin(\\omega s), \\tag{2}\n",
    "$$\n",
    "we define the corresponding partial derivative:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def dfds(s:np.ndarray[float], w:float=1) -> np.ndarray[float]:\n",
    "    \"\"\"The approximated derivative\"\"\"\n",
    "    return - w * np.sin(w*s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As mentioned previously in the documentation, in *DEC*, k-forms are approximated as k-cochains, whose coefficients correspond to the integration of the of the considered k-form on k-simplices (k-cells in the dual case). \n",
    "\n",
    "In the present case, it means that the coefficients of the exterior derivative we seek verify:\n",
    "$$\n",
    "c_i = \\int_{\\sigma^{(1)}_i}df,\n",
    "$$\n",
    "where the $\\sigma^{(1)}_i$ correspond to the 1-simplices (resp. 1-cells) of the considered complex.\n",
    "\n",
    "Assuming the partial derivative, $\\text{eq.}(2)$, to be constant on each 1-simplex, the sought coefficients can be approximated as:\n",
    "$$\n",
    "c_i \\approx \\left.\\frac{\\partial f}{\\partial s}\\right\\vert_{\\boldsymbol{x}_i} w_i, \n",
    "\\quad\\text{with:}\\quad\n",
    "w_i = \\int_{\\sigma^{(1)}_i}\\boldsymbol{e}_s^\\flat.\n",
    "$$\n",
    "This is the formulae to estimate at every 1-simplex (1-cell) at to compare to the computed values. It is composed of two elements:\n",
    "\n",
    "* The derivative of the considered function $f$ with respect to curvilinear abscissae, estimated at the center of the 1-simplices.\n",
    "* A *\"weight\"* coefficient ($w_i$), to compute for each 1-simplex, corresponding to the circulation of the base vector $\\boldsymbol{e}_s$ along the 1-simplex.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Computing these *\"weights\"* in the k=1 case, yeilds:\n",
    "$$\n",
    "w_i = \\int_{|\\boldsymbol{e}_i|}ds\\:\\hat{\\boldsymbol{e}}_s\\cdot\\hat{\\boldsymbol{e}}_i \\approx l_i\\cos(\\theta_i), \\tag{3}\n",
    "$$\n",
    "\n",
    ">**Note:** \n",
    "> In the dual case, the integration is performed on the 1-cells dual of the 1-simplices. The lengths ($l_i$) and angles ($\\theta_i$) must be updated acordingly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def weights(manifold:SimplicialManifold, \n",
    "            dual:bool) -> Optional[np.ndarray[float]]:\n",
    "    \"\"\"Esitmates the weight of each 1-simplex as the projection of its edge\n",
    "    vector along the radial direction.\n",
    "    \n",
    "    Notes\n",
    "    -----\n",
    "      * depending on the estimation we want (primal/dual), we compute the \n",
    "        radial vectors from the circumcenters of 0-simplices or 2-simplices,\n",
    "        because we realized that for a significant number of 1-simplices we get\n",
    "        slight errors if we simply took their circumcenters. Notably close to \n",
    "        the border.\n",
    "    \"\"\"\n",
    "\n",
    "    edges = edge_vectors(manifold, dual=dual)\n",
    "\n",
    "    if manifold.name == 'Sphere':\n",
    "        direction = colatitude_vector(manifold, 1) \n",
    "    elif manifold.name == 'Disk':\n",
    "        direction = radial_vector(manifold, 1) \n",
    "    else:\n",
    "        print('The input manifold is not supported')\n",
    "        return None\n",
    "\n",
    "    return np.abs(np.einsum('ij,ij->i', direction, edges))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Computing the expectation, in the sphere case:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xpct_ext_deri_sph = {duality: \n",
    "    Cochain(sph, 1, dfds(s) * weights(sph, duality=='dual'), duality=='dual') \n",
    "                 for duality, s in curv_abs_edges.items()}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Expectation, in the disk case:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "w = np.pi/radius_edges['primal'].max() \n",
    "\n",
    "xpct_ext_deri_dsk = {duality: \n",
    "      Cochain(dsk, 1, dfds(r,w)*weights(dsk, duality=='dual'), duality=='dual') \n",
    "                     for duality, r in radius_edges.items()}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Computation of the exterior derivatives"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### On the sphere"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We instanciate the primal and dual 0-cochain to test:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "signal_sph = {name: Cochain(sph, 0, f(s), dual=name=='dual')\n",
    "              for name, s in curv_abs.items()}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We derive these cochains using the `d()` function to call the `exterior_derivative()` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ext_deri_sph = {name: d(c) for name, c in signal_sph.items()}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, we define an `error()` function to estimate the discrepency between the *computed* & *expected* values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def error(x:np.ndarray[float], y:np.ndarray[float], \n",
    "          threshold:float=1e-9) -> np.ndarray[float]:\n",
    "    \"\"\"Computes the relative error between two arrays.\n",
    "\n",
    "    Notes\n",
    "    -----\n",
    "      * We added a threshold to handle points where the expected\n",
    "        value vanishes and avoid dividing by almost zero.\n",
    "    \"\"\"\n",
    "    \n",
    "    x, y = cp.deepcopy(x), cp.deepcopy(y) \n",
    "    \n",
    "    zero_indices = np.where(y<threshold)\n",
    "    y[zero_indices] = x[zero_indices] = 1\n",
    "\n",
    "    return x/y - 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We gather the **computed** & **expected** values as well as the **relative error** between them in a `pandas.DataFrame` structure. This will allow us to generate quantitative visualization in the next section."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_sph = pd.DataFrame()\n",
    "\n",
    "for duality, xder in ext_deri_sph.items():\n",
    "    \n",
    "    comp = np.abs(xder.values)\n",
    "    xpct = np.abs(xpct_ext_deri_sph[duality].values)\n",
    "\n",
    "    s1 = curv_abs_edges[duality]\n",
    "    r1 = lng.norm(sph[1].circumcenters,axis=-1)\n",
    "\n",
    "    df = pd.DataFrame({'Complex': duality,\n",
    "                       'Simplex id': np.arange(xder.complex[1].size),\n",
    "                       'Curvilinear abscissae': s1,\n",
    "                       'Colatitude': 180/np.pi * s1/r1,\n",
    "                       'Computed values': comp,\n",
    "                       'Expected values': xpct,\n",
    "                       'Relative error': error(comp, xpct)})\n",
    "    \n",
    "    data_sph = pd.concat((data_sph, df), ignore_index=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### On the disk"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "signal_dsk = {name: Cochain(dsk, 0, f(r,w=np.pi/r.max()), dual=name=='dual')\n",
    "               for name, r in radius.items()}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We derive these cochains using the `d()` function as before on the sphere:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ext_deri_dsk = {name: d(c) for name, c in signal_dsk.items()}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We gather the computed values, the expected ones, as well as the relative error in a `DataFrame` for visualization purposes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data_dsk = pd.DataFrame()\n",
    "for (duality, xder), (_, xpct) in zip(ext_deri_dsk.items(), \n",
    "                                   xpct_ext_deri_dsk.items()):\n",
    "    \n",
    "    comp = np.abs(xder.values)\n",
    "    xpct = np.abs(xpct.values)\n",
    "\n",
    "    # Since the computation is not valid for 1-cells on the border, we put those values to Nan:\n",
    "    if duality == 'dual':\n",
    "        borders = np.where(is_border)\n",
    "        comp[borders] = np.nan\n",
    "        xpct[borders] = np.nan\n",
    "\n",
    "    df = pd.DataFrame({'Complex': duality,\n",
    "                       'Simplex id': np.arange(dsk[1].size),\n",
    "                       'Computed values': comp,\n",
    "                       'Expected values': xpct,\n",
    "                       'Relative error': error(comp, xpct),\n",
    "                       'Borders': is_border,\n",
    "                       'Radius': radius_edges[duality]})\n",
    "    \n",
    "    data_dsk = pd.concat((data_dsk, df), ignore_index=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Results analysis on the sphere"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can display the results of these computations with the `visualize()` \n",
    "function from the `dxtr.visu` module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = pv.Plotter(shape=(2,2), border=False)\n",
    "\n",
    "for i, (name, s) in enumerate(signal_sph.items()):\n",
    "    fig.subplot(0,i)\n",
    "    visualize(s, fig=fig, display=False,\n",
    "              layout_parameters={'title': f'{name} 0-cochain to derive.'})\n",
    "\n",
    "for i, (name, c) in enumerate(ext_deri_sph.items()):\n",
    "    fig.subplot(1,i)\n",
    "    visualize(c, fig=fig, display=False, scaling_factor=10,\n",
    "              layout_parameters={'title': f'Ext. deri. of a {name} 0-cochain.'})\n",
    "\n",
    "fig.link_views()\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also visualize the correlation between expected and computed values of the exterior derivative in a more quantitative manner:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = px.scatter(data_sph, \n",
    "         x='Computed values', \n",
    "         y='Expected values', \n",
    "         color= 'Colatitude',\n",
    "         facet_col='Complex',\n",
    "         trendline='ols', \n",
    "         hover_data=('Simplex id', 'Colatitude'),\n",
    "         title='<b>Exterior derivative</b> | expectation vs computation.')\n",
    "\n",
    "fig.show()\n",
    "\n",
    "# Extracting the regression coefficients:\n",
    "print('Correlation between expected & computed values:')\n",
    "for subplot in px.get_trendline_results(fig).iterrows():\n",
    "    row = subplot[1]\n",
    "    residual, slope = row.px_fit_results.params\n",
    "    print(f\"  - {row['Complex'][0].upper()}{row['Complex'][1:]} complex: {slope:.2%} (residual: {residual:.2e})\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remarks on the figure above:**\n",
    "- At first glance, the correlations seems rather good.\n",
    "- The point cloud in the dual case is a bit more spread, suggesting a lower precision in this case.\n",
    "- Zooming on the left hand-side panel, one could suspect a drift in precision from low to high colatiude values."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can investigate further by quantifying the relative error between the expected and computed values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "px.box(data_sph, y='Relative error', color=\"Complex\", \n",
    "       hover_data=('Colatitude', 'Computed values', 'Expected values'),\n",
    "       title='<b>Exterior derivative</b> | Relative error distribution'\n",
    "       ).show()\n",
    "\n",
    "px.scatter(data_sph, x='Colatitude', y='Relative error', \n",
    "           hover_data='Simplex id', facet_col=\"Complex\", color=\"Complex\",\n",
    "           title='<b>Exterior derivative</b> | Relative error VS colatitude'\n",
    "           ).show()\n",
    "\n",
    "\n",
    "print(f'Relative error (median +/- std dev) on:')\n",
    "for cplx in data_sph['Complex'].unique():\n",
    "    select = data_sph['Complex'] == cplx\n",
    "    slct_data = data_sph[select]['Relative error']\n",
    "    md = slct_data.median()\n",
    "    sd = slct_data.std()\n",
    "\n",
    "    nbr_good = np.count_nonzero((- .01 < slct_data.values) & \n",
    "                                (slct_data.values < .01))\n",
    "    nbr_tot = len(slct_data)\n",
    "\n",
    "    print(f'  - {cplx[0].upper()}{cplx[1:]} complex:')\n",
    "    print(f'      - err = {md:.2e} +/- {sd:.2e}.')\n",
    "    print(f'      - data with accuracy > 99% : {nbr_good/nbr_tot:.2%}.')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remarks on the figure above:**\n",
    "- The upper graph Shows that the precision is indeed good in the primal mesh, as the exterior derivative is computed with more than 99% accuracy in more than 97% of the cases.\n",
    "- The results are less good in the dual case (this was expected), as only 79% of data are computed with an accuracy higher than 99%.\n",
    "- Let's also notice that on the dual complex, the pathological points seems to be located around the *\"equator\"* (*i.e.* around the 90° colatitude).\n",
    "- Zooming on the left hand-side panel, we confirm the feeling we got on the previous figure: The loss of precision on the primal complex is mostly located close to the *\"south pole\"*, it is even increasing exponentially here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Results analysis on the disk"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we did with the sphere, we can first visualize the signal and its computed exterior derivative:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = pv.Plotter(shape=(2,2), border=False)\n",
    "\n",
    "for i, (name, s) in enumerate(signal_dsk.items()):\n",
    "    fig.subplot(0,i)\n",
    "    visualize(s, fig=fig, display=False,\n",
    "              layout_parameters={'title': f'{name} 0-cochain to derive.'})\n",
    "\n",
    "for i, (name, xderi) in enumerate(ext_deri_dsk.items()):\n",
    "    fig.subplot(1,i)\n",
    "    title = f'Ext. Deri. of a {name}, scalar-valued, 0-cochain.'\n",
    "    visualize(xderi, fig=fig, display=False, scaling_factor= 10,\n",
    "              layout_parameters={'title': title})\n",
    "    \n",
    "fig.link_views()\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remarks on the figure above:**\n",
    "- Note that we scaled the computed exterior derivatives by a 10 factor to make them more visible.\n",
    "- In the dual case (right hand side), we have big values on the border because \n",
    "  the computation of the exterior derivative is not valid here. Indeed these \n",
    "  outermost 1-cells do not have outer borders..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's now have a closer, more quantitative look on the data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = px.scatter(data_dsk, \n",
    "         x='Computed values', \n",
    "         y='Expected values', \n",
    "         color= 'Radius',\n",
    "         facet_col='Complex',\n",
    "         trendline='ols', \n",
    "         hover_data=('Simplex id', 'Radius'),\n",
    "         title='<b>Exterior derivative</b> | expectation vs computation.')\n",
    "\n",
    "fig.show()\n",
    "\n",
    "# Extracting the regression coefficients:\n",
    "print('Correlation between expected & computed values:')\n",
    "for subplot in px.get_trendline_results(fig).iterrows():\n",
    "    row = subplot[1]\n",
    "    residual, slope = row.px_fit_results.params\n",
    "    print(f\"  - {row['Complex'][0].upper()}{row['Complex'][1:]} complex: {slope:.2%} (residual: {residual:.2e})\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remarks on the graph above:**\n",
    "* In the *dual case*, we removed the values on the border for the 1-cells here are only connected to 1 0-cell, making the computation wrong.\n",
    "* The small residual values suggests that the correlation between expected and computed values seem rather good "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "px.box(data_dsk, y='Relative error', color=\"Complex\", \n",
    "       hover_data=('Radius', 'Computed values', 'Expected values'),\n",
    "       title='<b>Exterior derivative</b> | Relative error distribution'\n",
    "       ).show()\n",
    "\n",
    "px.scatter(data_dsk, x='Radius', y='Relative error', \n",
    "           hover_data='Simplex id', facet_col=\"Complex\", color=\"Complex\",\n",
    "           title='<b>Exterior derivative</b> | Relative error VS colatitude'\n",
    "           ).show()\n",
    "\n",
    "\n",
    "print(f'Relative error (median +/- std dev) on:')\n",
    "for cplx in data_dsk['Complex'].unique():\n",
    "    select = data_dsk['Complex'] == cplx\n",
    "    slct_data = data_dsk[select]['Relative error']\n",
    "    md = slct_data.median()\n",
    "    sd = slct_data.std()\n",
    "\n",
    "    nbr_good = np.count_nonzero((- .01 < slct_data.values) & \n",
    "                                (slct_data.values < .01))\n",
    "    nbr_tot = len(slct_data)\n",
    "\n",
    "    print(f'  - {cplx[0].upper()}{cplx[1:]} complex:')\n",
    "    print(f'      - err = {md:.2e} +/- {sd:.2e}.')\n",
    "    print(f'      - data with accuracy > 99% : {nbr_good/nbr_tot:.2%}.')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remarks on the figure above:**\n",
    "- The upper graph Shows that the precision is indeed good in the primal mesh, as the exterior derivative is computed with more than 99% accuracy in more than 98% of the cases.\n",
    "- The results are less good in the dual case (this was expected), as only 73% of data are computed with an accuracy higher than 99%.\n",
    "- Interestingly, we note that the mean relative error on the primal complex is bigger than in the sphere case but with less outliers, we get *in fine* a slightly higher percentage of accurate values (98% instead of 97%).\n",
    "- We note that in both cases (primal and dual), the error is increasing as we move toward the border of the disk."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "dxtr",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.18"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
