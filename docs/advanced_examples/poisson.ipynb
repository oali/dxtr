{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The Poisson equation\n",
    "\n",
    "**Abstract:** The Poisson equation can be seen as the *\"Hello world\"* of \n",
    "scientific computing, *i.e.* the first test to perform on a new system. In order \n",
    "to assess the **Dxtr** library, we implement this classic Boundary Value Problem \n",
    "*(BVP)* on a simple 2D circular domain and compare the results to theoretical \n",
    "expectations.\n",
    "\n",
    "> **Note:** You can [**download this notebook**](./poisson.ipynb \"download\") and play with it directly on your local machine."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Presentation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The poission equation correspond to the following classic boundary value problem:\n",
    "$$\n",
    "\\begin{cases}\n",
    "\\Delta\\phi = q &\\text{in}\\; \\Omega \\\\\n",
    "\\phi = \\phi_0 &\\text{on}\\; \\partial\\Omega \\\\\n",
    "\\end{cases}\n",
    "\\tag{1}\n",
    "$$\n",
    "\n",
    "We want to compute numerically the solution of this *BVP* and compare it to the \n",
    "theoretical expectations that can easily be derived for the specific case of a \n",
    "2D circular domain.\n",
    "\n",
    "We will consider the following specific numerical values: \n",
    "* $r_0 = 1$ (radius of the domain)\n",
    "* $q=-1$\n",
    "* $\\phi_0 = 10$.\n",
    "\n",
    "The analytical expression of the solution of this *BVP* reads:\n",
    "$$\n",
    "\\phi(r) = \\frac{1}{4}(1 - r^2) + 10, \n",
    "\\tag{2}\n",
    "$$\n",
    "with $r$ the radial distance from the center of the disk."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dependencies"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from __future__ import annotations\n",
    "\n",
    "import numpy as np\n",
    "import pandas as pd\n",
    "import scipy.sparse as sp\n",
    "import scipy.sparse.linalg as lng\n",
    "import plotly.express as px\n",
    "import plotly as pl\n",
    "import pyvista as pv\n",
    "\n",
    "from dxtr import SimplicialManifold, Cochain\n",
    "from dxtr.complexes import disk\n",
    "from dxtr.operators import laplacian\n",
    "from dxtr.visu import visualize"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Domain defintion"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the disk domain generator available in the `dxtr.complexes.example_complexes` sub-module, we define three 2D circular domains of various resolutions.\n",
    "\n",
    "> **Note:** Since we are going to use the Hodge-Laplacian operator, that relies on the definition of a dual cellular complex, we need to implement our domains as `SimplicialManifold` instances."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "disks = {size: disk(size=size, manifold=True)\n",
    "         for size in ['small', 'large', 'massive']}\n",
    "\n",
    "for size, disk in disks.items():\n",
    "    N0, N1, N2 = disk.shape\n",
    "    print(f'the {size} disk is composed of {N0} 0-simplices, {N1} 1-simplices & {N2} 2-simplices.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's visualize one the generated simplicial complexes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "size = 'small'\n",
    "disk = disks[size]\n",
    "visualize(disk, size=2, width=1, \n",
    "          title=f'<b>{size} disk</b> | (0,1,2)-simplices:{disk.shape}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Implementation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We solve the BVP using the `spsolver` of the `scipy.sparse.linalg` module. To \n",
    "that end, we define the `solve_poisson_problem_on()` function that instantiates \n",
    "the Poisson BVP on an input `SimplicialManifold` and, most importantly, \n",
    "implements the Dirichlet boundary conditions corresponding to the second line of \n",
    "$\\text{eq.}(1)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def solve_poisson_problem_on(manifold:SimplicialManifold, **kwargs) -> Cochain:\n",
    "    \"\"\"Instanciates and solves Poisson's equation on a given manifold.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    manifold\n",
    "        The `SimplicialManifold` to solve on.\n",
    "    \n",
    "    Other Parameters\n",
    "    ----------------\n",
    "    diffusion_coefficient: float\n",
    "        Default is 1\n",
    "    source: float\n",
    "        Default is -1\n",
    "    boundary_value: float\n",
    "        Default is 10\n",
    "    \n",
    "    Returns\n",
    "    -------\n",
    "    Cochain\n",
    "        The sought solution of Poisson's problem.\n",
    "    \"\"\"\n",
    "    \n",
    "    # Parameters\n",
    "    D = kwargs.get('diffusion_coefficient', 1)\n",
    "    q = kwargs.get('source', -1)\n",
    "    bnd_val = kwargs.get('boundary_value', 10)\n",
    "\n",
    "    # Get the 0-simplices on the boundary of the domain\n",
    "    k = 0\n",
    "    N0 = manifold[k].size\n",
    "    bnd_vids = manifold.border()[k]\n",
    "    is_in_bnd = [1 if vid in bnd_vids else 0 for vid in range(N0)]\n",
    "    bnd_cond = Cochain(manifold, dim=k, \n",
    "                       values={vid: bnd_val for vid in bnd_vids})\n",
    "\n",
    "    # Defines mask matrices to apply Dirichlet BCs\n",
    "    mask_bnd = sp.diags(is_in_bnd)\n",
    "    mask_in = sp.identity(N0) - mask_bnd\n",
    "\n",
    "    # Convert to csr format\n",
    "    mask_bnd = sp.csr_matrix(mask_bnd)\n",
    "    mask_in = sp.csr_matrix(mask_in)\n",
    "\n",
    "    # Remove the rows with only zeros\n",
    "    mask_bnd = mask_bnd[mask_bnd.getnnz(0)>0]\n",
    "    mask_in = mask_in[mask_in.getnnz(0)>0]\n",
    "\n",
    "    # Building the system to solve\n",
    "    L = D * laplacian(manifold, 0).values\n",
    "\n",
    "    source = Cochain(manifold, dim=0, values=q)\n",
    "\n",
    "    lhs = mask_in @ L @ mask_in.T\n",
    "    rhs = mask_in @ (source.toarray() - L @ mask_bnd.T @ bnd_cond.toarray())\n",
    "\n",
    "    # Solving the problem\n",
    "    solution = lng.spsolve(lhs, rhs)\n",
    "\n",
    "    # Re-assembling the solution\n",
    "    full_solution = mask_in.T @ solution + mask_bnd.T @ bnd_cond.toarray()\n",
    "\n",
    "    return Cochain(manifold, dim=0, values=full_solution)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We compute the solutions for each domain with increasing resolution:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "solution = {size: solve_poisson_problem_on(disk)\n",
    "            for size, disk in disks.items()}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Result analysis"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can first have a quick glance at the computed solutions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = pv.Plotter(shape=(1,3), border=False)\n",
    "\n",
    "for i, (size, sol) in enumerate(solution.items()):\n",
    "    fig.subplot(0, i)\n",
    "    params = {'title': f'{size} sphere \\n({sol.shape[0]} vertices)'}\n",
    "    visualize(solution['small'], scaling=.05, fig=fig, display=False,\n",
    "              layout_parameters=params)\n",
    "\n",
    "fig.link_views()\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now try to estimate the error between the computed values and the \n",
    "theoretical expectation. To that end, we will define an `expectation()` function \n",
    "based on $\\text{eq.}(2)$, as well as an `error()` function to compute the \n",
    "relative *Root Mean Square* error between the expectation and the computations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def expectation(x:np.ndarray[float]) -> np.ndarray[float]:\n",
    "    \"\"\"Theoretical expectation, given by eq.(2)\"\"\"\n",
    "    return .25 * (1 - x**2) + 10"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def error(values:np.ndarray[float], \n",
    "          expectations:np.ndarray[float], average:bool=True\n",
    "          ) -> float|np.ndarray[float]:\n",
    "    \"\"\"Computes the relative RMS error.\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    values\n",
    "        The list of computed values to assess.\n",
    "    expectations\n",
    "        The list of expected values.\n",
    "    average\n",
    "        If True, a single averaged value is returned;\n",
    "        else, an array is returned.\n",
    "    \n",
    "    Returns\n",
    "    -------\n",
    "        The seeked error value.\n",
    "\n",
    "    Notes\n",
    "    -----\n",
    "    * The use of this function to estimate the precision of our framework is\n",
    "      inspired by the Ben-Chen et al (2010) paper.\n",
    "    \"\"\"\n",
    "    \n",
    "    try:\n",
    "        values = np.asarray(values)\n",
    "        expectations = np.asarray(expectations)\n",
    "\n",
    "        assert values.shape == expectations.shape, ('WARNING:'\n",
    "            + f'values shape {values.shape} !='\n",
    "            + f'expectations shape {expectations.shape}')\n",
    "        \n",
    "        res = (values - expectations) ** 2\n",
    "        nrm = (expectations ** 2).sum()\n",
    "        if nrm < 1e-10: nrm = 1\n",
    "        \n",
    "        if average:\n",
    "            return np.sqrt(res.sum() / nrm)\n",
    "        else:\n",
    "            return np.sqrt(res) / np.sqrt(nrm)\n",
    "\n",
    "    except AssertionError as msg:\n",
    "        print(msg)\n",
    "        return None"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We wrap the results, the expectation and the error into a `DataFrame` for \n",
    "visualization."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sol = pd.DataFrame()\n",
    "for size, cchn in solution.items():\n",
    "    \n",
    "    Nn = cchn.complex[-1].size\n",
    "    \n",
    "    x = cchn.positions\n",
    "    r = np.linalg.norm(x, axis=1)\n",
    "    y = cchn.values\n",
    "    xpct_y = expectation(r)\n",
    "    err = error(y, xpct_y, average=False)\n",
    "\n",
    "    df = pd.DataFrame({'Manifold': 'disk',\n",
    "                       'Size': size,\n",
    "                       'Number of top simplices': Nn,\n",
    "                       'Radius': r,\n",
    "                       'Values': y,\n",
    "                       'Expected values': xpct_y,\n",
    "                       'Error': err}\n",
    "                       ).sort_values('Radius')\n",
    "    \n",
    "    sol = pd.concat((sol, df), ignore_index=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We define a function to visualize the solutions and the expectation as \n",
    "function of the radial position."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot(data:pd.DataFrame, **kwargs)->pl.graph_objs._figure.Figure:\n",
    "    \"\"\"Plots the solution and expectation as functions of the radial position.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    data\n",
    "        The `pandas.DataFrame` to visualize.\n",
    "\n",
    "    Other parameters\n",
    "    ----------------\n",
    "    manifold : str\n",
    "        The name of the considered domain. By default the value given \n",
    "        in the input `DataFrame` is used.\n",
    "    title : str\n",
    "        The title of the plot. By defaut \"Poisson | disk\".\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "        A plotly figure.\n",
    "    \"\"\"\n",
    "    operator = 'Poisson'\n",
    "    manifold = kwargs.get('manifold', df['Manifold'].unique()[0])\n",
    "\n",
    "    fig = px.scatter(data, x='Radius', y='Values', color='Size')\n",
    "    \n",
    "    # Adding the expectation on top.\n",
    "    select = data['Size'] == 'small'\n",
    "    ref = px.line(data[select], x='Radius', y='Expected values')\n",
    "    fig.add_trace(ref.data[0])\n",
    "\n",
    "    # Cosmetics\n",
    "    fig['data'][-1].name = 'Expectation'\n",
    "    fig['data'][-1].showlegend = True\n",
    "    fig['data'][-1]['line']['color']='gray'\n",
    "    fig['data'][-1]['line']['dash']='dot'\n",
    "\n",
    "    fig.update_layout(template='plotly_white',\n",
    "                    title=kwargs.get('title', \n",
    "                                    f'<b>{operator}</b> | {manifold}'),\n",
    "                    showlegend=kwargs.get('showlegend', True)\n",
    "                    )\n",
    "\n",
    "    return fig\n",
    "\n",
    "\n",
    "plot(sol).show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can aslo visualize directly the error between the computed and expected values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_error(data:pd.DataFrame, **kwargs)->pl.graph_objs._figure.Figure:\n",
    "    \"\"\"Plots the relative RMS error between the computed solutions and theoretical expectation.\n",
    "\n",
    "    Parameters\n",
    "    ----------\n",
    "    data\n",
    "        The `pandas.DataFrame` to visualize.\n",
    "\n",
    "    Returns\n",
    "    -------\n",
    "        A `plotly` figure.\n",
    "    \"\"\"\n",
    "    operator = 'Poisson'\n",
    "    manifold = kwargs.get('Manifold', df['Manifold'].unique()[0])\n",
    "\n",
    "    fig = px.scatter(data, x='Radius', y='Error', color='Size', log_y=True)\n",
    "\n",
    "    fig.update_layout(template='plotly_white',\n",
    "                      title=kwargs.get('title', \n",
    "                                       f'<b>{operator}</b> | {manifold}'),\n",
    "                      showlegend=kwargs.get('showlegend', True))\n",
    "    return fig\n",
    "\n",
    "plot_error(sol).show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Conclusion:** \n",
    "From the graphs above we see that the Dxtr-based simulation yields results in good accordance with theoretical expectations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "dxtr",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.18"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
