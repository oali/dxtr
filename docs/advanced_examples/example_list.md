# Some working examples

In the following pages, you will find some notebooks showcasing the **Dxtr** library in action.

- [x] **The Poisson problem.** This is the well-known *"Hello world!"* boundary value problem. In this notebook we show how to instanciate it in *DEC* and checks the accuracy of the solutions.
- [x] **Accuracy of the exterior derivatve.** The exterior derivative is the founding operator of *DEC*. It is therefore crutial to estimate its accuracy to better understand the limitations of our framework. This is the goal of this notebook.
- [x] **Hodge-Laplacian Spectral analysis on spheres.** The Laplacian might be the most famous differential operator ever. In this notebook, we check that its spectrum matches the expected spherical harmonics eigenvalues when computed on unit spheres; for differential forms of various degrees.  

!!! info "Experimenting directly with these examples:"
    All the presented notebooks can be downloaded from a link within their first cell.