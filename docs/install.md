# 

## Installation

To start using **Dxtr** you can choose one of the following three options: 

1. get it directly from **PyPI** using `pip`, the main package installer for Python; 
2. fetch the library from the _mosaic_ channel on **Anaconda** using `conda`, a package and environment management system that works on all major operating systems;
3. install the Python source files from the Inria's **GitLab** repository.

Only the last option provides direct access to the source files.
This makes it the preferred method for anyone interested in exploring the library's implementation.

!!! warning "Use a Virtual Environment"
    We strongly recommend creating a virtual environment before installing **Dxtr**.
    Virtual environments help isolate dependencies for specific projects, ensuring a clean and manageable Python environment.
    If you are unfamiliar with virtual environments, you can refer to the _Real Python_ guide :point_right: [here](https://realpython.com/python-virtual-environments-a-primer/).


=== "From PyPI :fontawesome-brands-python:"

    !!! tip "Install `Python` and `pip` first!"
        This method relies on the `pip` package management system.
        If `pip` is not already installed on your machine, please follow the instructions [here](https://packaging.python.org/en/latest/tutorials/installing-packages/).
    
    Once `pip` is succesfully installed on your machine (and a virtual environment is set up):
    
    ```bash
    conda env create -n dxtr # (1)
    pip install dxtr # (2)
    ```
    
    1. Create the `dxtr` virtual environment using conda
    2. Fetch the PyPI package

=== "From Anaconda :fontawesome-solid-cloud:"

    !!! tip "Install `conda` first!"
        This method relies on the `conda` package and environment management system.
        If `conda` is not already installed on your machine, follow the instructions [here](https://docs.conda.io/en/latest/miniconda.html).
    
    Once `conda` is successfully installed on your machine:
    
    ```bash
    conda env create -n dxtr # (1)
    conda install dxtr -c mosaic # (2)
    conda activate dxtr # (3)
    ```
    
    1. Create the `dxtr` virtual environment using conda
    2. Fetch the package using conda
    3. Activate the environment and start using the library

=== "From GitLab :fontawesome-brands-gitlab:"

    !!! tip "Install `git` first!"
        This method relies on the `git` tool.
        If `git` is not already installed on your machine, follow the instructions [here](https://git-scm.com/downloads).
    
    ```bash
    git clone https://gitlab.inria.fr/oali/dxtr.git #(1)
    conda env create -f conda/env.yaml #(2)
    conda activate dxtr #(3)
    python -m pip install -e . #(4)
    ```
    
    1. Clone the package source code
    2. Create a new conda environment
    3. Activate the environment 
    4. Install the package


---

## Tests

Once the library is installed, we recommend running **unit tests** to ensure that all major features are functioning properly.


!!! warning inline end "Be patient!"
    Running tests takes a few minutes depending on your machine. 

```bash
cd dxtr #(1)
conda activate dxtr #(2)
conda list pytest #(3)
python -m pip install -e '.[test]' #(4)
pytest #(5)
```

1.  Move to the root of the `dxtr` repository.
2.  Activate your `dxtr` conda environment 
3.  Verify that the `pytest` and `pystest-cov` packages are installed in your environment.
4.  :warning: If the packages `pytest` and `pytest-cov` are missing, you must install them in your `dxtr` environment before proceeding.
5.  Once `pytest` is installed, run the test procedure at the root of the project. The test options are defined in the `pyproject.toml` file.


!!! note 
     Running tests is only required if you performed the **gitlab** procedure. If you installed the library directly from **conda** or **pipy** you can download and run the [advanced examples notebooks](./advanced_examples/example_list.md) to test your installation.