#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

import click
import nbformat
from nbconvert.preprocessors import ClearOutputPreprocessor

DESCRIPTION = """Modifies Jupyter notebooks in predefined directories."""


@click.command(help=DESCRIPTION)
@click.option('--clear-output', is_flag=True, help='Clear all outputs in the notebook')
@click.option('--dynamic-plot', is_flag=True, help='Enable PyVista dynamic plot mode')
def main(clear_output, dynamic_plot):
    """Modifies notebooks in predefined directories.

    Parameters
    ----------
    clear_output : bool
        If ``True`` (default ``False``), all outputs in the notebooks will be cleared.
    dynamic_plot : bool
        If ``True`` (default ``False``), PyVista dynamic plot mode will be enabled in the notebooks.

    Notes
    -----
    This function iterates over all Jupyter Notebook files located in the `docs/` directory and its
    subdirectory `docs/advanced_examples`. For each notebook, the function processes the file based
    on the specified options. The input notebook path is the same as the output notebook path for all
    processes, modifying the notebooks in-place.
    """
    if clear_output:
        print("Clearing all outputs in notebooks...")
    if dynamic_plot:
        print("Enabling PyVista dynamic plot mode...")

    # - Define the paths to the notebooks
    notebooks = [f"docs/{f}" for f in os.listdir("docs/") if f.endswith(".ipynb")]
    notebooks += [f"docs/advanced_examples/{f}" for f in os.listdir(f"docs/advanced_examples/") if f.endswith(".ipynb")]

    # - modify all notebooks
    for file in notebooks:
        print(f'\nModifying file: {file} -> {file}')
        process_notebook(input_notebook_path=file, output_notebook_path=file,
                         clear_output=clear_output, dynamic_plot=dynamic_plot)


def process_notebook(input_notebook_path, output_notebook_path, clear_output=False, dynamic_plot=False):
    """Process a Jupyter notebook: Clear outputs, modify for PyVista and write to another path.

    This function orchestrates the following:
      1. Loads the notebook from the specified path.
      2. Clears all outputs.
      3. Modifies the notebook to support PyVista and Sphinx documentation.
      4. Write the notebook to the specified path

    Parameters
    ----------
    input_notebook_path : str
        Path to the input Jupyter notebook file (.ipynb).
    output_notebook_path : str
        Path to write the Jupyter notebook file (.ipynb).
    clear_output: bool, optional
        Clear all outputs in the notebook, by default False.
    dynamic_plot : bool, optional
        Whether to enable PyVista dynamic plot mode, by default False.

    Raises
    ------
    FileNotFoundError
        If the notebook specified by `input_notebook_path` does not exist.
    Exception
        If any issues arise during the notebook clearing or modification

    Notes
    -----
    - This function is part of a workflow for preparing Jupyter notebooks for HTML-based documentation.
    - By enabling `dynamic_plot`, PyVista's interactive plot mode is incorporated into the notebook.
    """
    try:
        # Step 1: Load the notebook from the specified path
        if not os.path.exists(input_notebook_path):
            raise FileNotFoundError(f"Notebook not found: {input_notebook_path}")

        with open(input_notebook_path, 'r', encoding='utf-8') as f:
            notebook_content = nbformat.read(f, as_version=4)
        print(f"Loaded notebook: {input_notebook_path}")

        # Step 2: Clear outputs
        if clear_output:
            notebook_content = clear_notebook(notebook_content)

        # Step 3: Modify notebook content for PyVista
        modified_notebook = modify_notebook_for_pyvista(notebook_content,
                                                        dynamic_plot=dynamic_plot)

        # Step 4: Write the modified notebook to the specified output path
        with open(output_notebook_path, 'w', encoding='utf-8') as f:
            nbformat.write(modified_notebook, f)
        print(f"Modified notebook written to: {output_notebook_path}")

    except Exception as e:
        print(f"Unexpected error during notebook processing: {e}")
        return None


def clear_notebook(notebook_content):
    """ Clear all outputs in a Jupyter notebook content.

    This function removes all cell outputs and return the notebook content.

    Parameters
    ----------
    notebook_content : dict, optional
        The loaded notebook content (as a Python dictionary).

    Returns
    -------
    dict
        Cleared notebook content.
    """
    try:
        # Clear outputs from the notebook
        clear_preprocessor = ClearOutputPreprocessor()
        cleared_notebook, _ = clear_preprocessor.preprocess(notebook_content, {})
        print("Cleared notebook outputs.")
        return cleared_notebook
    except Exception as e:
        print(f"Error while clearing notebook outputs: {e}")


def modify_notebook_for_pyvista(notebook_content, dynamic_plot=False):
    """ Modify notebook content for Mkdocs documentation by changing pyvista backend configurations

        TODO: follow issues to know when a full dynamic plot with `html` can be done
              https://github.com/pyvista/pyvista/issues/6250 --> add_text (title)
              https://github.com/pyvista/pyvista/issues/5389 --> labelled scalar bar

    Parameters
    ----------
    notebook_content : dict
        The loaded notebook content (as a Python dictionary).
    dynamic_plot : bool, optional
        Make the pyvista plot dynamic or static, by default False.

    Returns
    -------
    dict
        Modified notebook content.
    """
    try:
        # Set the PyVista backend flag
        pyvista_flag = 'html' if dynamic_plot else 'static'

        # Search for the existing PyVista configuration cell
        found_index = None
        for index, cell in enumerate(notebook_content.get("cells", [])):
            if (
                    "# ---- for online doc only ---- #" in cell.get("source", "")
                    and "pv.set_jupyter_backend" in cell.get("source", "")
            ):
                found_index = index
                break

        if found_index is not None:
            # Check and update the PyVista backend configuration if needed
            existing_cell = notebook_content["cells"][found_index]
            if f"pv.set_jupyter_backend('{pyvista_flag}')" not in existing_cell["source"]:
                # Update the existing cell with the new flag
                existing_cell["source"] = (
                    "# ---- for online doc only ---- #\n"
                    f"import pyvista as pv\n"
                    f"pv.set_jupyter_backend('{pyvista_flag}')\n"
                    "pv.start_xvfb(3)\n"
                    "# ---- end of online doc only ---- #"
                )
                existing_cell["metadata"]["tags"] = ["hide-input"]
        else:
            # Add a new first cell with PyVista backend configuration if it doesn't exist
            new_first_cell = nbformat.v4.new_code_cell(
                source=(
                    "# ---- for online doc only ---- #\n"
                    f"import pyvista as pv\n"
                    f"pv.set_jupyter_backend('{pyvista_flag}')\n"
                    "pv.start_xvfb(3)\n"
                    "# ---- end of online doc only ---- #"
                )
            )
            # Add metadata to hide the new cell in MkDocs-generated documentation
            new_first_cell["metadata"]["tags"] = ["hide-input"]

            # Insert the new first cell at the beginning of the cells list
            notebook_content["cells"].insert(0, new_first_cell)

        return notebook_content

    except Exception as e:
        print(f"Error while modifying notebook: {e}")
        return None


if __name__ == '__main__':
    main()
