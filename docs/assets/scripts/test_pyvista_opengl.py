import pyvista as pv
from pyvista import examples

pv.start_xvfb(wait=3, window_size=(100, 100))
pv.set_jupyter_backend('static')

mesh = examples.download_st_helens()
warped = mesh.warp_by_scalar('Elevation')
surf = warped.extract_surface().triangulate()
surf = surf.decimate_pro(0.75)  # reduce the density of the mesh by 75%
surf.plot(cmap='Purples', screenshot='test_pyvista_opengl.png')