---
hide:
  - navigation
---

# Welcome!

The **Dxtr** library implements the theory of *Discrete Exterior Calculus* (*DEC* in short) in python. Its purpose is to provide users with an intuitive framework to prototype differential geometry problems within the Exterior Calculus paradigm. The library is also suited to handle basic algebraic topology problems.


!!! abstract "In this documentation, you will find:"
    - [x] Some [installation instructions](./install.md).
    - [x] A [quick introduction](./context.md) to the *DEC* theory.
    - [x] An [overview](./architecture_overview.md) of the library architecture of the library & its main components. 
    - [x] Some [use cases](./advanced_examples/example_list.md) of the library components.
    - [x] An auto-generated [API reference guide](assets/_reference/API.md).
    
