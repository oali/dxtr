{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Visualization\n",
    "\n",
    "Visualizing complexes and cochains is fundamental in order to develop and analyse *DEC*-based simulations. That is why we developped the `visu` module within **Dxtr** in order to interact and visualize properly objects of interest."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `visu` module takes advantages of two existing visualization libraries: \n",
    "* [**plotly**](https://plotly.com/python/): A data visualization library will a lot of interactive features.\n",
    "* [**pyvista**](https://pyvista.org): A python labrary dedicated to 3D visualization."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We chose to rely on two libraries for they both offer a distinct set of useful functionalities:\n",
    "\n",
    " As a data-analysis driven library, **plotly** encapsulates a lot of features that enable direct interactions with the data. For instance display of information while overing data. We found such features especially usefull when working on `SimplicialComplex` or `SimplicialManifold` instances. It enables us to easily spot a simplex or a group of simplices within a complex and to retrieve their ids. However one shortcoming of plotly is its lack of geometry-dedicated functionalities. For instances, vector field representation is not straight-forward and efficient, making the use of this library ill-suited to display large scale and complex vector fields.\n",
    "\n",
    " That is where **pyvista** shines. As a direct spin-off of the [VTK library](https://vtk.org) the library handles efficiently complex structures and  vector fields. The library also provides advanced features such as multi-panel visualizations, clipping planes and multiple views synchronisation, making it particularly well suited to display Cochains values over large simplical complexes. However, the interactive features present in plotly do not come natively in pyvista.\n",
    "\n",
    "\n",
    " Let's now showcase some use cases."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualizing complexes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's start by visualizing a simple `SimplicialComplex`, calling the `visualize()` method from the `dxtr.visu` module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr.complexes import diamond\n",
    "from dxtr.visu import visualize\n",
    "\n",
    "cplx = diamond()\n",
    "\n",
    "visualize(cplx)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When provided with an instance of `SimplicialComplex` (or `SimplicialManifold`), the `visualize()` function will automatically rely on the **plotly** library to generate the visualization.\n",
    "\n",
    "The user can then interact with the generated scene in several ways, for instance:\n",
    "* Hiding k-simplices of any degrees.\n",
    "* Retrieving the id of any simplex.\n",
    "\n",
    "For instance, by overing the various vertices, *aka* nodes, on the scene, one can retrive the ids of the ones forming the equatorial triangle (*i.e.* 1,2,3) as well as they positions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note:** Abstract complexes (*i.e.* not embedded in any geometry) cannot be visualized:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr import AbstractSimplicialComplex\n",
    "\n",
    "asc = AbstractSimplicialComplex(indices=[[1,2,3],[2,4]])\n",
    "\n",
    "visualize(asc)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "One can change the size of the nodes, the thickness of the edges or the color of the complex, using respectively the arguments `point_size`, `edge_width` and `color`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "visualize(cplx, size=3, width=3, color='green')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also chose to visualize only simplices of given degrees:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "visualize(cplx, degrees=[0, 1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another useful feature of the `visualize()` function is the ability to highlight a subset of simplices. For instance, in the considered simplcial complex, we have the following 2-simplices:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's visualize one taken at random, using the `highlight` argument:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "k = 2\n",
    "Nk = cplx[k].size\n",
    "sid = np.random.randint(0, Nk)\n",
    "\n",
    "visualize(cplx, highlight={k: [sid]})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The highlighted simplex appears in orange within the visualization. \n",
    "\n",
    "The `highlight` argument must be given as a dictionary: \n",
    "\n",
    "- The keys correspond to topological dimensions (k),\n",
    "- The values to lists of k-simplex indices to consider.\n",
    "    \n",
    "We can give it multiple simplices of multiple dimensions, the highlighting color can be changed with the `color_accent` argument:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sid_closure = cplx.closure({k: sid})\n",
    "visualize(cplx, highlight=sid_closure, color_accent='red')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, let's mention the ability to visualize the dual cell-complex, if the complex we work on is instanciated as a `SimplicialManifold`. \n",
    "\n",
    "If nothing is specified, the `visualize()` function will render the simplicial complex:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr.complexes import icosahedron\n",
    "\n",
    "mfld = icosahedron(manifold=True)\n",
    "\n",
    "visualize(mfld)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can change this representation, using the `show` argument. Set to `dual`, it will generate a representation of the cell complex:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "visualize(mfld, show='dual')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Note that in the case of the dual complex, only 0 and 1-cells are displayed.\n",
    "\n",
    "We can also superimpose both complexes setting the `show` argument to `all`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "visualize(mfld, show='all')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualizing cochains"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To visualize `Cochain` objects, we can use the same function as before: `visualize()`.\n",
    "However, as the input type is `Cochain`, this time the function will rely on the **pyvista** library to perform the visualization."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Warning:** In the current version of the documentation, all pyvista-based visualizations are rendered as static images. This is due to technical difficulties related to the automatic documentation generation on the CI server."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualization of 0-cochains\n",
    "\n",
    "Let's consider first a primal random 0-cochain"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr.cochains import random_cochain\n",
    "\n",
    "k = 0\n",
    "cchn = random_cochain('icosa', k)\n",
    "\n",
    "# Before visualization we can define some basic layout properties:\n",
    "vmin, vmax = cchn.values.min(), cchn.values.max()\n",
    "\n",
    "title = f'A random primal {k}-cochain of values in [{vmin:.2f}, {vmax:.2f}].'\n",
    "\n",
    "lp = {'title': title,\n",
    "      'color_range': [0, .5],\n",
    "      'show_colorbar': True}\n",
    "\n",
    "\n",
    "# Once this is done, we can perform the visualization\n",
    "visualize(cchn, layout_parameters=lp)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note:**\n",
    "When working with `Cochain` and the pyvista-based visualization, plots can be tunes thanks to the keyword-argument `layout_parameters`, which is a dictionnary:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For dual cochains the process is exactly the same given that the displayed complex is the dual one:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "d_cchn = random_cochain('icosa', k, dual=True)\n",
    "\n",
    "title = f'This time a dual {k}-cochain is displayed.'\n",
    "\n",
    "lp = {'title': title,\n",
    "      'colormap': 'inferno'}\n",
    "\n",
    "visualize(d_cchn)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A very powerful properties of pyvista graphical objects is that they can easily be combined to form more advanced figures, the `visualization()` function can benefit from this modularity thanks to the keyword-argument `display`. Set to false, the graphical object is generated but not displayed, enabling the production of advanced figures:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyvista as pv\n",
    "\n",
    "fig = pv.Plotter(shape=(1,2), border=False) # We define a figure as a pyvista.Plotter() object with a shape that corresponds to the subfigures we intent to display.\n",
    "\n",
    "# We start populating the first subfigure.\n",
    "fig.subplot(0,0)\n",
    "primal_description = {'title': f'This is a primal {k}-cochain.'}\n",
    "visualize(cchn, display=False, fig=fig, layout_parameters=primal_description)\n",
    "\n",
    "# We switch to the second subfigure.\n",
    "fig.subplot(0,1)\n",
    "dual_description = {'title': f'This is a dual {k}-cochain.',\n",
    "                    'colormap': 'inferno'}\n",
    "visualize(d_cchn, display=False, fig=fig, layout_parameters=dual_description)\n",
    "\n",
    "\n",
    "fig.link_views() # We connect the subfigures so when one is moving the other follows.\n",
    "fig.show() # We display the whole figure."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remarks on the figure above:**\n",
    "* We call twice the `visualize()` function, once for each object/subfigure.\n",
    "* When called, we specify the `pv.Plotter()` where to store the graphics with the kwargs `fig`.\n",
    "* We also prevent the immediate visualization with the kwargs `display` set to `False`.\n",
    "* Details on the **pyvista** API can be found within the [online documentation](https://docs.pyvista.org)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "0-cochains depicts scalar fields defined on 0-simplices (*i.e.* vertices), they are displayed in pyvista as color gradients interpolated on surface elements connecting those vertices. Let's now consider higher-order cochains !"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Rendering of k-cochains with k > 0:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's instanciate and visualize a randon 1-Cochain."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "k = 1\n",
    "cchn_1 = random_cochain('icosa', k)\n",
    "\n",
    "visualize(cchn_1, scaling_factor=10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remarks on the figure above:**\n",
    "* For 1-cochain we chose to depict orientation by using arrow heads as colors on the edges would have been too subtle to detect.\n",
    "* if The arrows appear too small or big on the edges, their size can be adjusted using the `scaling_factor` keyword-argument.\n",
    "* Dual 1-cochain visualization works identically."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Visualization of 2-cochains:**\n",
    "\n",
    "As 2-cochain coefficients are supported by 2-simplices (triangles or polyhedra), it makes sense to depict them as color maps."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cchn_2 = 10 * random_cochain('icosa', 2)\n",
    "d_cchn_2 = random_cochain('icosa', 2, dual=True)\n",
    "\n",
    "fig = pv.Plotter(shape=(1,2), border=False)\n",
    "\n",
    "fig.subplot(0,0)\n",
    "visualize(cchn_2, fig=fig, display=False, \n",
    "          layout_parameters={'title': 'Primal 2-cochain',\n",
    "                             'colormap': 'inferno',\n",
    "                             'show_colorbar': True})\n",
    "\n",
    "fig.subplot(0,1)\n",
    "visualize(d_cchn_2, fig=fig, display=False, \n",
    "          layout_parameters={'title': 'Dual 2-cochain',\n",
    "                             'show_colorbar': True})\n",
    "\n",
    "fig.link_views()\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remark on the figure above:**\n",
    "\n",
    "Contrary to 0-cochains, defined on vertices, as 0-cochains are defined on surface elements, their values are rendered as plain colors on these surface elements, not gradients."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Vector field visualization"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the **Dxtr** library, one can define discrete vector fields as vector-valued dual 0-cochains. Such objects can also be displayed using the `visualize()` function. Let's consider the normal vector field defined on a sphere to illustrate this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr.cochains import normal_vector_field\n",
    "\n",
    "nrl = normal_vector_field('sphere')\n",
    "\n",
    "visualize(nrl, scaling_factor=.2,\n",
    "          layout_parameters={'title': f'{nrl.name}'})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Remark on the graph above:**\n",
    "\n",
    "Again we used the `scaling_factor` argument to adjuct the size of the vectors."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "dxtr",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.18"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
