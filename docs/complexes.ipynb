{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Complexes\n",
    "\n",
    "In the following we introduce the main classes of the **complexes** module: \n",
    "`Simplex`, `AbstractSimplicialComplex`, `SimplicialComplex` and \n",
    "`SimplicialManifold`. The goal is to showcase their basic manipulations \n",
    "(*i.e.* instanciation, visualization, reading, writing) and highlight their key \n",
    "features. For more advanced use cases, the interested reader should visit the \n",
    "**Dxtr in action** section of this documentation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simplices\n",
    "\n",
    "Simplices are the geometrical building units that consitute simplicial \n",
    "complexes when combined together. \n",
    "In the **Dxtr** library, simplices can be instanciated with the `Simplex` class \n",
    "and a list of indices:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr import Simplex\n",
    "\n",
    "ids = [0, 1, 2, 3]\n",
    "\n",
    "splx = Simplex(ids)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** the `Simplex` class is designed as a `dataclass`, *i.e.* a minimalist \n",
    "container that is not meant to be extensively modified.\n",
    "\n",
    "Such a simplex is said to be *abstract* as it is not embedded in any geometrical \n",
    "space. Maybe the most important property of an abstract simplex is its \n",
    "**topological dimension,** ***aka*** **degree** (often noted *k*) are accessible through the `dim` property.\n",
    "The abstract nature of a simplex can be checked through the `isabstract` \n",
    "propery:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if splx.isabstract:\n",
    "    print(f'{splx} is an abstract simplex of topological dimension {splx.dim}.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to get a geometrical simplex, one needs to provide position vectors for \n",
    "its indices:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vtcs = [[0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1]]\n",
    "\n",
    "splx.set_vertices(vtcs)\n",
    "\n",
    "if not splx.isabstract:\n",
    "    print(f'{splx} is not abstract anymore:')\n",
    "for idx, vtx in zip(splx.indices, splx.vertices):\n",
    "    print(f'    Its index #{idx} is located at position {vtx}.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once embedded, simplices acquire some geometrical properties, such as a volume \n",
    "and a circumcenter:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vol = splx.volume\n",
    "cctr = splx.circumcenter\n",
    "\n",
    "print(f'{splx} is centered around {cctr} and has a volume of {vol:.2e}.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** The `volume` of a (k)-simplex, here has to be understood as the \n",
    "*k-volume*: For a (1)-simplex it is the length of the associated edge, for a \n",
    "(2)-simplex it is the surface area of the associated triangle, for a \n",
    "(3)-simplex, the volume of the associated tetrahedron. This notion can be \n",
    "generalized to any dimension."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Simplices equipped with a geometry can be displayed with the `visualize()` \n",
    "function from the `visu` module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr.visu import visualize\n",
    "\n",
    "visualize(splx)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** For more information about the `visu` module and its possibilities, \n",
    "please check the dedicated tutorial in the **Dxtr in action** section of this \n",
    "documentation.\n",
    "\n",
    "Considered alone, simplices are not very useful. Things get interesting when \n",
    "they are combined into simplicial complexes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Abstract simplicial complexes\n",
    "\n",
    "An abstract simplicial complexes corresponds to a set of simplices for which no \n",
    "geometrical embedding has been specified. It is therefore a purely topological \n",
    "entity. For further details, check the dedicated [paragraph](context_DEC.md#simplicial_complex) in the ***DEC*** **in a nutshell** section of this documentation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Within the **Dxtr** library, such abstract complexes can be instanciated from a list of indices. These indices will form top level simplices of the complex.\n",
    "\n",
    "> **Note:** At instanciation, a name can be specified, but it is not mandatory."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr.complexes import AbstractSimplicialComplex\n",
    "\n",
    "top_simplex_indinces = [[0,1,2,3], [1,2,3,4], [4,5,6], [6,7]]\n",
    "\n",
    "asc = AbstractSimplicialComplex(top_simplex_indinces, name='Gil-Orestel')\n",
    "\n",
    "print(f'{asc} is an abstract simplicial complex of dim {asc.dim}.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** The dimension of an (abstract) simplicial complex corresponds to the \n",
    "topological dimension of its highest-degree simplex."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When instanciated, the `AbstractSimplicialComplex` class calls a \n",
    "`_build_topology()` *\"pseudo-private\"* method that is directly inspired by the \n",
    "work of Nathan Bell and Anil Hirani in the [**PyDEC** library](https://github.com/hirani/pydec)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `AbstractSimplicialComplex` class is constructed as a list of **modules** \n",
    "(`UserList[Module]`). A module corresponds to a list of all the simplices \n",
    "(`UserList[Simplex]`) with the same degree within the considered simplicial \n",
    "complex."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for k, splcs in enumerate(asc):\n",
    "    print(f'list of ({k})-simplices:')\n",
    "    for splx in splcs:\n",
    "        print(f'  - {splx.indices}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The number of simplices of each degree can be obtained either through the \n",
    "`shape` property of the complex or through the `size` property of each module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f'Number of simplices within {asc}: {asc.shape}.')\n",
    "print('This means that there are:')\n",
    "for k, mdl in enumerate(asc):\n",
    "    print(f'  - {mdl.size} ({k})-simplices.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Some useful properties:**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* A **pure** (abstract) simplicial complex is a complex where all top simplices \n",
    "have the same degree. The *\"purity\"* of a complex can be check with the `ispure` \n",
    "property:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if not asc.ispure: print(f'{asc} is not a pure complex.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** This notion of *\"purity\"* will be of importance latter to define `SimplicialManifold` objects."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* A **closed** (abstract) simplicial complex has no border (*i.e.* all its \n",
    "(n-1)-simplices are faces of exactly 2 n-simplices). The *\"closeness\"* of a \n",
    "complex can assessed through the `isclosed` property:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "if not asc.isclosed: print(f'{asc} do have some borders.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** If the considered complex is not closed, you can access its borders \n",
    "through the `border()` method. Please check the advanced tutorial in the \n",
    "**Dxtr in action** section for further informations."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simplicial Complexes\n",
    "\n",
    "When embedded into a real euclidean space, *i.e.* $\\mathbb{R}^n$, abstract \n",
    "simplicial complexes cease to be abstract and gain geometrical properties. Such \n",
    "*geometrical* simplicial complexes are represented within the **Dxtr** library \n",
    "by the `SimplicialComplex` class."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In addition to a list of indices, one needs to provide a list (or an array) of \n",
    "vertex position vectors in order to instanciate a `SimplicialComplex`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr import SimplicialComplex\n",
    "\n",
    "top_simplex_indinces = [[0,1,2,3], [1,2,3,4], [4,5,6], [6,7]]\n",
    "vertex_positions = [[0, 0, 0], [1, 0, 0], [0, 1, 0], [0, 0, 1], [1, 1, 1], \n",
    "                    [2, 1.5, 1], [1, 1.5, 1], [1, 1, 2]]\n",
    "\n",
    "cplx = SimplicialComplex(top_simplex_indinces, vertex_positions)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** Contrary to the `AbstractSimplicialComplex` class, when importing \n",
    "the `SimplicialComplex` one you do not need to fetch it in the `dxtr.complexes` \n",
    "sub-module, you can directly import it from the root of the package. This is \n",
    "also true for others *fundamental* classes such as `Simplex` or `Cochain`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first obvious benefit from this geometrical embedding is that you can now \n",
    "display this complex:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "visualize(cplx)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** Although one can instantiate a `SimplicialComplex` embedded within \n",
    "an arbitrary large euclidean space (*i.e.* $\\mathbb{R}^n$ with $n>3$); only the \n",
    "ones embedded in the *\"natural\"* 3D euclidean space can be visualized..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Vertices positons and k-volumes of all the simplices within a \n",
    "`SimplicialComplex` instance are obviously accessible. One can get it directly \n",
    "by parsing the simplices:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "idx, k = 4, 1\n",
    "\n",
    "print(f'Length of the {4}th ({k})-simplex: {cplx[k][idx].volume:.2e}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "But for practical reasons, the geometrical properties of all the simplices \n",
    "within a `Module` are also stored in arrays defined as attributes of this \n",
    "`Module`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "k = 2\n",
    "vols = cplx[k].volumes\n",
    "vtcs = cplx[k+1].vertices\n",
    "\n",
    "print(f'Volumes of all ({k})-simplices: \\n {vols} \\n')\n",
    "\n",
    "print(f'Vertex positions for each ({k+1})-simplex: \\n {vtcs}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Important remark:**\n",
    "\n",
    "The `SimplicialComplex` class directly inherits from the \n",
    "`AbstractSimplicialComplex` one. Consequently, all the topological methods and \n",
    "property of the latter are available to the former.\n",
    "\n",
    "As a matter of fact, if one instanciates a `SimplicialComplex` only with a list \n",
    "of top simplex indices, one would get an abstract complex. This is indeed the \n",
    "intended way to instanciate abstract complexes. The `AbstractSimplicialComplex` \n",
    "class is not really meant to be called directly. \n",
    "\n",
    "> **Note:** That is why it is not listed amongst the *\"fundamental\" objects* \n",
    "that can be called at the root of the package."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "indices = [[0,1,2], [1,2,3], [2,3,4]]\n",
    "abst_cplx = SimplicialComplex(indices, name='Iluvatar')\n",
    "\n",
    "if abst_cplx.isabstract: print(f'\\n{abst_cplx} is indeed abstract.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Reading & writing simplicial complexes:**\n",
    "\n",
    "`SimplicialComplex` objects can be instanciated directly from files written on \n",
    "disk in the `.ply` format. This can be achieved thanks to the class method \n",
    "`from_file()`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path_to_file = '../src/dxtr/utils/mesh_examples/sphere_small.ply'\n",
    "\n",
    "sphere = SimplicialComplex.from_file(path_to_file)\n",
    "\n",
    "visualize(sphere)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Likewise, a `to_file()` method can be called on any `SimplicialComplex` instance \n",
    "in order to save it on disk."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At this point it might be also relevant to mention that a series of pre-defined \n",
    "simplicial complexes can be found in the sub-module `dxtr.complexes.examples_complexes`:\n",
    "* `triangle`\n",
    "* `hexagon`\n",
    "* `tetrahedron`\n",
    "* `diamond`\n",
    "* `cube`\n",
    "* `icosahedron`\n",
    "* `triangular_grid`\n",
    "* `disk`\n",
    "* `sphere`\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr.complexes import cube\n",
    "\n",
    "cplx = cube()\n",
    "\n",
    "visualize(cplx)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Simplicial Manifolds\n",
    "\n",
    "As mentioned in the [DEC introduction](context_DEC.md#simplicial_manifold), a \n",
    "special type of simplicial complexes will be of particular interest to us: The \n",
    "ones that can locally approximate smooth manifolds. We refer to such simplicial \n",
    "complexes as **simplicial manifolds**. They are instanciated in the *Dxtr* \n",
    "library thanks to the `SimplicalManifold` class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr import SimplicialManifold\n",
    "\n",
    "ids = [[0,1,2], [1,2,3]]\n",
    "vcts = [[0, 0, 0], [1, 0, 0], [0, 1, 0], [1, 1, 0]]\n",
    "\n",
    "mfld = SimplicialManifold(ids, vcts)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For all the provided example structures mentioned above, one can instanciate a *\"manifold\"* version with the optional argument `manifold` set to `True`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr.complexes import icosahedron\n",
    "\n",
    "mfld = icosahedron(manifold=True)\n",
    "mfld.name = 'icosahedron'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The particularity of `SimplicialManifold` objects is that their structure enables the computation of their **dual cellular complex**, *c.f.* the [related paragraph](context_DEC.md#dual_cell_complex) in the DEC introduction. \n",
    "\n",
    "Dual cellular complexes rely on the circumcenters of the simplices of the **primal simplicial complex**. These circumcenters (`circumcenters`) are therefore a geometrical attribute specific to `SimplicialManifold` instances, such as the `covolumes`, *i.e.* the k-volumes of the dual k-cells that corresponds to polytopes between circumcenters. These two properties are stored in each module for optimal access during computation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "k = 1\n",
    "\n",
    "cctrs = mfld[k].circumcenters\n",
    "covols = mfld[k].covolumes\n",
    "\n",
    "for idx, (cctr, covol) in enumerate(zip(cctrs, covols)):\n",
    "    print(f'{idx+1}th ({k})-simplex in {mfld.name}:')\n",
    "    print(f'  - Circumcenters: {cctr}')\n",
    "    print(f'  - Covolumes: {covol:.2e} \\n')\n",
    "    if idx == 4: break"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dual complexes can be visualized, using the argument  `show` within the `visualize()` function. it can either be set to `dual` to specifically display the dual complex or `all` to display both complexes simultaneously:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "visualize(mfld, show='all')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** you can toggle on and off specific components of the complexes by clicking on the legend in the `plotly`-based visualization window."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As menstioned in the \n",
    "[inheritance graph](architecture_overview.md#complexes_module_orga) presented in \n",
    "the general overview of the library, the `SimplicialManifold` class inherits the \n",
    "topology-related attributes and properties of the `AbstractSimplicialComplex` \n",
    "class as well as the geometry-related ones of the `SimplicialComplex` one.\n",
    "\n",
    "However, in order to be promoted to `SimplicialManifold`, a simplicial complex \n",
    "must fullfil some requirements, see the \n",
    "[dedicated paragraph](context_DEC.md#simplicial_manifold) in the introductive \n",
    "section about DEC. In the **Dxtr** library, the validation of these requirements \n",
    "is achieved during instantiation by the `_isvalid_for_dualization()` function. \n",
    "In essene this function checks if the candidate complex is **pure** and that \n",
    "each of its (n-1)-simplices is the face of at most 2 (n)-simplices (n being \n",
    "the dimension of the complex).\n",
    "\n",
    "> **Note:** This last condition checks the fact that locally the complex is \n",
    "homeomorphic to a nD sphere."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr import SimplicialManifold\n",
    "\n",
    "ids = [[0,1,2], [1,2,3], [1,2,4]]\n",
    "vcts = [[0, 0, 0], [1, 0, 0], [0, 1, 0], [1, 1, 0], [.5, .5, 1]]\n",
    "cplx = SimplicialComplex(ids, vcts, name='cplx1')\n",
    "\n",
    "visualize(cplx, title=cplx.name)\n",
    "print(f'{cplx} is not homeomorphic to a 2D sphere.')\n",
    "\n",
    "mfld = SimplicialManifold(ids, vcts)\n",
    "\n",
    "print(mfld)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from typing import Optional\n",
    "\n",
    "class Mother:\n",
    "    \"\"\"A mother class\"\"\"\n",
    "    def __init__(self, lst:list[float]):\n",
    "        print('init Mother')\n",
    "        self._values = lst\n",
    "        \n",
    "    @property\n",
    "    def total_value(self):\n",
    "        return sum(self._values)\n",
    "\n",
    "class Daughter(Mother):\n",
    "    \"\"\"A daughter class inheriting from Mother.\n",
    "    Should only be instanciated IF a given criteria, depending on the \n",
    "    attributes and properties inherited from the mother class, is met.\n",
    "    \"\"\"\n",
    "    def __new__(cls, values) -> Optional[Daughter]:\n",
    "        # Create an instance of the Mother class\n",
    "        mother_instance = Mother.__new__(Mother, values)\n",
    "    \n",
    "        # Initialize the Mother instance\n",
    "        Mother.__init__(mother_instance, values)\n",
    "        \n",
    "        # Checks if the criteria is met\n",
    "        if not cls.check_criteria(mother_instance):\n",
    "            return None\n",
    "\n",
    "        return  super(Daughter, cls).__new__(cls)\n",
    "\n",
    "\n",
    "    def __init__(self, values):\n",
    "        # Initialize the Daughter instance\n",
    "        print('init Daughter')\n",
    "        super().__init__(values)\n",
    "        self._another_value = values[-1]\n",
    "\n",
    "\n",
    "    @staticmethod\n",
    "    def check_criteria(instance, threshold:float=10):\n",
    "        \"\"\"A criteria to meet to instantiate the class.\"\"\"\n",
    "        return instance.total_value > threshold\n",
    "\n",
    "\n",
    "# Usage\n",
    "daughter_instance = Daughter([1,2,3]) \n",
    "assert daughter_instance is None\n",
    "print('---')\n",
    "daughter_instance = Daughter([1,2,8]) \n",
    "assert isinstance(daughter_instance, Daughter)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Things to move to advanced tutorial"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Faces of simplices:**\n",
    "\n",
    "\n",
    "One important topological property of simplices is their recursive construction: \n",
    "A subset, *aka* a face, of a simplex, is also a simplex.\n",
    "In **Dxtr**, given two simplices, one can check if one is a face of the other \n",
    "with the `isfaceof()` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "splx1 = Simplex((2, 3, 7, 32))\n",
    "splx2 = Simplex((2, 7, 3))\n",
    "splx3 = Simplex((3, 32))\n",
    "splx4 = Simplex(())\n",
    "\n",
    "k2 = splx2.dim\n",
    "k3 = splx3.dim\n",
    "\n",
    "if splx2.isfaceof(splx1):\n",
    "    print(f'{splx2} is a {k2}-face of {splx1}\\n')\n",
    "\n",
    "if splx3.isfaceof(splx1):\n",
    "    print(f'{splx3} is a {k3}-face of {splx1}\\n')\n",
    "\n",
    "if not splx3.isfaceof(splx2):\n",
    "    print(f'but {splx3} is NOT a {k3}-face of {splx2}\\n')\n",
    "\n",
    "if splx1.isfaceof(splx1):\n",
    "    print(f'A simplex is a face of itself.\\n')\n",
    "\n",
    "if splx4.isfaceof(splx1) and splx4.isfaceof(splx2) and splx4.isfaceof(splx3):\n",
    "    print(f'The empty simplex is a face of all simplices.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Not sure I should put this here, maybe in an advanced tutorial on topological \n",
    "operations**\n",
    "\n",
    "If the considered complex is not closed, you can access its borders through the \n",
    "`border()` method. This method returns a dictionnary whose keys are the degrees \n",
    "of the simplices within the complex and the values correspond to lists of \n",
    "indices corresponding to the simplices forming the borders. For instance:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "border_indices = asc.border()\n",
    "\n",
    "print(f'Indices of the simplices on the border of {asc}: \\n {border_indices}')\n",
    "\n",
    "for k, ids in border_indices.items():\n",
    "    print(f'({k})-simplices on the border of {asc}:')\n",
    "    for idx in ids:\n",
    "        print(f'  - {asc[k][idx]}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The euler characteristics should also be mentioned in a topology-focused tutorial."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "dxtr",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.18"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
