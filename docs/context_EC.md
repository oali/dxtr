# Exterior Calculus

The theory of exterior (differential) calculus can be seen as an **algebraic organization of differential geometry**, initially proposed **by [Élie Cartan](https://en.wikipedia.org/wiki/Élie_Cartan)** at the turn of the XXth century.

[](){#dforms}
## Differential forms
The theory of exterior calculus is built around the concept of **(differential) k-form**; 
which corresponds to a skew-symmetric, fully covariant tensor field of rank k, 
defined within the cotangent bundle of a **n-dimensional manifold** (\(n\geq k\)), 
*i.e.*:
$$
\omega = \omega_{i\dots k}\boldsymbol{dx}^i\otimes\dots\otimes\boldsymbol{dx}^k
\quad \text{s.t.} \quad \omega_{p(i\dots k)} = (-1)^{\sigma(p)} \omega_{i\dots k},
$$
where \(p\) corresponds to any permutation of the k indices and 
\(\sigma(p) = \pm 1\) to its parity.

In this framework, linear forms — *aka* covectors — correspond to the specific 
case of 1-forms. Exterior calculus can therefore be considered as a 
generalization of vector calculus in arbitrary dimensions. 

Moreover, as covectors corresponds to linear forms acting on vectors in the 1D 
case; k-forms extend this interpretation to the k-dimensional case. 
They correspond to **k-linear operators** assigning scalar values to k-vectors:
$$
\begin{array}{crl}
\omega^{(k)} \colon 
& \mathcal{T}^k_{\mathcal{M}} &\to \mathbb{R} \\
& \boldsymbol{v}^{(k)} &\mapsto\omega^{(k)}\left(\boldsymbol{v}^{(k)}\right),
\end{array}
$$
where \(\boldsymbol{v}^{(k)}\) stands for a k-vector (1) defined in the kD extended tangent bundle 
{ .annotate}

1.  This notion will be explicited in a section below.

$$ \mathcal{T}^k_{\mathcal{M}} = \underbrace{\mathcal{T}_{\mathcal{M}}\otimes\dots\otimes\mathcal{T}_{\mathcal{M}}}_{\times k},$$ 
of the considered manifold \(\mathcal{M}\).

Given a nD manifold \(\mathcal{M}\), k-forms defined on it, k≤n, form a vector 
space, noted \(\Lambda^k(\mathcal{M})\) hereafter.

In particular, this allows to extends classic operators of *"vectorial"* 
differential geometry, such as gradient, divergence and curl to higher dimensions. 
This extension is made possible by the definition of a dedicated derivation 
operator: The **exterior derivative**, [see below](#exterior_derivative).

[](){#graded_structure}
!!! success "A graded algebraic structure"
    The generalization of vector calculus to higher dimensions is based on the algebraic structure of k-forms. Indeed, (k+l)-forms can be built from k-dimensional and l-dimensional ones using the **wedge product** and noted \(\wedge\):
    $$
    \omega^{(k+l)} = \alpha^{(k)}\wedge\beta^{(l)}.
    $$
    Equipped with this wedge product, all the k-forms defined on a n-dimensional manifold consitute a graded algebra, called the **exterior algebra**
    $$
    \Lambda(\mathcal{M})=\Lambda^0(\mathcal{M})\oplus\Lambda^1(\mathcal{M})\oplus
    \cdots\oplus\Lambda^n(\mathcal{M}).
    $$
    This graded algebra is of finite dimension, for the degree (k) of a k-form cannot exceed the dimension of the manifold it is defined upon, \(k\leq n\).

    Within this graded algebra, the set of k-forms defined on a nD manifold (for a given value of 0≤k≤n), corresponds to a \(\binom{n}{k}\)-dimensional vector space, where k-forms can be expanded over the canonical base forms:
    [](){#kform_expansion}
    $$
    \omega^{(k)} = 
    \omega_{i_1\dots i_k}\boldsymbol{dx}^{i_1}\wedge\dots\wedge\boldsymbol{dx}^{i_k},
    $$
    where the Einstein summation convention has been used for concision and runs over the permutations of k indices (\(i_1\dots i_k\)) over n.


## Exterior derivative [](){#exterior_derivative}
The exterior derivative is a linear operator that maps k-forms to (k+1)-forms. It is noted \(d\) and  extends the notion of differentiation to differential forms of arbitrary degrees.

This operator can be defined by a series of axioms it must follow:

=== "Match the usual derivative on 0-form"

    For a 0-form , *i.e.* a smooth function \(f\colon \mathcal{M}\to \mathbb{R}\), 
    it corresponds to the usual differential:
    $$
    df = \frac{\partial f}{\partial x^i}dx^i
    $$

=== "Linearity"

    It must be linear, *i.e.* given a k-form \(\alpha\) and an l-form \(\beta\):
    $$
    d(a\alpha + b\beta) = ad\alpha + bd\beta
    \quad\forall a,b\in\mathbb{R}.
    $$

=== "Liebniz rule"

    As differential forms as skew-symmetric, it must follow the **Liebniz rule:**
    $$
    d(\alpha\wedge\beta)
    = d\alpha\wedge\beta + (-1)^k\alpha\wedge d\beta
    \quad\forall (\alpha,\beta)\in\Lambda^k(\mathcal{M})\times\Lambda^l(\mathcal{M}).
    $$

=== "2-nilpotency"

    Finally it must be 2-nilpotent:
    $$
    d(d(\alpha)) = 0,\quad
    \forall \alpha\in\Lambda^k(\mathcal{M}).
    $$


Within a coordinate system defined on the considered manifold, it can be expressed as:
$$
\begin{array}{crl}
d\colon
& \Lambda^k &\to \Lambda^{k+1} \\
& \omega &\mapsto d\omega
\end{array}
\quad\text{s.t.}\quad
d\omega = \underset{I_k,i}\sum\frac{\partial \omega_{I_k}}{\partial x^i} dx^{I_k}\wedge dx^i,
$$

where \(I_k\) corresponds to a set of indices corresponding to k variables taken amongst the n ones needed to parametrize \(\mathcal{M}\). We sum over all the possible sets, *i.e.* \(\binomial{k,n}\), and \(i\) is an index runnning over all n variables.


!!! Success "Stokes-Cartan theorem"
    
    The cornerstone of the theory is the **Stokes-Cartan Theorem**, which binds together the three main ingredients of exterior calculus: The integral of a **k-form**, \(\omega^{(k)}\), over the boundary, \(\partial\sigma^{(k+1)}\), of some **(k+1)-dimensional oriented sub-manifold**, \(\sigma^{(k+1)}\), equals the integral of its **exterior derivative,** \(d\omega^{(k)}\), over the whole sub-manifold:
    $$
    \int_{\sigma^{(k+1)}}d\omega^{(k)} = \int_{\partial\sigma^{(k+1)}}\omega^{(k)}.
    $$
    This is a very powerful results for it provides a unifying view encompassing some major theorems in differential calculus and geometry, *e.g.* the second fundamental theorem of calculus, the Kelvin-Stokes theorem, Ostrogradsky's theorem, Green's theorem.

[](){#natural_pairing}

- [x] Algebraic interpretation
    Given a nD oriented manifold, k-forms defined on it constitute a vector space, dual to the one formed by k-vectors. Consequently the **Riesz representation theorem** ensures that the integration of k-forms over k-vectors defines an inner product, often refered to as their ***"natural pairing":***
    $$
    \begin{array}{c}
    \forall \sigma,\omega \in \mathcal{T}^{k}(\mathcal{M})\times\Lambda^{k}(\mathcal{M}),\; 
    \\ \\
    \omega(\sigma)=\langle \sigma, \omega\rangle = \int_\sigma\omega,
    \end{array}
    $$
    for it is obviously linear in both \(\sigma\) & \(\omega\).
    
    Considering a (k+1)-vector \(\sigma\) and a k-form \(\omega\), we can rewrite the Stokes-Cartan theorem as:
    $$
    \langle \sigma, d\omega\rangle 
    = \langle \partial\sigma, \omega\rangle 
    % \quad\Longleftrightarrow\quad
    % d\omega(\sigma) = \omega(\partial\sigma).
    $$
    This algebraic interpretation is another pivotal concept in exterior calculus and *DEC* for it states that **the exterior derivative is the adjoint operator of the boundary operator.** 

## Hodge duality [](){#hodge_duality}
The finite dimension of the graded algebra of k-forms defined on a nD manifold 
 \(\mathcal{M}\) leads to another key concept in exterior calculus: **Hodge duality**. 
Given a k-form, \(\alpha \in \Lambda^k(\mathcal{M})\), acting on the k-dimensional subset \(\Omega\subset\mathcal{M}\); we can construct a **dual (n-k)-form**, noted \(*\alpha\) here after, defined on 
\(\star\Omega\) (1), verifying:
{ .annotate}

1.  The orthogonal complement of the initial k-dim subset.

$$
\int_{\star\Omega}*\alpha = \int_{\Omega}\alpha.
\tag{1}
$$
The **isomorphism** that maps k-forms to (n-k)-ones is called the **Hodge star,** 
and is noted \(*(\cdot)\): 
$$
\begin{array}{crl}
*\colon 
& \Lambda^{k}(\mathcal{M}) \to & \Lambda^{(n-k)}(\mathcal{M}) \\
& \alpha \mapsto & *\alpha.
\end{array}
$$

!!! example 
    One ubiquituous application of this concept is the definition of the 
    vectorial product in the usual 3D euclidean space. Indeed, when we write
    $$\boldsymbol{a}\wedge\boldsymbol{b} = \boldsymbol{c},$$
    we implicitly:

    * Assimilate vectors and covectors, which is fine in euclidean spaces.
    * Associate to a 2-form, the *lhs* in the above formulae, defined within the 
    plane spanned by the two (co)vectors \(\boldsymbol{a}\) and \(\boldsymbol{b}\) 
    to a 1-form, the *rhs*, spanning the 1D space orthogonal to the 
    forementioned plane.

    Practically, if we consider, still in 3D, the basic forms: \(dx, dy, dz\), we have: 
    $$
    \begin{array}{cc}
    *dx = dy\wedge dz,&  *\left(dx\wedge dy\right) = dz \\
    *dy = dz\wedge dx,&  *\left(dy\wedge dz\right) = dx \\
    *dz = dx\wedge dy,&  *\left(dz\wedge dx\right) = dy \\
    *\left(dx\wedge dy\wedge dz\right) = 1 
    \end{array}
    $$

One noteworthy property of the Hodge operator is that applied twice to a k-form 
\(\omega\) defined on a nD manifold \(\mathcal{M}\), it yields back the k-form 
up to a sign:
$$
\forall\omega\in\Lambda^k(\mathcal{M}),\:  **\omega = (-1)^{k(n-k)}\omega.
$$


## Musical isomorphisms
As mentionned [above](#natural_pairing), k-forms and k-vectors defined on a nD manifold \(\mathcal{M}\) constitute dual vector spaces. The musical isomorphisms, "sharp"  and "flat" (noted respectively \(\cdot^{\sharp}, \cdot^{\flat}\)), enable to pass from one to the other:
$$
\mathbb{V}^k\left(\mathcal{M}\right)
\underset{\sharp}{\overset{\flat}{\rightleftarrows}}
\Lambda^k\left(\mathcal{M}\right)
$$

In the usual index-based formalization of differential geometry, given a manifold \(\left(\mathcal{M},\boldsymbol{g}\right)\), they simply correspond to a contraction with the metric tensor \(\boldsymbol{g}\) (1):
{ .annotate}

1.  The origin of the poetic name *"musical"* isomorphisms is apparent when looking at the formulae below: As sharp and flat respectively raise and lower tones in music their algebraic counterparts raise and lower indices, transforming covectors (*aka* 1-forms) into vectors and vice-versa.

$$
\begin{cases}
\forall \boldsymbol{v}\in\mathbb{V}\left(\mathcal{M}\right), \boldsymbol{v}=v^i\boldsymbol{e}_i &\Rightarrow 
\boldsymbol{v}^{\flat} = v_i\boldsymbol{e}^i,\; \text{with}\; v_i = g_{ij}v^j,\\ 
\forall \boldsymbol{\omega}\in\Lambda\left(\mathcal{M}\right), \boldsymbol{\omega}=\omega_i\boldsymbol{e}^i &\Rightarrow 
\boldsymbol{\omega}^{\sharp} = \omega^i\boldsymbol{e}_i,\; \text{with}\; \omega^i = g^{ij}\omega_j,\\ 
\end{cases}
$$
where \(g_{ij}, g^{ij}\) (resp.) are the metric coefficients in there twice covariant (contravariant) version.
---

In order to truly harnest this powerful tool in numerical simulations, a 
discrete version of this theory is needed. This is what the next section is 
about.
