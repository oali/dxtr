# The Dxtr library in broad strokes

Following **Dieter Rams** [principles for good Design](https://designmuseum.org/discover-design/all-stories/what-is-good-design-a-quick-look-at-dieter-rams-ten-principles) (the 4th & 10th especially), and drawing inspiration from the [*FEniCS* library](https://fenicsproject.org), we tried to make our implementation of *DEC* as close as possible to the [underlying mathematical concepts](./context_DEC.md).

In the following we will detail a bit the architecture of the library and 
present its main modules, classes and functions.

## Architecture

!!! info "**Fig. 1: Genreal structure of the Dxtr library:** "
    ```py
    src/dxtr
        ├── cochains #(2)
        │   ├── cochain
        │   ├── example_cochains
        │   └── whitneymap
        ├── complexes #(1)
        │   ├── abstractsimplicialcomplex
        │   ├── example_complexes
        │   ├── simplex
        │   ├── simplicialcomplex
        │   ├── simplicialmanifold
        │   └── Module
        ├── io
        │   ├── read
        │   └── write
        ├── math
        │   ├── geometry
        │   └── topology
        ├── operators #(3)
        │   ├── differential
        │   ├── curvature
        │   ├── hodge
        │   └── musical
        ├── utils
        │   ├── logging
        │   ├── quality_control
        │   ├── typecheck
        │   └── wrappers
        └── visu
            ├── visu_plotly
            ├── visu_pyvista
            └── visualize
    ```
    
    1.  See the [content description](#complexes_intro) of this module below and the  [dedicated section](./complexes.ipynb) hereafter.
    2.  See the [content description](#cochains_intro) of this module below and the  [dedicated section](./cochains.ipynb) hereafter.
    3.  See the [content description](#operators_intro) of this module below and the  [dedicated section](./operators.ipynb) hereafter.
    
    The library has been organized so that its main modules echoes the most important concepts in *DEC.*


Let's review quickly the organization of the three main modules.

[](){#complexes_intro}

- [x] **Complexes**

    The `complexes` module encapsulates classes needed to implement geometrical entities such as  *simplices* (`Simplex`) or *simplicial complexes* (`SimplicialComplex`). These classes are related through inheritance and aggregation as depicted through the dependency graph below.

    > :point_right: Basics use-case examples of `SimplicialComplex` manipulations can be found in the [dedicated notebook.](complexes.ipynb)

    !!! info "Fig.2: **`Complexes` Module hierarchy graph.**"
        ```mermaid
        flowchart BT

        subgraph *math* Module
            Top
            Geo
            end

        Asc -...-> Top[[*math.topology*]]
        Sm & Sc -....-> Geo[[*math.geometry*]]

        Asc(AbstractSimplicalComplex) --o mdl(Module) --o Splx(Simplex)
        Sc(SimplicialComplex) --> Asc
        Sm(SimplicialManifold) --> Sc
        ```
        The main classes & their relationships. plain arrows signal inheritance, dotted arrows dependency and plain lines ended with dots composition.

[](){#cochains_intro}

- [x] **Cochains**

    The `cochains` module is dedicated to the implementation of discrete differential forms, *aka cochains.* 
    
    Beside the `Cochain` sub-module, which contains the eponymous class, this module also features a `WhitneyMap` class. Given a `SimplicialManifold` instance, this class enables the computation of Whitney k-maps on it, *i.e.* the interpolation of k-cochains on top-level simplices. Such interpolation maps are central in our implementation of the `sharp()` operator, see [musical isomorphisms](context_DEC.md#musical_isomorphisms). This is also worth noticing that Whitney maps are central in the defintion of a a proper **Finite Element Method** adapted to differential form. Such a *FEM* is for now not available within the current version (1.0.0) of the **Dxtr** library, but the basic ingredients are here.

    > :point_right: Check the [dedicated notebook](cochains.ipynb) for some examples of cochain basic manipulations.

[](){#operators_intro}

- [x] **Operators**

    The `operators` module gathered functions acting on *cochains*. It is subdivied into various thematics:

    === "differential"
        The `differential` sub-module contains the implementation of the **exterior derivative**(`exterior_derivative`) and all the subsequent differential operators, *e.g.* the **Laplacian** (`laplacian()`) or *divergence* (`divergence()`)
    === "hodge"
        The `hodge` sub-module hosts the *hodge star* (`hodge_star()`) operator this enables to transform a **primal** cochain into a **dual** one and vice-versa.
    === "musical"
        The `musical` sub-module gathers the *flat* (`flat()`) & *sharp* (`sharp()`) musical isomorphisms. These operators purpose is to convert (discrete) vector fields into cochains (`flat()`) and conversly cochains into vector fields (`sharp()`).
    === "wedge"
        This sub-module encompasses the implementation of the **wedge product** (`wedge()`) that enables to generate a (k+l)-cochain from two cochains of degrees k and l (1).
        { .annotate}

        1.  In the current version (V 1.0.0), only primal cochains can be *"wedged"* together. The extension toward dual cochains is currently being developped.
        
    === "curvature"
        Implementations of the *Gaussian* & *mean* curvatures (resp. `gaussian_curvature()` & `mean_curvature()`) can be found in the `curvature` submodule(1).
        { .annotate}

        1.  This curvature module is currently (V 1.0.0) a work in progress.


    > :point_right: Some examples of operators acting on cochain can be seen in the [dedicated notebook](operators.ipynb).

    !!! info "Fig.3: Differential operator dependency graph"
        ```mermaid
        flowchart BT
            
            subgraph *differential*
                ED(Exterior derivative)
                CD(Codifferential)
                Lpc(Laplacian)
                Div(Divergence)
                Lie(Lie derivative):::Soon
                end
            
            subgraph *musical*
                Flt(Flat)
                Shp(Sharp)
                end
            subgraph *hodge*
                HS(Hodge star)
                end
            
            subgraph *wedge*
                Wdg(Wedge product):::Soon
                Ctr(Contraction):::Soon
                end

        CD --> ED & HS  
        Lpc ---> ED & CD
        Div ---> CD & HS
        Ctr ---> HS & Flt & Wdg 
        Lie ----> ED & Ctr


        classDef Soon stroke-dasharray: 5 5
        ```
        The gray boxes corresponds to various sub-modules. The boxes with dashed contour correspond to operators that are not yet fully implemented. The arrows points from an operator toward the ones its definition relies on.

    
- [x] **Additional modules**

    To support and complement these three *core* modules, you will also find:

    === "math"
        This module that contains functions & algorithms to perform all the needed computations required by the various data structures & methods.
    === "io"
        This module enables the user to read & write files on disk, mostly in the `.ply` & `.vtk` format.
    === "visu"    
        This module contains functions to visualize **simplicial complexes** & **cochains**. It relies on two visualization libraries: `plotly` and `pyvista` (optional). 
    === "utils"
        This is where housekeeping data structures, methods and decorators are centralized.

    !!! info "Fig.4: Dxtr submodule dependency graph"
        ```mermaid

        flowchart RL

        Op(operators) --> Cp(complexes) & Co(cochains) & Mat(math)
        Vis[visu] --> Cp(complexes) & Co(cochains) --> Mat(math) & Io(io) 
        Uti(utils)
        ```
        The arrows show the dependency relationship between the various modules of the library.   
        Note: No arrow points toward *utils* for clarity reason, but all other modules rely on it however. 

---

## External dependencies

The biggest external dependencies of the library come from its algebraic nature. 
We implemented a lot of attributes as array to gain in computing time and 
therefore lie heavily on the `numpy` library. Likewise, a lot of matrices, used 
to implement the chain complex notably, have been implemented in a sparse 
format, making use of the `sparse` module from the `scipy` library. Note that in 
order to efficiently implemented the exterior product (`wedge()` operator) we 
needed a sparse implementation of putatively large 3rd order tensors. To that 
end, we made use of the `COO` class from the `sparse` library, adding it to the 
list of external dependencies.


- [x] `numpy`: Used ubiquitously throughout the whole library.
- [x] `scipy`: Mainly the `sparse` module to implement big matrices such as the ones encountered in the chain complex implementation.
- [x] `sparse`: Only needed in the `wedge()` operator to efficiently handle putatively large 3rd order tensors.
- [x] `plotly`: A visualization library used to draw simplicial complexes.
- [x] `pyvista`: A visualization library specialized in mesh representation. Used to visualize `Cochain` instances. We also make use of its `UGrid` data structure & `.vtk` file format to save on disk `SimplicialComplex` and `Cochain` instances.

---

## Coding style

Throughout the **Dxtr** library, we manipulate a limited number of objects and concepts. 
In order to ligthen the code, we chose to use abreviations and aliases to refer 
to the most commonly used ones. To keep the code as self-explanable as possible 
we tried to say consistant and always used the same abreviation for a given concept.

=== "Structures, objects, classes"

    | Abreviation | Definition |
    |--: | :-- |
    | `splx/splcs` | instance of `Simplex` or `Iterable[Simplex]` |
    | `cplx` | instance of `SimplicialComplex` or `AbstractSimplicialComplex` |
    |`mfld` | instance of`SimplicialManifold`|
    |`mdl`| instance of `Module`|
    |`cchn`| instance of `Cochain`|
    | `bld`| blade, *i.e.* simple k-vector|
    |`pts` | instance of (N,3)-shaped `numpy.ndarray[float]` containing N position vectors in 3D|

=== "Indices, numbers"

    | Abreviation | Definition |
    |--: | :-- |
    | `idx/sid` | instance of `int`, index of a simplex in a Module.|
    |`ids/sids` | instance of `Iterable[int]`, multiple indices of simplex in a Module|
    |`fid/fids` | instance of `int|Iterable[int]`, index (list of indices) of the face(s) of a simplex|
    |`cfid/cfids` | instance of `int|Iterable[int]`, index (list of indices) of the coface(s) of a simplex|
    |`n` | := `cplx.dim`, instance of `int`, the topological dimension of a simplical complex/manifold|
    |`k` | := `splx.dim` or `cchn.dim`, instance of `int`, the topological dimension of a simplex or a cochain|
    |`D` | := `cplx.emd_dim`, instance of `int`, the geometrical dimension of the embedding|
    |`N/Nk/nbr` | := `mdl.size` or `cplx.shape[i]`, instance of `int`, the number of simplices within an ensemble|

=== "Geometry"
    | Abreviation | Definition |
    |--: | :-- |
    | `vol/vols` | :=`splx.volume` or `cplx[k].volumes`, instance of `float|Iterable[float]`, The k-volume(s) of k-simplex(ices)|
    |`covol/covols` | :=`splx.volume` or `cplx[k].volumes`, instance of `float|Iterable[float]`, The k-volume(s) of k-simplex(ices)|
    |`kg` | := `GaussianCurvature`, may refer to the computed values or the operator|

=== "Differential operators"
    Within the `operators.differential` module we defined aliases for the main differential operators that can be applied to a `Cochain` instance:

    | Abreviation | Definition |
    |--: | :-- |
    | `d()`| `exterior_derivative()`|
    | `delta()`| `codifferential()`|
    | `L()`| `laplacian()`|


!!! tip "*Pseudo* private functions"
    You may find within the various sub-modules of the Dxtr library, functions and methods prefixed by `_`. Those are considered *private* and are not meant to be used outside the class or module they belong to. You will not find them in the auto-generated documentation for they are considered as *inner cogs* in the **Dxtr** machinery. However, they do have a *doc block* capsule giving some information on they usage, but you will have to inspect the code itself to dig them out... :smiley: 

---

In the next sections, we briefly introduce the main modules and classes of the 
library. For technical details on specific methods or attributes, please refer 
to the [API Reference guide](assets/_reference/API.md).
