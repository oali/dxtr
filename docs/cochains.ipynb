{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Cochains\n",
    "\n",
    "As mentioned in the [*DEC* introduction](../context_DEC#cochains), **Cochains** \n",
    "represent the discrete version of differencial forms. Given a nD simplicial \n",
    "complex, a k-cochain (0≤k≤n) can be depicted as a series of values corresponding \n",
    "to its evaluation over all the k-simplices of the complex, see \n",
    "[formula](../context_DEC#derham_maps)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Within the **Dxtr** library, k-cochains are implemented through the `Cochain` \n",
    "class, stored within the `Cochains` module. As it is a fundemental object for \n",
    "the library, it can directly be imported from its root.\n",
    "\n",
    "\n",
    "In order to instanciate a `Cochain` one needs to provide **three mandatory \n",
    "arguments:**\n",
    "* `complex`: The `SimplicialComplex` instance upon which the cochain will be \n",
    "defined.\n",
    "* `dim`: The topological dimension of the desired cochain.\n",
    "* `values`:  The values of the cochain on each (`dim`)-simplex of the `complex`."
   ]
  },
  {
   "metadata": {},
   "cell_type": "code",
   "outputs": [],
   "execution_count": null,
   "source": [
    "from dxtr import Cochain\n",
    "from dxtr.complexes import disk\n",
    "\n",
    "cplx = disk() # The complex on which the cochain will be defined.\n",
    "\n",
    "k = 2 # The chosen topological dimension.\n",
    "Nk = cplx[k].size # The number of (k)-simplices in the chosen complex.\n",
    "values = [v for v in range(Nk)] # A series of values, one for each (k)-simplex.\n",
    "\n",
    "cchn = Cochain(cplx, k, values, name='my first cochain')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The three arguments used to instanciate the cochain becomes properties that can \n",
    "be retrieved as properties *a posteriori:*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(f'The values of {cchn.name}, a ({cchn.dim})-cochain defined on \\n{cchn.complex}, are: \\n {cchn.values}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Like, simplicial complexes, cochains can be visualized in **Dxtr**. However, \n",
    "when provided with a `Cochain` instance, the `visualize()` function is using \n",
    "calling the **pyvista** library, not the **plotly** one as it is the case by default \n",
    "for `SimplicialComplex` instances."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr.visu import visualize\n",
    "\n",
    "visualize(cchn)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** *Why using two different libraries for visualization?* \n",
    ">\n",
    "> Well, because both libraries have specific features that can be of interest to \n",
    "study simplicial complexes and cochain: As you can see on figures in the \n",
    "[complexes section](../complexes), the **plotly** library enables information \n",
    "display when overing the various elements of the displayed complex. This can be \n",
    "very useful for careful analysis of the complexes and their structures. Such a \n",
    "feature is not directly available out-of-the-box in the **pyvista** library (at \n",
    "least to our knowledge). However, the **pyvista** library excels at handleling \n",
    "large meshes and displaying properties computed on them. This makes it ideal for \n",
    "visualizing cochains and their dynamics."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cochain algebraic manipulations\n",
    "As the ultimate goal of the **Dxtr** library is to perform calculus on cochains, \n",
    "we implemented basic algebraic operators directly within the `Cochain` class by \n",
    "supercharging classic dunder methods:\n",
    "\n",
    "* `__add__()` and `__sub__()` enable respectively the addition and substractions \n",
    "of two `Cochain` instances by simply supercharging the usual `+` `-`  operators[^1].\n",
    "* `__mul__()` and `__rmul__()` supercharge the `*` operator[^1] and enable to \n",
    "multiply `Cochain` instances by scalar values, arrays or even by another `Cochain` \n",
    "instance.\n",
    "* Finally, `__truediv__()` supercharges the `/` operator[^1]  and enables the \n",
    "division on the right by scalar or array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "k = 2\n",
    "Nk = cplx[k].size \n",
    "\n",
    "v1, v2, v3 = 5, 2, 28\n",
    "vals_1 = [v1 for _ in range(Nk)]\n",
    "vals_2 = [v2 for _ in range(Nk)]\n",
    "vals_3 = [v3 for _ in range(Nk)]\n",
    "\n",
    "c1 = Cochain(cplx, k, vals_1)\n",
    "c2 = Cochain(cplx, k, vals_2)\n",
    "c3 = Cochain(cplx, k, vals_3)\n",
    "\n",
    "c4 = 2*c1 + c2*c2 - c3/2.\n",
    "\n",
    "if (c4.values == 0).all: print('One can do arithmetics with cochains !')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Thanks to the **pyvista** library, custom visualization can easily be generated \n",
    "to investigate cochain behaviors:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyvista as pv\n",
    "\n",
    "fig = pv.Plotter(shape=(2,2), border=False)\n",
    "\n",
    "for i, c in enumerate([c1, c2, c3, c4]):\n",
    "    \n",
    "    fig.subplot(i//2, i%2)\n",
    "    \n",
    "    params = {'title': f'2-cochain of val={c.values[0]}',\n",
    "              'show_colorbar': True,\n",
    "              'color_range': (0, 20)}\n",
    "    \n",
    "    visualize(c, fig=fig, display=False, layout_parameters=params)\n",
    "\n",
    "fig.link_views()\n",
    "fig.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dual cochains\n",
    "Cochains can either be associated with simplices of *(primal)* simplicial complex \n",
    "or with the cells of the corresponding *dual* cellular complex.\n",
    "\n",
    "**TODO:** Finish this section on duality..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## Vector-valued cochains & vector fields\n",
    "The values of cochain are not necessary scalars, they can also be defined as \n",
    "vectors[^2]. \n",
    "In this case, the corresponding cochain is said to be vector-valued. Such cochains \n",
    "can be implemented in **Dxtr**, by provinding for instance an array of `ndim` > 1. \n",
    "In this case, the properties `isvectorvalued` and `shape` will return respectively \n",
    "`True` and the shape of the array `numpy.asarray(Cochain.values)`.\n",
    "\n",
    "#### Vector fields\n",
    "Discrete vector fields can be handled in **Dxtr**. As such vector fields \n",
    "are definde as a series of vectors defined on a series of positions, it is natural \n",
    "to implement them as vector-valued 0-cochains, defined on vertices.  In most \n",
    "cases, we consider **dual vector-valued 0-cochains** for such cochains are \n",
    "defined at the circumcenters of the highest-degree simplices.\n",
    "\n",
    "[^1]: It also works for the addition and substraction assignment operators `+=`, \n",
    "`-=`, `*=` & `/=`.\n",
    "[^2]: Even higher order tensors, but so that this working case has not been explored.\n",
    "\n",
    "---\n",
    "Now that the `Cochain` data structure has been introduce, we can look at the various \n",
    "operators working on it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "indices =[[1,3,4], [3,4,5]]\n",
    "\n",
    "from dxtr import SimplicialComplex\n",
    "\n",
    "sc = SimplicialComplex(indices)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "dxtr",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.18"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
