{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Operators\n",
    "\n",
    "In this section we briefly introduce the ***\"fundamental\"* operators**  implemented within the **Dxtr** library: \n",
    "* The exterior derivative.\n",
    "* The Hodge star.\n",
    "* The musical isomorphisms.\n",
    "* The wedge product.\n",
    "\n",
    "We call *\"operators\"* functions that take as inputs `Cochain` instances and return new ones. These functions are gathered in the module `operators` and organized in thematic sub-modules. Let's briefly introduce each one of these sub-modules."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "vscode": {
     "languageId": "plaintext"
    }
   },
   "source": [
    "## The exterior derivative\n",
    "The exterior derivative is implemented by the `exterior_derivative()` function, located in the `dxtr.operators.differential` sub-module. This operator is the fundamental building block upon which all differential operators of the library depend."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "vscode": {
     "languageId": "plaintext"
    }
   },
   "source": [
    "> **Note:** For conviniency, we also defined an inituite and shorter alias to invoke the exterior derivative: `d()`, both functions can directly be called from the `dxtr.operators` module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr import Cochain\n",
    "from dxtr.cochains import random_cochain\n",
    "from dxtr.operators import exterior_derivative, d\n",
    "\n",
    "c = random_cochain('icosa') # Let's consider a random cochain defined on a icosahedral simplicial complex as example.\n",
    "\n",
    "ext_deri_c = exterior_derivative(c)\n",
    "\n",
    "print('Congratulations !')\n",
    "print(f'  * You have just computed the {ext_deri_c}')\n",
    "if isinstance(ext_deri_c, Cochain) & (ext_deri_c.complex == c.complex):\n",
    "    print(f'  * The exterior derivative of a {c.dim}D cochain is a {ext_deri_c.dim}D one, defined on the same complex.')\n",
    "\n",
    "dc = d(c)\n",
    "if (dc.values == ext_deri_c.values).all():\n",
    "    print('  * The alias `d()` yields the same values as the function `exterior_derivative()`.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As initially proposed by M. Desbrun, A. Hirani and co-workers — see refs mentioned [here](context.md) —  the discrete exterior derivative implementation relies on the [**chain/cochain complexes**](context_DEC.md#cochain_complex) connecting the consecutive modules of a simplicial complex.\n",
    "Technically, this means that given a k-cochain defined on a n-simplicial complex, its exterior derivative corresponds to a (k+1)-cochain whose values have been computed thanks to the incidence matrix between k and k+1 simplices:\n",
    "\n",
    "> **Note:** In **Dxtr** the incidence matrices can be obtained through the `boundary` and `coboundary` properties of a given module. The `boundary` of a k-module yields the *downward* incidence, *i.e.* k -> k-1; while the `coboundary` yields the *upward* incidence, k -> k+1. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c  = random_cochain('icosa')\n",
    "\n",
    "incicence_matrix = c.complex[c.dim].coboundary\n",
    "\n",
    "if (d(c).values == incicence_matrix @ c.values).all():\n",
    "    print(f'The exterior derivative of our {c.name} is properly computed.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An fundamental property of the exterior derivative is its [**2-nilpotency**](context_DEC.md#boundary_operator), this property is by construction verified within our implementation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for k in range(3):\n",
    "    c = random_cochain('icosa', dim=k)\n",
    "    assert (d(d(c)).values == 0).all()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another property to bear in mind (at play in the previous cell): The exterior derivative of a cochain defined on the top-simplices of a complex will always be null:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c = random_cochain('icosa', dim=2)\n",
    "dc = d(c)\n",
    "\n",
    "if c.dim == c.complex.dim:\n",
    "    assert (dc.values == 0).all()\n",
    "    print(dc.dim)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The exterior derivative can be computed for **primal** and **dual** cochains alike:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c = random_cochain('icosa', dual=True)\n",
    "dc = d(c)\n",
    "if dc.isdual:\n",
    "    print(f'The exterior derivative of a dual {c.dim}-cochain is a dual {dc.dim}-cochain.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Being a purely topological operator, the exterior derivative can be applied on cochains defined on **abstract simplicial complexes:**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr import SimplicialComplex\n",
    "\n",
    "indices = [[0,1,2,3,4], [1,2,3,4,5], [4,5,6], [6,7]]\n",
    "asc = SimplicialComplex(indices)\n",
    "\n",
    "if asc.isabstract:\n",
    "    \n",
    "    # Let's define a primal 1D cochain on this abstract complex...\n",
    "    k = 1\n",
    "    Nk = asc[k].size\n",
    "    c = Cochain(asc, k, [1 for i in range(Nk)])\n",
    "\n",
    "    # ...and compute its exterior derivative\n",
    "    dc = d(c)\n",
    "    if dc.complex.isabstract:\n",
    "        print('We computed the exterior derivative of a cochain defined on an abstrac complex.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's also mention that the exterior derivative can be applied to **vector-valued** cochains.\n",
    "\n",
    "**BUT:** This feature is still experimental (in **Dxtr** V 1.0.0) and should be used with caution, especially in the case of curved complexes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Hodge star\n",
    "\n",
    "As mentioned in the [introduction section](context_DEC.md#hodge_star_operator), the discrete `hodge_star()` operator maps primal k-cochains with dual (n-k)-cochains. This operator, tightly related to the geometry of the supporting simplicial complex, combines with the `exterior_derivative()` in order to implement all others differential operators. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When applied to a *primal* k-cochain of unit values, the `hodge_star()` should return a  *dual* (n-k)-cochain whose values correspond to the ratios of the volumes of the dual (n-k)-cells by the volumes of the k-simplices:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr.cochains import unit_cochain\n",
    "from dxtr.operators import hodge_star\n",
    "\n",
    "for k in range(3):\n",
    "    cchn = unit_cochain(dim=k, manifold=True)\n",
    "    mfld = cchn.complex\n",
    "    \n",
    "    cchn_2 = hodge_star(cchn)\n",
    "    \n",
    "    kvols, kcovols = mfld[k].volumes, mfld[k].covolumes\n",
    "    \n",
    "    s = -1 if k==2 else 1 # See note below.\n",
    "    has_proper_values = (cchn_2.values == s * kcovols/kvols).all()\n",
    "    has_proper_dim =  cchn_2.dim == mfld.dim - cchn.dim\n",
    "    \n",
    "    if cchn_2.isdual & has_proper_values & has_proper_dim:\n",
    "        print(f'The Hodge star on {k}-cochains is properly computed')\n",
    "    else:\n",
    "        print(cchn_2.values)\n",
    "        print( kcovols/kvols)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** In the previous cell, we see that the extected values for dual 0-cochains, computed throught the Hodge star applied to primal 2-cochains, are negative. This is due to a sign correction depending on the dimensionality of the simplicial complex to account for the intrinsic orientation of its top-simplices. :point_right: see A. Hirani's PhD manuscript, remark 4.1.2. on page 41."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When applied twice, the `hodge_star()` should yield back the identity map, with a sign correction depending on the topological dimensions of the considered cochain (`k` below) and simplicial complex (`n` below):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "for k in range(3):\n",
    "    cchn = random_cochain(dim=k, manifold=True)\n",
    "    cchn_bis = hodge_star(hodge_star(cchn))\n",
    "    \n",
    "    n = cchn.complex.dim\n",
    "    s = (-1)**(k*(n-k))\n",
    "    \n",
    "    try:\n",
    "        np.testing.assert_array_almost_equal(cchn.values, s*cchn_bis.values)\n",
    "        print(f'it works for {k}-cochains.')\n",
    "    except:\n",
    "        print(f'it does not work {k}-cochains.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Musical isomorphisms\n",
    "\n",
    "Musical isomorphisms provide maps from vector fields to 1-cochains (`flat()`) and from 1-cochains to vector fields (`sharp()`)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's first define a simple vector field, tangent to the icosahedral simplicial complex:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr.cochains import normal_vector_field\n",
    "\n",
    "normals = normal_vector_field('icosa')\n",
    "nrl, mfld =  normals.values, normals.complex\n",
    "\n",
    "ez = np.repeat([[0,0,1]], nrl.shape[0], axis=0)\n",
    "\n",
    "vct = Cochain(mfld, dim=0, dual=True,\n",
    "              values=np.cross(ez, nrl, axis=-1),\n",
    "              name='e_theta')\n",
    "\n",
    "if vct.isvectorvalued & vct.isdual & (vct.dim==0):\n",
    "    print(f'{vct.name} is a discrete vector field on a {mfld.name}.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's now compute its dedicated discrete 1-form:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr.operators import flat\n",
    "\n",
    "flat_vct = flat(vct, name='Our first flatten vector field') \n",
    "\n",
    "if isinstance(flat_vct, Cochain) & (not flat_vct.isvectorvalued):\n",
    "    dual = 'dual' if flat_vct.isdual else 'primal'\n",
    "    k = flat_vct.dim\n",
    "\n",
    "    print(f'{flat_vct.name} is a {dual}, {k}D, scalar-valued cochain.')\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the cell above, we see that by default when a vector field is flatten, the returned `Cochain` is a **primal** one. \n",
    "\n",
    "We can, however, require a dual `Cochain` to be output:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dual_flat_vct = flat(vct, dual=True, name='Our second flatten vector field')\n",
    "\n",
    "if isinstance(dual_flat_vct, Cochain) & (not dual_flat_vct.isvectorvalued):\n",
    "    dual = 'dual' if dual_flat_vct.isdual else 'primal'\n",
    "    k = dual_flat_vct.dim\n",
    "\n",
    "    print(f'This time, {dual_flat_vct.name} is a {dual}, {k}D, scalar-valued cochain.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Thanks to the `visualize()` function from the `dxtr.visu` module, we can display these various objects:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pyvista as pv\n",
    "from dxtr.visu import visualize\n",
    "\n",
    "fig = pv.Plotter(shape=(1,3), border=False)\n",
    "\n",
    "fig.subplot(0,0)\n",
    "visualize(vct, fig=fig, display=False, \n",
    "          layout_parameters={'title':'Initial vector field'})\n",
    "\n",
    "fig.subplot(0,1)\n",
    "visualize(flat_vct, fig=fig, scaling_factor=10, display=False, \n",
    "          layout_parameters={'title':'Flatten as Primal 1-cochain'})\n",
    "\n",
    "fig.subplot(0,2)\n",
    "visualize(dual_flat_vct, fig=fig, scaling_factor=10, display=False, \n",
    "          layout_parameters={'title':'Flatten as Dual 1-cochain'})\n",
    "\n",
    "\n",
    "fig.link_views()\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Note:** As one can see in the previous cell, when visualizing cochains and vector fields, we can benefit from the **pyvista** API and its intuitive customization."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From these flatten vector fields, it could be interesting to see if we can get back the initial vector field. This can be done, using the `sharp()` operator:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr.operators import sharp\n",
    "\n",
    "sharp_flat_vct = sharp(flat_vct, name='Our first sharpened cochain')\n",
    "\n",
    "if isinstance(sharp_flat_vct, Cochain) & sharp_flat_vct.isdual & sharp_flat_vct.isvectorvalued & (sharp_flat_vct.dim == 0):\n",
    "    print(f'{sharp_flat_vct.name} is indeed a proper discrete vector field.')\n",
    "\n",
    "if sharp(dual_flat_vct) is None:\n",
    "    print(f'WARNING: Sharpening a dual cochain is not possible yet.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> **Remark:** As shown above, the current version of the library (V 1.0.0), we cannot sharpen dual 1-`Cochain` instances.\n",
    "> This limitation could be overcome by dualizing the flatten dual cochain with the `hodge_star()` operator, but the results would not correspond to a properly computed vector field, we therefore do not encourage users to perform this *\"nasty hack\".*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Making use of the `visualize()` function, we can visually compare the initial vector field and the one obtained through the composition of the musical isomorphisms:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = pv.Plotter(shape=(1,3), border=False)\n",
    "\n",
    "fig.subplot(0,0)\n",
    "visualize(vct, fig=fig, display=False, \n",
    "          layout_parameters={'title':'Vector field'})\n",
    "\n",
    "fig.subplot(0,1)\n",
    "visualize(flat_vct, fig=fig, scaling_factor=10, display=False, \n",
    "          layout_parameters={'title':'Flat(vector field)'})\n",
    "\n",
    "fig.subplot(0,2)\n",
    "visualize(sharp_flat_vct, fig=fig, display=False, \n",
    "          layout_parameters={'title':'Sharp(flat(vector field))'})\n",
    "\n",
    "fig.link_views()\n",
    "fig.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "At first glance, the vector fields in the right hand-side panel seems rather close to the initial one on the left hand-side panel. But we know that by construction, the discrete musical isomorphisms are not proper inverse from one another. We can estimate the corresponding relative error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy.linalg as lng\n",
    "\n",
    "err = [lng.norm(v2 - v1)/ lng.norm(v1) \n",
    "       for v1, v2 in zip(vct.values, sharp_flat_vct.values)]\n",
    "\n",
    "print(f'The relative error between the amplitudes of the vectors and their sharp(flat()) counterparts is {np.mean(err):.2%} +/- {np.std(err):.2%}.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The wedge product"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Starting from two cochains of degrees $k_1$ and $k_2$, defined on a simplicial complex of dimension $n$, the wedge product enables the creation of a cochain of degree $k_1+k_2$, assuming $k_1+k_2 \\leq n$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The main property of the wedge product to show case at this point is its anti-symmetry:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from dxtr.operators import wedge\n",
    "from dxtr.cochains import Cochain\n",
    "from dxtr.complexes import icosahedron\n",
    "\n",
    "mfld = icosahedron(manifold=True)\n",
    "\n",
    "tol = 1e-15\n",
    "\n",
    "N1 = mfld[1].size\n",
    "N2 = mfld[2].size\n",
    "\n",
    "cchn_1 = Cochain(mfld, 1, values= np.random.random(N1))\n",
    "cchn_11 = wedge(cchn_1, cchn_1)\n",
    "\n",
    "if (np.abs(cchn_11.values) < tol).all():\n",
    "    print(f'The wedge of a {cchn_1.name} with it self yields a {cchn_11.name} of max value: {cchn_11.values.max():.2e}\\n')\n",
    "\n",
    "cchn_2 = Cochain(mfld, 1, values= 3*np.ones(N1))\n",
    "\n",
    "cchn_12 = wedge(cchn_1, cchn_2)\n",
    "cchn_21 = wedge(cchn_2, cchn_1)\n",
    "\n",
    "if (np.abs(cchn_12.values + cchn_21.values) < tol).all():\n",
    "    print('The wedge between two 1-cochains is indeed antisymmetric.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If one of the considered cochain in the wedge is a 0-cochain, the wedge works as a scalar multiplication; *i.e.* each component of the other considered k-cochain, associated with a k-simplex, is scaled by the average value of the 0-cochain taken over the 0-faces of the k-simplex:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N0 = mfld[0].size\n",
    "\n",
    "cchn_0 = Cochain(mfld, 0, np.random.random(N0))\n",
    "cchn_2 = Cochain(mfld, 2, np.random.random(N2))\n",
    "\n",
    "cchn_02 = wedge(cchn_0, cchn_2)\n",
    "\n",
    "error = []\n",
    "for sidx, val in cchn_02.items():\n",
    "    cfids = mfld.faces(2,0)[sidx]\n",
    "    xpct_val = cchn_0.values[cfids].mean() * cchn_2.values[sidx]\n",
    "    error.append(np.abs(val - xpct_val))\n",
    "\n",
    "if (np.asarray(error) < tol).all():\n",
    "    print('Wedge with a 0-cochain works as expected.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "dxtr",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.18"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
