import unittest
import numpy as np
import scipy.sparse as sp
import sparse as sprs

from dxtr.complexes import icosahedron, diamond
from dxtr.complexes.abstractsimplicialcomplex import (AbstractSimplicialComplex,
                            _top_simplices_are_well_defined, _format_simplices, 
                            ordered_splx_ids_loops, order_index_loop, 
                            _neighbors_through_vertices, _nonzero_element_indices, _relative_parity)

class TestAbstractSimplicialComplex(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """

        self.indices = [[0, 1, 2, 3],
                        [1, 2, 3, 4]]

        self.complex = AbstractSimplicialComplex(self.indices)

        indices = [[0, 1, 2],
                   [0, 1, 3],
                   [0, 2, 3],
                   [1, 2, 3]]

        self.notPure_indices = [[0, 1, 2], [2, 3], [3,4,5,6]] 
        self.closed_complex = AbstractSimplicialComplex(indices)

        self.icosahedron = icosahedron(manifold=True)
        self.diamond = diamond()


    def tearDown(self):
        """Concludes and closes the test.
        """
    

    def test_AbstracSimplicialComplex_notPure(self) -> None:
        notpure_cplx = AbstractSimplicialComplex(self.notPure_indices)
        self.assertIsInstance(notpure_cplx, AbstractSimplicialComplex)
        self.assertFalse(notpure_cplx.ispure)


    def test_AbstractSimplicialComplex_str(self) -> None:
        self.assertEqual(self.complex.__str__(),
            '3D Abstract Simplicial Complex of shape (5, 9, 7, 2).')
    
    
    def test_AbstractSimplicialComplex_name(self) -> None:
        self.assertEqual(self.complex.name, 
                '3D Abstract Simplicial Complex of shape (5, 9, 7, 2).')


    def test_AbstractSimplicialComplex_name_setter(self) -> None:
        self.complex.name = 'ASC'
        self.assertEqual(self.complex.name, 'ASC')


    def test_AbstractSimplicialComplex_star(self) -> None:
        cplx = self.complex
        
        # dict format
        splx = {0: 0}
        xpct_star = {0: [0], 1: [0, 1, 2], 2: [0, 1, 2], 3: [0]}

        for k, splcs in xpct_star.items():
            self.assertEqual(cplx.star(splx)[k], splcs)
        
        # dim specified separatly
        dim, splx = 0, 0
        for k, splcs in xpct_star.items():
            self.assertEqual(cplx.star(splx, dim=dim)[k], splcs)

        # ill-formated
        splx = [0]
        self.assertIsNone(cplx.star(splx))


    def test_AbstractSimplicialComplex_closure(self) -> None:
        cplx = self.complex
        
        # dict format
        splx = {3: 0}
        xpct_clsr = {0: [0, 1, 2, 3], 
                     1: [0, 1, 2, 3, 4, 6], 
                     2: [0, 1, 2, 3], 
                     3: [0]}

        for k, splcs in xpct_clsr.items():
            self.assertEqual(cplx.closure(splx)[k], splcs)
            
        # dim specified separatly
        dim, splx = 3, 0
        xpct_clsr = {0: [0, 1, 2, 3], 
                     1: [0, 1, 2, 3, 4, 6], 
                     2: [0, 1, 2, 3], 
                     3: [0]}

        for k, splcs in xpct_clsr.items():
            self.assertEqual(cplx.closure(splx, dim=dim)[k], splcs)
        
        # ill-formated
        splx = [0]
        self.assertIsNone(cplx.closure(splx))


    def test_AbstractSimplicialComplex_link(self) -> None:
        cplx = self.complex
        
        # dict format
        splx = {0: 0}
        xpct_link = {0: [1, 2, 3], 1: [3, 4, 6], 2: [3]}

        for k, splcs in xpct_link.items():
            self.assertEqual(cplx.link(splx)[k], splcs)
            
        # dim specified separatly
        dim, splx = 0, 0

        for k, splcs in xpct_link.items():
            self.assertEqual(cplx.link(splx, dim=dim)[k], splcs)
        
        # ill-formated
        splx = [0]
        self.assertIsNone(cplx.link(splx))


    def test_AbstractSimplicialComplex_interior(self) -> None:
        xpct_interior = {2: [3], 3: [0, 1]}
        for k, splcs in xpct_interior.items():
            self.assertEqual(self.complex.interior()[k], splcs)


    def test_AbstractSimplicialComplex_faces(self) -> None:
        
        cplx = self.complex
        
        xpct_faces = {0: [[0], [1], [2], [3], [4]],
                      1: [[0, 1], [0, 2], [0, 3], 
                          [1, 2], [1, 3], [1, 4], 
                          [2, 3], [2, 4], [3, 4]],
                      2: [[0, 1, 2], [0, 1, 3], [0, 2, 3], 
                          [1, 2, 3], [1, 2, 4], [1, 3, 4], [2, 3, 4]],
                      3: [[0, 1, 2, 3], [1, 2, 3, 4]]}

        for k, fcs in xpct_faces.items():
            print(cplx.faces(k, 0), fcs)
            np.testing.assert_array_almost_equal(cplx.faces(k, 0), fcs)
        
        xpct_faces_2 = {0: [[0, 1, 2, 3], [1, 2, 3, 4]],
                        1: [[0, 1, 2, 3, 4, 6], [3, 4, 5, 6, 7, 8]],
                        2: [[0, 1, 2, 3], [3, 4, 5, 6]],
                        3: [[0], [1]]}

        for k, fcs in xpct_faces_2.items():
            np.testing.assert_array_almost_equal(cplx.faces(3, k), fcs)
        
        # Dimensions not well ordered
        self.assertIsNone(cplx.faces(1, 2))


    def test_AbstractSimplicialComplex_cofaces(self) -> None:
        
        cplx = self.complex

        xpct_cfcs = {0:[[0], [1], [2], [3], [4]],
                     1:[[0, 1, 2], [0, 3, 4, 5], [1, 3, 6, 7], 
                        [2, 4, 6, 8], [5, 7, 8]],
                     2:[[0, 1, 2], [0, 1, 3, 4, 5], [0, 2, 3, 4, 6], 
                        [1, 2, 3, 5, 6], [4, 5, 6]],
                     3:[[0], [0, 1], [0, 1], [0, 1], [1]]}

        for k, cfcs in xpct_cfcs.items():
            self.assertEqual(cplx.cofaces(0, k), cfcs)

        xpct_cfcs_2 = {0:[[0], [0, 1], [0, 1], [0, 1], [1]],
                       1:[[0], [0], [0], [0, 1], [0, 1], [1], [0, 1], [1], [1]],
                       2:[[0], [0], [0], [0, 1], [1], [1], [1]],
                       3: [[0], [1]]}

        for k, cfcs in xpct_cfcs_2.items():
            self.assertEqual(cplx.cofaces(k, 3), cfcs)
    

    def test_AbstractSimplicialComplex_ordered_cofaces(self) -> None:
        
        cplx = self.complex

        xpct_ordered_cofaces = {1:[[0, 1, 2], [0, 3, 4, 5], [1, 3, 6, 7], 
                                   [2, 8, 4, 6], [5, 8, 7]],
                                2: [[0, 1, 2], [0, 1, 3, 4, 5], [0, 2, 3, 4, 6], 
                                    [1, 2, 3, 5, 6], [4, 5, 6]],
                                3: [[0], [0, 1], [0, 1], [0, 1], [1]]}

        # Chao
        # np.testing.assert_array_equal -> self.assertEqual
        # fixed some errors
        for k, xpct_cf in xpct_ordered_cofaces.items():
            self.assertEqual(cplx.cofaces(0, k, ordered=True), xpct_cf)


    def test_abstractsimplicialcomplex_incidence_matrix(self) -> None:
        cplx = self.complex
        
        for k in range(cplx.dim+1):
            mtrx = cplx.incidence_matrix(k,0)

            assert isinstance(mtrx, sp.csr_matrix)
            assert mtrx.shape == (cplx.shape[0], cplx.shape[k])
            np.testing.assert_array_equal(mtrx.data, np.ones((k+1)*cplx[k].size))
        
        self.assertIsNone(cplx.incidence_matrix(1,3))
        

    def test_AbstractSimplicialComplex_border(self) -> None:
    
        # 2D
        indices = [[0, 1, 2],
                   [0, 2, 3],
                   [0, 3, 4],
                   [0, 1, 4]]
        
        cplx = AbstractSimplicialComplex(indices)
        
        xpct_brd_ids = [np.array([1, 2, 3, 4]),
                        np.array([4, 5, 6, 7])]
        
        brd_ids = cplx.border()

        self.assertEqual(len(brd_ids), cplx.dim)
        
        for k, ids in brd_ids.items():
            np.testing.assert_array_equal(ids, xpct_brd_ids[k])

        # 3D
        indices =[[0, 1, 2, 5],
                  [0, 2, 3, 5],
                  [0, 3, 4, 5],
                  [0, 1, 4, 5],
                  [0, 1, 2, 6],
                  [0, 2, 3, 6],
                  [0, 3, 4, 6],
                  [0, 1, 4, 6]]
        
        cplx = AbstractSimplicialComplex(indices)

        xpct_brd_ids = [np.array([1, 2, 3, 4, 5, 6]),
                        np.array([6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]),
                        np.array([12, 13, 14, 15, 16, 17, 18, 19])]

        brd_ids = cplx.border()

        self.assertEqual(len(brd_ids), cplx.dim)
        
        for k, ids in brd_ids.items():
            np.testing.assert_array_equal(ids, xpct_brd_ids[k])

        # No borders
        self.assertIsNone(self.closed_complex.border())

      
    def test_top_simplices_are_well_defined(self) -> None:
        self.assertFalse(_top_simplices_are_well_defined(0))

        indices = [[1,2,3], [2,3,4]]
        self.assertTrue(_top_simplices_are_well_defined(indices))


    def test_format_simplices(self) -> None:
        splcs = [[4, 12, 6, 56], [1., 2., 3., 12.],
                 [1, 2, 3], [4, 5, 6],
                 [2, 3], [6, 7],
                 [0], [10]]

        xpct_frmt_splcs = np.array([[1,  2,  3, 12],
                                    [4,  6, 12, 56]])

        frmt_splcs, *others = _format_simplices(splcs)

        self.assertEqual(frmt_splcs.dtype, int)
        np.testing.assert_allclose(frmt_splcs, xpct_frmt_splcs, atol=1e-15)
        self.assertEqual(len(others), 3)
        np.testing.assert_allclose(
            others[-1], np.array(splcs[-2:]), atol=1e-15)

        sample1 = [[0, 1, 4],
                   [1, 3, 4],
                   [3, 2, 4],
                   [2, 0, 4]]

        frmt_splcs, *others = _format_simplices(sample1)

        xpct_frmt_splcs = np.array([[0, 1, 4],
                                    [0, 2, 4],
                                    [1, 3, 4],
                                    [2, 3, 4]])
        np.testing.assert_allclose(frmt_splcs, xpct_frmt_splcs, atol=1e-15)

    
    def test_ordered_splx_ids_loops(self) -> None:
        cplx = self.icosahedron
        n = cplx.dim

        # Loops of edges around triangles
        face_indices = cplx.faces(n)
        
        ordered_face_indices = ordered_splx_ids_loops(face_indices, 
                                                    cplx[1].adjacency)
        
        xpct_ordered_face_indices = np.array([[0, 1, 5], [0, 4, 6], [1, 9, 2],
                                            [2, 3, 12], [3, 4, 15], [5, 8, 10], 
                                            [6, 18, 7], [7, 8, 20], [9, 11, 13], 
                                            [10, 11, 23], [12, 16, 14], 
                                            [13, 25, 14], [15, 17, 19], 
                                            [16, 17, 27], [18, 19, 21], 
                                            [20, 24, 22], [21, 29, 22], 
                                            [23, 24, 26], [25, 26, 28], 
                                            [27, 28, 29]])
        
        np.testing.assert_array_equal(np.array(ordered_face_indices), 
                                    xpct_ordered_face_indices)

        # Loops of n-cofaces around each 0-simplex of a manifold
        coface_indices = cplx.cofaces(0,n)
        
        ordered_coface_indices = ordered_splx_ids_loops(coface_indices, 
                                                        cplx[n].adjacency)

        xpct_ordered_coface_indices = [[0, 1, 4, 3, 2],
                                    [0, 1, 6, 7, 5],
                                    [0, 2, 8, 9, 5],
                                    [2, 8, 11, 10, 3],
                                    [3, 10, 13, 12, 4],
                                    [1, 4, 12, 14, 6],
                                    [6, 14, 16, 15, 7],
                                    [5, 9, 17, 15, 7],
                                    [8, 9, 17, 18, 11],
                                    [10, 11, 18, 19, 13],
                                    [12, 13, 19, 16, 14],
                                    [15, 16, 19, 18, 17]]
        
        for ocfids, xpct_ocfids in zip(ordered_coface_indices,
                                    xpct_ordered_coface_indices):
            np.testing.assert_array_equal(ocfids, xpct_ocfids)

    
    def test_order_index_loop(self) -> None:
        adj = self.icosahedron[-1].adjacency
        cols, rows = adj.nonzero()

        cfids = [0, 1, 2, 3, 4]
        xpct_ordered_cfids = [0, 1, 4, 3, 2]

        ordered_cfids = order_index_loop(cfids, rows, cols)

        assert isinstance(ordered_cfids, list)
        np.testing.assert_array_equal(ordered_cfids, xpct_ordered_cfids)


    def test_compute_orientation(self) -> None:
        """Test wrote by Chao during his internship."""
        sample0 = [[1,2,3],[2,3,4]]
        sample1 = [[1,2,3],[3,4,5]]
        sample2 = [[1,2,3],[4,5,6]]
        sample3 = [[1,2,3,4],[2,3,4,5]]
        sample4 = [[1,2,3,4],[3,4,5,6]]

        asc0 = AbstractSimplicialComplex(sample0)
        asc1 = AbstractSimplicialComplex(sample1)
        asc2 = AbstractSimplicialComplex(sample2)
        asc3 = AbstractSimplicialComplex(sample3)
        asc4 = AbstractSimplicialComplex(sample4)

        xpct_splcs = [[np.array([[1], [2], [3], [4]]),
                   np.array([[1, 2], [1, 3], [2, 3], [2, 4], [3, 4]]),
                   np.array([[1, 2, 3], [2, 3, 4]])],
                  [np.array([[1], [2], [3], [4], [5]]),
                   np.array([[1, 2], [1, 3], [2, 3], [3, 4], [3, 5], [4, 5]]),
                   np.array([[1, 2, 3], [3, 4, 5]])],
                  [np.array([[1], [2], [3], [4], [5], [6]]),
                   np.array([[1, 2], [1, 3], [2, 3], [4, 5], [4, 6], [5, 6]]),
                   np.array([[1, 2, 3], [4, 5, 6]])],
                  [np.array([[1], [2], [3], [4], [5]]),
                   np.array([[1, 2], [1, 3], [1, 4], [2, 3], [2, 4], [2, 5], [3, 4],[3, 5], [4, 5]]),
                   np.array([[1, 2, 3], [1, 2, 4], [1, 3, 4], [2, 3, 4], [2, 3, 5], [2, 4, 5], [3, 4, 5]]),
                   np.array([[1,2,3,4],[2,3,4,5]])],
                  [np.array([[1], [2], [3], [4], [5], [6]]),
                   np.array([[1, 2], [1, 3], [1, 4], [2, 3], [2, 4], [3, 4], [3, 5], [3, 6],[4, 5],[4,6],[5, 6]]),
                   np.array([[1, 2, 3], [1, 2, 4], [1, 3, 4], [2, 3, 4], [3, 4, 5], [3, 4, 6],[3,5,6],[4,5,6]]),
                   np.array([[1,2,3,4],[3,4,5,6]])]]

        xpct_orientations = [np.array([1, -1]),
                             np.array([1, 1]),
                             np.array([1, 1]),
                             np.array([1, 1]),
                             np.array([1, 1])]

        for i,asc in enumerate([asc0,asc1,asc2,asc3,asc4]):
            splcs = asc.data
            for k, simplex in enumerate(splcs):
                np.testing.assert_array_equal(simplex, xpct_splcs[i][k])
            orientations = asc._top_simplex_orientations
            np.testing.assert_array_equal(orientations, xpct_orientations[i])


    def test_asc_contains(self) -> None:
        """Test wrote by Chao during his internship."""
        sample0 = [[1,2,3]]
        sample1 = [[1,2,3],[3,4,5]]
        sample2 = [[0,2,3],[4,5,6]]
        sample3 = [[1,2]]
        sample4 = [[1,2,3,4,5,6]]

        asc0 = AbstractSimplicialComplex(sample0)
        asc1 = AbstractSimplicialComplex(sample1)
        asc2 = AbstractSimplicialComplex(sample2)
        asc3 = AbstractSimplicialComplex(sample3)
        asc4 = AbstractSimplicialComplex(sample4)

        xpct_results = [True, False, False, False, False, False, True, True, True, True, False, True, True, False]

        results = []
        results.append(asc0 in asc1)
        results.append(asc0 in asc2)
        results.append(asc1 in asc2)
        results.append(asc1 in asc0)
        results.append(asc2 in asc0)
        results.append(asc2 in asc1)
        results.append(asc3 in asc0)
        results.append(asc3 in asc1)
        results.append(asc0 in asc4)
        results.append(asc1 in asc4)
        results.append(asc2 in asc4)
        results.append(asc3 in asc4)
        results.append(asc4 == asc4)
        results.append(asc0 in asc3)

        self.assertEqual(xpct_results, results)


    def test_get_indices(self) -> None:

        indices = self.complex.get_indices(self.indices)
        np.testing.assert_array_equal(indices, [0, 1])


    def test_AbstractSimplicialComplex_0_simplices_orientation(self) -> None:
        xpct_lowest_splx_orientation = np.ones(5)
        
        np.testing.assert_array_equal(self.complex.orientation(0),
                                      xpct_lowest_splx_orientation)
        
        self.assertIsNone(self.complex.orientation(1))


    def test_adjacency_tensor(self) -> None:
        cplx = self.diamond

        # 2, 1 case
        k, l = 2, 1
        
        tsr = cplx.adjacency_tensor(k, l)
        
        self.assertIsInstance(tsr, sprs.COO)
        self.assertEqual(tsr.shape, 
                         (cplx[k+l].size, cplx[k].size, cplx[l].size))
        
        xpct_data = np.array([ 1,  1,  1, -1, -1,  1,  1, -1, -1,  1,  1,  1, 
                               1,  1,  1, -1, -1,  1,  1, -1, -1,  1,  1,  1])
        np.testing.assert_array_equal(tsr.data, xpct_data)

        xpct_coords = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 
                                1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
                                [0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 
                                3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6],
                                [2, 4, 6, 1, 3, 6, 0, 3, 4, 0, 1, 2, 
                                5, 7, 8, 4, 6, 8, 3, 6, 7, 3, 4, 5]])
        np.testing.assert_array_equal(tsr.coords, xpct_coords)


        # 1, 2 case    
        inv_tsr = cplx.adjacency_tensor(l, k)
        self.assertTrue((tsr == inv_tsr.transpose((0,2,1))).all())

        # 2, 0 case
        k, l = 2, 0
        
        tsr = cplx.adjacency_tensor(k, l)
        
        np.testing.assert_array_equal(tsr.data, np.ones(21))
        
        xpct_coords = np.array([[0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 
                                 5, 5, 5, 6, 6, 6],
                                [0, 0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4,   
                                 5, 5, 5, 6, 6, 6], 
                                [0, 1, 2, 0, 1, 3, 0, 2, 3, 1, 2, 3, 1, 2, 4, 
                                 1, 3, 4, 2, 3, 4]])
        np.testing.assert_array_equal(tsr.coords, xpct_coords)


    def test_nonzero_element_indices(self) -> None:
        cplx = self.diamond
        k, l = 2, 1

        k_faces = cplx.faces(k+l, k)
        l_faces = cplx.faces(k+l, l)
        l_neighbors = _neighbors_through_vertices(cplx, k, l)
        
        xpct_nz_elements = [[0, 0, 2], [0, 0, 4], [0, 0, 6], [0, 1, 1], 
                            [0, 1, 3], [0, 1, 6], [0, 2, 0], [0, 2, 3], 
                            [0, 2, 4], [0, 3, 0], [0, 3, 1], [0, 3, 2], 
                            [1, 3, 8], [1, 3, 5], [1, 3, 7], [1, 4, 8], 
                            [1, 4, 4], [1, 4, 6], [1, 5, 3], [1, 5, 6], 
                            [1, 5, 7], [1, 6, 3], [1, 6, 4], [1, 6, 5]]
        
        nz_elements = _nonzero_element_indices(k_faces, l_faces, l_neighbors)
        np.testing.assert_array_equal(nz_elements, xpct_nz_elements)

        # with l = 0
        k, l = 1, 0

        k_faces = cplx.faces(k+l, k)
        l_faces = cplx.faces(k+l, l)
        l_neighbors = _neighbors_through_vertices(cplx, k, l)
        xpct_nz_elements = [[0, 0, 0], [0, 0, 1], [1, 1, 0], [1, 1, 2], 
                            [2, 2, 0], [2, 2, 3], [3, 3, 1], [3, 3, 2], 
                            [4, 4, 1], [4, 4, 3], [5, 5, 1], [5, 5, 4], 
                            [6, 6, 2], [6, 6, 3], [7, 7, 2], [7, 7, 4], 
                            [8, 8, 3], [8, 8, 4]]

        nz_elements = _nonzero_element_indices(k_faces, l_faces, l_neighbors)
        np.testing.assert_array_equal(nz_elements, xpct_nz_elements)

        # 1, 1 case
        k, l = 1, 1 
        
        k_faces = cplx.faces(k+l, k)
        l_faces = cplx.faces(k+l, l)
        l_neighbors = _neighbors_through_vertices(cplx, k, l)

        xpct_nz_elements = [[0, 0, 1], [0, 0, 3], [0, 1, 0], [0, 1, 3], 
                            [0, 3, 0], [0, 3, 1], [1, 0, 2], [1, 0, 4], 
                            [1, 2, 0], [1, 2, 4], [1, 4, 0], [1, 4, 2], 
                            [2, 1, 2], [2, 1, 6], [2, 2, 1], [2, 2, 6], 
                            [2, 6, 1], [2, 6, 2], [3, 3, 4], [3, 3, 6], 
                            [3, 4, 3], [3, 4, 6], [3, 6, 3], [3, 6, 4], 
                            [4, 3, 5], [4, 3, 7], [4, 5, 3], [4, 5, 7], 
                            [4, 7, 3], [4, 7, 5], [5, 4, 8], [5, 4, 5], 
                            [5, 5, 8], [5, 5, 4], [5, 8, 4], [5, 8, 5], 
                            [6, 6, 8], [6, 6, 7], [6, 7, 8], [6, 7, 6], 
                            [6, 8, 6], [6, 8, 7]]

        nz_elements = _nonzero_element_indices(k_faces, l_faces, l_neighbors)
        np.testing.assert_array_equal(nz_elements, xpct_nz_elements)


    def test_neighbors_through_vertices(self) -> None:
        cplx = self.diamond

        # 0-simplices 
        k, l = 0, 0
        nids = _neighbors_through_vertices(cplx, k, l)
        xpct_nids = [[0], [1], [2], [3], [4]]
        for eids, xpct_eids in zip(nids, xpct_nids):
            np.testing.assert_array_equal(eids, xpct_eids)

        k, l = 0, 1
        nids = _neighbors_through_vertices(cplx, k, l)
        xpct_nids = [[0, 1, 2], [0, 3, 4, 5], [1, 3, 6, 7], [2, 4, 6, 8],
                     [5, 7, 8]]
        for eids, xpct_eids in zip(nids, xpct_nids):
            np.testing.assert_array_equal(eids, xpct_eids)

        k, l = 1, 0
        nids = _neighbors_through_vertices(cplx, k, l)
        xpct_nids = [[0, 1], [0, 2], [0, 3], [1, 2], [1, 3], [1, 4], [2, 3], 
                     [2, 4], [3, 4]]
        for eids, xpct_eids in zip(nids, xpct_nids):
            np.testing.assert_array_equal(eids, xpct_eids)

        # 1-simplices
        k, l = 1, 1
        nids = _neighbors_through_vertices(cplx, k, l)
        self.assertEqual(len(nids), cplx[k].size)
        xpct_nids = [[1, 2, 3, 4, 5], [0, 2, 3, 6, 7], [0, 1, 4, 6, 8],  
                     [0, 1, 4, 5, 6, 7], [0, 2, 3, 5, 6, 8],  [0, 3, 4, 7, 8], 
                     [1, 2, 3, 4, 7, 8], [1, 3, 5, 6, 8], [2, 4, 5, 6, 7]]
        for eids, xpct_eids in zip(nids, xpct_nids):
            np.testing.assert_array_equal(eids, xpct_eids)
        
        # 2-simplices
        k, l = 2, 2
        nids = _neighbors_through_vertices(cplx, k, l)
        self.assertEqual(len(nids), cplx[k].size)
        xpct_nids = [[5, 6], [4, 6], [4, 5], [], [1, 2], [0, 2], [0, 1]]
        for eids, xpct_eids in zip(nids, xpct_nids):
            np.testing.assert_array_equal(eids, xpct_eids)
        
        k, l = 2, 1
        nids = _neighbors_through_vertices(cplx, k, l)
        self.assertEqual(len(nids), cplx[k].size)
        for ids, fids in zip(nids, cplx.faces(k,l)):
            self.assertFalse(set(fids).issubset(ids))
        xpct_nids = [[2, 4, 5, 6, 7], [1, 3, 5, 6, 8], [0, 3, 4, 7, 8], 
                     [0, 1, 2, 5, 7, 8], [0, 1, 4, 6, 8], [0, 2, 3, 6, 7], 
                     [1, 2, 3, 4, 5]]
        for eids, xpct_eids in zip(nids, xpct_nids):
            np.testing.assert_array_equal(eids, xpct_eids)
        
        k, l = 1,2
        nids = _neighbors_through_vertices(cplx, k, l)
        self.assertEqual(len(nids), cplx[k].size)
        xpct_nids = [[2, 3, 4, 5], [1, 3, 4, 6], [0, 3, 5, 6], [1, 2, 5, 6], 
                    [0, 2, 4, 6], [0, 1, 3, 6], [0, 1, 4, 5], [0, 2, 3, 5], 
                    [1, 2, 3, 4]]
        for eids, xpct_eids in zip(nids, xpct_nids):
            np.testing.assert_array_equal(eids, xpct_eids)

        # 3-simplices
        k, l = 3, 3
        nids = _neighbors_through_vertices(cplx, k, l)
        self.assertEqual(len(nids), cplx[k].size)
        xpct_nids = [[], []]
        for eids, xpct_eids in zip(nids, xpct_nids):
            np.testing.assert_array_equal(eids, xpct_eids)
        
        k, l = 3, 2
        nids = _neighbors_through_vertices(cplx, k, l)
        self.assertEqual(len(nids), cplx[k].size)
        xpct_nids = [[], []]
        for eids, xpct_eids in zip(nids, xpct_nids):
            np.testing.assert_array_equal(eids, xpct_eids)
        
        k, l = 2, 3
        nids = _neighbors_through_vertices(cplx, k, l)
        self.assertEqual(len(nids), cplx[k].size)
        xpct_nids = [[], [], [], [], [], [], []]
        for eids, xpct_eids in zip(nids, xpct_nids):
            np.testing.assert_array_equal(eids, xpct_eids)

        k, l = 3, 1
        nids = _neighbors_through_vertices(cplx, k, l)
        self.assertEqual(len(nids), cplx[k].size)
        for ids, fids in zip(nids, cplx.faces(k,l)):
            assert not set(fids).issubset(ids)
        xpct_nids = [[5, 7, 8], [0, 1, 2]]
        for eids, xpct_eids in zip(nids, xpct_nids):
            np.testing.assert_array_equal(eids, xpct_eids)
        
        k, l = 1, 3
        nids = _neighbors_through_vertices(cplx, k, l)
        self.assertEqual(len(nids), cplx[k].size)
        xpct_nids = [[1], [1], [1], [], [], [0], [], [0], [0]]
        for eids, xpct_eids in zip(nids, xpct_nids):
            np.testing.assert_array_equal(eids, xpct_eids)


    def test_relative_parity_11case(self) -> None:
        cplx = self.diamond
        
        k, l = 1, 1
        
        splx_ids = np.array([[0, 0, 1], [0, 0, 3], [0, 1, 0], [0, 1, 3], 
                             [0, 3, 0], [0, 3, 1], [1, 0, 2], [1, 0, 4], 
                             [1, 2, 0], [1, 2, 4], [1, 4, 0], [1, 4, 2], 
                             [2, 1, 2], [2, 1, 6], [2, 2, 1], [2, 2, 6], 
                             [2, 6, 1], [2, 6, 2], [3, 3, 4], [3, 3, 6], 
                             [3, 4, 3], [3, 4, 6], [3, 6, 3], [3, 6, 4], 
                             [4, 3, 5], [4, 3, 7], [4, 5, 3], [4, 5, 7], 
                             [4, 7, 3], [4, 7, 5], [5, 4, 8], [5, 4, 5], 
                             [5, 5, 8], [5, 5, 4], [5, 8, 4], [5, 8, 5], 
                             [6, 6, 8], [6, 6, 7], [6, 7, 8], [6, 7, 6], 
                             [6, 8, 6], [6, 8, 7]])
        kl_splcs = cplx[k+l].vertex_indices[splx_ids[:, 0]]
        k_splcs = cplx[k].vertex_indices[splx_ids[:, 1]]
        l_splcs = cplx[l].vertex_indices[splx_ids[:, 2]]
        
        xpct_parity = np.array([0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 
                                1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 
                                0, 1, 1, 1, 0, 0, 0, 1, 1, 1])
        
        np.testing.assert_array_equal(_relative_parity(kl_splcs, k_splcs, 
                                                       l_splcs), xpct_parity)
        
        # reverse order: parity should be the opposite since k*l is odd.
        np.testing.assert_array_equal(_relative_parity(kl_splcs, l_splcs, 
                                                       k_splcs),
                                      np.ones(42) - xpct_parity)


    def test_relative_parity_21case(self) -> None:
        cplx = self.diamond
        
        k, l = 1, 2
        
        splx_ids = np.array([[0, 0, 2], [0, 0, 3], [0, 1, 1], [0, 1, 3], 
                             [0, 2, 0], [0, 2, 3], [0, 3, 1], [0, 3, 2], 
                             [0, 4, 0], [0, 4, 2], [0, 6, 0], [0, 6, 1], 
                             [1, 3, 5], [1, 3, 6], [1, 4, 4], [1, 4, 6], 
                             [1, 5, 3], [1, 5, 6], [1, 6, 4], [1, 6, 5], 
                             [1, 7, 3], [1, 7, 5], [1, 8, 3], [1, 8, 4]])  
        kl_splcs = cplx[k+l].vertex_indices[splx_ids[:, 0]]
        k_splcs = cplx[k].vertex_indices[splx_ids[:, 1]]
        l_splcs = cplx[l].vertex_indices[splx_ids[:, 2]]
        
        xpct_parity = np.array([0, 0, 1, 0, 0, 0, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 
                                0, 0, 1, 1, 0, 1, 0, 0])

        np.testing.assert_array_equal(_relative_parity(kl_splcs, k_splcs, 
                                                       l_splcs), xpct_parity)
        
        # In the reverse case, parity should be the same since k*l is even.
        np.testing.assert_array_equal(_relative_parity(kl_splcs, l_splcs, 
                                                       k_splcs), xpct_parity)


    def test_relative_parity_with_0cochain(self) -> None:
        cplx = self.diamond

        # 1, 0 case
        k, l = 1, 0
        splx_ids = np.array([[0, 0, 0], [0, 0, 1], [1, 1, 0], [1, 1, 2], 
                             [2, 2, 0], [2, 2, 3], [3, 3, 1], [3, 3, 2], 
                             [4, 4, 1], [4, 4, 3], [5, 5, 1], [5, 5, 4], 
                             [6, 6, 2], [6, 6, 3], [7, 7, 2], [7, 7, 4], 
                             [8, 8, 3], [8, 8, 4]])

        kl_splcs = cplx[k+l].vertex_indices[splx_ids[:, 0]]
        k_splcs = cplx[k].vertex_indices[splx_ids[:, 1]]
        l_splcs = cplx[l].vertex_indices[splx_ids[:, 2]]
        
        np.testing.assert_array_equal(_relative_parity(kl_splcs, k_splcs, 
                                                       l_splcs), np.zeros(18))
        # 0, 1 case
        np.testing.assert_array_equal(_relative_parity(kl_splcs, k_splcs, 
                                                       l_splcs),
                                      _relative_parity(kl_splcs, l_splcs, 
                                                       k_splcs))
        
        # 2, 0 case
        k, l = 2, 0
        splx_ids = np.array([[0, 0, 0], [0, 0, 1], [0, 0, 2], [1, 1, 0], 
                             [1, 1, 1], [1, 1, 3], [2, 2, 0], [2, 2, 2], 
                             [2, 2, 3], [3, 3, 1], [3, 3, 2], [3, 3, 3], 
                             [4, 4, 1], [4, 4, 2], [4, 4, 4], [5, 5, 1], 
                             [5, 5, 3], [5, 5, 4], [6, 6, 2], [6, 6, 3], 
                             [6, 6, 4]])

        kl_splcs = cplx[k+l].vertex_indices[splx_ids[:, 0]]
        k_splcs = cplx[k].vertex_indices[splx_ids[:, 1]]
        l_splcs = cplx[l].vertex_indices[splx_ids[:, 2]]
        
        np.testing.assert_array_equal(_relative_parity(kl_splcs, k_splcs, 
                                                       l_splcs), np.zeros(21))
        # 0, 2 case
        np.testing.assert_array_equal(_relative_parity(kl_splcs, k_splcs,
                                                        l_splcs),
                                    _relative_parity(kl_splcs, l_splcs, 
                                                     k_splcs))

