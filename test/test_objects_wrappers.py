import unittest
import os as os
import numpy as np
import scipy.sparse as sp
import pyvista as pv

from dxtr.complexes import icosahedron
from dxtr.cochains import unit_cochain, normal_vector_field

from dxtr.utils.wrappers import (UGrid, Property,
                                   _ugrid_from_simplicialcomplex,
                                   _ugrid_from_dual_simplicialcomplex,
                                   _ugrid_from_cochain, _format_cochain_values, 
                                   _position_in, _vertices_in, _simplicies_from_cells, 
                                   _cell_vertex_incidence_matrix)


class TestIoWrappers(unittest.TestCase):

    def setUp(self) -> None:
        """Initiates the test.
        """
        
        # for UGrid tests
        self.cplx = icosahedron()
        self.mfld = icosahedron(manifold=True)
        self.cchn = unit_cochain()
        self.cchn0 = unit_cochain(dim=0)
        self.cchn2 = unit_cochain(dim=2)

        self.kvf = normal_vector_field()

        # For Property tests
        self.path = './data/meshes/square.ply'
        self.wrong_path = './data/meshes/square.txt'

        self.properties = [Property(name='x', type='float'),
                           Property(name='y', type='float'),
                           Property(name='z', type='float'),
                           Property(name='vertex_index', type='int'),
                           Property(name='vertex_indices', type='int')]
        
        self.rawdata = ['end_header\n',
                        '','','','','','','','','','','','','',
                        '6 0 1 2 3 4 5\n',
                        '6 2 3 6 8 7 9\n',
                        '6 4 3 9 10 11 12\n']
        
        self.rawdata_1 = ['6 0 1 2 3 4 5\n',
                        '6 2 3 6 8 7 9\n',
                        '6 4 3 9 10 11 12\n']
        
        
        self.rawdata_2 = ['4 0 1 3 4\n',
                          '4 1 2 4 5\n',
                          '4 3 4 7 6\n',
                          '5 4 5 7 9 8']

    def tearDown(self) -> None:
        """Concludes and closes the test.
        """


    def test_UGrid_Class_init(self) -> None:
        pts = [[0, 0, 0], [1, 0, 0],
               [0, 1, 0], [0, 0, 1]]
        cells = [3, 0, 1, 2, 3, 0, 1, 3,
                 3, 0, 2, 3, 3, 1, 2, 3]
        cell_types = [pv.CellType.TRIANGLE]*4

        ugrid = UGrid(cells, cell_types, pts)
        self.assertIsInstance(ugrid, pv.UnstructuredGrid)
        self.assertEqual(ugrid.n_points, len(pts))
        self.assertEqual(ugrid.n_cells, len(cells)/4)
        np.testing.assert_array_equal(ugrid.points, pts)


    def test_UGrid_Class_from(self) -> None:
        ugrid = UGrid.generate_from(self.cplx)
        self.assertIsInstance(ugrid, pv.UnstructuredGrid)

        # 2-cochain
        ugrid = UGrid.generate_from(self.cchn2)
        self.assertIsInstance(ugrid, pv.UnstructuredGrid)
        np.testing.assert_array_equal(ugrid.cell_data.active_scalars,
                                    self.cchn2.values)


    def test_ugrid_from_simplicialcomplex(self) -> None:

        # test complex.
        ugrid = _ugrid_from_simplicialcomplex(self.cplx)
        self.assertIsInstance(ugrid, pv.UnstructuredGrid)
        self.assertEqual(ugrid.n_points, self.cplx[0].size)
        self.assertEqual(ugrid.n_cells, self.cplx[-1].size)
        np.testing.assert_allclose(ugrid.points, self.cplx[0].vertices)
        
        # test manifold
        ugrid = _ugrid_from_simplicialcomplex(self.mfld)
        self.assertIsInstance(ugrid, pv.UnstructuredGrid)
        self.assertEqual(ugrid.n_points, self.mfld[0].size)
        self.assertEqual(ugrid.n_cells, self.mfld[-1].size)
        np.testing.assert_allclose(ugrid.points, self.mfld[0].vertices)

        # test with smallest_cell_dim specified
        k = 2
        ugrid = _ugrid_from_simplicialcomplex(self.mfld, smallest_cell_dim=k)
        self.assertIsInstance(ugrid, pv.UnstructuredGrid)
        self.assertEqual(ugrid.n_points, self.mfld[0].size)
        self.assertEqual(ugrid.n_cells, self.mfld[-1].size)
        np.testing.assert_allclose(ugrid.points, self.mfld[0].vertices)

        k = 1
        ugrid = _ugrid_from_simplicialcomplex(self.cplx, smallest_cell_dim=k)
        self.assertIsInstance(ugrid, pv.UnstructuredGrid)
        self.assertEqual(ugrid.n_points, self.cplx[0].size)
        self.assertEqual(ugrid.n_cells, sum(self.cplx.shape[1:]))
        np.testing.assert_allclose(ugrid.points, self.cplx[0].vertices)


    def test_ugrid_from_dual_simplicialcomplex(self) -> None:
        # Check not working with `SimplicialComplex`
        cplx = icosahedron()
        ugrid = _ugrid_from_dual_simplicialcomplex(cplx)
        self.assertIsNone(ugrid)

        # Basic working example
        mfld = icosahedron(manifold=True)
        
        ugrid = _ugrid_from_dual_simplicialcomplex(mfld)
        self.assertIsInstance(ugrid, pv.UnstructuredGrid)
        self.assertEqual(ugrid.n_points, mfld[-1].size)
        self.assertEqual(ugrid.n_cells,  mfld[0].size)
        np.testing.assert_allclose(ugrid.points, mfld[-1].circumcenters)
        
        # Call from _ugrid_from_simplicialcomplex with dual=True
        mfld = icosahedron(manifold=True)
        
        ugrid = _ugrid_from_simplicialcomplex(mfld, dual=True)
        self.assertIsInstance(ugrid, pv.UnstructuredGrid)
        self.assertEqual(ugrid.n_points, mfld[-1].size)
        self.assertEqual(ugrid.n_cells,  mfld[0].size)
        np.testing.assert_allclose(ugrid.points, mfld[-1].circumcenters)

        # Test with smallest_cell_dim specified
        k = 0
        ugrid = _ugrid_from_dual_simplicialcomplex(mfld, smallest_cell_dim=k)
        self.assertIsInstance(ugrid, pv.UnstructuredGrid)
        self.assertEqual(ugrid.n_points, mfld[-1].size)
        self.assertEqual(ugrid.n_cells, mfld[0].size)
        np.testing.assert_allclose(ugrid.points, mfld[-1].circumcenters)

        k = 1
        ugrid = _ugrid_from_dual_simplicialcomplex(mfld, smallest_cell_dim=k)
        self.assertIsInstance(ugrid, pv.UnstructuredGrid)
        self.assertEqual(ugrid.n_points, mfld[-1].size)
        self.assertEqual(ugrid.n_cells, sum(mfld.shape[:2]))
        np.testing.assert_allclose(ugrid.points, mfld[-1].circumcenters)


    def test_ugrid_from_cochain(self) -> None:
        # 0-cochain
        ugrid = _ugrid_from_cochain(self.cchn0)
        self.assertIsInstance(ugrid, pv.UnstructuredGrid)
        
        cplx= self.cchn0.complex
        self.assertEqual(ugrid.n_points, cplx[0].size)
        self.assertEqual(ugrid.n_cells, cplx.shape[-1])
        
        N0 = cplx.shape[0]
        data = ugrid.point_data.active_scalars
        np.testing.assert_array_equal(data[:N0], np.ones(N0))
        
        # 1-cochain & 2-cochain
        for k, cchn in enumerate([self.cchn, self.cchn2]):
            k += 1
            cplx = cchn.complex
            Nk = cplx.shape[k]
            ugrid = _ugrid_from_cochain(cchn)
            
            self.assertIsInstance(ugrid, pv.UnstructuredGrid)
            self.assertEqual(ugrid.n_points, cplx[0].size)
            self.assertEqual(ugrid.n_cells, sum(cplx.shape[k:]))

            data = ugrid.cell_data.active_scalars
            np.testing.assert_array_equal(data[:Nk], np.ones(Nk))
            np.testing.assert_array_equal(data[Nk:], 
                                          [np.nan]*sum(cplx.shape[k+1:]))
        
        
    def test_format_cochain_values(self) -> None:
        values = _format_cochain_values(self.cchn)

        self.assertIsInstance(values, np.ndarray)
        
        N1 = self.cchn.complex.shape[1]
        top_splx_nbr = sum(self.cchn.complex.shape[2:])
        
        np.testing.assert_array_equal(values[:N1, 0], self.cchn.values)
        np.testing.assert_array_equal(values[N1:, 0], [np.nan]*top_splx_nbr)


    def test_properties(self) -> None:
        self.assertEqual(self.properties[0].name, 'x')
        self.assertEqual(self.properties[0].type, 'float')
        self.assertIsNone(self.properties[0].dim)
        self.assertIsNone(self.properties[0].values)


    def test__position_in(self) -> None:
        self.assertTrue(_position_in(self.properties))
        self.assertFalse(_position_in(self.properties[:2]))


    def test__vertices_in(self) -> None:
        self.assertTrue(_vertices_in(self.properties))
        self.assertTrue(_vertices_in(self.properties[:-1]))
        self.assertFalse(_vertices_in(self.properties[:-2]))
    

    def test_simplicies_from_cells(self) -> None:
        simplices = _simplicies_from_cells(self.rawdata_1)
        xpct_splcs = [[0, 1, 2]]

        self.assertEqual(len(simplices), len(xpct_splcs))
        for splx, xpct_splx in zip(simplices, xpct_splcs):
            self.assertEqual(splx, xpct_splx)


    def test_cell_vertex_incidence_matrix(self) -> None:
        mtrx = _cell_vertex_incidence_matrix(self.rawdata_2)
        xpct_mtrx = np.array([[1, 0, 0, 0],
                              [1, 1, 0, 0],
                              [0, 1, 0, 0],
                              [1, 0, 1, 0],
                              [1, 1, 1, 1],
                              [0, 1, 0, 1],
                              [0, 0, 1, 0],
                              [0, 0, 1, 1],
                              [0, 0, 0, 1],
                              [0, 0, 0, 1]])

        self.assertIsInstance(mtrx, sp.coo_matrix)
        self.assertEqual(mtrx.shape, (10, 4))
        np.testing.assert_array_equal(mtrx.toarray(), xpct_mtrx)
