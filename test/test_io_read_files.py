import unittest
import os as os
import numpy as np
import pyvista as pv

from dxtr.io.read import (read_ply, read_vtk, _valid, _cells_circumcenters, 
                          _vertex_indices_around_cells, _top_simplices_within, 
                          _data_is_valid, _data_within)
from dxtr.complexes import icosahedron
from dxtr.cochains import unit_cochain
from dxtr.utils.wrappers import UGrid, Property

class TestIoReadFiles(unittest.TestCase):

    def setUp(self) -> None:
        """Initiates the test.
        """
        
        # self.data_folder = './data/meshes'
        # self.path = './data/meshes/square.ply'
        self.root = os.getcwd() 
        self.data_folder = self.root + '/src/dxtr/utils/mesh_examples/'
        
        self.correct_path = self.data_folder + 'icosahedron_complex.vtk'
        self.wrong_path = self.data_folder + 'square.txt'
        
        self.rawdata = ['end_header\n',
                        '','','','','','','','','','','','','',
                        '6 0 1 2 3 4 5\n',
                        '6 2 3 6 8 7 9\n',
                        '6 4 3 9 10 11 12\n']
        
        self.rawdata_1 = ['6 0 1 2 3 4 5\n',
                        '6 2 3 6 8 7 9\n',
                        '6 4 3 9 10 11 12\n']
        
        self.rawdata_2 = ['4 0 1 3 4\n',
                          '4 1 2 4 5\n',
                          '4 3 4 7 6\n',
                          '5 4 5 7 9 8']

        self.mfld = icosahedron(manifold=True)
        self.ugrid_mfld = UGrid.generate_from(self.mfld)
        self.cchn = unit_cochain('icosa', 1)
        self.ugrid_cchn = UGrid.generate_from(self.cchn)
    
    def tearDown(self) -> None:
        """Concludes and closes the test.
        """

    def test_read_ply(self) -> None:

        expected_simplices = [[183, 983, 1099], 
                              [191, 981, 982], 
                              [171, 967, 1634]]
        
        expected_positions = np.array([[1.        , 0.        , 0.        ],
                                       [0.99890089, 0.04687226, 0.        ],
                                       [0.99560598, 0.09364149, 0.        ]])

        
        simplices, positions = read_ply(self.data_folder + 'disk_small.ply')

        np.testing.assert_allclose(simplices[:3], expected_simplices)
        np.testing.assert_allclose(positions[:3], expected_positions)


    def test_read_vtk(self) -> None:
        
        path = self.data_folder + 'icosahedron_unit_1_cochain.vtk'
        simplices, positions, property = read_vtk(path)
        
        self.assertIsInstance(simplices, np.ndarray)
        self.assertIsInstance(positions, np.ndarray)
        np.testing.assert_array_equal(property.values, np.ones(30))
        
        xpct_simplices = np.array([[0, 1,  2], [0,  1,  5], [0, 2,  3], 
                                [0, 3,  4], [0,  4,  5], [1, 2,  7], 
                                [1, 5,  6], [1,  6,  7], [2, 3,  8], 
                                [2, 7,  8], [3,  4,  9], [3, 8,  9], 
                                [4, 5, 10], [4,  9, 10], [5, 6, 10], 
                                [6, 7, 11], [6, 10, 11], [7, 8, 11], 
                                [8, 9, 11], [9, 10, 11]])
        
        xpct_positions = np.array([[-1.        ,  1.61803399,  0.        ],
                                [ 1.        ,  1.61803399,  0.        ],
                                [ 0.        ,  1.        , -1.61803399],
                                [-1.61803399,  0.        , -1.        ],
                                [-1.61803399,  0.        ,  1.        ],
                                [ 0.        ,  1.        ,  1.61803399],
                                [ 1.61803399,  0.        ,  1.        ],
                                [ 1.61803399,  0.        , -1.        ],
                                [ 0.        , -1.        , -1.61803399],
                                [-1.        , -1.61803399,  0.        ],
                                [ 0.        , -1.        ,  1.61803399],
                                [ 1.        , -1.61803399,  0.        ]])
        
        np.testing.assert_array_equal(simplices, xpct_simplices)
        np.testing.assert_array_almost_equal(positions, xpct_positions)


    def test_read_vtk_wrong_file_type(self) -> None:
        wrong_path = self.data_folder + 'icosahedron_complex.vtu'
        simplices, positions, property = read_vtk(wrong_path)
        
        self.assertIsNone(simplices)
        self.assertIsNone(positions)
        self.assertIsNone(property)

    def test_valid(self) -> None:
        self.assertTrue(_valid(self.correct_path))
        self.assertFalse(_valid(self.wrong_path))

    def test_cells_circumcenters(self) -> None:
        xpct_cell_ccntrs = np.array([[0, 0, np.mean([0, 1, 2, 3, 4, 5])],
                                     [0, 0, np.mean([2, 3, 6, 7, 8, 9])],
                                     [0, 0, np.mean([4, 3, 9, 10, 11, 12])]])
        
        positions = np.vstack((np.zeros((2, 13)), np.arange(13))).T
        
        cell_ccntrs = _cells_circumcenters(positions, self.rawdata)
        
        assert isinstance(cell_ccntrs, np.ndarray)
        assert cell_ccntrs.shape == (3,3)
        np.testing.assert_array_almost_equal(cell_ccntrs, xpct_cell_ccntrs)
    
    def test_vertex_indices_around_cells(self)-> None:
        xpct_cell_brd_vids = [[0, 1, 2, 3, 4, 5], 
                              [2, 3, 6, 8, 7, 9], 
                              [4, 3, 9, 10, 11, 12]]
        
        for row, xpct_row in zip(_vertex_indices_around_cells(self.rawdata_1),
                                xpct_cell_brd_vids):
            np.testing.assert_array_equal(row, xpct_row)

    def test_top_simplices_within(self) -> None:
        nsplx_indices = _top_simplices_within(self.ugrid_mfld)
        assert isinstance(nsplx_indices, np.ndarray)
        np.testing.assert_array_equal(nsplx_indices, 
                                      self.mfld[-1].vertex_indices)

    def test_top_simplices_within_1_cochain(self) -> None:
        nsplx_indices = _top_simplices_within(self.ugrid_cchn)
        np.testing.assert_array_equal(nsplx_indices, 
                                      self.cchn.complex[-1].vertex_indices)

    def test_data_is_valid(self) -> None:
        valid_data_name = ['Primal_1-cochain']
        wrong_data_name = ['Primal_1-cochain', 'Dual_2-cochain']

        self.assertTrue(_data_is_valid(valid_data_name))
        self.assertFalse(_data_is_valid(wrong_data_name))

    def test_data_within_noData(self) -> None:
        complex_path = self.data_folder + 'icosahedron_complex.vtk'
        complex_ugrid = pv.read(complex_path, file_format='vtk')

        self.assertIsNone(_data_within(complex_ugrid))
    
    def test_data_within_dim0(self) -> None:
        cochain_path = self.data_folder + 'icosahedron_unit_0_cochain.vtk'
        cochain_ugrid = pv.read(cochain_path, file_format='vtk')

        property = _data_within(cochain_ugrid)
        
        self.assertIsInstance(property, Property)
        self.assertEqual(property.name, 'Primal 0-cochain')
        self.assertEqual(property.dim, 0)
        np.testing.assert_array_almost_equal(property.values, np.ones(12))

    def test_data_within_dim1(self) -> None:
        cochain_path = self.data_folder + 'icosahedron_unit_1_cochain.vtk'
        cochain_ugrid = pv.read(cochain_path, file_format='vtk')

        property = _data_within(cochain_ugrid)

        self.assertIsInstance(property, Property)
        self.assertEqual(property.name, 'Primal 1-cochain')
        self.assertEqual(property.dim, 1)
        np.testing.assert_array_almost_equal(property.values, np.ones(30))

    def test_data_within_dim2(self) -> None:
        cochain_path = self.data_folder + 'icosahedron_unit_2_cochain.vtk'
        cochain_ugrid = pv.read(cochain_path, file_format='vtk')

        property = _data_within(cochain_ugrid)

        self.assertIsInstance(property, Property)
        self.assertEqual(property.name, 'Primal 2-cochain')
        self.assertEqual(property.dim, 2)
        np.testing.assert_array_almost_equal(property.values, np.ones(20))

                        