import unittest
import numpy as np
import pyvista as pv

from dxtr.complexes import icosahedron
from dxtr.cochains import Cochain, random_cochain, normal_vector_field
from dxtr.visu.visu_pyvista import (_set_layout, _isvalid_for_visualization, 
                                    visualize_with_pyvista)

class TestVisuPyvista(unittest.TestCase):

    def setUp(self) -> None:
        """Initiates the test.
        """
        self.icosa = icosahedron(manifold=True)
        self.cochains = [random_cochain(dim=k) for k in range(3)]
        self.dual_cochains = [random_cochain(dim=k, dual=True) 
                              for k in range(3)]
        self.ill_4d_cochain = random_cochain(dim=2)
        self.ill_4d_cochain._dim = 4
        self.normals = [normal_vector_field(), normal_vector_field(dual=True)]

    def tearDown(self) -> None:
        """Concludes and closes the test.
        """

    def test_set_layout(self) -> None:
        
        # Checking default values
        fig = _set_layout(pv.Plotter())
        self.assertEqual(fig.background_color.name, 'white')

        # Enforcing custom parameters
        parameters = {'background_color': 'k',
                      'window_size': [100, 20],
                      'title': 'Mithrandir'}
        
        fig = _set_layout(pv.Plotter(), parameters)
        self.assertEqual(fig.background_color.name, 'black')
        np.testing.assert_array_equal(fig.window_size, 
                                      parameters['window_size'])

    
    def test_isvalid_for_visualization(self) -> None:

        # Check fig type
        fig = pv.UnstructuredGrid()
        self.assertFalse(_isvalid_for_visualization(self.cochains[1], fig))
        
        # Scalar-valued cochains
        for cochain in self.cochains:
            self.assertTrue(_isvalid_for_visualization(cochain, pv.Plotter()))

        # Check dim of cochain
        self.assertFalse(_isvalid_for_visualization(self.ill_4d_cochain,
                                                     pv.Plotter()))
        
        # Check of shape of the values
        fig = pv.Plotter()

        k = 1
        Nk = self.icosa[k].size

        values = np.arange(3*Nk).reshape((Nk, 3))
        cchn = Cochain(self.icosa, values=values, dim=k)
        self.assertTrue(_isvalid_for_visualization(cchn, fig))
        
        values = np.arange(4*Nk).reshape((Nk, 4))
        cchn = Cochain(self.icosa, values=values, dim=k)
        self.assertFalse(_isvalid_for_visualization(cchn, fig))

        values = np.arange(4*Nk).reshape((Nk, 2, 2))
        cchn = Cochain(self.icosa, values=values, dim=k)
        self.assertFalse(_isvalid_for_visualization(cchn, fig))
        

    def test_visualize_scalar_valued_cochain(self) -> None:
        # Primal cochains
        for cchn in self.cochains:
            fig = visualize_with_pyvista(cchn, display=False)
            self.assertIsInstance(fig, pv.Plotter)
        
        # Dual cochains
        for cchn in self.dual_cochains:
            fig = visualize_with_pyvista(cchn, display=False)
            self.assertIsInstance(fig, pv.Plotter)

    def test_visualize_vector_valued_cochain(self) -> None:
        for nrl in self.normals:
            fig = visualize_with_pyvista(nrl, display=False)
            self.assertIsInstance(fig, pv.Plotter)



    # def test_generate_pvdata_from_complex(self) -> None:
    #     # test primal complex.
    #     cplx = self.sph_cplx
    #     pvdata = generate_pvdata_from_complex(cplx)
        
    #     self.assertIsInstance(pvdata, pv.UnstructuredGrid)
    #     self.assertEqual(pvdata.n_points, cplx[0].size)
    #     self.assertEqual(pvdata.n_cells, cplx[-1].size)
    #     np.testing.assert_allclose(pvdata.points, cplx[0].vertices)
        
    #     # test on manifold
    #     cplx = self.sph_mfld
    #     pvdata = generate_pvdata_from_complex(cplx)
        
    #     self.assertIsInstance(pvdata, pv.UnstructuredGrid)
    #     self.assertEqual(pvdata.n_points, cplx[0].size)
    #     self.assertEqual(pvdata.n_cells, cplx[-1].size)
    #     np.testing.assert_allclose(pvdata.points, cplx[0].vertices)


    # def test_generate_pvdata_from_dual_complex(self) -> None:
    #     mfld = self.icosahedron 
    #     pvdata = generate_pvdata_from_dual_complex(mfld)
        
    #     self.assertIsInstance(pvdata, pv.UnstructuredGrid)
    #     self.assertEqual(pvdata.n_points, mfld[-1].size)
    #     self.assertEqual(pvdata.n_cells, mfld[0].size)
    #     np.testing.assert_allclose(pvdata.points, mfld[-1].circumcenters)

    #     xpct_pvdata_cells = np.array([5, 0,  1,  4,  3,  2,  5,  0,  1,  6,  7, 
    #                                   5,  5,  0,  2,  8,  9,  5,  5,  2,  8, 11, 
    #                                   10,  3, 5,  3, 10, 13, 12,  4,  5,  1,  4, 
    #                                   12, 14,  6,  5,  6, 14, 16, 15,  7, 5, 5,  
    #                                   9, 17, 15,  7, 5,  8,  9, 17, 18, 11,  5, 
    #                                   10, 11, 18, 19, 13,  5, 12, 13,19,16, 14,  
    #                                   5, 15, 16, 19, 18, 17])
    
    #     np.testing.assert_array_equal(pvdata.cells, xpct_pvdata_cells)


    # def test_generate_pvdata_from_manifold(self)-> None:        
    #     # test primal complex.
    #     cplx = self.sph_cplx
    #     pvdata = generate_pvdata_from_manifold(cplx)
        
    #     self.assertIsInstance(pvdata, pv.UnstructuredGrid)
    #     self.assertEqual(pvdata.n_points, cplx[0].size)
    #     self.assertEqual(pvdata.n_cells, cplx[-1].size)

    #     np.testing.assert_allclose(np.sort(pvdata.points, axis=0),
    #                             np.sort(cplx[0].vertices, axis=0))
        
    #     # test dual complex
    #     mfld = self.icosahedron
    #     pvdata = generate_pvdata_from_manifold(mfld, display='dual')
        
    #     self.assertIsInstance(pvdata, pv.UnstructuredGrid)
    #     self.assertEqual(pvdata.n_points, mfld[-1].size)
    #     self.assertEqual(pvdata.n_cells, mfld[0].size)

    #     np.testing.assert_allclose(pvdata.points, mfld[-1].circumcenters)

    #     xpct_pvdata_cells = np.array([5,  0,  1,  4,  3,  2,  5,  0,  1,  6,  7, 
    #                                   5,  5,  0,  2,  8,  9,  5,  5,  2,  8, 11, 
    #                                   10,  3, 5,  3, 10, 13, 12,  4,  5,  1,  4, 
    #                                   12, 14,  6,  5,  6, 14, 16, 15,  7,  5,
    #                                   5,  9, 17, 15,  7, 5,  8,  9, 17, 18, 11, 
    #                                   5, 10, 11, 18, 19, 13,  5, 12, 13, 19, 16,
    #                                   14, 5, 15, 16, 19, 18, 17])
    #     np.testing.assert_array_equal(pvdata.cells, xpct_pvdata_cells)

    #     # test primal and dual complex
    #     mfld = self.sph_mfld
    #     pvdata = generate_pvdata_from_manifold(mfld, display='all')

    #     for data in pvdata.values():
    #         self.assertIsInstance(data, pv.UnstructuredGrid)
        
    #     self.assertEqual(pvdata['primal'].n_points, pvdata['dual'].n_cells,)
    #     self.assertEqual(pvdata['dual'].n_cells,mfld[0].size )
    #     self.assertEqual(pvdata['primal'].n_cells, pvdata['dual'].n_points,)
    #     self.assertEqual(pvdata['dual'].n_points,mfld[-1].size)
        

    # def test_isvalid_for_visualization(self) -> None:
    #     self.assertFalse(isvalid_for_visualization(self.abstract_complex))
    #     self.assertTrue(isvalid_for_visualization(self.sph_cplx))
    #     self.assertTrue(isvalid_for_visualization(self.sph_mfld))
    #     self.assertFalse(isvalid_for_visualization(np.ones(10)))
