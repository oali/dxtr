import unittest
import numpy as np
from plotly import graph_objects as go 

from dxtr.complexes import (Simplex, AbstractSimplicialComplex, icosahedron,
                            triangular_grid)
from dxtr.visu.visu_plotly import (visualize_with_plotly,
                                   _generate_plot_data_manifold,
                                   _generate_plot_data_complex,
                                   _generate_plot_data_simplex,
                                   _generate_point_data,
                                   _generate_wireframe_data,
                                   _generate_surface_data,
                                   _compute_border_lines)

class TestVisuPlotly(unittest.TestCase):

    def setUp(self) -> None:
        """Initiates the test.
        """
        self.complex = icosahedron()
        self.abstract_complex = AbstractSimplicialComplex([[0,1,2],[1,2,3]])
        self.manifold = icosahedron(manifold=True)
        self.open_manifold = triangular_grid(manifold=True)
        self.simplex = Simplex(indices=[0, 1, 2, 3], 
                               _vertices=np.array([[0,0,0],
                                                   [1,0,0],
                                                   [0,1,0],
                                                   [0,0,1]]))
        
    def tearDown(self) -> None:
        """Concludes and closes the test.
        """

    def test_visualize_with_plotly(self) -> None:

        fig = visualize_with_plotly(self.complex, display=False)
        self.assertIsInstance(fig, go.Figure)

        nofig = visualize_with_plotly(self.abstract_complex, display=False)
        self.assertIsNone(nofig)

    def test_generate_plot_data_manifold(self) -> None:
        mfld = self.manifold
        n = mfld.dim
        
        xpct_data_len = {'primal': n+1, 'dual': n, 'all': 2*n+1}
        for display, xpct_len in xpct_data_len.items():
            data = _generate_plot_data_manifold(mfld, show=display)
            print(display, xpct_len, len(data))
            self.assertIsInstance(data, list)
            self.assertEqual(len(data), xpct_len)
    
    def test_generate_plot_data_complex(self) -> None:
        cplx = self.complex
        n = cplx.dim
        
        for display in ['primal', 'dual', 'all']:
            data = _generate_plot_data_complex(cplx, display=display)
            self.assertIsInstance(data, list)
            self.assertEqual(len(data), n+1)
        
        data = _generate_plot_data_complex(cplx,degrees=2)
        self.assertEqual(len(data), 1)
        self.assertIsInstance(data[0], go.Mesh3d)
   
    def test_generate_plot_data_complex_subset(self) -> None:
        cplx = self.complex
        points = cplx[0].vertices

        data = _generate_plot_data_complex(cplx, subset={0:[0], 1:[0], 2:[0]})
        self.assertEqual(len(data), 6)
        self.assertIsInstance(data[1], go.Scatter3d)
        self.assertEqual(data[1]['marker']['color'], 'orange')

        coords = ['x','y','z']
        for i, x in enumerate(coords):
            np.testing.assert_array_almost_equal(data[1][x], points[0,i])
        
        self.assertIsInstance(data[3], go.Scatter3d)
        self.assertEqual(data[3]['line']['color'], 'orange')
        for x in coords:
            self.assertEqual(len(data[3][x]), 4)
        
        self.assertIsInstance(data[-1], go.Mesh3d)
        self.assertEqual(data[-1]['color'], 'orange')

    def test_generate_plot_data_simplex(self) -> None:
        splx = self.simplex
        data = _generate_plot_data_simplex(splx)

        self.assertIsInstance(data, list)
        self.assertIsInstance(data[0], go.Scatter3d)
        self.assertIsInstance(data[1], go.Scatter3d)
        self.assertIsInstance(data[2], go.Mesh3d)
    
    def test_generate_point_data_on_manifold(self) -> None:
        mfld = self.manifold
        points = mfld[0].vertices

        data = _generate_point_data(mfld)
        self.assertIsInstance(data, go.Scatter3d)

        for i, var in enumerate(['x','y','z']):
            np.testing.assert_array_almost_equal(data[var], points[:,i])

        self.assertEqual(data['name'], '0-simplices')
        self.assertEqual(data['mode'], 'markers')
        self.assertEqual(data['marker']['color'], 'darkblue')
        self.assertEqual(data['marker']['symbol'], 'circle')
        self.assertEqual(data['marker']['size'], 10)
        self.assertEqual(data['marker']['line']['width'], 0)

        # Change the name
        custom_name = 'Galadriel'
        data_name = _generate_point_data(mfld, name=custom_name)
        self.assertEqual(data_name['name'], custom_name)

        # Change a keyword parameter
        custom_size = 30
        data_kwarg = _generate_point_data(mfld, size=custom_size)
        self.assertEqual(data_kwarg['marker']['size'], custom_size)

        # On dual Complex
        bayrcenters = mfld[-1].circumcenters
        data_dual = _generate_point_data(mfld, dual=True)
        self.assertIsInstance(data, go.Scatter3d)

        for i, var in enumerate(['x','y','z']):
            np.testing.assert_array_almost_equal(data_dual[var], 
                                                 bayrcenters[:,i])

        self.assertEqual(data_dual['name'], '0-cells')

        # Focus on a subset
        data_subset = _generate_point_data(mfld, subset=[0])
        self.assertIsInstance(data_subset, go.Scatter3d)
        self.assertEqual(len(data_subset['x']), 1)

    def test_generate_point_data_on_simplex(self) -> None:
        splx = self.simplex

        data_splx = _generate_point_data(splx)
        self.assertIsInstance(data_splx, go.Scatter3d)

        for i, var in enumerate(['x','y','z']):
            np.testing.assert_array_almost_equal(data_splx[var], 
                                                 splx.vertices[:,i])

        self.assertEqual(data_splx['name'], 'vertices')
    
    def test_generate_wireframe_data_on_manifold(self) -> None:
        mfld = self.manifold
        
        data = _generate_wireframe_data(mfld)
        
        self.assertIsInstance(data, go.Scatter3d)

        self.assertEqual(data['name'], '1-simplices')
        self.assertEqual(data['mode'], 'lines')
        self.assertEqual(data['line']['color'], 'darkblue')
        self.assertEqual(data['line']['width'], 5)

        # On dual 
        data = _generate_wireframe_data(mfld, dual=True)
        
        self.assertIsInstance(data, go.Scatter3d)

        self.assertEqual(data['name'], '1-cells')
        self.assertEqual(data['mode'], 'lines')
        self.assertEqual(data['line']['color'], 'darkblue')
        self.assertEqual(data['line']['width'], 5)

        # On subset
        data = _generate_wireframe_data(mfld, subset=[0])
        self.assertIsInstance(data, go.Scatter3d)
        
    def test_generate_wireframe_data_on_open_manifold(self) -> None:
        mfld = self.open_manifold
        N1 = len(mfld.interior()[1])
        N2 = len(mfld.border()[1])

        data = _generate_wireframe_data(mfld, dual=True)
        self.assertIsInstance(data, go.Scatter3d)

        for i in ['x', 'y', 'z']:
            self.assertEqual(len(data[i]), 2 * (2*N1+ 4*N2))

    def test_generate_wireframe_data_on_simplex(self) -> None:
        splx = self.simplex
        
        data = _generate_wireframe_data(splx)
        
        self.assertIsInstance(data, go.Scatter3d)

        self.assertEqual(data['name'], 'edges')
        self.assertEqual(data['mode'], 'lines')
        self.assertEqual(data['line']['color'], 'darkblue')
        self.assertEqual(data['line']['width'], 5)

    def test_generate_surface_data(self) -> None:
        mfld = self.manifold
        points = mfld[0].vertices        

        data = _generate_surface_data(mfld)
        self.assertIsInstance(data, go.Mesh3d)

        for i, var in enumerate(['x','y','z']):
            np.testing.assert_array_almost_equal(data[var], points[:,i])

        self.assertEqual(data['name'], '2-simplices')
        self.assertEqual(data['color'], 'darkblue')
        self.assertEqual(data['opacity'], .33)

        # On subset
        data = _generate_surface_data(mfld, subset=[0])
        self.assertIsInstance(data, go.Mesh3d)

        # With an intensity:
        values = np.ones(mfld[2].size)
        data = _generate_surface_data(mfld, intensity=values)
        
        self.assertIsInstance(data, go.Mesh3d)
        self.assertEqual(data['intensitymode'], 'cell')
        np.testing.assert_array_equal(data['intensity'], values)
    
    def test_generate_surface_data_on_simplex(self) -> None:
        splx = self.simplex
        
        data = _generate_surface_data(splx)
        
        self.assertIsInstance(data, go.Mesh3d)
        for i, var in enumerate(['x','y','z']):
            np.testing.assert_array_almost_equal(data[var], splx.vertices[:,i])

        self.assertEqual(data['name'], 'faces')
        self.assertEqual(data['color'], 'darkblue')
        self.assertEqual(data['opacity'], .33)

    def test_compute_border_lines(self) -> None:
        
        mfld = self.open_manifold
        N = len(mfld.border()[1])
        
        brd = _compute_border_lines(mfld)
        
        self.assertIsInstance(brd, np.ndarray)
        self.assertEqual(brd.shape, (2*N, 2, 3))
        self.assertEqual(brd.ndim, 3)

        xpct_brd = np.array([[[0.         , 0.        , 0.        ],
                              [1.         , 0.        , 0.        ]],
                             [[0.         , 0.        , 0.        ],
                              [0.5        , 0.8660254 , 0.        ]],
                             [[1.         , 0.        , 0.        ],
                              [2.         , 0.        , 0.        ]],
                             [[2.         , 0.        , 0.        ],
                              [3.         , 0.        , 0.        ]],
                             [[3.         , 0.        , 0.        ],
                              [3.5        , 0.8660254 , 0.        ]],
                             [[0.5        , 0.8660254 , 0.        ],
                              [1.         , 1.73205081, 0.        ]],
                             [[3.5        , 0.8660254 , 0.        ],
                              [4.         , 1.73205081, 0.        ]],
                             [[1.         , 1.73205081, 0.        ],
                              [1.5        , 2.59807621, 0.        ]],
                             [[4.         , 1.73205081, 0.        ],
                              [4.5        , 2.59807621, 0.        ]],
                             [[1.5        , 2.59807621, 0.        ],
                              [2.5        , 2.59807621, 0.        ]],
                             [[2.5        , 2.59807621, 0.        ],
                              [3.5        , 2.59807621, 0.        ]],
                             [[3.5        , 2.59807621, 0.        ],
                              [4.5        , 2.59807621, 0.        ]],
                             [[0.5        , 0.28867513, 0.        ],
                              [0.5        , 0.        , 0.        ]],
                             [[0.5        , 0.28867513, 0.        ],
                              [0.25       , 0.4330127 , 0.        ]],
                             [[1.5        , 0.28867513, 0.        ],
                              [1.5        , 0.        , 0.        ]],
                             [[2.5        , 0.28867513, 0.        ],
                              [2.5        , 0.        , 0.        ]],
                             [[3.         , 0.57735027, 0.        ],
                              [3.25       , 0.4330127 , 0.        ]],
                             [[1.         , 1.15470054, 0.        ],
                              [0.75       , 1.29903811, 0.        ]],
                             [[3.5        , 1.44337567, 0.        ],
                              [3.75       , 1.29903811, 0.        ]],
                             [[1.5        , 2.02072594, 0.        ],
                              [1.25       , 2.16506351, 0.        ]],
                             [[4.         , 2.30940108, 0.        ],
                              [4.25       , 2.16506351, 0.        ]],
                             [[2.         , 2.30940108, 0.        ],
                              [2.         , 2.59807621, 0.        ]],
                             [[3.         , 2.30940108, 0.        ],
                              [3.         , 2.59807621, 0.        ]],
                             [[4.         , 2.30940108, 0.        ],
                              [4.         , 2.59807621, 0.        ]]])
        
        np.testing.assert_array_almost_equal(brd, xpct_brd)