from tkinter import W
import unittest
import os
import numpy as np
import scipy.special as spcl

from copy import deepcopy
from dxtr.complexes import SimplicialComplex

class TestSimplicialComplex(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """

        # Structures to run tests on
        sample_0d = [[0]]
        sample_1d = [[0, 1]]
        sample_2d = [[0, 1, 2]]
        sample_3d = [[0, 1, 2, 3]]
        sample_4d = [[0, 1, 2, 3, 4]]
        sample_5d = [[0, 1, 2, 3, 4, 5]]
        
        sample_square = [[1, 2, 5],
                         [2, 3, 5],
                         [3, 4, 5],
                         [4, 1, 5]]

        sample_2tetras = [[0, 1, 2, 3],
                          [1, 2, 3, 4]]
        
        sample_non_pure = [[1, 2, 3, 4],
                           [2, 3, 4, 5],
                           [5, 6, 7],
                           [12, 34],
                           [548]]
        
        sample_hollow_tetra = [[4, 5, 6],
                               [4, 5, 7],
                               [4, 6, 7],
                               [5, 6, 7]]
        
        sample_hollow_square = [[1,2],
                                [2,3],
                                [3,4],
                                [4,1]]

        self.vertices = [[0, 0, 0],
                         [1, 0, 0],
                         [0, 1, 0],
                         [0, 0, 1],
                         [0, 0, -1]]

        self.samples = [sample_0d, sample_1d, sample_2d, 
                        sample_3d, sample_4d, sample_5d,
                        sample_square, sample_2tetras,
                        sample_non_pure, sample_hollow_tetra,
                        sample_hollow_square]

        self.abstract_complexes = [SimplicialComplex(smpl)
                                   for smpl in self.samples]

        self.non_abstract_complex = SimplicialComplex(sample_2tetras, 
                                                      self.vertices)

        self.path = os.getcwd() 
        self.path += '/src/dxtr/utils/mesh_examples/sphere_small.ply'


    def tearDown(self):
        """Concludes and closes the test.
        """
    

    def test_SimplicialComplex__str__(self) -> None:
        cplx = self.non_abstract_complex
        self.assertEqual(cplx.__str__(), 
            '3D Simplicial Complex of shape (5, 9, 7, 2), embedded in R^3.')


    def test_SimplicialComplex_from_file(self) -> None:
        cplx = SimplicialComplex.from_file(self.path)

        self.assertIsInstance(cplx, SimplicialComplex)


    def test_SimplicialComplex_isclosed(self) -> None:
        
        for cplx in self.abstract_complexes[:-2]:
            self.assertFalse(cplx.isclosed)
        
        for cplx in self.abstract_complexes[-2:]:
            self.assertTrue(cplx.isclosed)


    def test_SimplicialComplex_ispure(self) -> None:
        
        for cplx in self.abstract_complexes[:-3]+self.abstract_complexes[-2:]:
            self.assertTrue(cplx.ispure)
        
        self.assertFalse(self.abstract_complexes[-3].ispure)


    def test_SimplicialComplex_name(self) -> None:
        self.assertEqual(self.non_abstract_complex.name,
                         '3D Simplicial Complex')


    def test_SimplicialComplex_name_setter(self) -> None:
        self.non_abstract_complex.name = 'cplx'
        self.assertEqual(self.non_abstract_complex.name,
                         'cplx')


    def test_SimplicialComplex_isabstract(self) -> None:
        
        for cplx in self.abstract_complexes:
            self.assertTrue(cplx.isabstract)

        self.assertFalse(self.non_abstract_complex.isabstract)


    def test_SimplicialComplex_dim(self) -> None:
        
        for cplx, ids in zip(self.abstract_complexes, self.samples):
            self.assertEqual(cplx.dim, len(ids[0])-1)


    def test_SimplicialComplex_emb_dim(self) -> None:

        for cplx in self.abstract_complexes:
            self.assertIsNone(cplx.emb_dim)
        
        self.assertEqual(self.non_abstract_complex.emb_dim, 3)


    def test_SimplicialComplex_shape(self) -> None:
        n = 5
        for n, cplx in enumerate(self.abstract_complexes[1:n+1]):
            binomial_coefs = tuple(int(spcl.comb(n+2, k)) for k in range(1, n+2))
            self.assertEqual(cplx.shape[:-1], binomial_coefs)


    def test_SimplicialComplex_orientation(self) -> None:

        for cplx in self.abstract_complexes[1:]:
            self.assertEqual(cplx.orientation().shape[0], cplx.shape[-1])

        self.assertIsNone(self.abstract_complexes[0].orientation())

        for cplx in self.abstract_complexes[1:6]:
            self.assertEqual(cplx.orientation()[0], 1)


    def test_SimplicialComplex_euler_characteristic(self) -> None:

        for cplx in self.abstract_complexes:
            xpct_value = sum([(-1)**i * nbr_splx for i, nbr_splx
                              in  enumerate(cplx.shape)])

            self.assertEqual(cplx.euler_characteristic, xpct_value)


    def test_SimplicialComplex_vertices(self) -> None:

        expected_vertices = {0: np.array([[0,  0,  0],
                                          [1,  0,  0],
                                          [0,  1,  0],
                                          [0,  0,  1],
                                          [0,  0, -1]]),
                            1: np.array([[[0,  0,  0],
                                          [1,  0,  0]],
                                         [[0,  0,  0],
                                          [0,  1,  0]],
                                         [[0,  0,  0],
                                          [0,  0,  1]],
                                         [[1,  0,  0],
                                          [0,  1,  0]],
                                         [[1,  0,  0],
                                          [0,  0,  1]],
                                         [[1,  0,  0],
                                          [0,  0, -1]],
                                         [[0,  1,  0],
                                          [0,  0,  1]],
                                         [[0,  1,  0],
                                          [0,  0, -1]],
                                         [[0,  0,  1],
                                          [0,  0, -1]]]),
                            2: np.array([[[0,  0,  0],
                                          [1,  0,  0],
                                          [0,  1,  0]],
                                         [[0,  0,  0],
                                          [1,  0,  0],
                                          [0,  0,  1]],
                                         [[0,  0,  0],
                                          [0,  1,  0],
                                          [0,  0,  1]],
                                         [[1,  0,  0],
                                          [0,  1,  0],
                                          [0,  0,  1]],
                                         [[1,  0,  0],
                                          [0,  1,  0],
                                          [0,  0, -1]],
                                         [[1,  0,  0],
                                          [0,  0,  1],
                                          [0,  0, -1]],
                                         [[0,  1,  0],
                                          [0,  0,  1],
                                          [0,  0, -1]]]),
                            3: np.array([[[0,  0,  0],
                                          [1,  0,  0],
                                          [0,  1,  0],
                                          [0,  0,  1]],
                                         [[1,  0,  0],
                                          [0,  1,  0],
                                          [0,  0,  1],
                                          [0,  0, -1]]])}
  
        for k, mdl in enumerate(self.non_abstract_complex):
            print(k)
            print(mdl.vertices)
            np.testing.assert_almost_equal(mdl.vertices, expected_vertices[k])

        np.testing.assert_array_equal(self.non_abstract_complex.vertices,
                                      expected_vertices[0])

    def test_SimplicialComplex_update_geometry(self) -> None:
        indices = [[0, 1, 2]]

        f = lambda i: np.array([np.cos(2*np.pi/3*i),
                                np.sin(2*np.pi/3*i),
                                0])

        vertices = [f(i) for i in np.arange(3)]

        cplx = SimplicialComplex(indices, vertices)

        initial_cplx_id =  id(cplx._vertices)
        initial_volumes = {k: mdl.volumes 
                           for k,mdl in enumerate(deepcopy(cplx))}
        
        displacement = dict(zip(indices[0], vertices))
        
        cplx.update_geometry(displacement)

        self.assertEqual(id(cplx._vertices), initial_cplx_id)
        np.testing.assert_allclose(cplx._vertices, 2*np.asarray(vertices))

        for k, mdl in enumerate(cplx):
            np.testing.assert_allclose(mdl.volumes, 2**k*initial_volumes[k])
