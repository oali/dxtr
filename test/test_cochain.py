import unittest
import numpy as np
import numpy.linalg as lng
import scipy.sparse as sp

from typing import Generator
from pathlib import Path

from dxtr.complexes import SimplicialComplex, SimplicialManifold, icosahedron
from dxtr.cochains.cochain import Cochain, _format_indices_values, cochain_base

class TestCochain(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        root = Path().cwd()
        file_name = Path('icosahedron_unit_1_cochain.vtk')

        self.path_to_cochain = root /Path('src/dxtr/utils/mesh_examples')/ file_name
        self.cplx = icosahedron() 
        self.mfld = icosahedron(manifold=True) 

        indices = [[0, 1, 2], [0, 2, 3],
                   [0, 3, 4], [0, 4, 5],
                   [0, 5, 1], # upper dome ends here
                   [1, 7, 2],
                   [2, 8, 3], [3, 9, 4],
                   [4, 10, 5], [5, 6, 1],  # upper belt ends here
                   [7, 2, 8], [8, 3, 9],
                   [9, 4, 10], [10, 5, 6],
                   [6, 1, 7],  # lower belt ends here 
                   [6, 7, 11],
                   [7, 8, 11], [8, 9, 11],
                   [9, 10, 11], [10, 6, 11]]  # lower dome ends here
        
        self.abs_cplx = SimplicialComplex(indices)

    def tearDown(self):
        """Concludes and closes the test.
        """

    def test_cochain_instantiation(self) -> None:
        mfld = self.mfld

        cchn = Cochain(mfld, 0)

        self.assertIsInstance(cchn, Cochain)
        self.assertEqual(cchn.complex, mfld)
        self.assertEqual(cchn.dim, 0)
        self.assertIsNone(cchn.values)
        self.assertIsNone(cchn.shape)
        self.assertFalse(cchn.isdual)

        for k, Nk in enumerate(mfld.shape):
            cchn = Cochain(mfld, dim=k, values=k)
            np.testing.assert_array_equal(cchn.values,
                                          k*np.ones(Nk))
        
        cchn = Cochain(mfld, dim=k, values='values')
        self.assertIsNone(cchn.values)

    def test_cochain_instantiation_from_dict(self) -> None:
        mfld = self.mfld
        vals = {0: np.ones(3), 3:np.ones(3)}
        cchn = Cochain(mfld, 0, vals)

        self.assertIsInstance(cchn, Cochain)
        np.testing.assert_array_equal(cchn.values, np.ones((2,3)))

    def test_cochain_from_file(self) -> None:

        cchn = Cochain.from_file(self.path_to_cochain)

        self.assertIsInstance(cchn, Cochain)
        self.assertIsInstance(cchn.complex, SimplicialManifold)
        self.assertEqual(cchn.dim, 1)
        self.assertFalse(cchn.isdual)
        self.assertEqual(cchn.name, 'Primal 1-cochain')
        np.testing.assert_array_equal(cchn.values, np.ones(30))

    def test_cochain__str__(self) -> None:
        mfld = self.mfld
        cchn = Cochain(mfld, dim=1)

        self.assertEqual(cchn.__str__(), 
        f'{cchn.name}, defined on {mfld.name}.')

    def test_cochain_name(self) -> None:
        mfld = self.mfld
        k = 1
        Nk = mfld.shape[k] 
        cchn = Cochain(mfld, dim=k, values=np.ones(Nk))
        self.assertEqual(cchn.name, f'Primal {k}-cochain')
        
        cchn = Cochain(mfld, dim=k, values=np.ones(Nk),
                       name='Gandalf the White!')
        self.assertEqual(cchn.name, 'Gandalf the White!')

    def test_cochain_indices(self) -> None:
        mfld = self.mfld

        for k, N in enumerate(mfld.shape):
            vals = np.ones(N)
            cchn = Cochain(mfld, values=vals, dim=k)

            xpct_ids = list(range(N))
            np.testing.assert_array_equal(cchn.indices, xpct_ids)

    def test_cochain_values(self) -> None:
        mfld = self.mfld

        for k, N in enumerate(mfld.shape):
            vals = np.ones(N)
            cchn = Cochain(mfld, values=vals, dim=k)

            np.testing.assert_array_equal(cchn.values, vals)
    
    def test_cochain_shape(self) -> None:
        mfld = self.mfld

        self.assertIsNone(Cochain(mfld, 0).shape)

        for k, N in enumerate(mfld.shape):
            vals = sp.identity(N)
            cchn = Cochain(mfld, values=vals, dim=k)

            self.assertEqual(cchn.shape, vals.shape)
    
    def test_cochain_add(self) -> None:
        mfld = self.mfld
        
        for k, Nk in enumerate(mfld.shape):
            vals = np.ones(Nk)
            c1 = Cochain(mfld, values=vals, dim=k)
            c2 = Cochain(mfld, values=2*vals, dim=k)

            c3 = c1 + c2

            self.assertIsInstance(c3, Cochain)
            np.testing.assert_almost_equal(c3.values, 3*vals)
            self.assertEqual(c3.complex, mfld)
            self.assertEqual(c3.dim, k)
            self.assertEqual(c3.shape, vals.shape)

            c1 += c2

            np.testing.assert_almost_equal(c1.values, 3*vals)

            c4 = Cochain(mfld, values=sp.diags(np.ones(Nk)), dim=k)
            self.assertIsNone(c4 + c1)

    def test_cochain_sub(self) -> None:
        mfld = self.mfld
        
        for k, N in enumerate(mfld.shape):
            vals = np.ones(N)
            c1 = Cochain(mfld, values=vals, dim=k)
            c2 = Cochain(mfld, values=2*vals, dim=k)

            c3 = c1 - c2

            self.assertIsInstance(c3, Cochain)
            np.testing.assert_almost_equal(c3.values, -1*vals)
            self.assertEqual(c3.complex, mfld)
            self.assertEqual(c3.dim, k)
            self.assertEqual(c3.shape, vals.shape)

            c1 -= c2

            np.testing.assert_almost_equal(c1.values, -1*vals)

            # Check incompatible value types
            c4 = Cochain(mfld, values={0:1}, dim=k)
            c5 = c1 - c4
            self.assertIsNone(c5)

    def test_cochain_mul(self) -> None:
        mfld = self.mfld
        
        for k, Nk in enumerate(mfld.shape):
            vals = np.ones(Nk)
            c1 = Cochain(mfld, values=vals, dim=k)

            c2 = 2*c1

            self.assertIsInstance(c2, Cochain)
            np.testing.assert_almost_equal(c2.values, 2*vals)
            self.assertEqual(c2.complex, mfld)
            self.assertEqual(c2.dim, k)
            self.assertEqual(c2.shape, vals.shape)

            c1 *= 2

            np.testing.assert_almost_equal(c1.values, 2*vals)

            # With values as dict
            v1 = {0: 1}
            v2 = {0: 2}
            c1 = Cochain(mfld, values=v1, dim=k)
            c2 = Cochain(mfld, values=v2, dim=k)
            
            c3 = c2*c1

            np.testing.assert_array_equal(c3.indices, [0])
            np.testing.assert_array_equal(c3.values, [2])
            
            # Check incompatible value types
            c4 = Cochain(mfld, values=vals, dim=k)
            c5 = c4*c1
            self.assertIsNone(c5)

    def test_cochain_mul_2(self) -> None:
        mfld = self.mfld
        
        for k, N in enumerate(mfld.shape):

            # ndarray values
            vals = 2*np.ones(N)
            c1 = Cochain(mfld, values=vals, dim=k)

            vals = 3*np.ones(N)
            c2 = Cochain(mfld, values=vals, dim=k)

            c3 = c2 * c1

            self.assertIsInstance(c3, Cochain)
            np.testing.assert_almost_equal(c3.values, 6*np.ones(N))
            self.assertEqual(c3.complex, mfld)
            self.assertEqual(c3.dim, k)
            self.assertEqual(c3.shape, vals.shape)

            c1 *= c2

            np.testing.assert_almost_equal(c1.values, 6*np.ones(N))

            # sparse matrix values
            vals = 2*sp.identity(N)
            c1 = Cochain(mfld, values=vals, dim=k)

            vals = 3*sp.identity(N)
            c2 = Cochain(mfld, values=vals, dim=k)

            c3 = c2 * c1
            
            self.assertIsInstance(c3, Cochain)
            self.assertIsInstance(c3.values, sp.csr_matrix)
            np.testing.assert_almost_equal(c3.values.data, 6*np.ones(N))
            self.assertEqual(c3.complex, mfld)
            self.assertEqual(c3.dim, k)
            self.assertEqual(c3.shape, vals.shape)

            c1 *= c2

            self.assertIsInstance(c1.values, sp.csr_matrix)
            np.testing.assert_almost_equal(c1.values.data, 6*np.ones(N))

            # sparse matrix multiplication by scalar
            vals = 2*sp.identity(N)
            c1 = Cochain(mfld, values=vals, dim=k)

            c2 = 2 * c1
            self.assertIsInstance(c2, Cochain)
            self.assertIsInstance(c2.values, sp.csr_matrix)
            np.testing.assert_almost_equal(c2.values.data, 4*np.ones(N))
            self.assertEqual(c2.complex, mfld)
            self.assertEqual(c2.dim, k)
            self.assertEqual(c2.shape, vals.shape)

    def test_cochain_mul_array(self)-> None:
        mfld = self.mfld
        
        for k, Nk in enumerate(mfld.shape):
            c1 = Cochain(mfld, values=2*np.ones(Nk), dim=k)

            vals = 3*np.ones(Nk)

            c3 = c1 * vals

            self.assertIsInstance(c3, Cochain)
            np.testing.assert_almost_equal(c3.values, 6*np.ones(Nk))
            self.assertEqual(c3.complex, mfld)
            self.assertEqual(c3.dim, k)
            self.assertEqual(c3.shape, vals.shape)

            c1 *= vals

            np.testing.assert_almost_equal(c1.values, 6*np.ones(Nk))

    def test_cochain_rmul(self) -> None:
        mfld = self.mfld
        
        for k, N in enumerate(mfld.shape):
            vals = np.ones(N)
            c1 = Cochain(mfld, values=vals, dim=k)

            c2 = c1*2

            self.assertIsInstance(c2, Cochain)
            np.testing.assert_almost_equal(c2.values, 2*vals)
            self.assertEqual(c2.complex, mfld)
            self.assertEqual(c2.dim, k)
            self.assertEqual(c2.shape, vals.shape)
    
    def test_cochain_truediv_by_cochain(self) -> None:
        mfld = self.mfld
        
        for k, Nk in enumerate(mfld.shape):
            vals = 10 * np.ones(Nk)
            c1 = Cochain(mfld, values=vals, dim=k)
            c2 = Cochain(mfld, values=.5*vals, dim=k)

            self.assertIsNone(c1 / c2)

    def test_cochain_truediv_(self) -> None:
        mfld = self.mfld

        for k, Nk in enumerate(mfld.shape):
            vals = 10 * np.ones(Nk)
            c1 = Cochain(mfld, values=vals, dim=k)

            c2 = c1 / 2.5

            self.assertIsInstance(c2, Cochain)
            np.testing.assert_almost_equal(c2.values, vals/2.5)
            self.assertEqual(c2.complex, mfld)
            self.assertEqual(c2.dim, k)
            self.assertEqual(c2.shape, vals.shape)

            c1 /= 2
            np.testing.assert_almost_equal(c1.values, vals/2)

            # divide a scalar-valued `Cochain` by an array
            c1 /= .5 * np.ones(Nk)
            np.testing.assert_almost_equal(c1.values, vals)

            # dividing vector-valued cochain now
            vals = np.repeat([[2.4,0,0]], Nk, axis=0)
            c3 = Cochain(mfld, values=vals, dim=k)

            c4 = c3 / 2.4
            np.testing.assert_almost_equal(c4.values, vals / 2.4)
            
            c3 /= lng.norm(vals, axis=-1).reshape((Nk, 1))
            np.testing.assert_almost_equal(c3.values, vals / 2.4)

    def test_cochain_add_dict(self) -> None:
        mfld = self.mfld
        
        d1 = {1: [1, 2, 3],
              2: [1, 2, 3],
              3: [1, 2, 3]}

        d2 = {1: [4, 5, 6],
              2: [4, 5, 6],
              3: [4, 5, 6]}

        xpct_dict = {1: [5, 7, 9],
                     2: [5, 7, 9],
                     3: [5, 7, 9]}

        for k in range(mfld.dim):
            c1 = Cochain(mfld, values=d1, dim=k)
            c2 = Cochain(mfld, values=d2, dim=k)

            c3 = c1 + c2

            self.assertEqual(len(c3.values), len(xpct_dict))
            
            for idx, value in c3.items():
                self.assertIn(idx, xpct_dict.keys())
                np.testing.assert_array_equal(value, xpct_dict[idx])

    def test_cochain_add_spmatrix(self) -> None:
        mfld = self.mfld

        for k, Nk in enumerate(mfld.shape):
            mtrx = sp.identity(Nk)
            c1 = Cochain(mfld, values=mtrx, dim=k)
            c2 = Cochain(mfld, values=2*mtrx, dim=k)

            c3 = c1 + c2
            self.assertIsInstance(c3.values, sp.spmatrix)
            np.testing.assert_array_equal(c3.values.data[0], 3*np.ones(Nk))

    def test_cochain_sub_dict(self) -> None:
        mfld = self.mfld
        
        d1 = {1: 11, 2: 22, 3: 23}

        d2 = {1: 10, 2: 10, 3: 10}

        xpct_dict = {1: 1, 2: 12, 3: 13}

        for k in range(mfld.dim):
            c1 = Cochain(mfld, values=d1, dim=k)
            c2 = Cochain(mfld, values=d2, dim=k)

            c3 = c1 - c2

            self.assertEqual(len(c3.values), len(xpct_dict))
            
            for idx, value in c3.items():
                self.assertIn(idx, xpct_dict.keys())
                self.assertIn(value, xpct_dict.values())

    def test_cochain_sub_spmatrix(self) -> None:
        mfld = self.mfld

        for k, Nk in enumerate(mfld.shape):
            mtrx = sp.identity(Nk)
            c1 = Cochain(mfld, values=mtrx, dim=k)
            c2 = Cochain(mfld, values=2*mtrx, dim=k)

            c3 = c1 - c2
            self.assertIsInstance(c3.values, sp.spmatrix)
            np.testing.assert_array_equal(c3.values.data[0], -1*np.ones(Nk))

    def test_cochain_mul_dict(self) -> None:
        mfld = self.mfld
        
        d1 = {1: 1, 2: 2, 3: 3}

        xpct_dict = {1: 2, 2: 4, 3: 6}

        for k in range(mfld.dim):
            c1 = Cochain(mfld, values=d1, dim=k)

            c2 = c1*2
            
            for idx, value in c2.items():
                self.assertIn(idx, xpct_dict.keys())
                self.assertIn(value, xpct_dict.values())

    def test_cochain_rmul_dict(self) -> None:
        mfld = self.mfld
        
        d1 = {1: 1, 2: 2, 3: 3}

        xpct_dict = {1: 2, 2: 4, 3: 6}
        xpct_dict_2 = {1: 6, 2: 12, 3: 18}

        for k in range(mfld.dim):
            c1 = Cochain(mfld, values=d1, dim=k)

            c2 = 2*c1
            
            for idx, value in c2.items():
                self.assertIn(idx, xpct_dict.keys())
                self.assertIn(value, xpct_dict.values())

            c2 *= 3

            for idx, value in c2.items():
                self.assertIn(idx, xpct_dict_2.keys())
                self.assertIn(value, xpct_dict_2.values())

    def test_cochain_items(self) -> None:
        mfld = self.mfld
        k = 1
        d = {1: 1, 2: 2, 3: 3}
        c = Cochain(mfld, values=d, dim=k)
        
        self.assertIsInstance(c.items(), Generator)

        for k, v in c.items():
            print(k, v)
            self.assertEqual(v, d[k])

        k = 1
        l = list(range(mfld[k].size))
        c = Cochain(mfld, values=l, dim=k)

        self.assertIsInstance(c.items(), Generator)

        for k, v in c.items():
            self.assertEqual(v, k)

    def test_cochain_positions(self) -> None:
        mfld = self.mfld
        
        # primal cochain
        for k, Nk in enumerate(mfld.shape):
            cchn = Cochain(mfld, dim=k, values=np.ones(Nk))
            np.testing.assert_array_equal(cchn.positions, mfld[k].circumcenters)
            
            vals = {1:1, 0:1}
            cchn = Cochain(mfld, dim=k, values=vals)
            np.testing.assert_array_equal(cchn.positions, 
                                          mfld[k].circumcenters[[0,1]])
        
        # dual cochain
        n = mfld.dim
        for k, Nk in enumerate(mfld.shape):
            cchn = Cochain(mfld, dim=k, dual=True, values=np.ones(Nk))
            np.testing.assert_array_equal(cchn.positions, 
                                          mfld[n-k].circumcenters)
            
            vals = {1:1, 0:1}
            cchn = Cochain(mfld, dim=k, dual=True, values=vals)
            np.testing.assert_array_equal(cchn.positions, 
                                          mfld[n-k].circumcenters[[0,1]])

    def test_cochain_positions_abstract(self) -> None:
        cplx = self.abs_cplx
        for k, Nk in enumerate(cplx.shape):
            c = Cochain(cplx, dim=k, values=np.ones(Nk))
            self.assertIsNone(c.positions)
        
    def test_cochain_toarray(self) -> None:
        mfld = self.mfld
        for k, Nk in enumerate(mfld.shape):
            vals = list(range(Nk))
            cchn = Cochain(mfld, values=vals, dim=k)
            
            arr = cchn.toarray()
            self.assertIsInstance(arr, np.ndarray)
            np.testing.assert_array_equal(arr, np.arange(Nk))
        
        # With values defined from dict.
        vals = {0: 1, 1: 1, 2: 1}
        cchn = Cochain(mfld, values=vals, dim=1)
        
        arr = cchn.toarray()
        self.assertIsInstance(arr, np.ndarray)
        np.testing.assert_array_equal(arr, np.ones(3))

        # With values defined fron sp.matrix
        k =1 
        Nk = mfld[k].size
        vals = sp.diags(2*np.ones(Nk))
        cchn = Cochain(mfld, dim=k, values=vals)

        arr = cchn.toarray()
        self.assertIsInstance(arr, np.ndarray)
        np.testing.assert_array_equal(arr, vals.toarray())

    def test_value_closest_to(self) -> None:
        mfld = self.mfld
        
        for k in range(mfld.dim+1):
            Nk = mfld.shape[k]
            values = np.arange(Nk)
            cchn = Cochain(mfld, k, values=values)
            
            sid = np.random.randint(Nk)
            rnd = np.ones(3) + .1*np.random.rand(3) # Slight deviation 
                                                    # from the exact location.
            target = np.diag(rnd)@mfld[k].circumcenters[sid]
            value = cchn.value_closest_to(target)

            self.assertEqual(value, values[sid])

    def test_format_indices_values(self) -> None:
        
        mfld = self.mfld
        
        k = 1 
        Nk = mfld.shape[k]
        mdl = mfld[k]
        
        
        # Input as list 
        input = [1 for _ in range(Nk)]

        ids, vals = _format_indices_values(input, mdl)
        self.assertIsInstance(ids, np.ndarray)
        self.assertIsInstance(vals, np.ndarray)
        np.testing.assert_array_equal(vals, input)
        
        # Input as array 
        input = 3.1 * np.ones(Nk)

        ids, vals = _format_indices_values(input, mdl)
        self.assertIsInstance(ids, np.ndarray)
        self.assertIsInstance(vals, np.ndarray)
        np.testing.assert_array_equal(vals, input)
        
        # Input as scalar
        input = 3.1

        ids, vals = _format_indices_values(input, mdl)
        self.assertIsInstance(ids, np.ndarray)
        self.assertIsInstance(vals, np.ndarray)
        np.testing.assert_array_equal(vals, input)
        
        # Input as dict
        input = {1: 12, 56: 1, 2: 76}

        ids, vals = _format_indices_values(input, mdl)
        self.assertIsInstance(ids, np.ndarray)
        self.assertIsInstance(vals, np.ndarray)
        np.testing.assert_array_equal(ids, [1, 2, 56])
        np.testing.assert_array_equal(vals, [12, 76, 1])

        # Input as spmatrix
        data = 4.2*np.ones(Nk)
        input = sp.diags(data)

        ids, vals = _format_indices_values(input, mdl)
        self.assertIsInstance(ids, np.ndarray)
        self.assertIsInstance(vals, sp.spmatrix)
        np.testing.assert_array_equal(vals.data[0], data)
        np.testing.assert_array_equal(ids, np.arange(Nk))

        # Input is of wrong type
        input = 'YOU SHALL NOT PASS !'

        ids, vals = _format_indices_values(input, mdl)
        self.assertIsNone(ids)
        self.assertIsNone(vals)

    def test_cochain_base(self) -> None:
        mfld = self.mfld

        self.assertIsNone(cochain_base(mfld, None))

        for k in range(mfld.dim+1):
            cchn_base = cochain_base(mfld, k)

            self.assertIsInstance(cchn_base, Cochain)
            self.assertEqual(cchn_base.dim, k)
            np.testing.assert_array_equal(cchn_base.values.toarray(),
                                          np.eye(mfld[k].size))
        
        self.assertIsNone(cochain_base(self.cplx, 1, dual=True))

    def test_cochain_isvectorvalued(self) -> None:
        mfld = self.mfld

        for k, Nk in enumerate(mfld.shape):
            c = Cochain(mfld, dim=k, values=sp.identity(Nk))
            self.assertFalse(c.isvectorvalued)

            c = Cochain(mfld, dim=k, values=np.ones(Nk))
            self.assertFalse(c.isvectorvalued)
            
            c = Cochain(mfld, dim=k, values=np.ones((Nk, 3)))
            self.assertTrue(c.isvectorvalued)
            