from tkinter import W
import unittest
import numpy as np
import scipy.sparse as sp
import scipy.special as spcl

from dxtr.complexes import Simplex
from dxtr.complexes.module import Module, _has_proper_shape



class TestModule(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """

        # Structures to run tests on
        
        self.indices = np.asarray([[0], [1], [2], [3], [4]])
        self.sample_square = np.array([[0, 1, 4],
                                       [1, 2, 4],
                                       [2, 3, 4],
                                       [3, 0, 4]])
    
        
        self.vertices = np.array([[0, 1, 0],
                                  [1, 1, 0],
                                  [1, 0, 0],
                                  [0, 0, 0],
                                  [.5, .5, 0]])
        self.vertex_indices = np.arange(5)
        self.circumcenters = np.repeat([[1,0,0]], 4, axis=0)
        self.volumes = .25 * np.ones(4)
        self.covolumes = np.ones(4)
        self.deficit_angles = np.zeros(4)
        self.dihedral_angles = np.zeros(4)
        self.well_centeredness = np.asarray(4*[True])
        
        data = [-1, -1, -1,  1, -1, -1,  1, -1, -1,  1,  1, -1,  1,  1,  1,  1]
        indices = [0, 1, 2, 0, 3, 4, 3, 5, 6, 1, 5, 7, 2, 4, 6, 7]
        indptr = [ 0,  3,  6,  9, 12, 16]
        
        self.bnd = sp.csr_matrix((data, indices, indptr))

        data = [ 1,  1, -1, -1,  1,  1, -1,  1,  1, -1,  1,  1]
        indices = [0, 1, 0, 1, 2, 0, 2, 3, 2, 3, 1, 3]
        indptr = [ 0,  1,  2,  4,  5,  7,  8, 10, 12]
        
        self.cbnd = sp.csc_matrix((data, indices, indptr))

        self.chain_complex = [sp.diags([], dtype=int).tocsr(), 0, 
                              self.bnd, self.cbnd.T]

        self.Module = Module(self.sample_square, self.chain_complex)
        self.Module0 = Module(self.indices, self.chain_complex)
        self.Module0._vertices = self.vertices
        self.Module0._circumcenters = self.vertices


    def tearDown(self):
        """Concludes and closes the test.
        """
    

    def test_Module_dim(self):
        self.assertEqual(self.Module.dim, len(self.sample_square[0]) - 1)
    

    def test_Module_size(self):
        self.assertEqual(self.Module.size, len(self.sample_square))


    def test_Module_boundary(self):
        bnd = self.Module.boundary
        
        self.assertIsInstance(bnd, sp.csr_matrix)
        
        np.testing.assert_array_equal(bnd.toarray(), self.bnd.toarray())


    def test_Module_coboundary(self):
        cbnd = self.Module.coboundary
        
        self.assertIsInstance(cbnd, sp.csr_matrix)
        
        np.testing.assert_array_equal(cbnd.toarray(), self.cbnd.toarray())
        

    def test_Module_adjacency(self):
        adj = self.Module.adjacency

        xpct_adj = self.bnd.T @ self.bnd
        xpct_adj -= sp.diags(xpct_adj.diagonal(), dtype=int)
        
        self.assertIsInstance(adj, sp.csc_matrix)
        
        np.testing.assert_array_equal(adj.toarray(), xpct_adj.toarray())

        # 0-Module has no adjacency
        self.assertIsNone(self.Module0.adjacency)


    def test_Module_coadjacency(self):
        cadj = self.Module.coadjacency

        xpct_cadj = self.cbnd.T @ self.cbnd
        xpct_cadj -= sp.diags(xpct_cadj.diagonal(), dtype=int)
        
        self.assertIsInstance(cadj, sp.csc_matrix)
        
        np.testing.assert_array_equal(cadj.toarray(), xpct_cadj.toarray())

        # N-Module has no adjacency
        Nmdl = Module(self.sample_square, [0, 0, 0, 
                                              sp.diags([], dtype=int).tocsr()])
        self.assertIsNone(Nmdl.coadjacency)

        
    def test_Module_vertex_indices(self):
        np.testing.assert_array_equal(self.Module.vertex_indices, 
                                      self.sample_square)
        
        self.Module.set('vertex indices', self.vertex_indices)
    
        np.testing.assert_array_equal(self.Module.vertex_indices,
                                      self.vertex_indices)


    def test_Module_vertices(self):
        self.assertIsNone(self.Module.vertices)

        
        self.Module.set('vertices', self.vertices)
    
        np.testing.assert_array_equal(self.Module.vertices,
                                      self.vertices[self.sample_square])


    def test_Module_volumes(self):
        self.assertIsNone(self.Module.volumes)

        self.Module.set('volumes', self.volumes)
    
        np.testing.assert_array_equal(self.Module.volumes, self.volumes)


    def test_Module_covolumes(self):
        self.assertIsNone(self.Module.covolumes)

        self.Module.set('covolumes', self.covolumes)
    
        np.testing.assert_array_equal(self.Module.covolumes, self.covolumes)

    
    def test_Module_circumcenters(self) -> None:
        self.assertIsNone(self.Module.circumcenters)
        
        self.Module.set('circumcenters', self.circumcenters)
        np.testing.assert_array_equal(self.Module.circumcenters, 
                                      self.circumcenters)


    def test_Module_deficit_angles(self):
        self.assertIsNone(self.Module.deficit_angles)

        self.Module.set('deficit angles', self.deficit_angles)
    
        np.testing.assert_array_equal(self.Module.deficit_angles, 
                                      self.deficit_angles)
    
    
    def test_Module_dihedral_angles(self):
        self.assertIsNone(self.Module.dihedral_angles)

        self.Module.set('dihedral angles', self.dihedral_angles)
    
        np.testing.assert_array_equal(self.Module.dihedral_angles, 
                                      self.dihedral_angles)
    

    def test_Module_well_centeredness(self) -> None:
        self.assertIsNone(self.Module.well_centeredness)

        self.Module.set('well-centeredness', self.well_centeredness)
        np.testing.assert_array_equal(self.Module.well_centeredness, 
                                      self.well_centeredness)


    def test_Module_closest_simplex_to(self) -> None:

        center = [.5,.5,0]

        sidx = self.Module0.closest_simplex_to(center, index_only=True)

        self.assertEqual(sidx, 4)

        splx = self.Module0.closest_simplex_to(center)
        assert isinstance(splx, Simplex)
        self.assertEqual(splx.dim, 0)
        self.assertEqual(splx.indices, (sidx,))

        # N.B.: the circumcenters below are completely fake.
        self.Module._circumcenters = np.array([[.5, .5, 1],
                                                 [.45, .45, 0],
                                                 [0, 0, 0],
                                                 [.45, .51, 0]])
        splx = self.Module.closest_simplex_to(center)
        self.assertEqual( splx.indices, (3,0,4))

        # Ill-defined provided position
        ill_center = [.5, .5]
        self.Module.closest_simplex_to(ill_center)


    def test_Module_set_wrong_parameter(self) -> None:
        self.assertIsNone(self.Module.set('this parameter does not exists',
                                            self.well_centeredness))
        

    def test__has_proper_shape(self) -> None:
        n = 3
        arr1 = np.ones(1)
        arr2 = np.ones((3,2))
        arr3 = np.ones(3)
        
        self.assertFalse(_has_proper_shape(arr1, n))
        self.assertFalse(_has_proper_shape(arr2, n))
        self.assertTrue(_has_proper_shape(arr3, n))