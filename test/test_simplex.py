import unittest
import numpy as np
import scipy.sparse as sp

from dxtr.complexes import Simplex


class TestSimplex(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        # Structures to run tests on

        self.indices = [0, 1, 2]
        self.splx0 = Simplex(self.indices[:1])
        self.splx1 = Simplex(self.indices[:2])
        self.splx2 = Simplex(self.indices)

        self.simplices = [self.splx0,
                          self.splx1,
                          self.splx2]

        self.vertices = np.array([[0, 0, 0],
                                  [1, 0, 0],
                                  [0, 1, 0]])
        

        self.splx3 = Simplex([1])

    def tearDown(self):
        """Concludes and closes the test.
        """

    def test_Simplex_type(self):
        for splx in self.simplices:
            self.assertIsInstance(splx, tuple)

    def test_Simplex_indices(self):
        for k, splx in enumerate(self.simplices):
            self.assertEqual(splx.indices, self.indices[:k+1])
        
    def test_Simplex_isabstract(self):
        for k, splx in enumerate(self.simplices):
            self.assertTrue(splx.isabstract)

    def test_Simplex_dim(self):
        for k, splx in enumerate(self.simplices):
            self.assertEqual(splx.dim, k)

    def test_Simplex_vertices(self):
        for k, splx in enumerate(self.simplices):
            self.assertIsNone(splx.vertices)
        
            splx._vertices = self.vertices
            if k == 0:
                np.testing.assert_array_equal(splx.vertices, 
                                              self.vertices[0])
            else:
                np.testing.assert_array_equal(splx.vertices,
                                              self.vertices[:k+1])

    def test_simplex_set_vertices(self) -> None:
        vertex_indices = (0, 1, 2, 3)
        vertex_positions =  [[0, 0, 0],
                            [0, 1, 0],
                            [0, 0, 1],
                            [1/np.sqrt(3), 1/np.sqrt(3), 1/np.sqrt(3)]]

        # test input list
        splx = Simplex(vertex_indices)
        splx.set_vertices(vertex_positions)
        splx_vtcs = splx.vertices
        assert isinstance(splx_vtcs, np.ndarray)
        np.testing.assert_array_equal(splx_vtcs, np.asarray(vertex_positions))
        
        # test input array
        splx = Simplex(vertex_indices)
        splx.set_vertices(np.array(vertex_positions))
        splx_vtcs = splx.vertices
        assert isinstance(splx_vtcs, np.ndarray)
        np.testing.assert_array_equal(splx_vtcs, np.asarray(vertex_positions))

        # fail wrong embedding
        splx = Simplex(vertex_indices)
        splx.set_vertices(np.array(vertex_positions)[:,:-1])
        assert splx.vertices is None

        # fail test wrong input type
        splx = Simplex(vertex_indices)
        splx.set_vertices(str(vertex_positions))
        assert splx.vertices is None

        splx = Simplex(vertex_indices)
        vertex_positions_dict =  dict(zip(vertex_indices, vertex_positions))
        splx.set_vertices(vertex_positions_dict)
        assert splx.vertices is None

        # more complex input
        vertex_indices = (1, 2, 3, 5)
        vertex_positions_2 =  [[0, 12, 0],
                            [0, 0, 0],
                            [0, 1, 0],
                            [0, 0, 1],
                            [0, 12, 0],
                            [1/np.sqrt(3), 1/np.sqrt(3), 1/np.sqrt(3)]]
        
        splx = Simplex(vertex_indices)
        splx.set_vertices(vertex_positions_2)
        np.testing.assert_array_equal(splx.vertices, 
                                      np.asarray(vertex_positions))

    def test_Simplex_volume(self):
        for k, splx in enumerate(self.simplices):
            self.assertIsNone(splx.volume)
            
            splx._vertices = self.vertices
            if k == 0:
                self.assertEqual(splx.volume, 0)
            elif k == 1:
                self.assertEqual(splx.volume, 1)
            elif k == 2:
                self.assertEqual(splx.volume, .5)

    def test_Simplex_circumcenter(self):
        for k, splx in enumerate(self.simplices):
            self.assertIsNone(splx.circumcenter)
            
            splx._vertices = self.vertices
            if k == 0:
                # splx._vertices = self.vertices[0]
                np.testing.assert_array_equal(splx.circumcenter,
                                              np.zeros(3))
            elif k == 1:
                # splx._vertices = self.vertices[:k+1]
                np.testing.assert_array_equal(splx.circumcenter,
                                              np.array([.5, 0, 0]))
            elif k == 2:
                # splx._vertices = self.vertices[:k+1]
                np.testing.assert_array_equal(splx.circumcenter,
                                              np.array([.5, .5, 0]))

    def test_Simplex_isfaceof(self):
        self.assertTrue(self.splx0.isfaceof(self.splx1))
        self.assertTrue(self.splx0.isfaceof(self.splx2))
        self.assertTrue(self.splx1.isfaceof(self.splx2))
        
        self.assertFalse(self.splx2.isfaceof(self.splx1))
        self.assertFalse(self.splx2.isfaceof(self.splx0))
        self.assertFalse(self.splx1.isfaceof(self.splx0))