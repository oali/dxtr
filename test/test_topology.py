import unittest
import numpy as np
import scipy.sparse as sp

from dxtr.complexes import SimplicialComplex
from dxtr.math.topology import (star, closure, link, border, interior,
                                complex_simplicial_partition,
                                cell_simplicial_partition,
                                permutation_number_in_swap_cycle, 
                                count_permutations_between)


class TestTopology(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        sample_0d = [[0]]
        sample_1d = [[0, 1]]
        sample_2d = [[0, 1, 2]]
        sample_3d = [[0, 1, 2, 3]]
        sample_4d = [[0, 1, 2, 3, 4]]
        sample_5d = [[0, 1, 2, 3, 4, 5]]


        sample_square = [[1, 2, 5], [2, 3, 5],
                         [3, 4, 5], [4, 1, 5]]

        sample_2tetras = [[1, 2, 3, 4], [2, 3, 4, 5]]

        sample_non_pure = [[1, 2, 3, 4], [2, 3, 4, 5],
                           [5, 6, 7], [12, 34], [548]]

        sample_hollow_tetra = [[4, 5, 6], [4, 5, 7],
                               [4, 6, 7], [5, 6, 7]]

        sample_hollow_square = [[1,2], [2,3],
                                [3,4], [4,1]]


        samples = [sample_0d, sample_1d, sample_2d, 
                   sample_3d, sample_4d, sample_5d,
                   sample_square, sample_2tetras,
                   sample_non_pure, sample_hollow_tetra,
                   sample_hollow_square]

        self.complexes = [SimplicialComplex(smpl)
                          for smpl in samples]


    def tearDown(self):
        """Concludes and closes the test.
        """


    def test_star(self):
        
        # Test on simple square complex
        cplx = self.complexes[6]
        ch_cplx = cplx._chain_complex
        
        splx_ids = {0: (4,)}
        star_set = star(ch_cplx, splx_ids)
        
        xpct_star_set = {0: [4],
                         1: [2, 4, 6, 7],
                         2: [0, 1, 2, 3]}

        self.assertEqual(list(star_set.keys()), list(xpct_star_set.keys()))
        
        for k, xpct_values in xpct_star_set.items():
            np.testing.assert_array_equal(star_set[k], xpct_values)

        # Test general properties on many complexes
        for cplx in self.complexes:
            ch_cplx = cplx._chain_complex

            splx_ids = {0: [0]}
            star_set = star(ch_cplx, splx_ids)

            self.assertEqual(list(star_set.keys()),
                             list(range(cplx.dim+1)))
            
            for ids in star_set.values():
                self.assertIsInstance(ids, list)
            
            self.assertIn(0, star_set[0])

        
        # Test on non-pure complex
        cplx = self.complexes[8]
        ch_cplx = cplx._chain_complex

        # --
        for k, splx in enumerate([(548,), (12, 34)]):
            splx_ids = {k: cplx[k].index(splx)}

            star_set = star(ch_cplx, splx_ids)
        
            for k, splx_ids in star_set.items():
                idx = splx_ids[0]

                self.assertEqual(cplx[k][idx], splx)

        # --
        k = 0
        splx = (5,)
        splx_ids = {k: cplx[k].index(splx)}

        star_set = star(ch_cplx, splx_ids)
        dims = np.array(list(star_set.keys()))

        np.testing.assert_array_equal(dims, range(cplx.dim+1))

        for k, splx_ids in star_set.items():
            self.assertTrue(np.all([splx in cplx[k][idx]
                                    for idx in splx_ids]))
            self.assertFalse(np.any([splx not in cplx[k][idx]
                                     for idx in splx_ids]))


    def test_closure(self):

        # Test on simple square complex
        cplx = self.complexes[6]
        ch_cplx = cplx._chain_complex
        
        splx_ids = {2: [0]}
        clsr_set = closure(ch_cplx, splx_ids)
        
        xpct_clsr_set = {0: [0, 1, 4],
                         1: [0, 2, 4],
                         2: [0]}

        self.assertEqual(list(clsr_set.keys()), list(xpct_clsr_set.keys()))
        
        for k, xpct_values in xpct_clsr_set.items():
            np.testing.assert_array_equal(clsr_set[k], xpct_values)
        
        # Test general properties on many complexes
        for cplx in self.complexes:
            ch_cplx = cplx._chain_complex

            splx_ids = {cplx.dim: [0]}
            clsr_set = closure(ch_cplx, splx_ids)

            self.assertEqual(list(clsr_set.keys()),
                             list(range(cplx.dim+1)))
            
            for ids in clsr_set.values():
                self.assertIsInstance(ids, list)
            
            self.assertIn(0, clsr_set[cplx.dim])
        
        # Test on non-pure complex
        cplx = self.complexes[8]
        ch_cplx = cplx._chain_complex

        # --
        for k, splx in enumerate([(548,), (12, 34), (5, 6, 7)]):
            splx_ids = {k: cplx[k].index(splx)}

            clsr_set = closure(ch_cplx, splx_ids)
        
            for k, splx_ids in sorted(clsr_set.items())[-1:]:
                idx = splx_ids[0]
                self.assertEqual(cplx[k][idx], splx)


    def test_link(self):
       # Test on simple square complex
        cplx = self.complexes[6]
        ch_cplx = cplx._chain_complex
        
        splx_ids = {0: [4]}
        link_set = link(ch_cplx, splx_ids)
        
        xpct_link_set = {0: [0, 1, 2, 3],
                         1: [0, 1, 3, 5]}

        self.assertEqual(list(link_set.keys()), list(xpct_link_set.keys()))
        
        for k, xpct_values in xpct_link_set.items():
            np.testing.assert_array_equal(link_set[k], xpct_values)
        
        # Test on non-pure complex
        cplx = self.complexes[8]
        ch_cplx = cplx._chain_complex

        # --
        for k, splx in enumerate([(548,), (12, 34), (5, 6, 7)]):
            splx_ids = {k: cplx[k].index(splx)}

            link_set = link(ch_cplx, splx_ids)
            self.assertEqual(len(link_set), 0)
        
        # --
        for n, cplx in enumerate(self.complexes[1:6]):
                ch_cplx = cplx._chain_complex

                for i, splx in enumerate(cplx[0]):
                    link_set = link(ch_cplx, {0: i})
                    
                    self.assertEqual(len(link_set[n]), 1)

                    opposite_splx_idx = link_set[n][0]
                    opposite_splx = cplx[n][opposite_splx_idx]

                    self.assertFalse(splx[0] in opposite_splx)


    def test_border(self):
        # Test on simple 2D square and 3D diamond complexes
        xpct_brdr_set = [{0: [0, 1, 3, 4],
                          1: [0, 1, 4, 7]},
                         {0: [0, 1, 2, 3, 4],
                          1: [0, 1, 2, 3, 4, 5, 6, 7, 8],
                          2: [0, 1, 2, 4, 5, 6]}]

        for i, cplx in enumerate(self.complexes[6:8]):
            ch_cplx = cplx._chain_complex
        
            splx_ids = {cplx.dim: [0, 1]}
            brdr_set = border(ch_cplx, splx_ids)
        
            self.assertEqual(brdr_set, xpct_brdr_set[i])
        
        # Test border of border is empty
        for cplx in self.complexes:
            ch_cplx = cplx._chain_complex

            splx = {cplx.dim: 0}
            brdr_of_brdr = border(ch_cplx, border(ch_cplx, splx)) 
            nbr_splx_bob = [len(sids) for sids in brdr_of_brdr.values()]
            
            np.testing.assert_almost_equal(nbr_splx_bob, 0)

    
    def test_interior(self):
        # Test on simple 2D square and 3D diamond complexes
        xpct_intr_set = [{1: [2], 2: [0, 1]},
                         {2: [3], 3: [0, 1]}]

        for i, cplx in enumerate(self.complexes[6:8]):
            ch_cplx = cplx._chain_complex
        
            splx_ids = {cplx.dim: [0, 1]}
            intr_set = interior(ch_cplx, splx_ids)
        
            self.assertEqual(intr_set, xpct_intr_set[i])

        # 0-simplices are their own interior
        for cplx in self.complexes:
            ch_cplx = cplx._chain_complex
            
            splx_ids = {0: list(range(cplx.shape[0]))}
            intr_set = interior(ch_cplx, splx_ids)

            self.assertEqual(splx_ids, intr_set)


    def test_cell_simplicial_partition(self):
        # test in 3d & 4d
        sample_3d = [[0, 1, 2, 3]]
        sample_4d = [[0, 1, 2, 3, 4]]


        samples = [sample_3d, sample_4d]

        complexes = [SimplicialComplex(smpl) for smpl in samples]


        expected_indices = {3: [[[0, 0, 0, 0],
                                 [0, 0, 1, 0],
                                 [0, 1, 0, 0],
                                 [0, 1, 2, 0],
                                 [0, 2, 1, 0],
                                 [0, 2, 2, 0]],
                                 [[0, 0, 0],
                                 [0, 1, 0]],
                                 [[0, 0]]],
                            4: [[[0, 0, 0, 0, 0], [0, 0, 0, 1, 0],
                                 [0, 0, 1, 0, 0], [0, 0, 1, 2, 0],
                                 [0, 0, 2, 1, 0], [0, 0, 2, 2, 0],
                                 [0, 1, 0, 0, 0], [0, 1, 0, 1, 0],
                                 [0, 1, 3, 0, 0], [0, 1, 3, 3, 0],
                                 [0, 1, 4, 1, 0], [0, 1, 4, 3, 0],
                                 [0, 2, 1, 0, 0], [0, 2, 1, 2, 0],
                                 [0, 2, 3, 0, 0], [0, 2, 3, 3, 0],
                                 [0, 2, 5, 2, 0], [0, 2, 5, 3, 0],
                                 [0, 3, 2, 1, 0], [0, 3, 2, 2, 0],
                                 [0, 3, 4, 1, 0], [0, 3, 4, 3, 0],
                                 [0, 3, 5, 2, 0], [0, 3, 5, 3, 0]],
                                 [[0, 0, 0, 0], [0, 0, 1, 0],
                                 [0, 1, 0, 0], [0, 1, 2, 0],
                                 [0, 2, 1, 0], [0, 2, 2, 0]],
                                 [[0, 0, 0], [0, 1, 0]],
                                 [[0, 0]]]}

        for cplx in complexes:
            chn_cplx = cplx._chain_complex

            for k, xpct_ids in enumerate(expected_indices[cplx.dim]):
                self.assertEqual(cell_simplicial_partition(chn_cplx, 0, k),
                                    xpct_ids)
            
            self.assertIsNone(cell_simplicial_partition(chn_cplx, 0, 5))


    def test_complex_simplicial_partition(self):

        sample_2d = [[0, 1, 2]]
        sample_3d = [[0, 1, 2, 3]]
        sample_4d = [[0, 1, 2, 3, 4]]


        sample_2tetras = [[0, 1, 2, 3],
                        [1, 2, 3, 4]]

        sample_hollow_tetra = [[0, 1, 2], [0, 1, 3],
                            [0, 2, 3], [1, 2, 3]]

        samples = [sample_2d, sample_3d, sample_4d,
                sample_2tetras, sample_hollow_tetra]

        complexes = [SimplicialComplex(smpl) for smpl in samples]
        
        
        xpct_dual_partition_sample_2d = [np.array([[0,0],
                                                [1,0],
                                                [2,0]]),
                                    np.array([[0, 0, 0],
                                                [0, 1, 0],
                                                [1, 0, 0],
                                                [1, 2, 0],
                                                [2, 1, 0],
                                                [2, 2, 0]])]
        
        xpct_dual_partition_sample_3d = [np.array([[0, 0],
                                                [1, 0],
                                                [2, 0],
                                                [3, 0]]),
                                    np.array([[0, 0, 0],
                                                [0, 1, 0],
                                                [1, 0, 0],
                                                [1, 2, 0],
                                                [2, 1, 0],
                                                [2, 2, 0],
                                                [3, 0, 0],
                                                [3, 3, 0],
                                                [4, 1, 0],
                                                [4, 3, 0],
                                                [5, 2, 0],
                                                [5, 3, 0]]),
                                    np.array([[0, 0, 0, 0],
                                                [0, 0, 1, 0],
                                                [0, 1, 0, 0],
                                                [0, 1, 2, 0],
                                                [0, 2, 1, 0],
                                                [0, 2, 2, 0],
                                                [1, 0, 0, 0],
                                                [1, 0, 1, 0],
                                                [1, 3, 0, 0],
                                                [1, 3, 3, 0],
                                                [1, 4, 1, 0],
                                                [1, 4, 3, 0],
                                                [2, 1, 0, 0],
                                                [2, 1, 2, 0],
                                                [2, 3, 0, 0],
                                                [2, 3, 3, 0],
                                                [2, 5, 2, 0],
                                                [2, 5, 3, 0],
                                                [3, 2, 1, 0],
                                                [3, 2, 2, 0],
                                                [3, 4, 1, 0],
                                                [3, 4, 3, 0],
                                                [3, 5, 2, 0],
                                                [3, 5, 3, 0]])]
            
        xpct_dual_partition_sample_4d = [np.array([[0, 0],
                                                [1, 0],
                                                [2, 0],
                                                [3, 0],
                                                [4, 0]]),
                                        np.array([[0, 0, 0],
                                                [0, 1, 0],
                                                [1, 0, 0],
                                                [1, 2, 0],
                                                [2, 1, 0],
                                                [2, 2, 0],
                                                [3, 0, 0],
                                                [3, 3, 0],
                                                [4, 1, 0],
                                                [4, 3, 0],
                                                [5, 2, 0],
                                                [5, 3, 0],
                                                [6, 0, 0],
                                                [6, 4, 0],
                                                [7, 1, 0],
                                                [7, 4, 0],
                                                [8, 2, 0],
                                                [8, 4, 0],
                                                [9, 3, 0],
                                                [9, 4, 0]]),
                                        np.array([[0, 0, 0, 0],
                                                [0, 0, 1, 0],
                                                [0, 1, 0, 0],
                                                [0, 1, 2, 0],
                                                [0, 2, 1, 0],
                                                [0, 2, 2, 0],
                                                [1, 0, 0, 0],
                                                [1, 0, 1, 0],
                                                [1, 3, 0, 0],
                                                [1, 3, 3, 0],
                                                [1, 4, 1, 0],
                                                [1, 4, 3, 0],
                                                [2, 1, 0, 0],
                                                [2, 1, 2, 0],
                                                [2, 3, 0, 0],
                                                [2, 3, 3, 0],
                                                [2, 5, 2, 0],
                                                [2, 5, 3, 0],
                                                [3, 2, 1, 0],
                                                [3, 2, 2, 0],
                                                [3, 4, 1, 0],
                                                [3, 4, 3, 0],
                                                [3, 5, 2, 0],
                                                [3, 5, 3, 0],
                                                [4, 0, 0, 0],
                                                [4, 0, 1, 0],
                                                [4, 6, 0, 0],
                                                [4, 6, 4, 0],
                                                [4, 7, 1, 0],
                                                [4, 7, 4, 0],
                                                [5, 1, 0, 0],
                                                [5, 1, 2, 0],
                                                [5, 6, 0, 0],
                                                [5, 6, 4, 0],
                                                [5, 8, 2, 0],
                                                [5, 8, 4, 0],
                                                [6, 2, 1, 0],
                                                [6, 2, 2, 0],
                                                [6, 7, 1, 0],
                                                [6, 7, 4, 0],
                                                [6, 8, 2, 0],
                                                [6, 8, 4, 0],
                                                [7, 3, 0, 0],
                                                [7, 3, 3, 0],
                                                [7, 6, 0, 0],
                                                [7, 6, 4, 0],
                                                [7, 9, 3, 0],
                                                [7, 9, 4, 0],
                                                [8, 4, 1, 0],
                                                [8, 4, 3, 0],
                                                [8, 7, 1, 0],
                                                [8, 7, 4, 0],
                                                [8, 9, 3, 0],
                                                [8, 9, 4, 0],
                                                [9, 5, 2, 0],
                                                [9, 5, 3, 0],
                                                [9, 8, 2, 0],
                                                [9, 8, 4, 0],
                                                [9, 9, 3, 0],
                                                [9, 9, 4, 0]]),
                                        np.array([[0, 0, 0, 0, 0],
                                                [0, 0, 0, 1, 0],
                                                [0, 0, 1, 0, 0],
                                                [0, 0, 1, 2, 0],
                                                [0, 0, 2, 1, 0],
                                                [0, 0, 2, 2, 0],
                                                [0, 1, 0, 0, 0],
                                                [0, 1, 0, 1, 0],
                                                [0, 1, 3, 0, 0],
                                                [0, 1, 3, 3, 0],
                                                [0, 1, 4, 1, 0],
                                                [0, 1, 4, 3, 0],
                                                [0, 2, 1, 0, 0],
                                                [0, 2, 1, 2, 0],
                                                [0, 2, 3, 0, 0],
                                                [0, 2, 3, 3, 0],
                                                [0, 2, 5, 2, 0],
                                                [0, 2, 5, 3, 0],
                                                [0, 3, 2, 1, 0],
                                                [0, 3, 2, 2, 0],
                                                [0, 3, 4, 1, 0],
                                                [0, 3, 4, 3, 0],
                                                [0, 3, 5, 2, 0],
                                                [0, 3, 5, 3, 0],
                                                [1, 0, 0, 0, 0],
                                                [1, 0, 0, 1, 0],
                                                [1, 0, 1, 0, 0],
                                                [1, 0, 1, 2, 0],
                                                [1, 0, 2, 1, 0],
                                                [1, 0, 2, 2, 0],
                                                [1, 4, 0, 0, 0],
                                                [1, 4, 0, 1, 0],
                                                [1, 4, 6, 0, 0],
                                                [1, 4, 6, 4, 0],
                                                [1, 4, 7, 1, 0],
                                                [1, 4, 7, 4, 0],
                                                [1, 5, 1, 0, 0],
                                                [1, 5, 1, 2, 0],
                                                [1, 5, 6, 0, 0],
                                                [1, 5, 6, 4, 0],
                                                [1, 5, 8, 2, 0],
                                                [1, 5, 8, 4, 0],
                                                [1, 6, 2, 1, 0],
                                                [1, 6, 2, 2, 0],
                                                [1, 6, 7, 1, 0],
                                                [1, 6, 7, 4, 0],
                                                [1, 6, 8, 2, 0],
                                                [1, 6, 8, 4, 0],
                                                [2, 1, 0, 0, 0],
                                                [2, 1, 0, 1, 0],
                                                [2, 1, 3, 0, 0],
                                                [2, 1, 3, 3, 0],
                                                [2, 1, 4, 1, 0],
                                                [2, 1, 4, 3, 0],
                                                [2, 4, 0, 0, 0],
                                                [2, 4, 0, 1, 0],
                                                [2, 4, 6, 0, 0],
                                                [2, 4, 6, 4, 0],
                                                [2, 4, 7, 1, 0],
                                                [2, 4, 7, 4, 0],
                                                [2, 7, 3, 0, 0],
                                                [2, 7, 3, 3, 0],
                                                [2, 7, 6, 0, 0],
                                                [2, 7, 6, 4, 0],
                                                [2, 7, 9, 3, 0],
                                                [2, 7, 9, 4, 0],
                                                [2, 8, 4, 1, 0],
                                                [2, 8, 4, 3, 0],
                                                [2, 8, 7, 1, 0],
                                                [2, 8, 7, 4, 0],
                                                [2, 8, 9, 3, 0],
                                                [2, 8, 9, 4, 0],
                                                [3, 2, 1, 0, 0],
                                                [3, 2, 1, 2, 0],
                                                [3, 2, 3, 0, 0],
                                                [3, 2, 3, 3, 0],
                                                [3, 2, 5, 2, 0],
                                                [3, 2, 5, 3, 0],
                                                [3, 5, 1, 0, 0],
                                                [3, 5, 1, 2, 0],
                                                [3, 5, 6, 0, 0],
                                                [3, 5, 6, 4, 0],
                                                [3, 5, 8, 2, 0],
                                                [3, 5, 8, 4, 0],
                                                [3, 7, 3, 0, 0],
                                                [3, 7, 3, 3, 0],
                                                [3, 7, 6, 0, 0],
                                                [3, 7, 6, 4, 0],
                                                [3, 7, 9, 3, 0],
                                                [3, 7, 9, 4, 0],
                                                [3, 9, 5, 2, 0],
                                                [3, 9, 5, 3, 0],
                                                [3, 9, 8, 2, 0],
                                                [3, 9, 8, 4, 0],
                                                [3, 9, 9, 3, 0],
                                                [3, 9, 9, 4, 0],
                                                [4, 3, 2, 1, 0],
                                                [4, 3, 2, 2, 0],
                                                [4, 3, 4, 1, 0],
                                                [4, 3, 4, 3, 0],
                                                [4, 3, 5, 2, 0],
                                                [4, 3, 5, 3, 0],
                                                [4, 6, 2, 1, 0],
                                                [4, 6, 2, 2, 0],
                                                [4, 6, 7, 1, 0],
                                                [4, 6, 7, 4, 0],
                                                [4, 6, 8, 2, 0],
                                                [4, 6, 8, 4, 0],
                                                [4, 8, 4, 1, 0],
                                                [4, 8, 4, 3, 0],
                                                [4, 8, 7, 1, 0],
                                                [4, 8, 7, 4, 0],
                                                [4, 8, 9, 3, 0],
                                                [4, 8, 9, 4, 0],
                                                [4, 9, 5, 2, 0],
                                                [4, 9, 5, 3, 0],
                                                [4, 9, 8, 2, 0],
                                                [4, 9, 8, 4, 0],
                                                [4, 9, 9, 3, 0],
                                                [4, 9, 9, 4, 0]])]
        
        xpct_dual_partition_sample_2tetras = [np.array([[0, 0],
                                                        [1, 0],
                                                        [2, 0],
                                                        [3, 0],
                                                        [3, 1],
                                                        [4, 1],
                                                        [5, 1],
                                                        [6, 1]]), 
                                            np.array([[0, 0, 0],
                                                        [0, 1, 0],
                                                        [1, 0, 0],
                                                        [1, 2, 0],
                                                        [2, 1, 0],
                                                        [2, 2, 0],
                                                        [3, 0, 0],
                                                        [3, 3, 0],
                                                        [3, 3, 1],
                                                        [3, 4, 1],
                                                        [4, 1, 0],
                                                        [4, 3, 0],
                                                        [4, 3, 1],
                                                        [4, 5, 1],
                                                        [5, 4, 1],
                                                        [5, 5, 1],
                                                        [6, 2, 0],
                                                        [6, 3, 0],
                                                        [6, 3, 1],
                                                        [6, 6, 1],
                                                        [7, 4, 1],
                                                        [7, 6, 1],
                                                        [8, 5, 1],
                                                        [8, 6, 1]]), 
                                            np.array([[0, 0, 0, 0],
                                                        [0, 0, 1, 0],
                                                        [0, 1, 0, 0],
                                                        [0, 1, 2, 0],
                                                        [0, 2, 1, 0],
                                                        [0, 2, 2, 0],
                                                        [1, 0, 0, 0],
                                                        [1, 0, 1, 0],
                                                        [1, 3, 0, 0],
                                                        [1, 3, 3, 0],
                                                        [1, 3, 3, 1],
                                                        [1, 3, 4, 1],
                                                        [1, 4, 1, 0],
                                                        [1, 4, 3, 0],
                                                        [1, 4, 3, 1],
                                                        [1, 4, 5, 1],
                                                        [1, 5, 4, 1],
                                                        [1, 5, 5, 1],
                                                        [2, 1, 0, 0],
                                                        [2, 1, 2, 0],
                                                        [2, 3, 0, 0],
                                                        [2, 3, 3, 0],
                                                        [2, 3, 3, 1],
                                                        [2, 3, 4, 1],
                                                        [2, 6, 2, 0],
                                                        [2, 6, 3, 0],
                                                        [2, 6, 3, 1],
                                                        [2, 6, 6, 1],
                                                        [2, 7, 4, 1],
                                                        [2, 7, 6, 1],
                                                        [3, 2, 1, 0],
                                                        [3, 2, 2, 0],
                                                        [3, 4, 1, 0],
                                                        [3, 4, 3, 0],
                                                        [3, 4, 3, 1],
                                                        [3, 4, 5, 1],
                                                        [3, 6, 2, 0],
                                                        [3, 6, 3, 0],
                                                        [3, 6, 3, 1],
                                                        [3, 6, 6, 1],
                                                        [3, 8, 5, 1],
                                                        [3, 8, 6, 1],
                                                        [4, 5, 4, 1],
                                                        [4, 5, 5, 1],
                                                        [4, 7, 4, 1],
                                                        [4, 7, 6, 1],
                                                        [4, 8, 5, 1],
                                                        [4, 8, 6, 1]])]
        
        xpct_dual_partition_sample_hollow_tetra = [np.array([[0, 0],
                                                            [0, 1],
                                                            [1, 0],
                                                            [1, 2],
                                                            [2, 1],
                                                            [2, 2],
                                                            [3, 0],
                                                            [3, 3],
                                                            [4, 1],
                                                            [4, 3],
                                                            [5, 2],
                                                            [5, 3]]),
                                                np.array([[0, 0, 0],
                                                            [0, 0, 1],
                                                            [0, 1, 0],
                                                            [0, 1, 2],
                                                            [0, 2, 1],
                                                            [0, 2, 2],
                                                            [1, 0, 0],
                                                            [1, 0, 1],
                                                            [1, 3, 0],
                                                            [1, 3, 3],
                                                            [1, 4, 1],
                                                            [1, 4, 3],
                                                            [2, 1, 0],
                                                            [2, 1, 2],
                                                            [2, 3, 0],
                                                            [2, 3, 3],
                                                            [2, 5, 2],
                                                            [2, 5, 3],
                                                            [3, 2, 1],
                                                            [3, 2, 2],
                                                            [3, 4, 1],
                                                            [3, 4, 3],
                                                            [3, 5, 2],
                                                            [3, 5, 3]])]
        
        expectations = [xpct_dual_partition_sample_2d,
                        xpct_dual_partition_sample_3d,
                        xpct_dual_partition_sample_4d,
                        xpct_dual_partition_sample_2tetras, 
                        xpct_dual_partition_sample_hollow_tetra]
        
        for cplx, xpct in zip(complexes, expectations):
            chn_cplx = cplx._chain_complex
        
            dual_partition = complex_simplicial_partition(chn_cplx)
            
            for dp, xpct_dp in zip(dual_partition, xpct[::-1]):
                np.testing.assert_allclose(dp, xpct_dp)


    def test_permutation_number_in_swap_cycle(self) -> None:
        unsorted_list = [2,0,1]
        xpct_perm_nbrs = [2,2,2]
        for start_idx, xpct_perm_nbr in enumerate(xpct_perm_nbrs):
            visited = [False]*len(unsorted_list)
            perm_nbr = permutation_number_in_swap_cycle(unsorted_list, 
                                                        start_idx, visited)
            self.assertEqual(perm_nbr, xpct_perm_nbr)
        
        unsorted_list = [1,0,2]
        xpct_perm_nbrs = [1,1,0]
        for start_idx, xpct_perm_nbr in enumerate(xpct_perm_nbrs):
            visited = [False]*len(unsorted_list)
            perm_nbr = permutation_number_in_swap_cycle(unsorted_list, 
                                                        start_idx, visited)
            self.assertEqual(perm_nbr, xpct_perm_nbr)
        
        unsorted_list = [2,1,0]
        xpct_perm_nbrs = [3,0,3]
        for start_idx, xpct_perm_nbr in enumerate(xpct_perm_nbrs):
            visited = [False]*len(unsorted_list)
            perm_nbr = permutation_number_in_swap_cycle(unsorted_list, 
                                                        start_idx, visited)
            self.assertEqual(perm_nbr, xpct_perm_nbr)

        unsorted_list = [0,5,4,3,1,2,6,7]
        xpct_perm_nbrs = [0,9,9,0,9,9,0,0]
        for start_idx, xpct_perm_nbr in enumerate(xpct_perm_nbrs):
            visited = [False]*len(unsorted_list)
            perm_nbr = permutation_number_in_swap_cycle(unsorted_list, 
                                                        start_idx, visited)
            self.assertEqual(perm_nbr, xpct_perm_nbr)


    def test_count_permutations_between(self) -> None:
        l1 = [1, 2, 3, 4]
        l2 = [3, 4, 1, 2]
        
        self.assertEqual(count_permutations_between(l1, l1), 0)
        self.assertEqual(count_permutations_between(l1, l2), 6)
        self.assertEqual(count_permutations_between(l2, l1), 6)

        l3 = [3, 1, 2, 4]
        self.assertEqual(count_permutations_between(l1, l3), 2)

        # Checking ill-defined cases
        l1 = [1, 2, 3, 4]
        l2 = [1, 2, 3]
        self.assertEqual(count_permutations_between(l1, l2), None)
        self.assertEqual(count_permutations_between(l2, l1), None)
        
        l1 = [1, 2, 3, 4]
        l2 = [1, 2, 3, 5]
        self.assertEqual(count_permutations_between(l1, l2), None)
