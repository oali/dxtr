import unittest
import numpy as np


from dxtr.complexes import icosahedron
from dxtr.cochains import Cochain, random_cochain
from dxtr.operators.hodge import hodge_star, _compute_hodge_star_of
from dxtr.operators import d

class TestOperator(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        self.complex = icosahedron()
        self.manifold = icosahedron(manifold=True)

    def tearDown(self):
        """Concludes and closes the test.
        """



    def test_hodge_star_instanciation(self) -> None:
        """Testing that the function yields the proper objects.
        """
        
        # The hodge star of a cochain defined on a simplicial complex
        # must be None.
        k = 1
        cplx = self.complex
        Nk = cplx[k].size
        cochain = Cochain(cplx, dim=k, values=np.ones(Nk))
        assert hodge_star(cochain) is None


        # Let's check dimensions & duality.
        mfld = icosahedron(manifold=True)
        n = mfld.dim
        for k in range(n+1):
            
            # Primal case
            Nk = mfld[k].size
            primal_cochain = Cochain(mfld, dim=k, values=np.ones(Nk))
            hs = hodge_star(primal_cochain)
            
            self.assertIsInstance(hs, Cochain)
            self.assertEqual(hs.complex, mfld)
            self.assertTrue(hs.isdual is not primal_cochain.isdual)
            self.assertEqual(hs.dim, n-k)

            # Dual case
            Nn_k = mfld[n-k].size
            dual_cochain = Cochain(mfld, dim=k, values=np.ones(Nn_k), dual=True)
            hs = hodge_star(dual_cochain)
            
            self.assertIsInstance(hs, Cochain)
            self.assertEqual(hs.complex, mfld)
            self.assertTrue(hs.isdual is not dual_cochain.isdual)
            self.assertEqual(hs.dim, n-k)


    def test_hodge_star_values(self) -> None:
        mfld = self.manifold
        n = mfld.dim
        
        for mdl in mfld:
            k = mdl.dim
            
            
            # Primal case
            primal_cochain = Cochain(mfld, k, mdl.volumes)
            
            covols = mdl.covolumes
            xpct_value = covols if k != n else (-1)**(n-1) * covols
            
            dual_hs = hodge_star(primal_cochain)

            np.testing.assert_equal(dual_hs.values, xpct_value)

            # Applying twice the Hodge star,
            # should retrieve the initial values modulu (-1)**(k*(n-k))
            primal_hshs = hodge_star(dual_hs)

            np.testing.assert_allclose(primal_hshs.values,
                                    (-1)**(k*(n-k)) * primal_cochain.values,
                                    atol=1e-15)
        
            # Dual case
            dual_cochain = Cochain(mfld, n-k, mdl.covolumes, dual=True)
            
            vols = mdl.volumes
            xpct_value = vols if k != n else (-1)**(n-1) * vols
            
            primal_hs = hodge_star(dual_cochain)

            np.testing.assert_array_almost_equal(primal_hs.values, 
                                                (-1)**(k*(n-k))*xpct_value)

            # Applying twice the Hodge star,
            # should retrieve the initial values modulu (-1)**(k*(n-k))
            dual_hshs = hodge_star(primal_hs)
            np.testing.assert_allclose(dual_hshs.values,
                                    (-1)**(k*(n-k)) * dual_cochain.values,
                                    atol=1e-15)


    def test_hodge_star_too_high_dimensions(self) -> None:
        """The hodge star of any (n+1)-cochain must be a dual 0-valued 
        (-1)-cochain.
        """
        
        # Primal case
        n_cochain = random_cochain('icosa', dim=2, manifold=True)
        xderi = d(n_cochain)
        hs = hodge_star(xderi)
        self.assertIsInstance(hs, Cochain)
        self.assertIsNot(hs.isdual, xderi.isdual)
        
        Nn = xderi.complex[-1].size
        np.testing.assert_array_almost_equal(hs.values, np.zeros(Nn))
        
        # Dual case
        n_cochain = random_cochain('icosa', dim=2, dual=True, manifold=True)
        xderi = d(n_cochain)
        hs = hodge_star(xderi)
        self.assertIsInstance(hs, Cochain)
        self.assertIsNot(hs.isdual, xderi.isdual)
        
        N0 = xderi.complex[0].size
        np.testing.assert_array_almost_equal(hs.values, np.zeros(N0))


    def test__compute_hodge_star_of(self):
        mfld = self.manifold
        n = mfld.dim

        for k, mdl in enumerate(mfld):
            hs = _compute_hodge_star_of(mfld, k)
            hs_inv = _compute_hodge_star_of(mfld, n-k, dual=True)
            
            ratio = mdl.covolumes / mdl.volumes
            xpct_hs = ratio if k != n else (-1)**(n-1) * ratio
            xpct_hs_inv = (-1) ** (k * (n - k))  / xpct_hs

            np.testing.assert_allclose(hs.data, xpct_hs)
            np.testing.assert_allclose(hs_inv.data, xpct_hs_inv)