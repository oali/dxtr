import unittest

import numpy as np
from numpy import linalg as lng

from dxtr.complexes import SimplicialManifold, sphere, icosahedron
from dxtr.cochains import (Cochain, unit_cochain, random_cochain, 
                           normal_vector_field)


class TestExamplesCochains(unittest.TestCase):


    @classmethod
    def setUpClass(cls):
        """Initiates the test.
        """
        cls.cplx = sphere()
        cls.mfld = sphere(manifold=True)
        cls.icosa = icosahedron()
        cls.icosa_mfld = icosahedron(manifold=True)

    def tearDown(self):
        """Concludes and closes the test.
        """

    def test_unit_cochain(self) -> None:
        for dim in range(3):
            ucchn = unit_cochain(dim=dim)
            cplx = ucchn.complex
            k = ucchn.dim
            Nk = cplx.shape[k]

            self.assertIsInstance(ucchn, Cochain)
            self.assertEqual(k, dim)
            np.testing.assert_array_equal(ucchn.values, np.ones(Nk))

    def test_unit_cochain_on_sphere(self) -> None:
        ucchn = unit_cochain(complex_name='sphere')
        self.assertEqual(ucchn.complex.shape, self.cplx.shape)
        N1 = self.cplx.shape[1]
        np.testing.assert_array_equal(ucchn.values, np.ones(N1))

        # fail
        self.assertIsNone(unit_cochain(complex_name='triangle'))


    def test_random_cochain(self) -> None:
        for dim in range(3):
            rcchn = random_cochain(dim=dim)
            vals = rcchn.values
            cplx = rcchn.complex
            k = rcchn.dim
            Nk = cplx.shape[k]

            self.assertIsInstance(rcchn, Cochain)
            self.assertEqual(k, dim)
            self.assertTrue((np.greater_equal(vals,0) & np.less(vals,1)).all())
        
        self.assertEqual(cplx.shape, self.icosa.shape)
        np.testing.assert_array_equal(cplx[k].vertices, self.icosa[k].vertices)

        # on sphere
        rcchn = random_cochain(complex_name='sphere')
        self.assertEqual(rcchn.complex.shape, self.cplx.shape)

        # on manifold
        rcchn = random_cochain(manifold=True)
        self.assertEqual(rcchn.complex.shape, self.icosa.shape)
        self.assertIsInstance(rcchn.complex, SimplicialManifold)

        # fail
        self.assertIsNone(random_cochain(complex_name='triangle'))


    def test_normal_vector_field(self) -> None:
        for name in ['hexa', 'sphere', 'icosa']:
            nrls = normal_vector_field(manifold_name=name)
            self.assertIsInstance(nrls, Cochain)

            mfld = nrls.complex
            self.assertIsInstance(mfld, SimplicialManifold)

            N0 = mfld.shape[-1]
            if name=='hexa':
                np.testing.assert_array_equal(nrls.values, 
                                    np.array([[0, 0, 1]*N0]).reshape((N0, 3)))
   
        # fail
        self.assertIsNone(normal_vector_field(manifold_name='triangle'))


    def test_normal_vector_field_on_sphere(self) -> None:
        nrls = normal_vector_field(manifold_name='sphere', dual=True)
        self.assertIsInstance(nrls, Cochain)

        mfld = nrls.complex
        self.assertIsInstance(mfld, SimplicialManifold)

        cctrs = self.mfld[-1].circumcenters
        origin = cctrs.mean(axis=0)
        cctrs -= origin
        xpct_normals = np.array([x/lng.norm(x) for x in cctrs])
        np.testing.assert_array_equal(nrls.values, xpct_normals)
