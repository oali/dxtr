import unittest
import numpy as np
import numpy.linalg as lng
import scipy.sparse as sp


from dxtr.cochains import Cochain
from dxtr.complexes import triangular_grid, triangle, hexagon, sphere
from dxtr.operators.musical import (flat, sharp, _isvalid_for_sharp, 
                                    _dual_to_primal_flat,
                                    _dual_to_dual_flat,
                                    _isvalid_for_flat, 
                                    project,
                                    _dual_half_edges_vectors,
                                    _dual_half_edge_nsimplex_weights,
                                    _dual_edges_orientation,
                                    edge_nsimplex_weight, edge_nsimplex_couples, 
                                    interpolate_cochain_on_nsimplices)


class TestMusicalOperator(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        self.cplx = triangular_grid()
        self.mfld = triangular_grid(manifold=True)
        self.trgl = triangle(manifold=True)
        self.hexa = hexagon(manifold=True)
        self.sph = sphere(manifold=True)

    def tearDown(self):
        """Concludes and closes the test.
        """

    # Test flat-related functions
    # ----------------------------
    def test_isvalid_for_flat(self) -> None:
        Nn = self.mfld[-1].size
        vector_field = np.ones((Nn, 3))
        self.assertTrue(_isvalid_for_flat(vector_field, self.mfld))

        vector_field = np.ones((Nn + 1, 3))
        self.assertFalse(_isvalid_for_flat(vector_field, self.mfld))

        vector_field = np.ones((Nn, 2))
        self.assertFalse(_isvalid_for_flat(vector_field, self.mfld))

        vector_field = 10*[[1,1,1]]
        self.assertFalse(_isvalid_for_flat(vector_field, self.mfld))


    def test_edge_nsimplex_couples(self) -> None:
        idx_couples = edge_nsimplex_couples(self.mfld)
        self.assertIsInstance(idx_couples, np.ndarray)

        xpct_idx_couples = np.array([[ 0,  0], [ 1,  0], [ 2,  1], [ 3,  0],
                                     [ 3,  2], [ 4,  1], [ 4,  2], [ 5,  3], 
                                     [ 6,  1], [ 6,  4], [ 7,  3], [ 7,  4], 
                                     [ 8,  3], [ 8,  5], [ 9,  5], [10,  2], 
                                     [10,  6], [11,  6], [12,  4], [12,  7], 
                                     [13,  6], [13,  8], [14,  7], [14,  8], 
                                     [15,  5], [15,  9], [16,  7], [16, 10], 
                                     [17,  9], [17, 10], [18,  9], [18, 11], 
                                     [19, 11], [20,  8], [20, 12], [21, 12], 
                                     [22, 10], [22, 13], [23, 12], [23, 14], 
                                     [24, 13], [24, 14], [25, 11], [25, 15], 
                                     [26, 13], [26, 16], [27, 15], [27, 16], 
                                     [28, 15], [28, 17], [29, 17], [30, 14], 
                                     [31, 16], [32, 17]])
        
        self.assertEqual(idx_couples.shape, xpct_idx_couples.shape)
        np.testing.assert_array_equal(idx_couples, xpct_idx_couples)
    

    def test_edge_nsimplex_weight_flat(self) -> None:
        _, N1, Nn = self.mfld.shape
        border_eids = self.mfld.border()[1]
        nbr_border_edges = len(border_eids)

        edge_weights = edge_nsimplex_weight(on=self.mfld, 
                                            for_operator='dual_to_primal_flat')
        
        self.assertEqual(edge_weights.shape, (N1, Nn))
        self.assertIsInstance(edge_weights, sp.csr_matrix)
        np.testing.assert_array_almost_equal(edge_weights.sum(axis=1), 
                                      np.ones((N1, 1)))
        np.testing.assert_array_equal(edge_weights[border_eids].data,
                                    np.ones(nbr_border_edges))

    
    def test_project(self) -> None:

        manifold = self.mfld

        Nn = manifold.shape[-1]
        N1 = manifold.shape[1]
        ex = [1,0,0]
        vectorfield = np.array(Nn*ex).reshape((Nn, 3))

        # project of primal:
        coefs = project(vectorfield, on='primal', of=manifold)
        self.assertIsInstance(coefs, np.ndarray)
        self.assertEqual(coefs.shape, (N1,))
        
        xpct_coefs = np.array([1, 0.5, 1, -0.5, 0.5, 1, -0.5, 0.5, -0.5, 0.5, 1, 
                            0.5, 1, -0.5, 0.5, 1, -0.5, 0.5, -0.5, 0.5, 1, 0.5, 
                            1, -0.5, 0.5, 1, -0.5, 0.5, -0.5, 0.5, 1, 1, 1] )
        np.testing.assert_array_almost_equal(coefs, xpct_coefs)

        # project on dual
        proj_values = project(vectorfield, on='dual', of=manifold)
        
        xpct_proj_values = np.array([0.00000000e+00, -8.66025404e-01,
                                     0.00000000e+00, -8.66025404e-01,
                                    -8.66025404e-01,  0.00000000e+00,
                                    -8.66025404e-01, -8.66025404e-01,
                                    -8.66025404e-01,  8.66025404e-01,
                                    -1.92296269e-16, -8.66025404e-01,
                                     0.00000000e+00, -8.66025404e-01,
                                    -8.66025404e-01,  7.69185075e-16,
                                    -8.66025404e-01, -8.66025404e-01,
                                    -8.66025404e-01,  8.66025404e-01,
                                     3.84592537e-16, -8.66025404e-01,
                                    -1.53837015e-15, -8.66025404e-01,
                                    -8.66025404e-01,  1.53837015e-15,
                                    -8.66025404e-01, -8.66025404e-01,
                                    -8.66025404e-01,  8.66025404e-01,
                                    -1.53837015e-15,  1.53837015e-15,
                                     6.15348060e-15]) 
        print(proj_values)
        np.testing.assert_array_almost_equal(proj_values, xpct_proj_values)

    
    def test_dual_half_edges_vectors(self) -> None:
        mfld = self.mfld
        D = mfld.emb_dim
        n = mfld.dim
        k = n - 1
        Nk = mfld.shape[k]

        half_edges = _dual_half_edges_vectors(mfld)
        self.assertEqual(len(half_edges), Nk)
        
        for sid, vct in enumerate(half_edges):
            self.assertIsInstance(vct, np.ndarray)
            
            if sid in mfld.interior()[k]:
                self.assertEqual(vct.shape, (2, D))
            else:
                self.assertEqual(vct.shape, (1, D))

        # Test unit vectors
        half_edges = _dual_half_edges_vectors(mfld, normalized=True)
        for sid, vcts in enumerate(half_edges):
            np.testing.assert_array_almost_equal(lng.norm(vcts, axis=-1), 
                                                np.ones(vcts.shape[0]))
            
        # Test orientation choice
        half_edges_along_edge = _dual_half_edges_vectors(mfld, normalized=True)
        half_edges_twrd_nsplx = _dual_half_edges_vectors(mfld, normalized=True, 
                                        oriented_toward_nsimplex_centers=True)
        
        for sid, cfids in enumerate(mfld.cofaces(1)):
            if len(cfids) > 1:
                np.testing.assert_array_equal(half_edges_along_edge[sid][0],
                                            -1 * half_edges_twrd_nsplx[sid][0])
                np.testing.assert_array_equal(half_edges_along_edge[sid][1],
                                              half_edges_twrd_nsplx[sid][1])
            else:
                np.testing.assert_array_equal(*half_edges_along_edge[sid],
                                              *half_edges_twrd_nsplx[sid])
    

    def test_dual_half_edge_nsimplex_weights(self) -> None:
        mfld = self.mfld
        N1 = mfld.shape[1]
        
        coefs = _dual_half_edge_nsimplex_weights(mfld)
        
        self.assertEqual(len(coefs), N1)

        xpct_coefs = [np.ones(1) if len(cfids)==1 else .5*np.ones(2) 
                    for cfids in mfld.cofaces(1)]
        
        for sid, elmt in enumerate(coefs):
            self.assertIsInstance(elmt, np.ndarray)
            if sid in mfld.interior()[1]:
                self.assertEqual(elmt.size, 2)
            else:
                self.assertEqual(elmt.size, 1)
            np.testing.assert_array_almost_equal(elmt, xpct_coefs[sid])


    def test_dual_edges_orientation(self)-> None:
        mfld = self.mfld
        half_edges = _dual_half_edges_vectors(mfld, normalized=True)
        signs = _dual_edges_orientation(mfld, half_edges)
        
        xpct_signs = np.array([-1, -1, -1, -1,  1, -1, -1,  1, -1, -1,  1, -1, 
                                1, -1,  1,  1, -1,  1, -1, -1,  1, -1,  1, -1,
                                1,  1, -1,  1, -1, -1, -1, -1, -1])
        
        self.assertIsInstance(signs, np.ndarray)
        self.assertEqual(signs.dtype, 'int32')
        np.testing.assert_array_equal(signs, xpct_signs)


    def test_dual_to_primal_flat(self) -> None:
        nbr_triangles = self.mfld.shape[-1]
        vct_fld = np.array([1, 0, 0]*nbr_triangles).reshape(nbr_triangles, 3)
        
        cchn = _dual_to_primal_flat(vct_fld, self.mfld, name='Gandalf')

        self.assertIsInstance(cchn, Cochain)
        self.assertEqual(cchn.dim, 1)
        self.assertFalse(cchn.isdual)
        self.assertEqual(cchn.shape, (self.mfld[1].size,))
        self.assertEqual(cchn.complex, self.mfld)
        self.assertEqual(cchn.name, 'Gandalf')
        
        horizontal_edge_ids = [0, 2, 5, 10, 12, 15, 20, 22, 25, 30, 31, 32]
        np.testing.assert_array_equal(cchn.values[horizontal_edge_ids],
                                      np.ones(12))
        
        other_edge_ids = list(set(np.arange(self.mfld[1].size)) 
                            - set(horizontal_edge_ids))
        np.testing.assert_array_almost_equal(abs(cchn.values[other_edge_ids]), 
                                                0.5*np.ones(21))
        
        # Ill-suited vector field
        vct_fld = np.array([1, 0]*nbr_triangles).reshape(nbr_triangles, 2)
        cchn = flat(vct_fld, self.mfld)
        assert cchn is None


    def test_dual_to_dual_flat(self) -> None:

        mfld = self.mfld
        n = mfld.dim
        k = n-1
        Nn = mfld.shape[n]
        Nk = mfld.shape[k]
        D = mfld.emb_dim
        
        ex = [1,0,0]
        ex_vector_field = np.array(Nn*ex).reshape((Nn, D))

        ex_cchn = flat(ex_vector_field, mfld, dual=True, name='jojo')
        self.assertIsInstance(ex_cchn, Cochain)
        self.assertEqual(ex_cchn.dim, 1)
        self.assertTrue(ex_cchn.isdual)
        self.assertEqual(ex_cchn.shape, (Nk,))
        self.assertEqual(ex_cchn.complex, mfld)

        # Test on bigger structure
        mfld = self.sph
        n = mfld.dim
        k = n-1
        Nk = mfld.shape[k]

        ez = np.array([0, 0, 1])
        center = mfld[-1].circumcenters.mean(axis=0)
        etheta = []
        for vertex in mfld[-1].circumcenters:
            er = vertex - center
            er /= lng.norm(er)
            etheta.append(np.cross(ez,er))

        etheta = np.array(etheta)
        N = etheta.shape[0]
        etheta /= lng.norm(etheta, axis=1).reshape((N,1))

        etheta_cchn = _dual_to_dual_flat(etheta, mfld, name='etheta')
        
        self.assertIsInstance(etheta_cchn, Cochain)
        self.assertEqual(etheta_cchn.shape, (Nk,))
        self.assertEqual(etheta_cchn.name, 'etheta')
        
        vals = etheta_cchn.values
        self.assertIsInstance(vals, np.ndarray)
        self.assertEqual(vals.shape, (Nk,))


    def test_flat_from_vectorvaluedcochain(self) -> None:
        mfld = self.mfld
        nbr_triangles = mfld.shape[-1]
        vct_fld = np.array([1, 0, 0]*nbr_triangles).reshape(nbr_triangles, 3)

        vfcchn = Cochain(mfld, values=vct_fld, dim=mfld.dim, dual=True)

        cchn = flat(vfcchn)

        self.assertIsInstance(cchn, Cochain)
        self.assertEqual(cchn.dim, 1)
        self.assertFalse(cchn.isdual)
        self.assertEqual(cchn.shape, (mfld[1].size,))
        self.assertEqual(cchn.complex, mfld)

        horizontal_edge_ids = [0, 2, 5, 10, 12, 15, 20, 22, 25, 30, 31, 32]
        np.testing.assert_array_equal(cchn.values[horizontal_edge_ids],
                                      np.ones(12))
        
        other_edge_ids = list(set(np.arange(self.mfld[1].size)) 
                            - set(horizontal_edge_ids))
        np.testing.assert_array_almost_equal(abs(cchn.values[other_edge_ids]), 
                                                0.5*np.ones(21))
        
        
    # Test sharp-related functions
    # ----------------------------
    def test_isvalid_for_sharp(self) -> None:
        
        self.assertFalse(_isvalid_for_sharp(np.ones(10)))

        for k, xpct in {1: True, 2:False}.items():
            Nk = self.mfld.shape[k]
            values = np.ones(Nk)

            c1 = Cochain(self.mfld, k, values)
            assert _isvalid_for_sharp(c1) is xpct
        
        k = 1
        Nk = self.mfld.shape[k]
        values = np.ones(Nk)
        c1 = np.ones(Nk)
        self.assertFalse(_isvalid_for_sharp(c1))


    def test_edge_nsimplex_weight_sharp(self) -> None:
        for cplx in [self.trgl, self.mfld]:
            Nn = cplx[-1].size
            wmtrx = edge_nsimplex_weight(cplx, for_operator='sharp')
            np.testing.assert_allclose(wmtrx.sum(axis=0), 
                                    np.ones(Nn).reshape((1, Nn)))
            
        self.assertIsNone(edge_nsimplex_weight(self.mfld,
                                               for_operator='flarp'))


    def test_interpolate_cochain_on_nsimplices(self) -> None:
        k = 1
        cplx = self.mfld
        Nk = cplx.shape[k]
        Nn = cplx.shape[-1]
        values = np.ones(Nk)

        c1 = Cochain(cplx, k, values)

        kvf = interpolate_cochain_on_nsimplices(c1)

        self.assertIsInstance(kvf, np.ndarray)
        self.assertEqual(kvf.shape, (Nn, Nk, 3))
        
        # --
        k = 1
        cplx = self.trgl
        Nk = cplx.shape[k]
        Nn = cplx.shape[-1]
        values = np.ones(Nk)

        c1 = Cochain(cplx, k, values)

        kvf = interpolate_cochain_on_nsimplices(c1)

        self.assertIsInstance(kvf, np.ndarray)
        self.assertEqual(kvf.shape, (Nn, Nk, 3))

        xpct_kvf = np.array([[[6.66666667e-01, -8.32667268e-17, 0.00000000e+00],
                            [ 3.33333333e-01,  5.77350269e-01,  0.00000000e+00],
                            [-3.33333333e-01, 5.77350269e-01, 0.00000000e+00]]])
        np.testing.assert_allclose(kvf, xpct_kvf, atol=1e-9)
        
    
    def test_sharp(self) -> None:
        k = 1
        cplx = self.mfld
        N1 = cplx.shape[k]
        Nn = cplx.shape[-1]
        values = np.ones(N1)

        c1 = Cochain(cplx, k, values)
        v1 = sharp(c1)
        
        self.assertIsInstance(v1, Cochain)
        self.assertTrue(v1.isdual)
        self.assertIsInstance(v1.values, np.ndarray)
        self.assertEqual(v1.values.shape, (Nn, 3))
        self.assertEqual(v1.complex, cplx)

        # sharp of flat
        ex_field = np.vstack((np.ones(Nn), np.zeros((2,Nn)))).T
        np.testing.assert_allclose(sharp(flat(ex_field, cplx)).values, 
                                ex_field, atol=1e-9)
        
        ey_field = np.vstack(( np.zeros(Nn), np.ones(Nn), np.zeros(Nn))).T
        np.testing.assert_allclose(sharp(flat(ey_field, cplx)).values, 
                                ey_field, atol=1e-9)
        
        self.assertIsNone(sharp(values))
    

    # Test div-related function
    # -------------------------
    def test_edge_nsimplex_weight_div(self) -> None:
        mfld = self.hexa
        n = mfld.dim
        
        weights = edge_nsimplex_weight(on=mfld, for_operator='div')
        
        N1, Nn = mfld.shape[1:]
        self.assertEqual(weights.shape, (N1, Nn))
        
        N1_out = len(mfld.border()[1])
        N1_in = len(mfld.interior()[1])
        Ndata = 2*N1_in+N1_out
        l = np.unique(mfld[n-1].covolumes[mfld.interior()[1]]).mean()/2
        xcpt_weights = l*np.ones(Ndata)
        np.testing.assert_array_almost_equal(weights.data, xcpt_weights)
    