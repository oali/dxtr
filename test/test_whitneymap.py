import unittest
import numpy as np

from dxtr.complexes import triangular_grid
from dxtr.cochains.whitneymap import WhitneyMap
from dxtr.math.geometry import (circumcenter_barycentric_coordinates,
                                gradient_barycentric_coordinates, whitney_form)

class TestWhitneyMap(unittest.TestCase):

    def setUp(self) -> None:
        """Initiates the test.
        """
        self.mfld = triangular_grid(manifold=True)

    def tearDown(self) -> None:
        """Concludes and closes the test.
        """
    
    def test_WhitneyMap_class(self)->None:
        # Whitney 1-form
        rows = [0, 0, 0]
        cols = [0, 2, 4]
        values = [[0,1,0], [0,1,0], [0,1,0]]
        positions = np.array([[.5, .5, 0]])

        xpct_indices = [(0, 0), (0, 2), (0, 4)]
        
        wm = WhitneyMap(values, positions, rows, cols)
        
        self.assertEqual(wm.degree, 1)
        self.assertEqual(wm.base.shape, (3, 3))
        self.assertIsInstance(wm.base, np.ndarray)
        self.assertIsInstance(wm.positions, np.ndarray)
        np.testing.assert_allclose(wm.base, values)
        np.testing.assert_allclose(wm.positions, positions)
        np.testing.assert_allclose(wm.indices, xpct_indices)

        # Whitney 2-form
        rows = [0, 0, 0, 0]
        cols = [0, 1, 2, 3]
        values = [[[ 0,  1,  1],
                   [-1,  0, -1],
                   [-1,  1,  0]],
                   [[ 0,  1,  1],
                   [-1,  0,  1],
                   [-1, -1,  0]],
                   [[ 0, -1,  1],
                   [ 1,  0,  1],
                   [-1, -1,  0]],
                   [[ 0,  1, -1],
                   [-1,  0,  1],
                   [ 1, -1,  0]]]
        positions = [.5,.5,.5]
        
        xpct_indices = [(0, 0), (0, 1), (0, 2), (0, 3)]

        wm2 = WhitneyMap(values, positions, rows, cols)
        
        self.assertEqual(wm2.degree, 2)
        self.assertEqual(wm2.base.shape, (4, 3, 3))
        self.assertIsInstance(wm2.base, np.ndarray)
        self.assertIsInstance(wm2.positions, np.ndarray)
        np.testing.assert_allclose(wm2.base, values)
        np.testing.assert_allclose(wm2.positions, positions)
        np.testing.assert_allclose(wm2.indices, xpct_indices)

    
    def test_whitney_map_from_manifold(self) -> None:
        # Whitney map for 1-forms.
        k = 1
        wm = WhitneyMap.of(self.mfld, k)

        self.assertEqual(wm.degree, k)
        self.assertIsInstance(wm, WhitneyMap)
        self.assertIsInstance(wm.base, np.ndarray)
        self.assertEqual(wm.base.shape, 
                         (self.mfld.shape[-1]*3, self.mfld.emb_dim))
        
        self.assertIsNone(WhitneyMap.of(self.mfld, 2))
