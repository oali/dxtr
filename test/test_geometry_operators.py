import unittest
import numpy as np
import numpy.linalg as lng
import scipy.sparse as sp

from copy import deepcopy

from dxtr.complexes import SimplicialManifold, icosahedron, sphere, disk
from dxtr.cochains import Cochain
from dxtr.operators.curvature import (positions, projector, gaussian_curvature, 
                                     mean_curvature, normals, _isvalid,
                                     extrinsic_curvature, normal_curvature,
                                     Kg, Km)


class TestGeometricOperator(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        """Initiates the test.
        """

        cls.spheres = {size: sphere(manifold=True, size=size) 
                        for size in ['small', 'large']}
        
        cls.sph_cplx = sphere()
        cls.sph_mfld = cls.spheres['small']
        cls.sph_mfld_2 = sphere()
        cls.ico_cplx = icosahedron()
        cls.ico_mfld = icosahedron(manifold=True)
        cls.dsk_mfld = disk(manifold=True)

        
        indices = [[0, 1, 2, 3], [1, 2, 3, 4]]
        positions = [[0, 0, 0],
                    [1, 0, 0],
                    [0, 1, 0],
                    [0, 0, 1],
                    [1, 1, 1]]
        cls.mfld_3D = SimplicialManifold(indices, positions)


    def test_positions(self) -> None:
        mfld = self.ico_mfld

        x = positions(mfld)
        self.assertIsInstance(x, Cochain)
        self.assertEqual(x.complex, mfld)
        self.assertEqual(x.dim, 0)
        self.assertFalse(x.isdual)
        np.testing.assert_array_equal(x.values, mfld[0].vertices)
        
        # test dual positions
        x = positions(mfld, dual=True)
        self.assertIsInstance(x, Cochain)
        self.assertEqual(x.complex, mfld)
        self.assertEqual(x.dim, 0)
        self.assertTrue(x.isdual)
        np.testing.assert_array_equal(x.values, mfld[-1].circumcenters)

        # test on complex
        cplx = self.ico_cplx

        x = positions(cplx)
        self.assertIsInstance(x, Cochain)
        self.assertEqual(x.complex, cplx)
        self.assertEqual(x.dim, 0)
        self.assertFalse(x.isdual)
        np.testing.assert_array_equal(x.values, cplx[0].vertices)

        # test fail dual position on complex
        self.assertIsNone(positions(cplx, dual=True))

    def test_projector(self) -> None:
        mfld =self.dsk_mfld
        Nn = mfld.shape[-1]
        ex_fld = np.repeat([[1, 0, 0]], Nn, axis=0)
        vfld = Cochain(mfld, dim=0, dual=True, values=ex_fld)

        proj = projector(vfld)
        self.assertIsInstance(proj, Cochain)
        self.assertEqual(proj.dim, 0)
        self.assertTrue(proj.isdual)
        self.assertEqual(proj.shape, (Nn, 3, 3))

        xpct_values = np.repeat([[[1,0,0],
                                [0,0,0],
                                [0,0,0]]], Nn, axis=0)
        
        np.testing.assert_array_equal(proj.values, xpct_values)

        # Test amplitude: if the norm of the vector is doubled, the one 
        # of the projector must be multiplied by 4.
        vfld *= 2
        proj2 = projector(vfld)
        np.testing.assert_array_equal(proj2.values, 4*proj.values)

        # Test normalization
        proj3 = projector(vfld, normalized=True)
        np.testing.assert_array_equal(proj3.values, proj.values)

    def test_normal_of(self) -> None:  
        
        sph = self.sph_mfld
        nrl_vct = normals(sph)
        
        D = sph.emb_dim
        N0 = sph[0].size
        
        self.assertIsInstance(nrl_vct, Cochain)
        self.assertEqual(nrl_vct.dim, 0)
        self.assertFalse(nrl_vct.isdual)
        self.assertEqual(nrl_vct.complex, sph)
        self.assertEqual(nrl_vct.name, 'Unit Normal')
        self.assertEqual(nrl_vct.shape[0], N0)
        self.assertEqual(nrl_vct.values.shape, (N0, D))
        self.assertTrue(nrl_vct.isvectorvalued)
        
        np.testing.assert_array_almost_equal(lng.norm(nrl_vct.values, axis=-1),
                                             np.ones(N0))

        # Aligned with the radii of the sphere
        pts = sph[0].vertices
        radii = pts - pts.mean(axis=0)
        self.assertLess(lng.norm(np.cross(nrl_vct.values, radii), 
                                 axis=-1).mean(), 
                        1e-2)

    def test_normal_of_dual(self) -> None:  
        sph = self.sph_mfld
        nrl_vct = normals(sph, dual=True)
        
        D = sph.emb_dim
        N0 = sph[-1].size
        
        self.assertIsInstance(nrl_vct, Cochain)
        self.assertEqual(nrl_vct.dim, 0)
        self.assertTrue(nrl_vct.isdual)
        self.assertEqual(nrl_vct.complex, sph)
        self.assertEqual(nrl_vct.name, 'Unit Normal')
        self.assertEqual(nrl_vct.shape[0], N0)
        self.assertEqual(nrl_vct.values.shape, (N0, D))
        self.assertTrue(nrl_vct.isvectorvalued)
        
        np.testing.assert_array_almost_equal(lng.norm(nrl_vct.values, axis=-1),
                                            np.ones(N0))

        # Aligned with the radii of the sphere
        pts = sph[-1].circumcenters
        radii = pts - pts.mean(axis=0)
        self.assertLess(lng.norm(np.cross(nrl_vct.values, radii), 
                                 axis=-1).mean(), 
                        1e-2)

    def test_gaussian_curvature(self) -> None:
        sph = self.sph_mfld
        
        gauss = gaussian_curvature(sph)
        self.assertIsInstance(gauss, Cochain)
        self.assertEqual(gauss.dim, 0)
        self.assertEqual(gauss.complex, sph)
        self.assertEqual(gauss.name, 'Gaussian curvature')
        
        covolumes = sph[0].covolumes
        x = sum(kg*v for kg, v in zip(gauss.values, covolumes))
        chi = sph.euler_characteristic
        
        self.assertLess(abs(x/(2*np.pi*chi) - 1), 1e-10)

        # fail test with SimplicialComplex as input
        self.assertIsNone( gaussian_curvature(self.sph_cplx))

    def test_mean_curvature_primal(self) -> None:
        dsk = self.dsk_mfld
        
        mn_curv = mean_curvature(dsk)
        
        self.assertIsInstance(mn_curv, Cochain)
        self.assertEqual( mn_curv.dim, 0)
        self.assertEqual( mn_curv.complex, dsk)
        self.assertEqual( mn_curv.name, 'Mean Curvature')
        self.assertEqual( mn_curv.shape[0], dsk[0].size)
        
        # Mean curvature of flat surface must vanish at inner vertices.
        inner_vtx_ids = dsk.interior()[0]
        np.testing.assert_array_almost_equal(mn_curv.values[inner_vtx_ids], 
                                             np.zeros(len(inner_vtx_ids)))
        
        # Check the computation on spheres of increasing resolution.
        for size, cplx in self.spheres.items():
            pts, ctr = cplx[0].vertices, cplx[0].vertices.mean(axis=0)
            radii = lng.norm(pts - ctr, axis=1)
            R, dR = radii.mean(), radii.std()

            mn_curv = mean_curvature(cplx)
            K = mn_curv.values
            Km =  K.mean()

            xpct_value, threshold = 1/R, Km*dR/R**2
            good_values = mn_curv.values[abs(K - xpct_value) < threshold]
            ratio_good_values = good_values.shape[0]/cplx.shape[0]
            self.assertGreaterEqual(ratio_good_values, .98)

        # Curvature must scale with inverse of radius on sphere.
        for R in range(1,4):
            if R == 1:
                sphr = sphere(manifold=True)
            else:
                dsplcmt = {vid: r for vid, r in enumerate(radii)}
                sphr.update_geometry(dsplcmt)
                
            pts, ctr =sphr[0].vertices,sphr[0].vertices.mean(axis=0)
            radii = pts - ctr
            R, dR = lng.norm(radii, axis=1).mean(), lng.norm(radii, 
                                                             axis=1).std()
            
            mn_curv = mean_curvature(sphr)
            K = mn_curv.values
            Km =  K.mean()

            xpct_value, threshold = 1/R, dR/R**2
            good_values = mn_curv.values[abs(K - xpct_value) < threshold]
            ratio_good_values = good_values.shape[0]/sphr.shape[0]
            self.assertGreaterEqual(ratio_good_values, .98)

    def test_mean_curvature_dual(self) -> None:
        dsk = self.dsk_mfld
        mn_curv = mean_curvature(dsk, dual=True)
        
        self.assertIsInstance(mn_curv, Cochain)
        self.assertEqual(mn_curv.dim, 0)
        self.assertTrue(mn_curv.isdual)
        self.assertEqual(mn_curv.complex, dsk)
        self.assertEqual(mn_curv.name, 'Mean Curvature')
        self.assertEqual(mn_curv.shape[0], dsk[-1].size)
        
        # Mean curvature of flat surface must vanish at inner vertices.
        bnd_nsids = [dsk.cofaces(1)[idx][0] for idx in dsk.border()[1]]
        inner_nsids = [idx for idx in range(dsk[-1].size) 
                       if idx not in bnd_nsids]
        np.testing.assert_array_almost_equal(mn_curv.values[inner_nsids], 
                                            np.zeros(len(inner_nsids)))
        
    def test_mean_curvature_vector(self) -> None:
        
        sph = self.sph_mfld
        mn_curv_vf = mean_curvature(sph, return_vector=True)
        
        self.assertIsInstance(mn_curv_vf, Cochain)
        self.assertEqual(mn_curv_vf.dim, 0)
        self.assertEqual(mn_curv_vf.complex, sph)
        self.assertEqual(mn_curv_vf.name, 'Mean Curvature Normal Vector Field')
        
        N0 = sph[0].size
        self.assertEqual(mn_curv_vf.shape[0], N0)
        self.assertEqual(mn_curv_vf.values.shape, (N0, sph.emb_dim))

        unit_nrl_vf = deepcopy(mn_curv_vf.values)
        unit_nrl_vf /= lng.norm(unit_nrl_vf, axis=-1).reshape((N0, 1))

        pts = sph[0].vertices
        radii = pts - pts.mean(axis=0)
        xpct_unit_nrl_vf = radii / lng.norm(radii, axis=-1).reshape((N0, 1))

        cross_prod = np.cross(unit_nrl_vf, xpct_unit_nrl_vf)
        ampli_cross_prod = lng.norm(cross_prod, axis=-1)
        self.assertLess(ampli_cross_prod.mean(), 1e-2)

    def test_mean_curvature_vector_dual(self) -> None:
        sph = self.sph_mfld
        mn_curv_vf = mean_curvature(sph, return_vector=True, dual=True)
        
        self.assertIsInstance(mn_curv_vf, Cochain)
        self.assertEqual(mn_curv_vf.dim, 0)
        self.assertEqual(mn_curv_vf.complex, sph)
        self.assertEqual(mn_curv_vf.name, 'Mean Curvature Normal Vector Field')
        
        Nn = sph[-1].size
        self.assertEqual(mn_curv_vf.shape[0], Nn)
        self.assertEqual(mn_curv_vf.values.shape , (Nn, sph.emb_dim))

        unit_nrl_vf = deepcopy(mn_curv_vf.values)
        unit_nrl_vf /= lng.norm(unit_nrl_vf, axis=-1).reshape((Nn, 1))

        pts = sph[-1].circumcenters
        radii = pts - pts.mean(axis=0)
        xpct_unit_nrl_vf = radii / lng.norm(radii, axis=-1).reshape((Nn, 1))

        cross_prod = np.cross(unit_nrl_vf, xpct_unit_nrl_vf)
        ampli_cross_prod = lng.norm(cross_prod, axis=-1)
        self.assertLess(ampli_cross_prod.mean(), 1e-2)

    def test_extrinsic_curvature(self) -> None:
        mfld = self.sph_mfld
        N1, D = mfld.shape[1], mfld.emb_dim
        xcurv = extrinsic_curvature(mfld)

        self.assertIsInstance(xcurv, Cochain)
        self.assertEqual(xcurv.dim, 1)
        self.assertFalse(xcurv.isdual)
        self.assertTrue(xcurv.isvectorvalued)
        self.assertEqual(xcurv.values.shape, (N1, D))

        # Test that the extrinsic curvature is homogeneous on the sphere
        xcurv_norm = lng.norm(xcurv.values, axis=-1)
        N1 = xcurv_norm.shape[0]
        
        xk_mn, xk_std = xcurv_norm.mean(), xcurv_norm.std()
        self.assertTrue((xk_mn - xk_std) < 1 < (xk_mn + xk_std))


        # Test that the extrinsic curvature lives within the tangent bundle.
        nrl = normals(mfld).values
        fids = mfld.faces(1)
        nrl_on_1splcs = nrl[fids].mean(axis=1)
        angle = np.array([np.dot(n, dn) for n, dn in zip(nrl_on_1splcs, 
                                                        xcurv.values)])
        np.testing.assert_almost_equal(angle, np.zeros(N1))

        # Test that it does not compute on 3-SimplicialManifold
        self.assertIsNone( extrinsic_curvature(self.mfld_3D))
    
    def test_extrinsic_curvature_dual(self) -> None:
        mfld = self.sph_mfld
        N1, D = mfld.shape[1], mfld.emb_dim
        xcurv = extrinsic_curvature(mfld, dual=True)

        self.assertIsInstance(xcurv, Cochain)
        self.assertEqual(xcurv.dim, 1)
        self.assertTrue(xcurv.isdual)
        self.assertEqual(xcurv.values.shape, (N1, D))

        # Test that the extrinsic curvature is homogeneous on the sphere
        xcurv_norm = lng.norm(xcurv.values, axis=-1)
        N1 = xcurv_norm.shape[0]
        
        xk_mn, xk_std = xcurv_norm.mean(), xcurv_norm.std()
        self.assertTrue((xk_mn - xk_std) < 1 < (xk_mn + xk_std))

        # Test that the extrinsic curvature lives within the tangent bundle.
        nrl = normals(mfld, dual=True).values
        fids = mfld.cofaces(1)
        nrl_on_1splcs = nrl[fids].mean(axis=1)
        angle = np.array([np.dot(n, dn) for n, dn in zip(nrl_on_1splcs, 
                                                        xcurv.values)])
        np.testing.assert_almost_equal(angle, np.zeros(N1))

    def test_normal_curvature(self) -> None:
        mfld = sphere(manifold=True)

        nrls = normals(mfld, dual=True).values
        e_z = np.array([0, 0, 1])
        e_theta = Cochain(mfld, dim=0, dual=True, 
                        values=np.cross(e_z, nrls, axis=-1))
        
        e_phi = Cochain(mfld, dim=0, dual=True,
                        values=np.cross(e_theta.values, nrls, axis=-1))
        
        ncurv_theta = normal_curvature(e_theta)
        ncurv_phi = normal_curvature(e_phi)

        for ncurv in [ncurv_theta, ncurv_phi]:
            self.assertIsInstance(ncurv, Cochain)
            self.assertEqual(ncurv.complex, mfld)
            self.assertEqual(ncurv.dim, 0)
            self.assertTrue(ncurv.isdual)
            self.assertFalse(ncurv.isvectorvalued)
            self.assertLessEqual(np.abs(ncurv.values.mean()-1), 2e-2)

        # The normal curvature should scale inversly to the radius.
        center = mfld[0].vertices.mean(axis=0)
        displacements = {vid: splx.vertices - center 
                        for vid, splx in enumerate(mfld[0])}
        mfld.update_geometry(displacements)
        
        ncurv_theta_2 = normal_curvature(e_theta)
        self.assertLessEqual(np.abs(ncurv_theta_2.values.mean()/.5-1), 3e-2)

    def test_kg(self) -> None:
        sph = self.sph_mfld
        
        gauss = gaussian_curvature(sph)
        kg = Kg(sph)
        self.assertIsInstance(kg, Cochain)
        self.assertEqual(kg.dim, gauss.dim)
        self.assertEqual(kg.complex, gauss.complex)
        self.assertEqual(kg.name, gauss.name)
        np.testing.assert_array_equal(kg.values, gauss.values)
    
    def test_km(self) -> None:
        sph = self.sph_mfld
        
        mean_curv = mean_curvature(sph)
        km = Km(sph)
        self.assertIsInstance(km, Cochain)
        self.assertEqual(km.dim, mean_curv.dim)
        self.assertEqual(km.complex, mean_curv.complex)
        self.assertEqual(km.name, mean_curv.name)
        np.testing.assert_array_equal(km.values, mean_curv.values)

    def test_isvalid(self) -> None:
        mfld = self.sph_mfld
        Nn = mfld.shape[-1]
        unit_vectors = np.repeat([[1,0,0]], Nn, axis=0)
        
        good_direction = Cochain(mfld, dim=0, values=unit_vectors, dual=True)
        self.assertTrue(_isvalid(good_direction))
        # 
        # wrong dim
        N1 = mfld.shape[1]
        unit_vectors1 = np.repeat([[1,0,0]], N1, axis=0)
        ill_direction = Cochain(mfld, dim=1, values=unit_vectors1, dual=True)
        self.assertFalse(_isvalid(ill_direction))

        # is not dual
        ill_direction = Cochain(mfld, dim=0, values=unit_vectors)
        self.assertFalse(_isvalid(ill_direction))

        # Wrong vector shape
        ill_direction = Cochain(mfld, dim=0, values=np.ones((Nn, 1)), dual=True)
        self.assertFalse(_isvalid(ill_direction))
        
        # Wrong vector type
        ill_direction = Cochain(mfld, dim=0, values=sp.identity(Nn), dual=True)
        self.assertFalse(_isvalid(ill_direction))