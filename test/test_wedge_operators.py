import unittest
import numpy as np

from dxtr.cochains import Cochain
from dxtr.complexes import icosahedron, triangle, tetrahedron, sphere, diamond
from dxtr.operators.wedge import wedge, _are_valid, _wedge_coefficients


class TestWedgeOperator(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        self.complex = icosahedron()
        self.manifold = icosahedron(manifold=True)
        self.triangle = triangle()
        self.tetrahedron = tetrahedron()
        self.diamond = diamond()

    def tearDown(self):
        """Concludes and closes the test.
        """
    

    def test_are_valid(self) -> None:
        cplx = self.complex
        mfld = self.manifold
        
        k = 1
        Nk = cplx[k].size
        Nk1 = cplx[k+1].size

        c1 = Cochain(cplx, k, np.ones(Nk))
        self.assertTrue(_are_valid(c1, c1))

        c2 = Cochain(cplx, k+1, np.ones(Nk1))
        self.assertTrue(_are_valid(c1, c2))
        
        # Dual cochains not accepted
        c1d = Cochain(cplx, k, np.ones(Nk), dual=True)
        c2d = Cochain(cplx, k+1, np.ones(Nk1), dual=True)
        self.assertFalse(_are_valid(c1d, c2d))

        # Invalid input type.
        c3 = np.ones(Nk)
        self.assertFalse(_are_valid(c2, c3))

        # Vector-valued input `Cochain`.
        c4 = Cochain(cplx, k, np.ones((Nk,3)))
        self.assertFalse(_are_valid(c1, c4))

        # Inputs defined on different complexes.
        c5 = Cochain(mfld, k, np.ones(Nk))
        self.assertFalse(_are_valid(c2, c5))

        # Input not sharing the same duality
        c6 = Cochain(cplx, k, np.ones(Nk), dual=True)
        self.assertFalse(_are_valid(c1, c6) )


    def test_wedge_coefficients_one_triangle(self) -> None:
        
        cplx = self.triangle

        decimal = 15
        precision = 10 ** (-decimal)
        
        # two 1-cochains
        k = 1
        Nk = cplx[k].size

        ck1 = Cochain(cplx, k, np.random.rand(Nk))
        ck2 = Cochain(cplx, k, np.random.rand(Nk))
        
        c1, c2 = ck1.values, ck2.values
        xpct_value = 1 / 6 * (c1[0]*c2[2] - c1[2]*c2[0] +
                              c1[0]*c2[1] - c1[1]*c2[0] +
                              c1[1]*c2[2] - c1[2]*c2[1] )
        
        value = _wedge_coefficients(ck1, ck2)[0]
        self.assertTrue(abs(value - xpct_value) < precision)

        # Anti-symmetry
        inv_value = _wedge_coefficients(ck2, ck1)[0]
        self.assertTrue(abs(value + inv_value) < precision)

        null_value = _wedge_coefficients(ck1, ck1)[0]
        self.assertTrue(null_value < precision)

        # 2-cochain & 0-cochain
        k, l = 2, 0

        Nk = cplx[k].size
        Nl = cplx[l].size

        ck = Cochain(cplx, k, np.random.rand(Nk))
        cl = Cochain(cplx, l, np.random.rand(Nl))
        
        xpct_value = ck.values * cl.values.mean()
        value = _wedge_coefficients(ck, cl)[0]
        self.assertTrue(abs(value - xpct_value) < precision)

        inv_value = _wedge_coefficients(cl, ck)[0]
        self.assertTrue(abs(value - inv_value) < precision)

        # 1-cochain & 0-cochain
        k, l = 1, 0

        Nk = cplx[k].size
        Nl = cplx[l].size

        ck = Cochain(cplx, k, np.random.rand(Nk))
        cl = Cochain(cplx, l, np.random.rand(Nl))
        
        xpct_values = [ckval * cl.values[cplx.faces(k,l)].mean(axis=1)[i] 
                       for i, ckval in enumerate(ck.values)]
        values = _wedge_coefficients(ck, cl)
        np.testing.assert_almost_equal(values, xpct_values, decimal)
        
        inv_values = _wedge_coefficients(cl, ck)
        np.testing.assert_almost_equal(inv_values, values, decimal)


    def test_wedge_coefficients_one_tetrahedron(self) -> None:
        cplx = self.tetrahedron
        
        decimal = 15
        precision = 10 ** (-decimal)

        # 1-cochain & 2-cochain
        k, l = 2, 1
        Nk, Nl = cplx[k].size, cplx[l].size
        
        ck = Cochain(cplx, k, np.random.rand(Nk))
        cl = Cochain(cplx, l, np.random.rand(Nl))

        c2, c1 = ck.values, cl.values
        xpct_value = 1 / 12 * (c2[0] * ( c1[2] + c1[4] + c1[5]) +
                            c2[1] * (-c1[1] - c1[3] + c1[5]) +
                            c2[2] * ( c1[0] - c1[3] - c1[4]) +
                            c2[3] * ( c1[0] + c1[1] + c1[2]))

        value = _wedge_coefficients(ck, cl)[0]
        self.assertTrue(abs(value - xpct_value) < precision)
        
        inv_value = _wedge_coefficients(cl, ck)[0]
        self.assertTrue(abs(value - inv_value) < precision)

        # 3-cochain & 0-cochain
        k, l = 3, 0
        Nk, Nl = cplx[k].size, cplx[l].size
        
        ck = Cochain(cplx, k, np.random.rand(Nk))
        cl = Cochain(cplx, l, np.random.rand(Nl))
        
        xpct_value = ck.values * cl.values.mean()
        
        value = _wedge_coefficients(ck, cl)[0]
        self.assertTrue(abs(value - xpct_value) < precision)

        inv_value = _wedge_coefficients(cl, ck)[0]
        self.assertTrue(abs(value - inv_value) < precision)


    def test_wedge_coefficients_on_large_complex(self) -> None:
        cplx = sphere('massive')
        
        N0, N1, N2 = cplx.shape
        
        decimal = 15

        # 2-cochain & 0-cochain
        
        init_values = np.random.rand(N2)
        ck = Cochain(cplx, 2, init_values)
        cl = Cochain(cplx, 0, np.random.rand(N0))
        
        fids = cplx.faces(2,0)
        lvals = cl.values[fids].mean(axis=1)
        xpct_values = [ckval * lvals[i] for i, ckval in enumerate(init_values)]
        
        np.testing.assert_array_almost_equal(_wedge_coefficients(ck, cl), 
                                            xpct_values, decimal)
        
        # Same dim cochains
        c1 = Cochain(cplx, 1, np.random.rand(N1))
        c1bis = Cochain(cplx, 1, np.random.rand(N1))
        self.assertEqual(_wedge_coefficients(c1, c1bis).shape[0], N2)


    def test_wedge_wrong_input(self) -> None:
        cplx = self.complex
        c1 = Cochain(cplx, 1, np.ones(cplx[1].size))
        
        self.assertIsNone(wedge(c1, np.ones(12)))


    def test_wedge_too_big_inputs(self) -> None:
        """Test that it returns a null `Cochain` if cochains dimensions too big.
        """
        cplx = self.complex
        _, N1, N2 = cplx.shape
        
        c1 = Cochain(cplx, 1, np.ones(N1))
        c2 = Cochain(cplx, 2, np.ones(N2))

        c3 = wedge(c1, c2)

        self.assertIsInstance(c3, Cochain)
        self.assertIs(c3.complex, cplx)
        self.assertEqual(c3.dim, cplx.dim)
        np.testing.assert_array_equal(c3.values, np.zeros(N2))


    def test_wedge_0cochains(self) -> None:
        """We test here the outer product with a 0-`Cochain`.
        """
        cplx = self.complex
        N0, N1, N2 = cplx.shape
        
        # 2 0-cochains together
        c1 = Cochain(cplx, 0, np.arange(N0))
        c2 = Cochain(cplx, 0, 2*np.ones(N0))
        
        c12 = wedge(c1, c2)
        
        self.assertIsInstance(c12, Cochain)
        self.assertIs(c12.complex, cplx)
        self.assertEqual(c12.dim, c1.dim)
        np.testing.assert_array_equal(c12.values, 2*np.arange(N0))

        # 2-cochain wedge 0-cochain
        c3 = Cochain(cplx, 2, np.arange(N2))

        c23 = wedge(c2, c3)

        self.assertIsInstance(c23, Cochain)
        self.assertEqual(c23.dim, c3.dim)
        np.testing.assert_array_equal(c23.values, 2*c3.values)

        # 1-cochain wedge 0-cochain
        c4 = Cochain(cplx, 1, 3 * np.arange(N1))

        c42 = wedge(c4,c2)

        self.assertIsInstance(c42, Cochain)
        self.assertEqual(c42.dim, c4.dim)
        np.testing.assert_array_equal(c42.values, 2*c4.values)


    def test_wedge_antisymmetry(self) -> None:
        """We test here that the wedge is indeed antisymmetrical."""

        cplx = self.diamond
        _, N1, N2, _ = cplx.shape

        # wedge between 2 1-cochains must be anti-symmetrical
        c1 = Cochain(cplx, 1, np.ones(N1))
        c1b = Cochain(cplx, 1, np.arange(N1))

        np.testing.assert_array_equal(wedge(c1, c1b).values, 
                                      -wedge(c1b, c1).values)

        # wedge between 1-cochain and 2-cochain must be symmetrical
        c2 = Cochain(cplx, 2, np.arange(N2))

        np.testing.assert_array_equal(wedge(c1, c2).values, 
                                      wedge(c2, c1).values)
        
        # wedge with twice the same `Cochain` should vanish
        np.testing.assert_array_equal(wedge(c1, c1).values, np.zeros(N2))


    def test_wedge_volume_form(self) -> None:
        """We test here that the wedge between unit `Cochain` yields a discrte 
        version of the volume form.
        """
        cplx = diamond()
        k, l = 2, 1

        c1 = Cochain(cplx, l, cplx[l].volumes)
        c2 = Cochain(cplx, k, cplx[k].volumes)

        xpct_volumes = cplx[k+1].volumes

        print(xpct_volumes)
        print(wedge(c2, c1).values)
        print(xpct_volumes/wedge(c2, c1).values)

