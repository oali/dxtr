import unittest
import os

from typing import Iterable

import numpy as np
from numpy import pi

import pandas as pd

from dxtr.complexes import Simplex, SimplicialComplex, sphere
from dxtr.utils.quality_control import (simplex_shape_regularity_score,
                                        shape_regularity_score,
                                        well_centered_score,
                                        complex_quality)

class TestQualityControl(unittest.TestCase):

    def setUp(self) -> None:
        """Initiates the test.
        """

        # Structures to run tests on
        # icosahedron (super regular, high quality)
        indices = [[0, 1, 2], [0, 2, 3],
                [0, 3, 4], [0, 4, 5],
                [0, 5, 1], # upper dome ends here
                [1, 7, 2],
                [2, 8, 3], [3, 9, 4],
                [4, 10, 5], [5, 6, 1],  # upper belt ends here
                [7, 2, 8], [8, 3, 9],
                [9, 4, 10], [10, 5, 6],
                [6, 1, 7],  # lower belt ends here 
                [6, 7, 11],
                [7, 8, 11], [8, 9, 11],
                [9, 10, 11], [10, 6, 11]]  # lower dome ends here

        gr = (1 + np.sqrt(5)) / 2

        positions = np.array([[-1, gr, 0], [1, gr, 0],
                            [0, 1, -gr], [-gr, 0, -1],
                            [-gr, 0, 1], [0, 1, gr],
                            [gr, 0, 1], [gr, 0, -1],
                            [0, -1, -gr], [-1, -gr, 0],
                            [0, -1, gr], [1, -gr, 0]])

        self.icosa = SimplicialComplex(indices, positions)

        # Cube (not so regular)
        indices = [[0, 1, 2], [0, 2, 3],
                [0, 1, 5], [0, 4, 5],
                [1, 2, 6], [1, 5, 6],
                [2, 3, 7], [2, 6, 7],
                [0, 3, 4], [3, 4, 7],
                [4, 5, 6], [4, 6, 7]]

        positions = np.array([[0, 0, 0],
                            [1, 0, 0],
                            [1, 1, 0],
                            [0, 1, 0],
                            [0, 0, 1],
                            [1, 0, 1],
                            [1, 1, 1],
                            [0, 1, 1]])

        self.cube = SimplicialComplex(indices, positions)

        # Sphere
        self.sphere = sphere('small')
        

    def tearDown(self) -> None:
        """Concludes and closes the test.
        """


    def pts_2D(self, angle:float, radius:float=1) -> np.ndarray[float, int]:
        """Computes the vertex position of an isosceles triangle.
            
            Useful function for the test_simplex_shape_regulatory_score below.

        Parameters
        ----------
        angle :
            The isoscele angle given in radian.
        radius:
            The radius of the circumscribed circle.
        
        Returns
        -------
        the 3*3 array containing the position vectors of the triangle summits. 
        """
        c = np.cos(pi - angle)
        s = np.sin(pi - angle)
        return radius * np.array([[1, 0, 0],
                                [c, s, 0],
                                [c, -s, 0]])
    

    def pts_3D(self, angles:Iterable[float], 
                radius:float=1) -> np.ndarray[float, int]:
        """Computes the vertex position of an isosceles triangle.

            Useful function for the test_simplex_shape_regulatory_score below.

        Parameters
        ----------
        angles :
            two angles, the azimutal and the base isoscele one,
            given in radian.
        radius:
            The radius of the circumscribed sphere.
        
        Returns
        -------
        the 3*3 array containing the position vectors 
        of the triangle summits. 
        """
        theta, phi = angles
        R_base = radius * np.sin(2*theta)
        z_base = - radius * np.cos(2*theta)
        
        c = np.cos(pi - phi)
        s = np.sin(pi - phi)

        return np.array([[0, 0, radius],
                        [R_base, 0, z_base],
                        [R_base*c, R_base*s, z_base],
                        [R_base*c, -R_base*s, z_base]])


    def test_simplex_regularity_score(self) -> None:
        
        # 1-simplex
        ids = [0, 1]
        pts = np.array([[0,0,0], [0,0,1]])
        splx = Simplex(ids, _vertices=pts)
        self.assertEqual(simplex_shape_regularity_score(splx), 1)
        
        # 2-simplices
        ids = [0, 1, 2]
        
        splx = Simplex(ids, _vertices=self.pts_2D(pi))
        self.assertAlmostEqual(simplex_shape_regularity_score(splx), 0)

        splx = Simplex(ids, _vertices=self.pts_2D(pi/3))
        self.assertAlmostEqual(simplex_shape_regularity_score(splx), 1)

        splx = Simplex(ids, _vertices=self.pts_2D(0))
        self.assertAlmostEqual(simplex_shape_regularity_score(splx),
                               1- 1/np.sqrt(2))

        # Cube
        for splx in self.cube[2]:
            print(simplex_shape_regularity_score(splx))

        # 3-simplices
        ids = [0, 1, 2, 3]
        
        splx = Simplex(ids, _vertices=self.pts_3D((0, pi/3)))
        self.assertAlmostEqual(simplex_shape_regularity_score(splx), 0)

        splx = Simplex(ids, _vertices=self.pts_3D((pi/2, pi/3)))
        self.assertAlmostEqual(simplex_shape_regularity_score(splx), 0)

        splx = Simplex(ids, _vertices=self.pts_3D((pi/5, pi/3)))
        self.assertAlmostEqual(simplex_shape_regularity_score(splx), 1, 
                                                              delta=1e-2)
    

    def test_shape_regularity_score(self) -> None:

        med, std = shape_regularity_score(self.icosa)
        self.assertAlmostEqual(med, 1)
        self.assertAlmostEqual(std, 0)

        med, std = shape_regularity_score(self.cube)
        self.assertAlmostEqual(med, 0.8284271247)
        self.assertAlmostEqual(std, 0)

        scores = shape_regularity_score(self.sphere, return_scores=True)
        self.assertEqual(scores.shape[0], self.sphere.shape[-1])
        self.assertEqual(len(np.where((scores > 1) | (scores < 0))[0]), 0)


    def test_well_centered_score(self) -> None:
        self.assertAlmostEqual(well_centered_score(self.icosa, verbose=True), 1)
        
        ratio, scores = well_centered_score(self.cube, return_scores=True)
        self.assertAlmostEqual(ratio, 1)
        np.testing.assert_array_almost_equal(scores, np.zeros(12))

    
    def test_complex_quality(self) -> None:

        quality_scores = complex_quality(self.sphere)
        self.assertIsInstance(quality_scores, pd.DataFrame)
        
        expected_columns = ['Shape regularity score', 
                            'Well-centeredness score', 
                            'Well-centered']

        np.testing.assert_array_equal(quality_scores.columns, expected_columns)

        assert len(quality_scores) == self.sphere.shape[-1]
        