import unittest
import numpy as np

from dxtr import SimplicialComplex, Cochain

from dxtr.complexes import sphere
from dxtr.cochains import unit_cochain, normal_vector_field
from dxtr.utils.typecheck import typecheck, valid_input


class TestTypeCheck(unittest.TestCase):

    def setUp(self) -> None:
        """Initiates the test.
        """
        
        self.cplx = sphere()
        self.mfld = sphere(manifold=True)
        self.kvector = normal_vector_field(manifold_name='sphere')

        k = 1
        self.cochain = unit_cochain(complex_name='sphere')
        # Nk = self.cplx[k].size
        # self.cochain = Cochain(self.cplx, dim=k, values=np.ones(Nk))


    def tearDown(self) -> None:
        """Concludes and closes the test.
        """

        
    def test_typecheck(self)->None:

        def simple_function(obj, expected_type=SimplicialComplex) -> bool:
            return isinstance(obj, expected_type)

        # First test with Checking if inputs are SimplicialComplex
        @typecheck(SimplicialComplex)
        def typecheked_func(complex, *args, **kwargs):
            return simple_function(complex, *args, **kwargs)
        
        # simplest cases
        self.assertTrue(typecheked_func(self.cplx))
        self.assertTrue(typecheked_func(self.mfld))
        
        # with optional argument
        self.assertTrue(typecheked_func(self.mfld,
                                        expected_type=SimplicialComplex))

        # counter test
        self.assertIsNone(typecheked_func(self.cochain))

        # First test with Checking if inputs are SimplicialComplex
        @typecheck(Cochain)
        def typecheked_func(complex, *args, **kwargs):
            return simple_function(complex, *args, **kwargs)
        
        self.assertIsNone(typecheked_func(self.cplx))
        self.assertIsNone(typecheked_func(self.mfld))
        self.assertTrue(typecheked_func(self.cochain, expected_type=Cochain))

        
    def test_valid_input(self) -> None:

        @valid_input
        def f(obj):
            return True

        self.assertTrue(f(self.mfld))
        self.assertTrue(f(self.cochain))
        self.assertTrue(f(self.kvector))
        self.assertIsNone(f(np.ones(10)))