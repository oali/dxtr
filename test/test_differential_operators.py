import unittest
import numpy as np
import numpy.linalg as lng
import scipy.sparse as sp

from dxtr.complexes import icosahedron, sphere
from dxtr.cochains import Cochain, cochain_base, unit_cochain, random_cochain
from dxtr.operators.differential import (exterior_derivative, codifferential, 
                                         laplacian, d, delta, L)


class TestDifferentialOperator(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """
        self.complex = icosahedron()
        self.manifold = icosahedron(manifold=True)
        self.sphere = sphere(manifold=True)


    def tearDown(self):
        """Concludes and closes the test.
        """


    def test_exterior_derivative_ouput_format(self) -> None:
        """The exterior derivative yields a `Cochain` of same duality and 
        of increased dimension.
        """
        mfld = self.manifold
        n = mfld.dim

        for k in range(n):
            for isdual in [False, True]:
                cochain = random_cochain('icosa', dim=k, dual=isdual, 
                                         manifold=True)
                xderi = exterior_derivative(cochain)

                self.assertIsInstance(xderi, Cochain)
                self.assertEqual(xderi.dim, k+1)
                self.assertEqual(xderi.isdual, isdual)

                Nkup = mfld[n-k-1].size if isdual else mfld[k+1].size
                self.assertEqual(xderi.values.shape[0], Nkup)

    
    def test_exterior_derivative_of_constant_0cochain(self) -> None:
        """The exterior derivative of a 0-cochain must vanish.
        """
        mfld = self.manifold
        n = mfld.dim

        # The exterior derivative of a constant 0-cochain should vanish
        k = 0

        # Primal case:
        cst_0_primal_cochain = unit_cochain('icosa', dim=k, manifold=True)
        deri_cst = exterior_derivative(cst_0_primal_cochain)
        
        Nkup = mfld[k+1].size
        np.testing.assert_array_equal(deri_cst.values, np.zeros(Nkup))

        # Dual case:
        cst_0_dual_cochain = unit_cochain('icosa', dim=k, dual=True, 
                                          manifold=True)
        deri_cst = exterior_derivative(cst_0_dual_cochain)
        
        Nkup = mfld[n-k-1].size
        np.testing.assert_array_equal(deri_cst.values, np.zeros(Nkup))


    def test_exterior_derivative_applied_twice(self) -> None:
        """Applied twice, the exterior derivative should yield a zero `Cochain`.
        This is an important feature of the theory that needs to be well grasped 
        by our implementation.
        """
        mfld = self.manifold
        n = mfld.dim
        k = 0

        # Primal case:
        rand_0_primal_cochain = random_cochain('icosa', dim=k, manifold=True)
        xdderi = exterior_derivative(exterior_derivative(rand_0_primal_cochain))
        
        Nk2up = mfld[k+2].size
        np.testing.assert_array_equal(xdderi.values, np.zeros(Nk2up))

        # Dual case:
        rand_0_dual_cochain = random_cochain('icosa', dim=k, manifold=True,
                                            dual=True)
        xdderi = exterior_derivative(exterior_derivative(rand_0_dual_cochain))
        
        Nk2up = mfld[n-k-2].size
        np.testing.assert_array_equal(xdderi.values, np.zeros(Nk2up))


    def test_exterior_derivative_null_on_top_elements(self) -> None:
        """The exterior derivative of a Cochain defined on top-simplices
        or dual top dual cells mush yields zeros.
        """
        mfld = self.manifold
        n = mfld.dim

        # Primal case:
        rand_n_primal_cochain = random_cochain('icosa', dim=n, manifold=True)
        xderi = exterior_derivative(rand_n_primal_cochain)
        
        Nn = mfld[n].size
        np.testing.assert_array_equal(xderi.values, np.zeros(Nn))
        
        # Dual case:
        rand_n_dual_cochain = random_cochain('icosa', dim=n, dual=True,
                                            manifold=True)
        xderi = exterior_derivative(rand_n_dual_cochain)
        
        N0 = mfld[0].size
        np.testing.assert_array_equal(xderi.values, np.zeros(N0))

        # If applied to cochain of even higher dimension, 
        # the exterior derivative must remains egual to zero.
        k = n + 1
        
        # Primal case:
        Nk = mfld[n].size
        rndm = np.random.random(Nk)
        rand_n_primal_cochain = Cochain(mfld, dim=k,values=rndm)

        xderi = exterior_derivative(rand_n_primal_cochain)
        np.testing.assert_array_equal(xderi.values, np.zeros(Nk))
        
        # Dual case:
        Nk = mfld[0].size
        rndm = np.random.random(Nk)
        rand_n_dual_cochain = Cochain(mfld, dim=k,values=rndm, dual=True)

        xderi = exterior_derivative(rand_n_dual_cochain)
        np.testing.assert_array_equal(xderi.values, np.zeros(Nk))


    def test_exterior_derivative_on_sparse_matrix(self) -> None:
        """We check the proper shape of the outputs of the exterior derivatvie
        when the inputs corresponds to sparse matrices.
        This case happends when we compute the Laplacian operator directlu on a 
        complex, so far in the case of spectral analysis.
        """
        
        mfld = self.manifold
        n = mfld.dim
        N = mfld.shape
        
        for k in range(0, n+1):
            # Primal case
            cb = cochain_base(mfld, dim=k)
            xderi = exterior_derivative(cb)
            self.assertIsInstance(xderi.values, sp.csr_matrix)
            self.assertEqual(xderi.values.shape, (N[np.clip(k+1,0,n)], N[k]))
            
            # Primal case
            cb = cochain_base(mfld, dim=k, dual=True)
            xderi = exterior_derivative(cb)
            self.assertIsInstance(xderi.values, sp.csr_matrix)
            self.assertEqual(xderi.values.shape, (N[n-np.clip(k+1,0,n)], 
                                                  N[n-k]))


    def test_codifferential_instantiation(self) -> None:
        """Checking that the codifferential yields the proper objects.
        """
        # If the cochain is not defined on a `SimplicialComplex`, we get None.
        
        cplx = self.complex
        k = 1
        Nk = cplx[k].size
        cochain = Cochain(cplx, dim=k, values=np.ones(Nk))
        self.assertIsNone(codifferential(cochain))

        # Checking the properties of the codiff
        mfld = self.manifold
        n = mfld.dim

        for k in range(0, n+1):
            # Primal case
            Nk = mfld[k].size
            cochain = Cochain(mfld, dim=k, values=np.ones(Nk))
            codiff = codifferential(cochain)
            
            self.assertIsInstance(codiff, Cochain)
            self.assertEqual(codiff.dim, k - 1)
            self.assertEqual(codiff.isdual, cochain.isdual)
            self.assertEqual(codiff.values.shape[0], 
                             mfld[np.clip(k-1, 0, n)].size)
            
            # Dual case
            Nk = mfld[n-k].size
            cochain = Cochain(mfld, dim=k, values=np.ones(Nk), dual=True)
            codiff = codifferential(cochain)
            
            self.assertIsInstance(codiff, Cochain)
            self.assertEqual(codiff.dim, k - 1)
            self.assertEqual(codiff.isdual, cochain.isdual)
            self.assertEqual(codiff.values.shape[0], 
                             mfld[np.clip(n-k+1, 0, n)].size)


    def test_codifferential_of_constant_nCochain(self) -> None:
        """The codifferential of a constant n-cochain must vanish
        """
        mfld = self.manifold
        n = mfld.dim
        
        # Primal case
        Nn = mfld[n].size
        Nn_1 = mfld[n-1].size
        cochain = Cochain(mfld, dim=n, values=np.ones(Nn))

        np.testing.assert_array_almost_equal(codifferential(cochain).values,
                                            np.zeros(Nn_1))
        
        # Dual case
        k = n
        N0 = mfld[n-k].size
        N1 = mfld[n-k+1].size
        cochain = Cochain(mfld, dim=k, values=np.ones(N0), dual=True)

        np.testing.assert_array_almost_equal(codifferential(cochain).values,
                                            np.zeros(N1))
        
        # The codifferential of any 0-cochain should vanish
        # Primal case
        N0 = mfld[0].size
        cochain = Cochain(mfld, dim=0, values=np.random.random(N0))
        np.testing.assert_array_almost_equal(codifferential(cochain).values,
                                            np.zeros(N0))
        # Dual case
        Nn = mfld[n].size
        cochain = Cochain(mfld, dim=0, values=np.random.random(Nn), dual=True)
        np.testing.assert_array_almost_equal(codifferential(cochain).values,
                                            np.zeros(Nn))
        

    def test_codifferential_applied_twice(self) -> None:
        """Applying twice the codifferential to any cochain must make it vanish.
        """
        mfld = self.manifold
        n = mfld.dim
        
        # Primal case
        Nn = mfld[n].size
        Nn_2 = mfld[n-2].size
        cochain = Cochain(mfld, dim=n, values=np.random.random(Nn))
        
        codiff_1 = codifferential(cochain)
        codiff_2 = codifferential(codiff_1)
        np.testing.assert_array_almost_equal(codiff_2.values, np.zeros(Nn_2))
        
        # Dual case
        N0 = mfld[0].size
        N2 = mfld[2].size
        cochain = Cochain(mfld, dim=n, values=np.random.random(N0), dual=True)
        
        codiff_1 = codifferential(cochain)
        codiff_2 = codifferential(codiff_1)
        np.testing.assert_array_almost_equal(codiff_2.values, np.zeros(N2))


    def test_laplacian(self) -> None:
        """Basic tests on the laplacian.
        """

        # Returns None if applied on a `SimplicialComplex`-based `Cochain`
        self.assertIsNone(laplacian(unit_cochain('icosa')))
        
        # Returns None if applied on a `SimplicialComplex`
        self.assertIsNone(laplacian(self.complex, dim=1))

        # The Laplacian of a constant Cochain should vanish.        
        # Primal case
        cochain = unit_cochain('icosa', dim=0, manifold=True)
        lpc = laplacian(cochain)
        
        self.assertIsInstance(lpc, Cochain)
        self.assertEqual(lpc.isdual, cochain.isdual)
        self.assertEqual(lpc.dim, cochain.dim)
        
        N0 = self.manifold[0].size
        self.assertEqual(lpc.values.shape[0], N0)

        np.testing.assert_array_almost_equal(lpc.values, np.zeros(N0))
        
        # Dual case
        cochain = unit_cochain('icosa', dim=0, dual=True, manifold=True)
        lpc = laplacian(cochain)
        
        self.assertIsInstance(lpc, Cochain)
        self.assertEqual(lpc.isdual, cochain.isdual)
        self.assertEqual(lpc.dim, cochain.dim)
        Nn = self.manifold[-1].size
        self.assertEqual(lpc.values.shape[0], Nn)

        np.testing.assert_array_almost_equal(lpc.values, np.zeros(Nn))


    def test_laplacian_on_cochain_base(self) -> None:
        """Testing the Laplacian when applied directly on a 
        `SimplicialManifold`and not a `Cochain`. This happends during spectral 
        analysis. We generate a base of Cochains and consequently the Laplacian 
        is fed with sparse matrices. Here we check that when fed with sparse 
        matrices, the Laplacian, at least, returns sparse matrices-valued 
        Cochains with the proper dimensions.
        """
        mfld = self.manifold
        n = mfld.dim

        for k in range(0, n+1):
            
            # Primal case
            Nk = mfld[k].size
            lpc = laplacian(mfld, dim=k)
            
            self.assertIsInstance(lpc, Cochain)
            self.assertEqual(lpc.dim, k)
            self.assertIsInstance(lpc.values, sp.csr_matrix)
            self.assertEqual(lpc.values.shape, (Nk, Nk))

            # Dual case
            Nk = mfld[n-k].size
            lpc = laplacian(mfld, dim=k, dual=True)
            
            self.assertIsInstance(lpc, Cochain)
            self.assertEqual(lpc.dim, k)
            self.assertIsInstance(lpc.values, sp.csr_matrix)
            self.assertEqual(lpc.values.shape, (Nk, Nk))


    def test_laplacian_and_curvature(self) -> None:
        """One important use of the Laplacian is in the definition of curvature.
        We Check that in the case of a sphere we get the expected values.
        The results are rather encouraging on the primal mesh but less good 
        on the dual one.
        TODO: Improve it !
        """
        mfld = self.sphere

        # Primal case
        positions = Cochain(mfld, dim=0, values=mfld[0].vertices)
        curvature = .5 * L(positions)
        curv = lng.norm(curvature.values, axis=-1)
        xpct_curv = np.ones(mfld[0].size)
        
        err_mn = np.abs(curv - xpct_curv).mean()
        err_sd = np.abs(curv - xpct_curv).std()
        self.assertTrue(err_mn < 1e-3)
        self.assertTrue(err_sd < 2e-2)

        # Dual case
        positions = Cochain(mfld, dim=0, dual=True, 
                            values=mfld[-1].circumcenters)
        curvature = .5 * L(positions)
        curv = lng.norm(curvature.values, axis=-1)
        
        xpct_curv = np.ones(mfld[-1].size)
        err_mn = np.abs(curv - xpct_curv).mean()
        err_sd = np.abs(curv - xpct_curv).std()
        
        self.assertTrue(err_mn < 1e-1)
        self.assertTrue(err_sd < 2e-1)
