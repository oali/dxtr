from tkinter import W
import unittest
import os
import numpy as np
import numpy.linalg as lng

from copy import deepcopy
from numpy import pi
from dxtr.complexes.simplicialmanifold import (SimplicialManifold,
                                             _detect_ill_centered_cofaces,
                                             dual_edge_vectors, edge_vectors,
                                             _orient_top_simplex_couples,
                                             _compute_dihedral_angles)
from dxtr.math.geometry import circumcenter
from dxtr.complexes import (triangular_grid, icosahedron, sphere, cube, 
                            hexagon, disk)

class TestSimplicialManifold(unittest.TestCase):
    
    @classmethod
    def setUp(cls):
        """Initiates the test.
        """
        
        # Structures to run tests on
        cls.cube = cube(manifold=True)
        cls.icosa = icosahedron(manifold=True)
        cls.icosa_cplx = icosahedron()
        cls.sphere = sphere(manifold=True) 
        cls.trigrid = triangular_grid(manifold=True)
        cls.hexa_cplx = hexagon()
        cls.sph = sphere()
        cls.dsk = disk(manifold=True)

        cls.path = os.getcwd() 
        cls.path += '/src/dxtr/utils/mesh_examples/sphere_small.ply'
    
    def tearDown(self):
        """Concludes and closes the test.
        """
    
    def test_SimplicialManifold_from_file(self) -> None:
        mfld = SimplicialManifold.from_file(self.path)

        self.assertIsInstance(mfld, SimplicialManifold)

    def test_SimplicialManifold_name(self) -> None:
        self.assertEqual(self.cube.name, 
                         '2D Simplicial Manifold')
    
    def test_SimplicialManifold_name_setter(self) -> None:
        self.cube.name = 'cube'
        self.assertEqual(self.cube.name, 'cube')
    
    def test_SimplicialManifold_circumcenters(self) -> None:
        xpct_circumcenters = [[[-1.        ,  1.61803399,  0.        ],
                               [ 1.        ,  1.61803399,  0.        ],
                               [ 0.        ,  1.        , -1.61803399],
                               [-1.61803399,  0.        , -1.        ],
                               [-1.61803399,  0.        ,  1.        ],
                               [ 0.        ,  1.        ,  1.61803399],
                               [ 1.61803399,  0.        ,  1.        ],
                               [ 1.61803399,  0.        , -1.        ],
                               [ 0.        , -1.        , -1.61803399],
                               [-1.        , -1.61803399,  0.        ],
                               [ 0.        , -1.        ,  1.61803399],
                               [ 1.        , -1.61803399,  0.        ]],
                              [[ 0.        ,  1.61803399,  0.        ],
                               [-0.5       ,  1.30901699, -0.80901699],
                               [-1.30901699,  0.80901699, -0.5       ],
                               [-1.30901699,  0.80901699,  0.5       ],
                               [-0.5       ,  1.30901699,  0.80901699],
                               [ 0.5       ,  1.30901699, -0.80901699],
                               [ 0.5       ,  1.30901699,  0.80901699],
                               [ 1.30901699,  0.80901699,  0.5       ],
                               [ 1.30901699,  0.80901699, -0.5       ],
                               [-0.80901699,  0.5       , -1.30901699],
                               [ 0.80901699,  0.5       , -1.30901699],
                               [ 0.        ,  0.        , -1.61803399],
                               [-1.61803399,  0.        ,  0.        ],
                               [-0.80901699, -0.5       , -1.30901699],
                               [-1.30901699, -0.80901699, -0.5       ],
                               [-0.80901699,  0.5       ,  1.30901699],
                               [-1.30901699, -0.80901699,  0.5       ],
                               [-0.80901699, -0.5       ,  1.30901699],
                               [ 0.80901699,  0.5       ,  1.30901699],
                               [ 0.        ,  0.        ,  1.61803399],
                               [ 1.61803399,  0.        ,  0.        ],
                               [ 0.80901699, -0.5       ,  1.30901699],
                               [ 1.30901699, -0.80901699,  0.5       ],
                               [ 0.80901699, -0.5       , -1.30901699],
                               [ 1.30901699, -0.80901699, -0.5       ],
                               [-0.5       , -1.30901699, -0.80901699],
                               [ 0.5       , -1.30901699, -0.80901699],
                               [-0.5       , -1.30901699,  0.80901699],
                               [ 0.        , -1.61803399,  0.        ],
                               [ 0.5       , -1.30901699,  0.80901699]],
                              [[-1.11022302e-16,  1.41202266e+00, -5.39344663e-01],
                               [-1.11022302e-16,  1.41202266e+00,  5.39344663e-01],
                               [-8.72677996e-01,  8.72677996e-01, -8.72677996e-01],
                               [-1.41202266e+00,  5.39344663e-01,  0.00000000e+00],
                               [-8.72677996e-01,  8.72677996e-01,  8.72677996e-01],
                               [ 8.72677996e-01,  8.72677996e-01, -8.72677996e-01],
                               [ 8.72677996e-01,  8.72677996e-01,  8.72677996e-01],
                               [ 1.41202266e+00,  5.39344663e-01,  0.00000000e+00],
                               [-5.39344663e-01,  5.55111512e-17, -1.41202266e+00],
                               [ 5.39344663e-01,  5.55111512e-17, -1.41202266e+00],
                               [-1.41202266e+00, -5.39344663e-01,  0.00000000e+00],
                               [-8.72677996e-01, -8.72677996e-01, -8.72677996e-01],
                               [-5.39344663e-01,  5.55111512e-17,  1.41202266e+00],
                               [-8.72677996e-01, -8.72677996e-01,  8.72677996e-01],
                               [ 5.39344663e-01,  5.55111512e-17,  1.41202266e+00],
                               [ 1.41202266e+00, -5.39344663e-01,  0.00000000e+00],
                               [ 8.72677996e-01, -8.72677996e-01,  8.72677996e-01],
                               [ 8.72677996e-01, -8.72677996e-01, -8.72677996e-01],
                               [-5.55111512e-17, -1.41202266e+00, -5.39344663e-01],
                               [-5.55111512e-17, -1.41202266e+00,  5.39344663e-01]]]
        
        mfld = self.icosa
        
        for k, mdl in enumerate(mfld):
            cctrs = mdl.circumcenters

            self.assertIsInstance(cctrs, np.ndarray)
            self.assertEqual(cctrs.shape, (mfld.shape[k], mfld.emb_dim))

            if k == 0:
                np.testing.assert_array_equal(cctrs, mdl.vertices)
            else:
                xpct_cctrs = np.array([circumcenter(vtcs) 
                                       for vtcs in mdl.vertices])
                np.testing.assert_array_equal(cctrs, xpct_cctrs)

        np.testing.assert_almost_equal(cctrs, xpct_circumcenters[k])

    def test_SimplicialManifold_covolumes(self) -> None:

        n0, n1, n2 = self.icosa.shape

        expected_icosahedron_covolumes ={0: 2.88675135 * np.ones(n0),
                                         1: 1.15470054 * np.ones(n1),
                                         2: np.ones(n2)}
        
        for k, mdl in enumerate(self.icosa):
            np.testing.assert_allclose(mdl.covolumes,
                                       expected_icosahedron_covolumes[k])
    
    def test_SimplicialManifold_deficit_angles(self) -> None:
        mfld = self.sphere
        chi = mfld.euler_characteristic
        k = mfld.dim - 2

        total_curvature = mfld[k].deficit_angles.sum()
        self.assertAlmostEqual(total_curvature, 2*pi*chi)
    
    def test_SimplicialManifold_update_geometry(self):
        mfld = self.icosa
        ids = [0, 1, 2]
        
        init_ccntrs = {}
        init_ccntrs_id = {}
        init_covols = {}
        init_covols_id = {}
        for k, mdl in enumerate(mfld):
            init_ccntrs[k] = deepcopy(mdl._circumcenters)
            init_covols[k] = deepcopy(mdl._covolumes)
            init_ccntrs_id[k] = deepcopy(id(mdl._circumcenters))        
            init_covols_id[k] = deepcopy(id(mdl._covolumes))

        displacement = {idx: mfld[0].vertices[idx] for idx in ids}

        mfld.update_geometry(displacement)

        for k, mdl in enumerate(mfld):
            self.assertEqual(id(mdl._circumcenters), init_ccntrs_id[k])
            self.assertEqual(id(mdl._covolumes), init_covols_id[k])

        np.testing.assert_allclose(mfld[2]._circumcenters[0], 
                                   2*init_ccntrs[2][0])

        # Inverse displacement
        displacement = {idx: -.5*mfld[0].vertices[idx] for idx in ids}
        mfld.update_geometry(displacement)

        for k, mdl in enumerate(mfld):
            np.testing.assert_allclose(mdl._circumcenters, init_ccntrs[k])
            np.testing.assert_allclose(mdl._covolumes, init_covols[k])

        # move all vertices
        ids = list(range(mfld[0].size))

        displacement = {idx: mfld[0].vertices[idx] for idx in ids}
        mfld.update_geometry(displacement)
        
        n = mfld.dim
        for k, mdl in enumerate(mfld):
            np.testing.assert_allclose(mdl._circumcenters, 2*init_ccntrs[k])
            np.testing.assert_allclose(mdl._covolumes, 2**(n-k)*init_covols[k])
    
    def test_detect_ill_centered_cofaces(self) -> None:
    
        # Test on a well-centered 2-complex
        sid_fid_couples = _detect_ill_centered_cofaces(self.hexa_cplx)
        
        n = self.hexa_cplx.dim
        
        self.assertEqual(len(sid_fid_couples), n+1)
        self.assertIsNone(sid_fid_couples[0])
        self.assertIsNone(sid_fid_couples[1])
        self.assertIsNone(sid_fid_couples[2])

        # Construct a ill-centered basic example.
        indices = [[0, 1, 2], [1, 2, 3]]
        vertices = np.array([[-1,0,0],
                             [0,-1.1,0],
                             [0,1.1,0],
                             [1,0,0]])
        mfld = SimplicialManifold(indices, vertices)
        sid_fid_couples = _detect_ill_centered_cofaces(mfld)

        self.assertEqual(len(sid_fid_couples), mfld.dim + 1)
        self.assertIsNone(sid_fid_couples[0])
        self.assertIsNone(sid_fid_couples[-1])
        self.assertEqual(sid_fid_couples[1].shape, (2,2))

        xpct_sids_fids = np.array([[2, 0],[2, 1]])
        np.testing.assert_array_equal(sid_fid_couples[1], xpct_sids_fids)

    def test_update_curvature(self) -> None:

        moved_vidx = 3 

        unchanged_vids = [[1, 5, 6, 7, 10, 11], [0, 4, 6]]

        for i, mfld in enumerate([self.icosa,
                                 self.cube]):
            k = mfld.dim - 2
            old_kg = deepcopy(mfld[k].deficit_angles)

            displacement = {moved_vidx: .1*mfld[0].vertices[moved_vidx]}

            mfld.update_geometry(displacement)

            new_kg = mfld[k].deficit_angles
            np.testing.assert_allclose(new_kg[unchanged_vids[i]],
                                       old_kg[unchanged_vids[i]])
            self.assertAlmostEqual(new_kg.sum(), old_kg.sum())

        mfld = self.sphere
        chi = mfld.euler_characteristic
        k = mfld.dim - 2
        kg_sum = mfld[k].deficit_angles.sum()
        self.assertAlmostEqual(kg_sum, 2*pi*chi)
    
    def test_dual_edge_vectors(self) -> None:
        
        # Test on flat open manifold
        mfld = self.trigrid
        N, D = mfld.shape[1], mfld.emb_dim

        edges = dual_edge_vectors(mfld)
        self.assertIsInstance(edges, np.ndarray)
        self.assertEqual(edges.shape, (N,D))
        np.testing.assert_array_almost_equal(edges[:,2], np.zeros(N))

        # Test unit edges
        edges =dual_edge_vectors(mfld, normalized=True)
        np.testing.assert_array_almost_equal(lng.norm(edges, axis=-1),
                                             np.ones(N))
        
        # Test on closed manifold
        mfld = self.icosa
        n = mfld.dim
        N, D = mfld.shape[1], mfld.emb_dim
        cfids = mfld.cofaces(n-1)
        
        edges = dual_edge_vectors(mfld)
        self.assertIsInstance(edges, np.ndarray)
        self.assertEqual(edges.shape, (N,D))
        
        xpct_edges = edges = mfld[-1].boundary @ mfld[-1].circumcenters
        np.testing.assert_array_almost_equal(edges, xpct_edges)
        
    def test_edge_vectors(self) -> None:
        self.assertIsInstance(edge_vectors(self.icosa_cplx), np.ndarray)
        self.assertIsNone(edge_vectors(self.icosa_cplx, dual=True)) 
        self.assertIsInstance(edge_vectors(self.icosa), np.ndarray)

    def test_orient_top_simplex_couples(self) -> None:
        # Test on closed complexes
        for cplx in [self.icosa_cplx, self.sph]:
            k = cplx.dim - 1
            Nk = cplx.shape[k]

            ordered_ids = _orient_top_simplex_couples(cplx)

            self.assertIsInstance(ordered_ids, np.ndarray)
            self.assertEqual(ordered_ids.ndim, 3)
            self.assertEqual(ordered_ids.shape, (Nk, k+1, k+2))
            for ids in ordered_ids:
                np.testing.assert_array_equal(ids[0,:2], ids[1,:2])


    def test_compute_dihedral_angles(self) -> None:
        
        # Testing on flat open manifold
        _compute_dihedral_angles(self.dsk)
        angles = self.dsk.dihedral_angles
        Nk_in = len(self.dsk.interior()[1])
        np.testing.assert_array_almost_equal(angles, np.pi*np.ones(Nk_in))

        # Test on closed manifold
        N = self.icosa.shape[-2]
        _compute_dihedral_angles(self.icosa)
        angles = self.icosa.dihedral_angles
        assert isinstance(angles, np.ndarray)
        assert angles.shape == (N,)

        # The dihedral angles of the icosahedron are know: 
        # np.arccos(-np.sqrt(5)/3)
        np.testing.assert_allclose(angles, np.arccos(-np.sqrt(5)/3)*np.ones(N))
