import unittest
from pathlib import Path
import numpy as np

from dxtr import SimplicialComplex, Cochain, logger

from dxtr.complexes import sphere
from dxtr.utils.logging import require_pyvista, mutelogging, LEVEL


class TestLogging(unittest.TestCase):

    def setUp(self) -> None:
        """Initiates the test.
        """
        
        self.cplx = sphere()
        self.mfld = sphere(manifold=True)

        k = 1
        Nk = self.cplx[k].size
        self.cochain = Cochain(self.cplx, dim=k, values=np.ones(Nk))
        

    def tearDown(self) -> None:
        """Concludes and closes the test.
        """

    def test_require_pyvista(self) -> None:

        @require_pyvista
        def f(complex:SimplicialComplex) -> bool:
            return isinstance(complex,SimplicialComplex) 
        
        self.assertTrue(f(self.cplx))

    def test_mutelogging(self) -> None:
        
        @mutelogging
        def f() -> int:
            inside_log_level = logger.level
            return inside_log_level
        
        self.assertEqual(f(), LEVEL['ERROR'])