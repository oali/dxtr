import unittest
import itertools as it
import numpy as np
import scipy.special as scsp

from numpy import pi
from numpy import linalg as lng

from dxtr.math.geometry import (barycentric_coordinates,
                                gradient_barycentric_coordinates,
                                circumcenter_barycentric_coordinates,
                                circumcenter, dot, wedge_between, whitney_form, 
                                volume_blade, volume_simplex, volume_polytope, 
                                dihedral_angle, angle_defect)

class TestGeometry(unittest.TestCase): 

    def setUp(self) -> None:
        """Initiates the test.
        """
        pass

    def tearDown(self) -> None:
        """Concludes and closes the test.
        """

    def test_barycentric_coordinates(self) -> None:

        vct = np.array([.5, .5, 0])
        ex = np.array([1, 0, 0])
        ey = np.array([0, 1, 0])
        ez = np.array([0, 0, 1])
        
        vtcs = np.array([[0,0,0],
                        [2,0,0],
                        [0,2,0]])
        
        for i, ei in enumerate((ex, ey, ez)):
            np.testing.assert_allclose(
                barycentric_coordinates(vtcs[i], vtcs), ei)
        
        coord = barycentric_coordinates(vct, vtcs)
        
        np.testing.assert_allclose(
            sum(xi*vtcs[i] for i, xi in enumerate(coord)), vct)
        

    def test_gradient_barycentric_coordinates(self) -> None:
        
        # 1-simplex
        vertices = np.array([[0, 0, 0],
                            [1, 0, 0]])
        
        xpct_dbc = np.array([[-1, 0, 0],
                            [ 1, 0, 0]])
        
        np.testing.assert_allclose(
            gradient_barycentric_coordinates(vertices), xpct_dbc)
        
        # 2-simplex
        vertices = np.array([[0, 0, 0],
                            [1, 0, 0],
                            [0, 1, 0]])
        
        xpct_dbc = np.array([[-1, -1, 0],
                            [ 1, 0, 0],
                            [0, 1, 0]])

        np.testing.assert_allclose(
            gradient_barycentric_coordinates(vertices), xpct_dbc)

        # 3-simplex
        vertices = np.array([[0, 0, 0],
                            [0, 1, 0],
                            [1, 0, 0],
                            [0, 0, 1]])
        
        xpct_dbc = np.array([[-1, -1, -1],
                            [0, 1, 0],
                            [1, 0, 0],
                            [0, 0, 1]])
        
        np.testing.assert_allclose(
            gradient_barycentric_coordinates(vertices), xpct_dbc)
    

    def test_circumcenter_barycentric_coordinates(self) -> None:
        node = np.array([[1, 0, 0]])

        edge = np.array([[0, 0, 0],
                         [1, 0, 0]])

        triangle = np.array([[0, 0, 0],
                             [1, 0, 0],
                             [0, 1, 0]])

        tetra = np.array([[0, 0, 0],
                          [1, 0, 0],
                          [0, 1, 0],
                          [0, 0, 1]])

        expected_barycentric_coordinates = [[1],
                                            [.5, .5],
                                            [0, .5, .5],
                                            [-.5, .5, .5, .5]]

        for simplex, expct_bary_coord in zip([node, edge, triangle, tetra],
                                             expected_barycentric_coordinates):

            np.testing.assert_allclose(expct_bary_coord,
                                       circumcenter_barycentric_coordinates(simplex),
                                       atol=1e-15)
        
        ill_defined_simplex = np.array([[0, 0, 0],
                                        [1, 0, 0],
                                        [0, 1, 0],
                                        [0, 0, 1],
                                        [0, 1, 1]])
        self.assertIsNone(circumcenter_barycentric_coordinates(ill_defined_simplex))


    def test_circumcenter(self) -> None:
        simplex = {'node': np.array([[1, 0, 0]]),
                   'edge': [[0, 0, 0], [1, 0, 0]],
                   'triangle': np.array([[0, 0, 0],
                                         [1, 0, 0],
                                         [0, 1, 0]]),
                   'tetra': np.array([[0, 0, 0],
                                      [1, 0, 0],
                                      [0, 1, 0],
                                      [0, 0, 1]])}

        expected_circumcenter = {'node': np.array([1, 0, 0]),
                                 'edge': np.array([.5, 0, 0]),
                                 'triangle': np.array([.5, .5, 0]),
                                 'tetra': np.array([.5, .5, .5])}

        for name, nodes in simplex.items():
            np.testing.assert_allclose(circumcenter(nodes),
                                       expected_circumcenter[name], atol=1e-15)

        expected_circumradius = {'node': 0,
                                 'edge': .5,
                                 'triangle': np.sqrt(.5),
                                 'tetra': np.sqrt(3*.5/2)}

        for name, nodes in simplex.items():
            self.assertEqual(circumcenter(nodes, return_radius=True)[1],
                             expected_circumradius[name])


    def test_dot(self) -> None:
        
        # 1-blades
        bld0 = np.array([[1, 0, 0]])
        bld1 = np.array([[0, 1, 0]])

        self.assertEqual(dot(bld0, bld1), 0)
        self.assertEqual(dot(bld0, bld0), 1)
        self.assertEqual(dot(bld1, bld1), 1)

        # 2-blades
        bld0 = np.array([[1, 0, 0], [0, 1, 0]])
        bld1 = np.array([[1, 0, 0], [0, 0, 1]])

        self.assertEqual(dot(bld0, bld1), 0)
        self.assertEqual(dot(bld0, bld0), .25)
        self.assertEqual(dot(bld1, bld1), .25)

        # 3-blades
        bld0 = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
        bld1 = np.array([[-1, 0, 0], [0, 1, 0], [0, 0, 1]])

        self.assertEqual(dot(bld0, bld1), - 1/36)
        self.assertEqual(dot(bld0, -bld1),  1/36)
        self.assertEqual(dot(bld0, bld0),  1/36)
        self.assertEqual(dot(bld1, bld1),  1/36)

        # ill-defined blade
        bld0 = np.array([[1, 0, 0]])
        bld1 = np.array([[0, 1, 0], [1, 0, 0]])
        self.assertIsNone(dot(bld0, bld1))


    def test_wedge_product(self) -> None:
        ex = np.array([1,0,0])
        ey = np.array([0,1,0])
        ez = np.array([0,0,1])

        np.testing.assert_allclose(wedge_between(ex, ey), - wedge_between(ey, ex))
        np.testing.assert_allclose(wedge_between(ex, ex), np.zeros((3,3)))
        np.testing.assert_allclose(wedge_between(2*ex, ey), 2*wedge_between(ex, ey))
        np.testing.assert_allclose(wedge_between(ex+ez, ey), 
                                   wedge_between(ex, ey) + wedge_between(ez, ey))


    def test_whitney_form(self)->None:

        # Whitney 0-form on a 2D surface
        n, k = 2, 0
        
        vertices = np.array([[0, 0, 0],
                             [1, 0, 0],
                             [0, 1, 0]])
        
        bary_coords = circumcenter_barycentric_coordinates(vertices)
        bc_grads = gradient_barycentric_coordinates(vertices)

        np.testing.assert_allclose([whitney_form(bary_coords, bc_grads, ids) 
                                    for ids in it.combinations(range(n+1), 
                                                               (k+1))],
                                bary_coords)
        
        # Whitney 1-form on a 2D surface
        n, k = 2, 1
        
        vertices = np.array([[0, 0, 0],
                             [1, 0, 0],
                             [0, 1, 0]])
        
        bary_coords = circumcenter_barycentric_coordinates(vertices)
        bc_grads = gradient_barycentric_coordinates(vertices)
        
        xpct_whitney_1forms = np.array([[.5, .5, 0],
                                        [.5, .5, 0], 
                                        [-.5, .5,  0]])
        
        np.testing.assert_allclose([whitney_form(bary_coords, bc_grads, ids) 
                                    for ids in it.combinations(range(n+1), 
                                                               (k+1))],
                                xpct_whitney_1forms)
        

        # Another test within a isocele rectangle
        c = np.cos(np.pi/3)
        s = np.sin(np.pi/3)
        
        vertices = np.array([[0, 0, 0], [1, 0, 0], [c, s, 0]])
        bary_coords = circumcenter_barycentric_coordinates(vertices)
        bc_grads = gradient_barycentric_coordinates(vertices)

        whitney_1forms = [whitney_form(bary_coords, bc_grads, ids) 
                        for ids in it.combinations(range(n+1), (k+1))]

        xpct_whitney_1forms = np.array([[2/3, 0, 0],
                                        [1/3, 0.57735027, 0],
                                        [-1/3, 0.57735027, 0]])
        
        np.testing.assert_allclose(whitney_1forms, xpct_whitney_1forms, 
                                   atol=1e-9)
        
        # Checking the normalized version
        normalized_whitney_1forms = [whitney_form(bary_coords, bc_grads, ids, 
                                                  normalized=True) 
                                for ids in it.combinations(range(n+1), (k+1))]
        
        for wf in normalized_whitney_1forms:
            self.assertAlmostEqual(lng.norm(wf), 1)


        # Whitney 2-form within a tetrahedron
        n, k = 3, 2
        vertices = np.array([[0, 0, 0],
                            [1, 0, 0],
                            [0, 1, 0],
                            [0, 0, 1]])
        
        bary_coords = circumcenter_barycentric_coordinates(vertices)
        bc_grads = gradient_barycentric_coordinates(vertices)

        whitney_2forms = [whitney_form(bary_coords, bc_grads, ids) 
                          for ids in it.combinations(range(n+1), (k+1))]
        
        for wf in whitney_2forms:
            assert wf.shape == (3,3)
            np.testing.assert_array_equal(wf, -wf.T)
        
        # Checking the normalized version
        normalized_whitney_2forms = [whitney_form(bary_coords, bc_grads, ids,
                                                  normalized=True) 
                          for ids in it.combinations(range(n+1), (k+1))]
        for wf in normalized_whitney_2forms:
            self.assertAlmostEqual(lng.norm(wf), 1)

        # Higher order Whitney form cannot be computed
        vertices = np.array([[0, 0, 0],
                            [1, 0, 0],
                            [0, 1, 0],
                            [0, 0, 1]])
        ids = (0,1,2,3)
        bary_coords = circumcenter_barycentric_coordinates(vertices)
        bc_grads = gradient_barycentric_coordinates(vertices)

        self.assertIsNone(whitney_form(bary_coords, bc_grads, ids))


    def test_volume_blade(self)-> None:
        
        blade_1 = np.array([[0, 1]])
        blade_2 = np.array([[1, 0], [0, 1]])
        blade_3 = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
        blade_4 = np.array([[1, 0, 0, 0], [0, 1, 0, 0],
                            [0, 0, 1, 0], [0, 0, 0, 1]])
        blade_5 = np.array([[1, 0, 0, 0, 0], [0, 1, 0, 0, 0],  [0, 0, 1, 0, 0], 
                            [0, 0, 0, 1, 0], [0, 0, 0, 0, 1]])

        for i, pos in enumerate([blade_1, blade_2, blade_3, blade_4, blade_5]):
            self.assertEqual(volume_blade(pos), 1 / scsp.factorial(i+1))


    def test_volume_simplex(self) -> None:
        pts = np.array([[0, 0, 0]])
        self.assertEqual(volume_simplex(pts), 1)

        line = np.array([[0, 0], [0, 1]])
        triangle = np.array([[0, 0], [0, 1], [1, 0]])
        tetrahedron = np.array([[0, 0, 0], [0, 1, 0], [1, 0, 0], [0, 0, 1]])

        for i, pos in enumerate([line, triangle, tetrahedron]):
            self.assertEqual(volume_simplex(pos), 1 / scsp.factorial(i+1))

        ill_defined_splx = np.array([[0, 0], [0, 1], [1, 0], [1, 1]])
        self.assertIsNone(volume_simplex(ill_defined_splx))


    def test_volume_polytope(self) -> None:

        # 1-polytope
        pos = [np.array([[0, 0, 0]]), 
               np.array([[1, 0, 0], 
                         [-1, 0, 0]])]
        ids = [[0, 0],
               [0, 1]]

        self.assertAlmostEqual(volume_polytope(pos, ids), 2)

        # 2-polytope
        pos = [np.array([[0, 0, 0]]), 
               np.array([[1, 0, 0], 
                         [-1, 0, 0],
                         [0, -1, 0]]),
               np.array([[-1, -1, 0],
                         [1, -1, 0]])]

        ids = [[0, 0, 0],
               [0, 1, 0],
               [0, 1, 1],
               [0, 2, 1]]

        self.assertAlmostEqual(volume_polytope(pos, ids), 2)

        # 3-polytope
        pos = [np.array([[0, 0, 0]]), 
               np.array([[1, 0, 0], 
                         [0, 1, 0],
                         [0, 0, 1]]),
               np.array([[1, 1, 0],
                         [0, 1, 1],
                         [1, 0, 1]]),
               np.array([[1, 1, 1]])]

        ids = [[0, 0, 0, 0],
               [0, 1, 0, 0],
               [0, 1, 1, 0],
               [0, 2, 1, 0],
               [0, 2, 2, 0],
               [0, 0, 2, 0]]

        self.assertAlmostEqual(volume_polytope(pos, ids), 1)

        # ill-defined polytope
        pos = [np.array([[0, 0, 0]]), 
               np.array([[1, 0, 0], 
                         [-1, 0, 0]])]
        ids = [[0],[1]]

        self.assertIsNone(volume_polytope(pos, ids))


    def test_volume_polytope_ill_centered(self) -> None:
        # 1-polytope
        pos = [np.array([[0, 0, 0]]), np.array([[1, 0, 0], [.5, 0, 0]])]
        ids = [[0, 0], [0, 1]]
        ill_ctr = np.array([[0,1]])
        
        self.assertEqual(volume_polytope(pos, ids, 
                        ill_centered_simplices=ill_ctr), .5)

        # 2-polytope
        pos = [np.array([[0, 0, 0]]), 
               np.array([[-1, 0, 0], 
                         [-1, 0, 0],
                         [0, -1, 0]]),
               np.array([[-1, -1, 0],
                         [-1, -1, 0]])]

        ids = [[0, 0, 0], [0, 1, 0], [0, 1, 1], [0, 2, 1]]
        
        ill_ctr = np.array([[0,1]])

        self.assertEqual(volume_polytope(pos, ids, 
                        ill_centered_simplices=ill_ctr), 0)


    def test_dihedral_angle(self)-> None:

        # 1-blade in 2D
        bld1 = np.array([[0, 1]])
        bld2 = np.array([[1, 0]])
        bld3 = np.array([[2, 0]])

        self.assertAlmostEqual(dihedral_angle(bld1, bld1), 0)
        self.assertAlmostEqual(dihedral_angle(bld1, bld2), pi/2)
        self.assertEqual(dihedral_angle(bld1, bld2), dihedral_angle(bld2, bld1))
        self.assertEqual(dihedral_angle(bld1, bld2), dihedral_angle(bld1, bld3))
        
        # 1-blade in 3D
        bld1 = np.array([[0, 1, 0]])
        bld2 = np.array([[1, 0, 0]])

        self.assertAlmostEqual(dihedral_angle(bld1, bld1), 0)
        self.assertAlmostEqual(dihedral_angle(bld1, bld2), pi/2)
        self.assertEqual(dihedral_angle(bld1, bld2), dihedral_angle(bld2, bld1))

        # 2-blades in 3D
        bld1 = np.array([[1, 0, 0], [0, 1, 0]])
        bld2 = np.array([[1, 0, 0], [0, 0, 1]])
        bld3 = np.array([[1, 0, 0], [0, .5, .5]])
        bld4 = np.array([[1, 0, 0], [0, -.5, .5]])

        self.assertAlmostEqual(dihedral_angle(bld1, bld1), 0)
        self.assertAlmostEqual(dihedral_angle(bld1, bld2), pi/2)
        self.assertEqual(dihedral_angle(bld1, bld2), dihedral_angle(bld2, bld1))
        self.assertAlmostEqual(dihedral_angle(bld1, bld3), pi/4)
        self.assertIsNone(dihedral_angle(bld1, bld3[::-1]))
        self.assertAlmostEqual(dihedral_angle(bld1, bld4), 3*pi/4)

        # 3-blades in 4D
        bld1 = np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 1, 0]])
        bld2 = np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, 0, 1]])
        bld3 = np.array([[1, 0, 0, 0], [0, 1, 0, 0], [0, 0, .5, .5]])
        bld4 = np.array([[0, 1, 0, 0], [1, 0, 0, 0], [0, 0, .5, .5]])

        self.assertAlmostEqual(dihedral_angle(bld1, bld1), 0)
        self.assertAlmostEqual(dihedral_angle(bld1, bld2), pi/2)
        self.assertEqual(dihedral_angle(bld1, bld2), dihedral_angle(bld2, bld1))
        self.assertAlmostEqual(dihedral_angle(bld1, bld3), pi/4)
        self.assertIsNone(dihedral_angle(bld1, bld4))


    def test_angle_defect(self) -> None:
        # 0D hinge
        blades = np.array([[[[2, 0, 0]],
                            [[ 1, -0.61803399, -1.61803399]]],   
                           [[[2, 0, 0]],
                               [[1, -0.61803399,  1.61803399]]],
                           [[[1, -0.61803399, -1.61803399]],
                               [[-0.61803399, -1.61803399, -1]]],   
                           [[[-0.61803399, -1.61803399, -1]],   
                               [[-0.61803399, -1.61803399, 1]]],   
                           [[[-0.61803399, -1.61803399, 1]],   
                               [[1, -0.61803399, 1.61803399]]]])
         
        self.assertAlmostEqual(angle_defect(blades), pi/3)
