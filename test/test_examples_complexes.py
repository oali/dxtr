import unittest
import numpy as np

from dxtr.complexes import (SimplicialComplex, SimplicialManifold, disk, 
                            sphere, icosahedron, triangular_grid, hexagon,
                            triangle, diamond, cube)

class TestExamplesComplexes(unittest.TestCase):

    def setUp(self):
        """Initiates the test.
        """

        self.sizes = {'small': (1593, 4773, 3182),
                      'large':(20753, 62253, 41502), 
                      'massive':(46282, 138840, 92560)}
        
    def tearDown(self):
        """Concludes and closes the test.
        """

    def test_disk(self) -> None:
        disks = {size: disk(size=size) for size in self.sizes.keys()}

        for _, dsk in disks.items():
            self.assertIsInstance(dsk, SimplicialComplex)

    def test_sphere(self)-> None:
        spheres = {size: sphere(size=size) for size in self.sizes.keys()}

        for size, shape in self.sizes.items():
            sph = spheres[size]
            self.assertIsInstance(sph, SimplicialComplex)
            self.assertEqual(sph.shape, shape)
        
        self.assertIsInstance(sphere(manifold=True), SimplicialManifold)
        self.assertIsNone(sphere(size='huge'))

    def test_icosahedron(self)-> None:
        icosa = icosahedron()
        icosa_mfld = icosahedron(manifold=True)

        self.assertIsInstance(icosa, SimplicialComplex)
        self.assertEqual(icosa.shape, (12, 30, 20))
        self.assertIsInstance(icosa_mfld, SimplicialManifold)

    def test_hexagon(self) -> None:
        cplx = hexagon()
        assert isinstance(cplx, SimplicialComplex)
        assert cplx.shape == (7, 12, 6)

        xpct_vertices = np.array([[ 0,  0,  0],
                                  [ 1,  0,  0],
                                  [ .5,  8.66025404e-01,  0],
                                  [-.5,  8.66025404e-01,  0],
                                  [ -1,  1.22464680e-16,  0],
                                  [-.5, -8.66025404e-01,  0],
                                  [ .5, -8.66025404e-01,  0]])
        np.testing.assert_almost_equal(cplx[0].vertices, 
                                       xpct_vertices, decimal=2)

        assert isinstance(hexagon(manifold=True), SimplicialManifold)

    def test_triangular_grid(self) -> None:

        cplx = triangular_grid()
        assert isinstance(cplx, SimplicialComplex)
        assert cplx.shape == (16, 33, 18)
        
        xpct_vertices = np.array([[0. , 0.,         0.],
                                  [1. , 0.,         0.],
                                  [2. , 0.,         0.],
                                  [3. , 0.,         0.],
                                  [0.5, 0.8660254,  0.],
                                  [1.5, 0.8660254,  0.],
                                  [2.5, 0.8660254,  0.],
                                  [3.5, 0.8660254,  0.],
                                  [1. , 1.73205081, 0.],
                                  [2. , 1.73205081, 0.],
                                  [3. , 1.73205081, 0.],
                                  [4. , 1.73205081, 0.],
                                  [1.5, 2.59807621, 0.],
                                  [2.5, 2.59807621, 0.],
                                  [3.5, 2.59807621, 0.],
                                  [4.5, 2.59807621, 0.]])
        
        np.testing.assert_almost_equal(cplx[0].vertices, 
                                       xpct_vertices, decimal=2)

        cplx = triangular_grid(4, edge_length=5, manifold=True)
        assert isinstance(cplx, SimplicialManifold)
        assert cplx.shape == (25, 56, 32)
        print(cplx[0].vertices)
        xpct_vertices = np.array([[ 0. ,  0.        ,  0.],
                                  [ 5. ,  0.        ,  0.],
                                  [10. ,  0.        ,  0.],
                                  [15. ,  0.        ,  0.],
                                  [20. ,  0.        ,  0.],
                                  [ 2.5,  4.33012702,  0.],
                                  [ 7.5,  4.33012702,  0.],
                                  [12.5,  4.33012702,  0.],
                                  [17.5,  4.33012702,  0.],
                                  [22.5,  4.33012702,  0.],
                                  [ 5. ,  8.66025404,  0.],
                                  [10. ,  8.66025404,  0.],
                                  [15. ,  8.66025404,  0.],
                                  [20. ,  8.66025404,  0.],
                                  [25. ,  8.66025404,  0.],
                                  [ 7.5, 12.99038106,  0.],
                                  [12.5, 12.99038106,  0.],
                                  [17.5, 12.99038106,  0.],
                                  [22.5, 12.99038106,  0.],
                                  [27.5, 12.99038106,  0.],
                                  [10. , 17.32050808,  0.],
                                  [15. , 17.32050808,  0.],
                                  [20. , 17.32050808,  0.],
                                  [25. , 17.32050808,  0.],
                                  [30. , 17.32050808,  0.]])
        
        np.testing.assert_almost_equal(cplx[0].vertices, xpct_vertices,
                                       decimal=2)

    def test_cube(self) -> None:
        cplx = cube()
        assert isinstance(cplx, SimplicialComplex)
        assert cplx.shape == (8, 18, 12)

        assert isinstance(cube(manifold=True), SimplicialManifold)
    
    def test_triangle(self) -> None:
        cplx = triangle()
        assert isinstance(cplx, SimplicialComplex)
        assert cplx.shape == (3,3,1)

        xpct_pos = np.array([[0,0,0], 
                             [1,0,0], 
                             [np.cos(np.pi/3), np.sin(np.pi/3), 0]])
        np.testing.assert_array_almost_equal(cplx[0].vertices,
                                             xpct_pos)

        assert isinstance(triangle(manifold=True), SimplicialManifold)
    
    def test_diamond(self) -> None:
        cplx = diamond()
        assert isinstance(cplx, SimplicialComplex)
        assert cplx.shape == (5, 9, 7, 2)
        theta = 2*np.pi/3
        xpct_pos = np.array([[0, 0,-1],
                             [1, 0, 0],
                             [np.cos(theta), np.sin(theta),0],
                             [np.cos(2*theta), np.sin(2*theta),0],
                             [0, 0, 1]])
        np.testing.assert_array_almost_equal(cplx[0].vertices,
                                             xpct_pos)
        assert isinstance(diamond(manifold=True), SimplicialManifold)