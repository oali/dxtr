import unittest
from pathlib import Path
import numpy as np

from dxtr.complexes import SimplicialManifold, SimplicialComplex, sphere
from dxtr.cochains import Cochain, unit_cochain, normal_vector_field
from dxtr.utils.wrappers import Property
from dxtr.io.write import (
                        #    valid_input, write_file, 
                           write_ply, 
                        #    write_vtk, 
                           format_path_properly, 
                                #  _vertex_positions, _top_simplices, _edges, 
                                #  _extract_property_from
                                 )


class TestIoWriteFiles(unittest.TestCase):

    def setUp(self) -> None:
        """Initiates the test.
        """
        
        self.sphere = sphere(manifold=True)
        self.cochain = unit_cochain(complex_name='sphere')
        self.kvector = normal_vector_field(manifold_name='sphere')

        self.path = Path().cwd() / Path('.temp')
        self.path.mkdir(parents=True, exist_ok=True)

        self.path_2 = self.path / Path('here')
        self.path_2.mkdir(parents=True, exist_ok=True)


    def tearDown(self) -> None:
        """Concludes and closes the test.
        """
        for file in self.path.iterdir():
            if file.is_dir(): 
                 file.rmdir()
            else:
                file.unlink()

        self.path.rmdir()
    

    def test_write_simplcial_complex(self) -> None:
        # Testing that recording a SimplicialManifold we can re-open it.
        file_name = 'test_mfld'
        extension = '.ply'
        
        self.sphere.to_file(file_name, extension)
        
        path = Path().cwd() / Path(file_name + extension)
        mfld = SimplicialManifold.from_file(path)
        self.assertIsInstance(mfld, SimplicialManifold)

        path.unlink() # N.B.: Remove the temp file created for the test.
    
    def test_write_simplcial_complex_vtk(self) -> None:
        # Testing that recording a SimplicialManifold we can re-open it.
        file_name = 'test_mfld'
        extension = '.vtk'
        
        self.sphere.to_file(file_name, extension)
        
        path = Path().cwd() / Path(file_name + extension)
        # mfld = SimplicialManifold.from_file(path)
        # self.assertIsInstance(mfld, SimplicialManifold)

        path.unlink() # N.B.: Remove the temp file created for the test.

    def test_write_cochain(self) -> None:
        # Testing the writing/reading of a cochain
        file_name = 'test_cchn'
        extension = '.vtk'
        
        self.cochain.to_file(file_name, extension)
        
        path = Path().cwd() / Path(file_name + extension)
        cchn = Cochain.from_file(path)
        self.assertIsInstance(cchn, Cochain)
        np.testing.assert_array_equal(cchn.values, self.cochain.values)

        path.unlink() # N.B.: Remove the temp file created for the test.

    def test_write_ply(self) -> None:

        # Testing that recording a SimplicialManifold we can re-open it.
        path = self.path / Path('test_mfld_write_ply.ply')
        points = self.sphere[0].vertices
        indices = self.sphere[-1].vertex_indices
        write_ply(indices, points, path)

        mfld = SimplicialManifold.from_file(path)
        
        self.assertIsInstance(mfld, SimplicialManifold)
        self.assertEqual(mfld.shape, self.sphere.shape)
        for k in range(mfld.dim+1):
            np.testing.assert_almost_equal(mfld[k].vertices, 
                                        self.sphere[k].vertices)
            np.testing.assert_almost_equal(mfld[k].boundary.toarray(),
                                        self.sphere[k].boundary.toarray())


    def testformat_path_properly(self) -> None:
        path = self.path_2
        file_name = 'test_format_path_property'

        for extention in ['.ply', '.vtk']:
            new_path = format_path_properly(path, file_name, extention)
        
            self.assertIsInstance(new_path, Path)
            self.assertEqual(new_path.suffix, extention)

        self.assertIsNone(format_path_properly(path, file_name, Path('.ply')))
        self.assertIsNone(format_path_properly(path, file_name, 'ply'))
        self.assertIsNone(format_path_properly(path, file_name, '.obj'))
        
        self.assertEqual(format_path_properly(None, file_name, '.ply').parent,
                         Path().cwd())

