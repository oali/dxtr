# Welcome to the **Dxtr** repository!

![PyPI - Version](https://img.shields.io/pypi/v/dxtr?logo=pypi&logoColor=white&link=https%3A%2F%2Fpypi.org%2Fproject%2Fdxtr%2F)
![Conda Version](https://img.shields.io/conda/vn/mosaic/dxtr?logo=anaconda&logoColor=white&label=mosaic%2Fdxtr&color=%2344A833&link=https%3A%2F%2Fanaconda.org%2Fmosaic%2Fdxtr)

| master                                                                                                                                                    | develop                                                                                                                                                    |
|-----------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|
| [![pipeline status](https://gitlab.inria.fr/oali/dxtr/badges/master/pipeline.svg)](https://gitlab.inria.fr/oali/dxtr/-/commits/develop)                   | [![pipeline status](https://gitlab.inria.fr/oali/dxtr/badges/develop/pipeline.svg)](https://gitlab.inria.fr/oali/dxtr/-/commits/develop)                   |
| [![coverage report](https://gitlab.inria.fr/oali/dxtr/badges/master/coverage.svg?job=test_coverage)](https://gitlab.inria.fr/oali/dxtr/-/commits/develop) | [![coverage report](https://gitlab.inria.fr/oali/dxtr/badges/develop/coverage.svg?job=test_coverage)](https://gitlab.inria.fr/oali/dxtr/-/commits/develop) |

The **Dxtr** library implements the theory of *Discrete Exterior Calculus (
DEC)* in python. Its purpose is to provide users with an intuitive framework to prototype differential geometry problems within the Exterior Calculus paradigm. The library is also suited to handle basic algebraic topology problems.

|                      |                                                                                                                                                   |
|----------------------|---------------------------------------------------------------------------------------------------------------------------------------------------|
| **Lead development** | [Olivier Ali](https://www.olivierali.info)                                                                                                        |
| **Research team**    | [Mosaic](https://team.inria.fr/mosaic/)                                                                                                           |
| **Institute**        | [Inria](http://www.inria.fr)                                                                                                                      |
| **Language**         | ![PyPI - Python Version](https://img.shields.io/pypi/pyversions/dxtr?logo=python&logoColor=white&link=https%3A%2F%2Fpypi.org%2Fproject%2Fdxtr%2F) |
| **Supported OS**     | Linux, MacOs, Windows                                                                                                                             |
| **Licence**          | LGPL                                                                                                                                              |
| **Funding**          | Inria AEx [Discotik](https://www.inria.fr/en/discotik)                                                                                            |

## Requirements

* Python >= 3.9
* numpy
* scipy
* pandas
* plotly
* pyvista (optional... but recommended)

## Documentation

Accessible :point_right: [**here**](https://oali.gitlabpages.inria.fr/dxtr/) :point_left:

In this documentation you will find:

* Detailed [**instructions to install**](https://oali.gitlabpages.inria.fr/dxtr/install/) the library.
* Some introductory [**notes on the mathematical foundations**](https://oali.gitlabpages.inria.fr/dxtr/context/) of the *DEC* theory.
* A [**description of the library**](https://oali.gitlabpages.inria.fr/dxtr/architecture_overview/) architecture and its main features.
* Some working [**examples**](https://oali.gitlabpages.inria.fr/dxtr/advanced_examples/example_list/).
* A detailed [***API* reference guide**](https://oali.gitlabpages.inria.fr/dxtr/_reference/API/).

## Support

If you encounter an error or need help, please [raise an issue](https://gitlab.inria.fr/oali/dxtr/-/issues).

## Contributing

**Dxtr** is a collaborative project and contributions are welcome.
If you want to contribute, please contact the [coordinator](mailto:olivier.ali@inria.fr) prior to any [merge request](https://gitlab.inria.fr/oali/dxtr/-/merge_requests) and
check the [gitlab merge request guidelines](https://docs.gitlab.com/ee/development/contributing/merge_request_workflow.html), if needed.

## Citation

If you consider publishing scientific work using **Dxtr**,
please use the following BibTeX entry to refer to the library:

```
TODO
```

## Acknowledgement

* The **Dxtr** library draws inspiration from the legacy [**pydec**](https://github.com/hirani/pydec) library. Thanks !
* The development of the library has been supported by the **Inria** *Action Exploratoire* [**Discotik**](https://www.inria.fr/fr/discotik)
